    <!-- Navigation -->
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a href="#menu" class="mr-3" id="toggle"><span></span></a>
        <div id="menu">
<?php include('sidebar.php') ?>
</div>


        <a class="navbar-brand" href="index.php">

<img src="images/logo-light.png" alt="" class="img-fluid">
        </a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav mx-auto">

            <li <?php if (basename($_SERVER['PHP_SELF']) == "index.php") { ?> class="nav-item active" <?php } ?>>
              <a class="nav-link px-4" href="index.php"> 
                <div class="text-center">
                  <i class="fas fa-home"></i>
                </div>
                <div class="text-center">
                  หน้าแรก
                </div>
              </a>
            </li>

            <!-- <li <?php if (basename($_SERVER['PHP_SELF']) == "about-us.php") { ?> class="nav-item active" <?php } ?>>
              <a class="nav-link" href="about-us.php">
                <div class="text-center">
                  
                </div>
                <div class="text-center">
                  เกี่ยวกับเรา
                </div>
              </a>
            </li> -->
          
           <li <?php if (basename($_SERVER['PHP_SELF']) == "search.php") { ?> class="nav-item active" <?php } ?>>
              <a class="nav-link px-4" href="search.php">
                <div class="text-center">
                  <i class="fas fa-search"></i>
                </div>
                <div class="text-center">
                  ค้นหาเบอร์
                </div>
              </a>
            </li>
   
           <li <?php if (basename($_SERVER['PHP_SELF']) == "article.php") { ?> class="nav-item active" <?php } ?>>
              <a class="nav-link px-4" href="article.php">
                <div class="text-center">
                  <i class="fas fa-book"></i>
                </div>
                <div class="text-center">
                  บทความ
                </div>
              </a>
            </li>
            <li <?php if (basename($_SERVER['PHP_SELF']) == "howto.php") { ?> class="nav-item active" <?php } ?>>
              <a class="nav-link px-4" href="howto.php">
                <div class="text-center">
                  <i class="fas fa-shopping-cart"></i>
                </div>
                <div class="text-center">
                   วิธีสั่งซื้อ/โอนเงิน
                </div>
              </a>
            </li>
             <li <?php if (basename($_SERVER['PHP_SELF']) == "contact-us.php") { ?> class="nav-item active" <?php } ?>>
              <a class="nav-link px-4" href="contact-us.php">
                <div class="text-center">
                  <i class="fas fa-file"></i>
                </div>
                 <div class="text-center">
                    ติดต่อเรา
                </div>
              </a>
            </li>
        </ul>
        </div>
      </div>
    </nav>



<script>
  var theToggle = document.getElementById('toggle');

// based on Todd Motto functions
// https://toddmotto.com/labs/reusable-js/

// hasClass
function hasClass(elem, className) {
  return new RegExp(' ' + className + ' ').test(' ' + elem.className + ' ');
}
// addClass
function addClass(elem, className) {
    if (!hasClass(elem, className)) {
      elem.className += ' ' + className;
    }
}
// removeClass
function removeClass(elem, className) {
  var newClass = ' ' + elem.className.replace( /[\t\r\n]/g, ' ') + ' ';
  if (hasClass(elem, className)) {
        while (newClass.indexOf(' ' + className + ' ') >= 0 ) {
            newClass = newClass.replace(' ' + className + ' ', ' ');
        }
        elem.className = newClass.replace(/^\s+|\s+$/g, '');
    }
}
// toggleClass
function toggleClass(elem, className) {
  var newClass = ' ' + elem.className.replace( /[\t\r\n]/g, " " ) + ' ';
    if (hasClass(elem, className)) {
        while (newClass.indexOf(" " + className + " ") >= 0 ) {
            newClass = newClass.replace( " " + className + " " , " " );
        }
        elem.className = newClass.replace(/^\s+|\s+$/g, '');
    } else {
        elem.className += ' ' + className;
    }
}

theToggle.onclick = function() {
   toggleClass(this, 'on');
   return false;
}
</script>