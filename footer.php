<!-- Footer -->
<footer class="py-5 bg-dark">

    <div class="container">
        <div class="row">
            <div class="col-md-3 ">
                <img src="images/logo-light.png" alt="" class="mb-3">
                <div class="address-title">
                    <h3 class="text-primary">เบอร์ขายดี</h3>
                </div>
                <div class="address-description">

                    <ul>
                        <li>
                            <a href="mailto:berkaidee@gmail.com">
                                <i class="fas fa-envelope"></i> berkaidee@gmail.com
                            </a>
                        </li>
                         <li>
                           <a href="tel:+66956369565"><i class="fas fa-mobile-alt"></i> 095-636-9565</a>
                        </li>
                        <li>
                           <a href="tel:+66814562456"> <i class="fas fa-mobile-alt"></i> 081-456-2456</a>
                        </li>
                       
                    </ul>


                </div>


            </div>
            <div class="col-md-3">
                <div class="page-title">
                    <h4 class="text-primary">เมนูลัด</h4>
                </div>
                <ul>
                    <li>
                        <a href="about-us.php">
                            <i class="fas fa-angle-right"></i> เกี่ยวกับเรา</a>
                    </li>
                    <li>
                        <a href="howto.php">
                            <i class="fas fa-angle-right"></i> วิธีการสั่งซื้อ / โอนเงิน</a>
                    </li>
                    <li>
                        <a href="search.php">
                            <i class="fas fa-angle-right"></i> ค้นหาเบอร์</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3">
                <div class="page-title">
                    <h4 class="text-primary">ติดต่อผ่าน line ได้ตลอด 24 ชม.</h4>
                </div>
                <div class="line-box">
                       <a href="http://line.me/ti/p/~kamol789.com">
                        <img src="images/kamol-line.png" class="img-fluid" alt="">
                    </a>
                    <a href="http://line.me/ti/p/~berkaidee">
                        <img src="images/berkaidee-line.png" class="img-fluid" alt=""> </a>
                 
                </div>

            </div>
            <div class="col-md-3">
                <div class="dbd-certificate">
                    <img src="images/trust-banner.png" alt="" class="img-fluid" data-toggle="modal" data-target="#myModal">
                    <p>เว็บไซต์นี้ได้รับการจดทะเบียนพาณิชย์อิเล็กทรอนิกส์</p>
                </div>
            </div>
        </div>

        <hr>
        <div class="copyright ">
            <p class="m-0 text-left text-white">สงวนลิขสิทธิ์ 2016-2018 &copy; เบอร์ขายดี</p>
        </div>
    </div>
    <!-- /.container -->
</footer>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="modal-title" id="myModalLabel">เว็บไซต์นี้ซื้อขายมั่นใจ โปร่งใส ได้สินค้าแน่นอน </h5>
            </div>
            <div class="modal-body">
                <img src="images/cerrtificate-book.jpg" alt="" class="img-fluid" data-pin-nopin="true">
            </div>

        </div>
    </div>
</div>
<!-- All Jquery -->
<script src="vendor/jquery/jquery.min.js"></script>
<!-- Sweet Alert -->
<script src="admin/assets/plugins/sweetalert/sweetalert2.all.js"></script>
<!-- Custom JavaScript -->
<script src="scripts/service.js"></script>
<script src="scripts/script.js"></script>