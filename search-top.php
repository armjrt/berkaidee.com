<div class="search-ber">
                            <div class="form-search">
                                <form id="searchForm">
                                    <div class="page-header mb-5">
                            ค้นหาเบอร์
                        </div>
                                    <div class="row">
                                        <div class="col-md-6 pr-0">
                                            <div class="form-group ">
                                                <label for="">ระบุเบอร์ที่ต้องการค้นหา</label>
                                                <input name="num" type="text" class="form_search2 form-control" id="num" onkeypress="return Numbers(event);" onkeyup="keyUp(this, event)"
                                                    value="0" maxlength="1">
                                                <input name="num0" type="text" class="form_search2 form-control" id="num0" value="" maxlength="1" onkeypress="return Numbers(event);"
                                                    onkeyup="keyUp(this,event)">
                                                <input name="num1" type="text" class="form_search2 form-control" id="num1" value="" maxlength="1" onkeypress="return Numbers(event);"
                                                    onkeyup="keyUp(this,event)"> -
                                                <input name="num2" type="text" class="form_search2 form-control" id="num2" value="" maxlength="1" onkeypress="return Numbers(event);"
                                                    onkeyup="keyUp(this,event)">
                                                <input name="num3" type="text" class="form_search2 form-control" id="num3" value="" maxlength="1" onkeypress="return Numbers(event);"
                                                    onkeyup="keyUp(this,event)">
                                                <input name="num4" type="text" class="form_search2 form-control" id="num4" value="" maxlength="1" onkeypress="return Numbers(event);"
                                                    onkeyup="keyUp(this,event)"> -
                                                <input name="num5" type="text" class="form_search2 form-control" id="num5" value="" maxlength="1" onkeypress="return Numbers(event);"
                                                    onkeyup="keyUp(this,event)">
                                                <input name="num6" type="text" class="form_search2 form-control" id="num6" value="" maxlength="1" onkeypress="return Numbers(event);"
                                                    onkeyup="keyUp(this,event)">
                                                <input name="num7" type="text" class="form_search2 form-control" id="num7" value="" maxlength="1" onkeypress="return Numbers(event);"
                                                    onkeyup="keyUp(this,event)">
                                                <input name="num8" type="text" class="form_search2 form-control" id="num8" value="" maxlength="1" onkeypress="return Numbers(event);"
                                                    onkeyup="keyUp(this,event)">
                                            </div>
                                            <div class="form-group">
                                                <div class="number-check">
                                                    <label for="">เลือกเฉพาะเลข</label>
                                                    <input type="checkbox" class="no-check" id="n0" name="n0" value="0">
                                                    <label class="label-check" for="n0">0</label>
                                                    <input type="checkbox" class="no-check" id="n1" name="n1" value="1">
                                                    <label class="label-check" for="n1">1</label>
                                                    <input type="checkbox" class="no-check" id="n2" name="n2" value="2">
                                                    <label class="label-check" for="n2">2</label>
                                                    <input type="checkbox" class="no-check" id="n3" name="n3" value="3">
                                                    <label class="label-check" for="n3">3</label>
                                                    <input type="checkbox" class="no-check" id="n4" name="n4" value="4">
                                                    <label class="label-check" for="n4">4</label>
                                                    <input type="checkbox" class="no-check" id="n5" name="n5" value="5">
                                                    <label class="label-check" for="n5">5</label>
                                                    <input type="checkbox" class="no-check" id="n6" name="n6" value="6">
                                                    <label class="label-check" for="n6">6</label>
                                                    <input type="checkbox" class="no-check" id="n7" name="n7" value="7">
                                                    <label class="label-check" for="n7">7</label>
                                                    <input type="checkbox" class="no-check" id="n8" name="n8" value="8">
                                                    <label class="label-check" for="n8">8</label>
                                                    <input type="checkbox" class="no-check" id="n9" name="n9" value="9">
                                                    <label class="label-check" for="n9">9</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="form-group col-md-6">
                                                    <label for="">งบประมาณ</label>
                                                    <select name="bud" id="budget" class="form-control">
                                                        <option value="">-------- เลือกราคา --------</option>
                                                        <option value="0-10000">ราคาต่ำกว่า 10,000</option>
                                                        <option value="10000-20000">ราคา 10,000-20,000</option>
                                                        <option value="20000-30000">ราคา 20,000-30,000</option>
                                                        <option value="30000-40000">ราคา 30,000-40,000</option>
                                                        <option value="40000-50000">ราคา 40,000-50,000</option>
                                                        <option value="50000-100000">ราคา 50,000-100,000</option>
                                                        <option value="100000-150000">ราคา 100,000-150,000</option>
                                                        <option value="150000-200000">ราคา 150,000-200,000</option>
                                                        <option value="200000-300000">ราคา 200,000-300,000</option>
                                                        <option value="300000-500000">ราคา 300,000-500,000</option>
                                                        <option value="500001-0">มากกว่า 500,000 ขึ้นไป</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="">เครือข่าย</label>
                                                    <select name="system" id="system" class="form-control">
                                                        <option value="">------------ เลือก ------------</option>
                                                        <option value="111">DTAC</option>
                                                        <option value="222">TRUE</option>
                                                        <option value="333">AIS</option>
                                                        <option value="444">Tot3G</option>
                                                        <option value="555">I-mobile</option>
                                                        <option value="666">Mycat</option>
                                                        <option value="777">Penguin</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="">ผลรวม</label>
                                                    <select name="all" id="all" class="form-control">
                                                        <option value="">-------- เลือกผลรวม --------</option>
                                                        <option value="8">
                                                            8 </option>
                                                        <option value="9">
                                                            9 </option>
                                                        <option value="10">
                                                            10 </option>
                                                        <option value="11">
                                                            11 </option>
                                                        <option value="12">
                                                            12 </option>
                                                        <option value="13">
                                                            13 </option>
                                                        <option value="14">
                                                            14 </option>
                                                        <option value="15">
                                                            15 </option>
                                                        <option value="16">
                                                            16 </option>
                                                        <option value="17">
                                                            17 </option>
                                                        <option value="18">
                                                            18 </option>
                                                        <option value="19">
                                                            19 </option>
                                                        <option value="20">
                                                            20 </option>
                                                        <option value="21">
                                                            21 </option>
                                                        <option value="22">
                                                            22 </option>
                                                        <option value="23">
                                                            23 </option>
                                                        <option value="24">
                                                            24 </option>
                                                        <option value="25">
                                                            25 </option>
                                                        <option value="26">
                                                            26 </option>
                                                        <option value="27">
                                                            27 </option>
                                                        <option value="28">
                                                            28 </option>
                                                        <option value="29">
                                                            29 </option>
                                                        <option value="30">
                                                            30 </option>
                                                        <option value="31">
                                                            31 </option>
                                                        <option value="32">
                                                            32 </option>
                                                        <option value="33">
                                                            33 </option>
                                                        <option value="34">
                                                            34 </option>
                                                        <option value="35">
                                                            35 </option>
                                                        <option value="36">
                                                            36 </option>
                                                        <option value="37">
                                                            37 </option>
                                                        <option value="38">
                                                            38 </option>
                                                        <option value="39">
                                                            39 </option>
                                                        <option value="40">
                                                            40 </option>
                                                        <option value="41">
                                                            41 </option>
                                                        <option value="42">
                                                            42 </option>
                                                        <option value="43">
                                                            43 </option>
                                                        <option value="44">
                                                            44 </option>
                                                        <option value="45">
                                                            45 </option>
                                                        <option value="46">
                                                            46 </option>
                                                        <option value="47">
                                                            47 </option>
                                                        <option value="48">
                                                            48 </option>
                                                        <option value="49">
                                                            49 </option>
                                                        <option value="50">
                                                            50 </option>
                                                        <option value="51">
                                                            51 </option>
                                                        <option value="52">
                                                            52 </option>
                                                        <option value="53">
                                                            53 </option>
                                                        <option value="54">
                                                            54 </option>
                                                        <option value="55">
                                                            55 </option>
                                                        <option value="56">
                                                            56 </option>
                                                        <option value="57">
                                                            57 </option>
                                                        <option value="58">
                                                            58 </option>
                                                        <option value="59">
                                                            59 </option>
                                                        <option value="60">
                                                            60 </option>
                                                        <option value="61">
                                                            61 </option>
                                                        <option value="62">
                                                            62 </option>
                                                        <option value="63">
                                                            63 </option>
                                                        <option value="64">
                                                            64 </option>
                                                        <option value="65">
                                                            65 </option>
                                                        <option value="66">
                                                            66 </option>
                                                        <option value="67">
                                                            67 </option>
                                                        <option value="68">
                                                            68 </option>
                                                        <option value="69">
                                                            69 </option>
                                                        <option value="70">
                                                            70 </option>
                                                        <option value="71">
                                                            71 </option>
                                                        <option value="72">
                                                            72 </option>
                                                        <option value="73">
                                                            73 </option>
                                                        <option value="74">
                                                            74 </option>
                                                        <option value="75">
                                                            75 </option>
                                                        <option value="76">
                                                            76 </option>
                                                        <option value="77">
                                                            77 </option>
                                                        <option value="78">
                                                            78 </option>
                                                        <option value="79">
                                                            79 </option>
                                                        <option value="80">
                                                            80 </option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="">ค้นหาบางเลข</label>
                                                    <input class="form-control" name="some" type="text" id="some" value="" size="10" maxlength="10">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <p>
                                        <button type="button" class="btn btn-primary" id="searchBtn">ค้นหา</button>
                                        <button type="reset" class="btn btn-default">ยกเลิก</button>
                                    </p>
                                </form>
                            </div>
                        </div>