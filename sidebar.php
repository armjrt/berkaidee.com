<!-- Sidebar Widgets Column -->
<div class="col-md-12 p-0 sidebar">
<!--     <div class="card mb-4 horo">
        <h5 class="card-header" data-toggle="collapse" href="#collapse" role="button" aria-expanded="false" aria-controls="collapse">
            <i class="fab fa-searchengin"></i> ทำนายเบอร์</h5>
        <div class="card-body collapse" id="collapse">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="ใส่เบอร์โทร 10 หลัก" id="numberSearchFilter" maxlength="10">
                <span class="input-group-btn">
                    <button class="btn btn-primary" id="numberSearchFilterBtn">ทำนาย</button>
                </span>
            </div>
        </div>
    </div> -->
    <!-- Search Widget -->
    <div class="card mb-4">
        <h5 class="card-header" data-toggle="collapse" href="#collapse1" role="button" aria-expanded="false" aria-controls="collapse1">
            <i class="fas fa-mobile-alt"></i> หมวดหมู่เบอร์
        </h5>
        <div class="card-body collapse" id="collapse1">
            <!-- <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search for...">
                            <span class="input-group-btn">
                                <button class="btn btn-secondary" type="button">Go!</button>
                            </span>
                        </div> -->
            <ul id="categoriesList"></ul>
        </div>
    </div>
    <!-- Categories Widget -->
    <div class="card my-4">
        <h5 class="card-header" data-toggle="collapse" href="#collapse2" role="button" aria-expanded="false" aria-controls="collapse2">
            <i class="fas fa-mobile-alt"></i> เบอร์ตามช่วงราคา</h5>
        <div class="card-body collapse" id="collapse2">
            <ul id="priceList"></ul>
        </div>
    </div>
    <!-- Side Widget -->
    <div class="card my-4">
        <h5 class="card-header" data-toggle="collapse" href="#collapse3" role="button" aria-expanded="false" aria-controls="collapse3">
            <i class="fas fa-mobile-alt"></i> เบอร์ตามผลรวม</h5>
        <div class="card-body collapse" id="collapse3">
            <ul id="compoundList"></ul>
        </div>
    </div>
    <div class="card my-4">
        <h5 class="card-header" data-toggle="collapse" href="#collapse4" role="button" aria-expanded="false" aria-controls="collapse4">
            <i class="fab fa-telegram-plane"></i> สถานะการจัดส่ง EMS</h5>
        <div class="card-body collapse" id="collapse4">
            <ul id="emsTrackingList"></ul>
            <a href="ems-tracking.php" class="viewall">ดูรายการจัดส่งทั้งหมด
                <i class="fas fa-angle-double-right"></i>
            </a>
        </div>
    </div>
    <div class="card my-4">
        <h5 class="card-header" data-toggle="collapse" href="#collapse5" role="button" aria-expanded="false" aria-controls="collapse5">
            <i class="fas fa-book"></i> บทความแนะนำ</h5>
        <div class="card-body collapse" id="collapse5">
            <ul id="articleList" class="list-unstyled article-sidebar"></ul>
            <a href="article.php" class="viewall ">ดูบทความทั้งหมด
                <i class="fas fa-angle-double-right"></i>
            </a>
        </div>
    </div>
</div>