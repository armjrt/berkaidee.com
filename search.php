<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="keywords" content="berkaidee, เบอร์ขายดี, เบอร์มังกร, เบอร์รวย, เบอร์มงคล, เลขศาสตร์, ทำนายเบอร์, เบอร์มีระดับ, ทำนายเบอร์, เบอร์หงษ์, เบอร์กวนอู, เบอร์ platinum, เบอร์ gold, เบอร์ silver, ปรึกษาเบอร์, บริการขายเบอร์, แหล่งซื้อขายเบอร์มือถือ, เบอร์ราคาถูก, เบอร์ดี, เบอร์สวย, ซิมเบอร์สวย, เบอร์vip, เบอร์เฮง, เบอร์หาม, เบอร์789,เบอร์289, เบอร์รับทรัพย์, เบอร์รับโชค, บริหารจัดหาเบอร์, รวมเบอร์, เบอร์สวยที่สุดในประเทศไทย ">
    <meta name="description" content="เบอร์ขายดี เบอร์ดี ของคนมีระดับบริการรับจัดหา ซื้อ-ขายเบอร์มงคล เบอร์สวย เลขศาสตร์ เบอร์ดี  เบอร์หงส์ 289 เบอร์มังกร 789 เบอร์รับทรัพย์-รับโชค ศูนย์รวมเลขสวยเบอร์มงคล ที่ถูกต้องตามหลักโหราศาสตร์ไทย เบอร์ขายดี เบอร์มงคลที่ดีและสวยที่สุดในประเทศไทย">
    <meta name="author" content="berkaidee">
    <meta property="og:image:type" content="image/jpg">
    <meta property="og:description" content="berkaidee เบอร์ดี ของคนมีระดับ">
    <meta property="og:type" content="berkaidee">
    <link rel="apple-touch-icon" sizes="57x57" href="images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="images/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="images/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon/favicon-16x16.png">
    <link rel="manifest" href="images/favicon/manifest.json">
    <title>Berkaidee - เบอร์ขายดี เบอร์ดีของคนมีระดับ</title>
    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="fontawesome/fontawesome-all.css" rel="stylesheet">
    <link href="fontawesome/font-custom.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/modern-business.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
</head>
<body>
    <?php include ("navbar.php");?>
    <!-- Page Content -->
    <header class="mb-5">
    </header>
    <div class="container styleContainer py-4 rounded border box-shadow">
        <div class="row">
            <!-- Post Content Column -->
            <div class="col-md-12">
                <div class="content">
                    <!--bercategory-->
                    <div class="ber-category mb-5">
                        <div class="page-header">
                            ค้นหาเบอร์
                        </div>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="index.php">หน้าแรก</a>
                            </li>
                            <li class="breadcrumb-item active">ค้นหาเบอร​์</li>
                        </ol>
                        <div class="search-ber">
                            <div class="form-search">
                                <form id="searchForm">
                                    <div class="row">
                                        <div class="col-md-6 pr-0">
                                            <div class="form-group ">
                                                <label for="">ระบุเบอร์ที่ต้องการค้นหา</label>
                                                <input name="num" type="text" class="form_search2 form-control" id="num" onkeypress="return Numbers(event);" onkeyup="keyUp(this, event)"
                                                    value="0" maxlength="1">
                                                <input name="num0" type="text" class="form_search2 form-control" id="num0" value="" maxlength="1" onkeypress="return Numbers(event);"
                                                    onkeyup="keyUp(this,event)">
                                                <input name="num1" type="text" class="form_search2 form-control" id="num1" value="" maxlength="1" onkeypress="return Numbers(event);"
                                                    onkeyup="keyUp(this,event)"> -
                                                <input name="num2" type="text" class="form_search2 form-control" id="num2" value="" maxlength="1" onkeypress="return Numbers(event);"
                                                    onkeyup="keyUp(this,event)">
                                                <input name="num3" type="text" class="form_search2 form-control" id="num3" value="" maxlength="1" onkeypress="return Numbers(event);"
                                                    onkeyup="keyUp(this,event)">
                                                <input name="num4" type="text" class="form_search2 form-control" id="num4" value="" maxlength="1" onkeypress="return Numbers(event);"
                                                    onkeyup="keyUp(this,event)"> -
                                                <input name="num5" type="text" class="form_search2 form-control" id="num5" value="" maxlength="1" onkeypress="return Numbers(event);"
                                                    onkeyup="keyUp(this,event)">
                                                <input name="num6" type="text" class="form_search2 form-control" id="num6" value="" maxlength="1" onkeypress="return Numbers(event);"
                                                    onkeyup="keyUp(this,event)">
                                                <input name="num7" type="text" class="form_search2 form-control" id="num7" value="" maxlength="1" onkeypress="return Numbers(event);"
                                                    onkeyup="keyUp(this,event)">
                                                <input name="num8" type="text" class="form_search2 form-control" id="num8" value="" maxlength="1" onkeypress="return Numbers(event);"
                                                    onkeyup="keyUp(this,event)">
                                            </div>
                                            <div class="form-group">
                                                <div class="number-check">
                                                    <label for="">เลือกเฉพาะเลข</label>
                                                    <input type="checkbox" class="no-check" id="n0" name="n0" value="0">
                                                    <label class="label-check" for="n0">0</label>
                                                    <input type="checkbox" class="no-check" id="n1" name="n1" value="1">
                                                    <label class="label-check" for="n1">1</label>
                                                    <input type="checkbox" class="no-check" id="n2" name="n2" value="2">
                                                    <label class="label-check" for="n2">2</label>
                                                    <input type="checkbox" class="no-check" id="n3" name="n3" value="3">
                                                    <label class="label-check" for="n3">3</label>
                                                    <input type="checkbox" class="no-check" id="n4" name="n4" value="4">
                                                    <label class="label-check" for="n4">4</label>
                                                    <input type="checkbox" class="no-check" id="n5" name="n5" value="5">
                                                    <label class="label-check" for="n5">5</label>
                                                    <input type="checkbox" class="no-check" id="n6" name="n6" value="6">
                                                    <label class="label-check" for="n6">6</label>
                                                    <input type="checkbox" class="no-check" id="n7" name="n7" value="7">
                                                    <label class="label-check" for="n7">7</label>
                                                    <input type="checkbox" class="no-check" id="n8" name="n8" value="8">
                                                    <label class="label-check" for="n8">8</label>
                                                    <input type="checkbox" class="no-check" id="n9" name="n9" value="9">
                                                    <label class="label-check" for="n9">9</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="form-group col-md-6">
                                                    <label for="">งบประมาณ</label>
                                                    <select name="bud" id="budget" class="form-control">
                                                        <option value="">-------- เลือกราคา --------</option>
                                                        <option value="0-10000">ราคาต่ำกว่า 10,000</option>
                                                        <option value="10000-20000">ราคา 10,000-20,000</option>
                                                        <option value="20000-30000">ราคา 20,000-30,000</option>
                                                        <option value="30000-40000">ราคา 30,000-40,000</option>
                                                        <option value="40000-50000">ราคา 40,000-50,000</option>
                                                        <option value="50000-100000">ราคา 50,000-100,000</option>
                                                        <option value="100000-150000">ราคา 100,000-150,000</option>
                                                        <option value="150000-200000">ราคา 150,000-200,000</option>
                                                        <option value="200000-300000">ราคา 200,000-300,000</option>
                                                        <option value="300000-500000">ราคา 300,000-500,000</option>
                                                        <option value="500001-0">มากกว่า 500,000 ขึ้นไป</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="">เครือข่าย</label>
                                                    <select name="system" id="system" class="form-control">
                                                        <option value="">------------ เลือก ------------</option>
                                                        <option value="111">DTAC</option>
                                                        <option value="222">TRUE</option>
                                                        <option value="333">AIS</option>
                                                        <option value="444">Tot3G</option>
                                                        <option value="555">I-mobile</option>
                                                        <option value="666">Mycat</option>
                                                        <option value="777">Penguin</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="">ผลรวม</label>
                                                    <select name="all" id="all" class="form-control">
                                                        <option value="">-------- เลือกผลรวม --------</option>
                                                        <option value="8">
                                                            8 </option>
                                                        <option value="9">
                                                            9 </option>
                                                        <option value="10">
                                                            10 </option>
                                                        <option value="11">
                                                            11 </option>
                                                        <option value="12">
                                                            12 </option>
                                                        <option value="13">
                                                            13 </option>
                                                        <option value="14">
                                                            14 </option>
                                                        <option value="15">
                                                            15 </option>
                                                        <option value="16">
                                                            16 </option>
                                                        <option value="17">
                                                            17 </option>
                                                        <option value="18">
                                                            18 </option>
                                                        <option value="19">
                                                            19 </option>
                                                        <option value="20">
                                                            20 </option>
                                                        <option value="21">
                                                            21 </option>
                                                        <option value="22">
                                                            22 </option>
                                                        <option value="23">
                                                            23 </option>
                                                        <option value="24">
                                                            24 </option>
                                                        <option value="25">
                                                            25 </option>
                                                        <option value="26">
                                                            26 </option>
                                                        <option value="27">
                                                            27 </option>
                                                        <option value="28">
                                                            28 </option>
                                                        <option value="29">
                                                            29 </option>
                                                        <option value="30">
                                                            30 </option>
                                                        <option value="31">
                                                            31 </option>
                                                        <option value="32">
                                                            32 </option>
                                                        <option value="33">
                                                            33 </option>
                                                        <option value="34">
                                                            34 </option>
                                                        <option value="35">
                                                            35 </option>
                                                        <option value="36">
                                                            36 </option>
                                                        <option value="37">
                                                            37 </option>
                                                        <option value="38">
                                                            38 </option>
                                                        <option value="39">
                                                            39 </option>
                                                        <option value="40">
                                                            40 </option>
                                                        <option value="41">
                                                            41 </option>
                                                        <option value="42">
                                                            42 </option>
                                                        <option value="43">
                                                            43 </option>
                                                        <option value="44">
                                                            44 </option>
                                                        <option value="45">
                                                            45 </option>
                                                        <option value="46">
                                                            46 </option>
                                                        <option value="47">
                                                            47 </option>
                                                        <option value="48">
                                                            48 </option>
                                                        <option value="49">
                                                            49 </option>
                                                        <option value="50">
                                                            50 </option>
                                                        <option value="51">
                                                            51 </option>
                                                        <option value="52">
                                                            52 </option>
                                                        <option value="53">
                                                            53 </option>
                                                        <option value="54">
                                                            54 </option>
                                                        <option value="55">
                                                            55 </option>
                                                        <option value="56">
                                                            56 </option>
                                                        <option value="57">
                                                            57 </option>
                                                        <option value="58">
                                                            58 </option>
                                                        <option value="59">
                                                            59 </option>
                                                        <option value="60">
                                                            60 </option>
                                                        <option value="61">
                                                            61 </option>
                                                        <option value="62">
                                                            62 </option>
                                                        <option value="63">
                                                            63 </option>
                                                        <option value="64">
                                                            64 </option>
                                                        <option value="65">
                                                            65 </option>
                                                        <option value="66">
                                                            66 </option>
                                                        <option value="67">
                                                            67 </option>
                                                        <option value="68">
                                                            68 </option>
                                                        <option value="69">
                                                            69 </option>
                                                        <option value="70">
                                                            70 </option>
                                                        <option value="71">
                                                            71 </option>
                                                        <option value="72">
                                                            72 </option>
                                                        <option value="73">
                                                            73 </option>
                                                        <option value="74">
                                                            74 </option>
                                                        <option value="75">
                                                            75 </option>
                                                        <option value="76">
                                                            76 </option>
                                                        <option value="77">
                                                            77 </option>
                                                        <option value="78">
                                                            78 </option>
                                                        <option value="79">
                                                            79 </option>
                                                        <option value="80">
                                                            80 </option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="">ค้นหาบางเลข</label>
                                                    <input class="form-control" name="some" type="text" id="some" value="" size="10" maxlength="10">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <p>
                                        <button type="button" class="btn btn-primary" id="searchBtn">ค้นหา</button>
                                        <button type="reset" class="btn btn-default">ยกเลิก</button>
                                    </p>
                                </form>
                            </div>
                        </div>
                        <div class="search-result">
                            <h3 class="text-primary mb-3">ผลลัพธ์การค้นหา :</h3>
                            <div class="berkaidee-listview table-responsive-sm">
                                <div id="productsTable" class="row "></div>
                                <!-- <table id="productsTable" class="table berlist">
                                    <tbody></tbody>
                                </table> -->
                            </div>
                        </div>
                        <nav id="pagination" style="display: none;" aria-label="Page navigation example">
                            <ul class="pagination justify-content-center"></ul>
                            <div class="text-center"></div>
                        </nav>
                    </div>
                    <!-- end bernew-->
                </div>
            </div>
            <!--end bernew-->
        </div>
        <!--col-lg-9-->
        <!-- /.row -->
    </div>
    <!-- /.container -->
    <?php include("footer.php");?>
    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript">
        function check_number(ch) {
            var len, digit;
            if (ch == " ") {
                return false;
                len = 0;
            } else {
                len = ch.length;
            }
            for (var i = 0; i < len; i++) {
                digit = ch.charAt(i)
                if (digit >= "0" && digit <= "10") {;
                } else {
                    return false;
                }
            }
            return true;
        }

        function Check(FrmRegister) {

            if (FrmRegister.number.value == "") {
                alert("กรุณาใส่เบอร์โทรศัพท์ 10 หลัก");
                FrmRegister.number.focus();
                return false;
            }

            if (FrmRegister.number.value.length != 10) {
                alert("ต้องระบุตัวเลข 10 หลัก");
                FrmRegister.number.focus();
                return false;
            }

        }

        function Numbers(e) {
            var keynum;
            var keychar;
            var numcheck;
            if (window.event) { // IE
                keynum = e.keyCode;
            } else if (e.which) { // Netscape/Firefox/Opera
                keynum = e.which;
            }
            if (keynum == 13 || keynum == 8 || typeof (keynum) == "undefined") {
                return true;
            }
            keychar = String.fromCharCode(keynum);
            numcheck = /^[0-9]$/;
            return numcheck.test(keychar);
        }

        function keyUp(obj, e) {
            if (window.event) { // IE
                keynum = e.keyCode;
            } else if (e.which) { // Netscape/Firefox/Opera
                keynum = e.which;
            }
            if (keynum == 13) {
                searchNumber();
            } else {
                num = $(obj).attr("name").replace("num", "");
                if ($(obj).val() != "" && num < 8 && num != '') {
                    num++;
                    $("input[name=num" + num + "]").select();
                } else if (num > 0) {
                    if (keynum == 8) {
                        num--;
                        $("input[name=num" + num + "]").select();
                        if ($("input[name=num" + num + "]").val() != "") {
                            temp_val = $("input[name=num" + num + "]").val();
                            $("input[name=num" + num + "]").val(temp_val);
                        }
                    }
                } else if (num == '') {
                    $("input[name=num0]").select();
                }
            }
        }
    </script>
</body>
</html>