<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="keywords" content="berkaidee, เบอร์ขายดี, เบอร์มังกร, เบอร์รวย, เบอร์มงคล, เลขศาสตร์, ทำนายเบอร์, เบอร์มีระดับ, ทำนายเบอร์, เบอร์หงษ์, เบอร์กวนอู, เบอร์ platinum, เบอร์ gold, เบอร์ silver, ปรึกษาเบอร์, บริการขายเบอร์, แหล่งซื้อขายเบอร์มือถือ, เบอร์ราคาถูก, เบอร์ดี, เบอร์สวย, ซิมเบอร์สวย, เบอร์vip, เบอร์เฮง, เบอร์หาม, เบอร์789,เบอร์289, เบอร์รับทรัพย์, เบอร์รับโชค, บริหารจัดหาเบอร์, รวมเบอร์, เบอร์สวยที่สุดในประเทศไทย ">
    <meta name="description" content="เบอร์ขายดี เบอร์ดี ของคนมีระดับบริการรับจัดหา ซื้อ-ขายเบอร์มงคล เบอร์สวย เลขศาสตร์ เบอร์ดี  เบอร์หงส์ 289 เบอร์มังกร 789 เบอร์รับทรัพย์-รับโชค ศูนย์รวมเลขสวยเบอร์มงคล ที่ถูกต้องตามหลักโหราศาสตร์ไทย เบอร์ขายดี เบอร์มงคลที่ดีและสวยที่สุดในประเทศไทย">
    <meta name="author" content="berkaidee">
    <meta property="og:image:type" content="image/jpg">
    <meta property="og:description" content="berkaidee เบอร์ดี ของคนมีระดับ">
    <meta property="og:type" content="berkaidee">
    <link rel="apple-touch-icon" sizes="57x57" href="images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="images/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="images/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon/favicon-16x16.png">
    <link rel="manifest" href="images/favicon/manifest.json">
    <title>Berkaidee - เบอร์ขายดี เบอร์ดีของคนมีระดับ</title>
    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="fontawesome/fontawesome-all.css" rel="stylesheet">
    <link href="fontawesome/font-custom.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/modern-business.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
</head>
<body>
    <?php include ("navbar.php");?>
    <!-- Page Content -->
    <header class="mb-5"> </header>
    <div class="container styleContainer py-4 rounded border box-shadow">
        <div class="row">
            <!-- Post Content Column -->
            <div class="col-md-12">
                <div class="content">
                    <!--bercategory-->
                    <div class="ber-category mb-5">
                        <div class="page-header">ติดต่อเรา</div>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html">หน้าแรก</a></li>
                            <li class="breadcrumb-item active">ติดต่อเรา</li>
                        </ol>
                        <div class="contact-us">
                            <div class="row">
                                <div class="col-sm-6 contact-detail">
                                    <!--  <img src="assets/img/logo-solid.png" height="60" alt="" > -->
                                    <div class="address-description">
                                        <p>
                                            <i class="fa fa-phone"></i>
                                          <a href="tel:+66956369565"><i class="fas fa-mobile-alt"></i> 095-636-9565 K. กมล</a>
                                        </p>
                                        <p>
                                            <i class="fa fa-phone"></i>
                                             <a href="tel:+66814562456"> <i class="fas fa-mobile-alt"></i> 081-456-2456 K. สมชัย</a>
                                        </p>
                                        
                                      
                                           <p>
                                            <a href="http://line.me/ti/p/~kamol789.com">
                                                <i class="fab fa-line"></i> kamol789.com</a>
                                        </p>
                                        <p>
                                            <a href="http://line.me/ti/p/~berkaidee">
                                                <i class="fab fa-line"></i> berkaidee</a>
                                        </p>
                                       <p>
                                            <i class="fa fa-envelope"> </i>
                                            <a href="mailto:berkaidee@gmail.com">berkaidee@gmail.com</a>
                                        </p>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <h3 class="contact-form-title">ส่งข้อความ</h3>
                                    <form id="contactUsForm" novalidate>
                                        <div class="form-group">
                                            <label for="">ชื่อ-สกุล</label>
                                            <input type="text" class="form-control" id="contactName" name="contactName">
                                        </div>
                                        <div class="form-group">
                                            <label for="">อีเมล์</label>
                                            <input type="email" class="form-control" id="contactEmail" name="contactEmail">
                                        </div>
                                        <div class="form-group">
                                            <label for="">เบอร์โทรศัพท์</label>
                                            <input type="text" class="form-control" id="contactTel" name="contactTel">
                                        </div>
                                        <div class="form-group">
                                            <label for="">ข้อความ</label>
                                            <textarea class="form-control" rows="3" id="contactDetail" name="contactDetail"></textarea>
                                        </div>
                                        <div class="g-recaptcha" data-sitekey="6Lc28RAUAAAAADqg9k3CJ-Ov3TaX_7ctZiFi-wbX"></div>
                                        <p>
                                            <button type="submit" class="btn btn-primary mt-3" name="contactUsBtn">ส่งข้อความ</button>
                                        </p>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end bernew-->
                </div>
            </div>
            <!--end bernew-->
        </div>
        <!--col-lg-9-->
        <!-- /.row -->
    </div>
    <!-- /.container -->
    <?php include("footer.php");?>
    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="https://www.google.com/recaptcha/api.js" async="" defer=""></script>
    <!-- Validation -->
    <script src="js/validation.min.js"></script>
</body>
</html>