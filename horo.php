<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="keywords" content="berkaidee, เบอร์ขายดี, เบอร์มังกร, เบอร์รวย, เบอร์มงคล, เลขศาสตร์, ทำนายเบอร์, เบอร์มีระดับ, ทำนายเบอร์, เบอร์หงษ์, เบอร์กวนอู, เบอร์ platinum, เบอร์ gold, เบอร์ silver, ปรึกษาเบอร์, บริการขายเบอร์, แหล่งซื้อขายเบอร์มือถือ, เบอร์ราคาถูก, เบอร์ดี, เบอร์สวย, ซิมเบอร์สวย, เบอร์vip, เบอร์เฮง, เบอร์หาม, เบอร์789,เบอร์289, เบอร์รับทรัพย์, เบอร์รับโชค, บริหารจัดหาเบอร์, รวมเบอร์, เบอร์สวยที่สุดในประเทศไทย ">
    <meta name="description" content="เบอร์ขายดี เบอร์ดี ของคนมีระดับบริการรับจัดหา ซื้อ-ขายเบอร์มงคล เบอร์สวย เลขศาสตร์ เบอร์ดี  เบอร์หงส์ 289 เบอร์มังกร 789 เบอร์รับทรัพย์-รับโชค ศูนย์รวมเลขสวยเบอร์มงคล ที่ถูกต้องตามหลักโหราศาสตร์ไทย เบอร์ขายดี เบอร์มงคลที่ดีและสวยที่สุดในประเทศไทย">
    <meta name="author" content="berkaidee">
    <meta property="og:image:type" content="image/jpg">
    <meta property="og:description" content="berkaidee เบอร์ดี ของคนมีระดับ">
    <meta property="og:type" content="berkaidee">
    <link rel="apple-touch-icon" sizes="57x57" href="images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="images/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="images/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon/favicon-16x16.png">
    <link rel="manifest" href="images/favicon/manifest.json">
    <link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.css" />
    <link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials-theme-minima.css" />
    <title>Berkaidee - เบอร์ขายดี เบอร์ดีของคนมีระดับ</title>
    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="fontawesome/fontawesome-all.css" rel="stylesheet">
    <link href="fontawesome/font-custom.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/modern-business.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
</head>
<body>
    <?php include ("navbar.php");?>
    <!-- Page Content -->
    <header class="mb-5"></header>
    <div class="container styleContainer py-4 rounded border box-shadow">
        <div class="row">
            <!-- Post Content Column -->
            <div class="col-md-12">
                <div class="content">
                    <!--bercategory-->
                    <div class="ber-category mb-5">
                        <div class="page-header">
                            ทำนายเบอร์
                        </div>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="home.php">หน้าแรก</a>
                            </li>
                            <li class="breadcrumb-item active">ทำนายเบอร์</li>
                        </ol>
                        <div class="horo-content">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">เลขฐานผลรวม</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">เลขฐาน 10</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="horoContent"></div>
                        </div>
                    </div>
                    <!-- end bernew-->
                </div>
            </div>
            <!--end bernew-->
        </div>
        <!--col-lg-9-->
        <!-- /.row -->
    </div>
    <!-- /.container -->
    <?php include("footer.php");?>
    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.min.js"></script>
    <script>
        $("#share").jsSocials({
            shares: [{
                    share: "email"
                },
                {
                    share: "facebook",
                    logo: "fab fa-facebook"
                },
                {
                    share: "twitter",
                    logo: "fab fa-twitter"
                },
                {
                    share: "googleplus",
                    logo: "fab fa-google"
                }
            ]
        });
    </script>
</body>
</html>