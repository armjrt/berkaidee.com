<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="keywords" content="berkaidee, เบอร์ขายดี, เบอร์มังกร, เบอร์รวย, เบอร์มงคล, เลขศาสตร์, ทำนายเบอร์, เบอร์มีระดับ, ทำนายเบอร์, เบอร์หงษ์, เบอร์กวนอู, เบอร์ platinum, เบอร์ gold, เบอร์ silver, ปรึกษาเบอร์, บริการขายเบอร์, แหล่งซื้อขายเบอร์มือถือ, เบอร์ราคาถูก, เบอร์ดี, เบอร์สวย, ซิมเบอร์สวย, เบอร์vip, เบอร์เฮง, เบอร์หาม, เบอร์789,เบอร์289, เบอร์รับทรัพย์, เบอร์รับโชค, บริหารจัดหาเบอร์, รวมเบอร์, เบอร์สวยที่สุดในประเทศไทย ">
    <meta name="description" content="เบอร์ขายดี เบอร์ดี ของคนมีระดับบริการรับจัดหา ซื้อ-ขายเบอร์มงคล เบอร์สวย เลขศาสตร์ เบอร์ดี  เบอร์หงส์ 289 เบอร์มังกร 789 เบอร์รับทรัพย์-รับโชค ศูนย์รวมเลขสวยเบอร์มงคล ที่ถูกต้องตามหลักโหราศาสตร์ไทย เบอร์ขายดี เบอร์มงคลที่ดีและสวยที่สุดในประเทศไทย">
    <meta name="author" content="berkaidee">
    <meta property="og:image:type" content="image/jpg">
    <meta property="og:description" content="berkaidee เบอร์ดี ของคนมีระดับ">
    <meta property="og:type" content="berkaidee">
    <link rel="apple-touch-icon" sizes="57x57" href="images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="images/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="images/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon/favicon-16x16.png">
    <link rel="manifest" href="images/favicon/manifest.json">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">

    <title>Berkaidee - เบอร์ขายดี เบอร์ดีของคนมีระดับ</title>
    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="fontawesome/fontawesome-all.css" rel="stylesheet">
    <link href="fontawesome/font-custom.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/modern-business.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-46870628-4"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-46870628-4');
</script>

</head>
<body class="styleBody">
    <?php include ("navbar.php");?>
    <!-- Page Content -->
    <!-- test -->
    <header>
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div id="bannerList" class="carousel-inner" role="listbox"></div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon " aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>

        </div>
        <div class="container ">
            
        </div>
        
    </header>
    <div class="container p-0 position-relative my-3 rounded border box-shadow">
            <div class="bg-search">
                <div class="position-relative mb-3 p-4 rounded ">
                     <div class="page-header mb-5">
                            ทำนายเบอร์
                        </div>
                        <p class="text-info text-center font-weight-400">ระบบวิเคราะห์เบอร์ ที่แม่นยำที่สุด ครอบคลุมทุกศาสตร์</p>
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="ใส่เบอร์โทร 10 หลัก" id="numberSearchFilter" maxlength="10">
                        <span class="input-group-btn ml-2">
                            <button class="btn btn-primary" id="numberSearchFilterBtn">ทำนาย</button>
                        </span>
                    </div>
                    <p class="text-danger mt-3">***การทำนายเบอร์ เป็นความเชื่อส่วนบุคล โปรดใช้วิจารณญาน</p>
                </div>
            </div>
    </div>
    <div class="container p-0 position-relative mb-3 rounded border box-shadow">
            <div class="bg-search">
                <div class="position-relative rounded border">

                    <?php include('search-top.php'); ?>
                </div>
            </div>
    </div>
    <div class="container styleContainer py-4 rounded border box-shadow position-relative">
        <div class="row">
            <!-- Post Content Column -->
            <div class="col-md-12">
                <div class="content">
                    <!--bercategory-->
                    <div class="ber-category mb-3">
                        <div class="page-header mb-5">
                            หมวดหมู่เบอร์ขายดี
                        </div>
                        <div class="row">
                            <div class="col-sm-4 col-xs-6 mb-3 col-6">
                                <div class="category-number text-center">
                                    <a href="categories.php?categories_id=1">
                                        <div class="icon-category">
                                            <i class="demo-icon icon-dragon"></i>
                                        </div>
                                        <div class="title">เบอร์มังกร</div>

                                    </a>
                                 <!--    <p>เงินก้อนใหญ่ มีอำนาจบารมี มั่งคั่งร่ำรวย</p> -->
                                </div>
                            </div>
                            <div class="col-sm-4 col-xs-6 mb-3 col-6">
                                <div class="category-number text-center">
                                    <a href="categories.php?categories_id=2">
                                        <div class="icon-category">
                                            <i class="demo-icon icon-swan"></i>
                                        </div>
                                        <div class="title">เบอร์หงษ์</div>
                                    </a>
                                   <!--  <p>289 มีอำนาจบารมี เงินก้อนใหญ่ ธุรกิจมั่นคง </p> -->
                                </div>
                            </div>
                            <div class="col-sm-4 col-xs-6 mb-3 col-6">
                                <div class="category-number text-center">
                                    <a href="categories.php?categories_id=3">
                                        <div class="icon-category">
                                        <img src="images/money.svg" alt="" class="svg-img">
                                        </div>
                                        <div class="title">เบอร์เศรษฐี</div>
                                    </a>
                                   <!--  <p> การแข่งขัน ค้าขายรุ่งเรือง มีอำนาจบารมี</p> -->
                                </div>
                            </div>
                            <div class="col-sm-4 col-xs-6 mb-3 col-6">
                                <div class="category-number text-center">
                                    <a href="categories.php?categories_id=4">
                                        <div class="icon-category">
                                            <i class="far fa-gem"></i>
                                        </div>
                                        <div class="title">เบอร์ Platinum</div>
                                    </a>
                                   <!--  <p>เบอร์มงคลระดับ VIP</p> -->
                                </div>
                            </div>
                            <div class="col-sm-4 col-xs-6 mb-3 col-6">
                                <div class="category-number text-center">
                                    <a href="categories.php?categories_id=5">
                                        <div class="icon-category">
                                            <i class="fa fa-star"></i>
                                        </div>
                                        <div class="title">เบอร์ Gold</div>
                                    </a>
                               <!--      <p>เบอร์มงคลคัดพิเศษ</p> -->
                                </div>
                            </div>
                            <div class="col-sm-4 col-xs-6 mb-3 col-6">
                                <div class="category-number text-center">
                                    <a href="categories.php?categories_id=6">
                                        <div class="icon-category">
                                            <i class="fa fa-thumbs-up"></i>
                                        </div>
                                        <div class="title">เบอร์ Silver</div>
                                    </a>
                                    <!-- <p>เบอร์ดีเลขมงคล</p> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container styleContainer my-3 p-4 rounded border box-shadow">
        <div class="row">
            <div class="col-md-12">
                <div class="content">
                    <!-- end bernew-->
                    <div class="ber-new">
                        <div class="page-header mb-5">
                            เบอร์มาใหม่
                            
                        </div>
                        <div class="berkaidee-listview table-responsive-sm">
                            <div id="productsTable" class="row">
                            </div>
                            <!-- <table id="productsTable" class="table berlist">
                                <tbody></tbody>
                            </table> -->
                            <div class="view-more text-right mt-3">
                                <a href="categories.php" class="viewall text-dark">ดูเบอร์ใหม่ทั้งหมด
                                <i class="fas fa-angle-double-right"></i>
                            </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end bernew-->
        </div>
        <!--col-lg-9-->
        <!-- /.row -->
    </div>
    <!-- /.container -->
    <?php include("footer.php");?>
    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
</body>
</html>