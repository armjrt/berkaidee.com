<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="keywords" content="berkaidee, เบอร์ขายดี, เบอร์มังกร, เบอร์รวย, เบอร์มงคล, เลขศาสตร์, ทำนายเบอร์, เบอร์มีระดับ, ทำนายเบอร์, เบอร์หงษ์, เบอร์กวนอู, เบอร์ platinum, เบอร์ gold, เบอร์ silver, ปรึกษาเบอร์, บริการขายเบอร์, แหล่งซื้อขายเบอร์มือถือ, เบอร์ราคาถูก, เบอร์ดี, เบอร์สวย, ซิมเบอร์สวย, เบอร์vip, เบอร์เฮง, เบอร์หาม, เบอร์789,เบอร์289, เบอร์รับทรัพย์, เบอร์รับโชค, บริหารจัดหาเบอร์, รวมเบอร์, เบอร์สวยที่สุดในประเทศไทย ">
    <meta name="description" content="เบอร์ขายดี เบอร์ดี ของคนมีระดับบริการรับจัดหา ซื้อ-ขายเบอร์มงคล เบอร์สวย เลขศาสตร์ เบอร์ดี  เบอร์หงส์ 289 เบอร์มังกร 789 เบอร์รับทรัพย์-รับโชค ศูนย์รวมเลขสวยเบอร์มงคล ที่ถูกต้องตามหลักโหราศาสตร์ไทย เบอร์ขายดี เบอร์มงคลที่ดีและสวยที่สุดในประเทศไทย">
    <meta name="author" content="berkaidee">
    <meta property="og:image:type" content="image/jpg">
    <meta property="og:description" content="berkaidee เบอร์ดี ของคนมีระดับ">
    <meta property="og:type" content="berkaidee">
    <link rel="apple-touch-icon" sizes="57x57" href="images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="images/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="images/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon/favicon-16x16.png">
    <link rel="manifest" href="images/favicon/manifest.json">

    <title>Berkaidee - เบอร์ขายดี เบอร์ดีของคนมีระดับ</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="fontawesome/fontawesome-all.css" rel="stylesheet">
    <link href="fontawesome/font-custom.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/modern-business.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
</head>

<body>

    <?php include ("navbar.php");?>

    <!-- Page Content -->
    <header class="mb-5">

    </header>
    <div class="container styleContainer py-4 rounded border box-shadow">



        <div class="row">
            
            <!-- Post Content Column -->
            <div class="col-md-12">



                <div class="content">

                    <!--bercategory-->
                    <div class="ber-category mb-5">
                        <div class="page-header">
                            วิธีสั่งซื้อ/ชำระเงิน
                        </div>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="index.html">หน้าแรก</a>
                            </li>
                            <li class="breadcrumb-item active">วิธีสั่งซื้อ</li>
                        </ol>
                        <div class="howto">
                            <h5 class="page-title text-primary">วิธีการชำระสินค้า</h5>
                            <p>SMS มาที่เบอร์ 081-456-2456 ** ส่งของทุกวัน เวลาประมาณ 15.00 น. ** ลูกค้าโอนเงินก่อน 12.00 น.
                                จะทำการส่งของ ให้ภายในวันที่โอนเงิน หากโอนเงินแล้วสามารถแจ้งการชำระเงินได้ทาง เบอร์โทรศัพท์หรือทาง
                                Line ครับ
                                <br> ** กรณีนัดรับส่งสินค้าราคาตั้งแต่ 15,000 บาท ขึ้นไป ในกรุงเทพและปริมณฑล</p>
                            <div class="line-box" style="padding-bottom : 10px;">
                                  <a href="http://line.me/ti/p/~kamol789.com">
                                    <img src="images/kamol-line.png" class="img-fluid" style="margin-bottom : 10px;" alt="">
                                </a>
                                <a href="http://line.me/ti/p/~berkaidee">
                                    <img src="images/berkaidee-line.png" class="img-fluid" style="margin-bottom : 10px;" alt=""> </a>
                              
                            </div>


                            <p> ทุกเบอร์พร้อมส่ง รับจองเบอร์ภายใน 24 ชม. </p>
                            <h5 class="page-title text-primary">บริการชำระผ่านบัตรเครดิตวีซ่า/มาสเตอร์การ์ด</h5>
                            <img src="images/visa-mastercard-logo.png" height="50" alt="" style="margin-bottom :20px;">

                            <h6 class="page-title">ฟรี ! ค่าธรรมเนี่ยมเมื่อซื้อสินค้าตั้งแต่ 20,000 บาทขึ้นไป </h6>
                            <p> ยินดีรับบัตรเครดิต บริการชำระผ่านบัตรเครดิตทุกธนาคาร VISA,Mastercard คิดค่าธรรมเนียม 3% ทุกรายการ
                                สำหรับลูกค้าที่มีบัตรเครดิต ธนาคารสิกรไทย 0% 3-6 เดือน
                                <br> ***หลังจากลูกค้าได้รับซิม ให้ทำตามเงื่อนไขด้านล่าง เพื่อความถูกต้อง
                            </p>
                            <ol>
                                <li>ลูกค้า ตรวจสอบสินค้า หมายเลขซิม ลองเอาซิมใส่เครื่องมือถือ โทรเข้า-ออก เพื่อเช็คสัญญาน ว่าเบอร์ที่ลูกค้าสั่งซื้อ
                                    เลขหมายถูกต้องหรือไม่</li>
                                <li>ลูกค้า ต้องรีบไปดำเนินการติดต่อที่ศูนย์บริการ เพื่อทำการลงทะเบียนหรือโอนสิทธิเปลี่ยนเป็นชื่อลูกค้าภายใน
                                    30 วัน นับตั้งแต่วันที่มีการจัดส่งสินค้า</li>
                                <li>หากซิมเสีย-ไม่มีสัญญาณ-โทรเข้า โทรออกไม่ได้ ทางร้านยินดีคืนเงินให้ลูกค้าเต็มจำนวน (กรณีอื่นงดเปลี่ยนคืนเงิน)</li>
                            </ol>
                        </div>
                    </div>
                    <!-- end bernew-->


                </div>
            </div>

            <!--end bernew-->






        </div>
        <!--col-lg-9-->



        <!-- /.row -->

    </div>



    <!-- /.container -->

    <?php include("footer.php");?>
    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>
