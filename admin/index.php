<!DOCTYPE html>
<html lang="en">
<head>
  <?php include 'shared/header.php'; ?>
</head>
<body class="fix-header card-no-border logo-center">
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50"><circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /></svg>
    </div>
    <div id="main-wrapper">
        <header class="topbar">
            <?php include 'shared/navbar.php'; ?>
        </header>
        <aside class="left-sidebar">
            <?php include 'shared/sidebar.php'; ?>
        </aside>
        <div class="page-wrapper">
            <?php include 'shared/breadcrumb.php'; ?>
            <div class="container-fluid">
                <div class="row">
                    <?php include 'route.php'; ?>
                </div>
            </div>
            <footer class="footer">&copy; <script> document.write(new Date().getFullYear()); </script> Berkaidee Admin by berkaidee.com</footer>
        </div>
    </div>
    <?php include 'shared/footer.php'; ?>
</body>
</html>