<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon-32x32.png">
    <title>Berkaidee</title>
    <!-- Bootstrap Core CSS -->
    <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="assets/css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="assets/css/colors/blue.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <section id="wrapper">
        <div class="login-register" style="background-image:url(assets/images/background/login-register.jpg);">
            <div class="login-box card">
                <div class="card-body">
                    <form id="loginForm" class="form-horizontal form-material">
                        <h3 class="box-title m-b-20">เข้าสู่ระบบ</h3>
                        <div class="form-group ">
                            <div class="col-xs-12">
                                <input class="form-control" type="email" id="email" name="email" placeholder="อีเมล์" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <input class="form-control" type="password" id="password" name="password" placeholder="รหัสผ่าน" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12 font-14">
                                <a href="javascript:void(0)" id="showResetPasswordBtn" class="text-dark pull-right"><!-- <i class="fa fa-lock m-r-5"></i> --> ลืมรหัสผ่าน ?</a>
                            </div>
                        </div>
                        <div class="form-group text-center m-t-20">
                            <div class="col-xs-12">
                                <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit" id="loginBtn" name="loginBtn">เข้าสู่ระบบ</button>
                            </div>
                        </div>
                        <!--<div class="form-group m-b-0">
                            <div class="col-sm-12 text-center">
                                <div>ยังไม่มีบัญชีผู้ใช้ ? <a href="pages-register.html" class="text-info m-l-5"><b>สมัครสมาชิก</b></a></div>
                            </div>
                        </div>-->
                    </form>
                    <!--<form id="resetPasswordForm" class="form-horizontal hide" style="margin-top: 10px;">
                        <div class="form-group">
                            <div class="col-xs-12">
                                <h3>Reset Password</h3>
                                <p class="text-muted">กรุณากรอกอีเมล์ของท่าน</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <input class="form-control" type="text" id="emailReset" name="emailReset" placeholder="อีเมล์">
                            </div>
                        </div>
                        <div class="form-group text-center m-t-20">
                            <div class="col-xs-12">
                                <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit" id="resetPasswordBtn">Reset</button>
                            </div>
                        </div>
                    </form>-->
                </div>
            </div>
        </div>
    </section>
    <!-- All Jquery -->
    <script src="assets/plugins/jquery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="assets/plugins/bootstrap/js/popper.min.js"></script>
    <script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- Slimscrollbar scrollbar JavaScript -->
    <script src="assets/js/jquery.slimscroll.js"></script>
    <!-- Wave Effects -->
    <script src="assets/js/waves.js"></script>
    <!-- Menu sidebar -->
    <script src="assets/js/sidebarmenu.js"></script>
    <!-- Stickey kit -->
    <script src="assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="assets/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- Validation -->
    <script src="assets/js/validation.min.js"></script>
    <!-- Sweet Alert -->
    <script src="assets/plugins/sweetalert/sweetalert2.all.js"></script>
    <!-- Custom JavaScript -->
    <script src="assets/js/custom.min.js"></script>
    <script src="scripts/service.js"></script>
    <script src="scripts/script.js"></script>
    <!-- Style switcher -->
    <script src="assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>
</body>
</html>