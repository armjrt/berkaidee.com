<!-- Select2 -->
<link href="assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet">
<div class="col-12">
      <div class="card">
            <div class="card-body">
                  <h4 class="card-title">เลขลำดับ</h4>
                  <div class="form-group text-right m-b-0" style="margin-top: -30px;">
                        <button class="btn btn-info" type="button" id="addHoroDegreeBtn"><span class="glyphicon glyphicon-pencil"></span>&nbsp;&nbsp;เพิ่มเลขลำดับ</button>
                        <button class="btn btn-info hide" type="button" id="viewHoroDegreeBtn"><span class="glyphicon glyphicon-eye-open"></span>&nbsp;&nbsp;รายการเลขลำดับ</button>
                  </div>
                  <div class="content-loader m-t-10">
                        <div id="viewContent" class="table-responsive">
                              <table id="horoDegreeTable" class="table table-bordered table-striped table-hover">
                                    <thead>
                                          <tr>
                                                <th class="text-center">หมายเลข</th>
                                                <th class="text-center">รายละเอียด</th>
                                                <th></th>
                                          </tr>
                                    </thead>
                                    <tbody></tbody>
                              </table>
                        </div>
                        <div id="manageContent" class="hide">
                              <form id="horoDegreeForm" novalidate>
                                    <div class="form-group">
                                          <label for="horoDegreeNumber">หมายเลข</label>
                                          <input type="text" class="form-control" name="horoDegreeNumber" id="horoDegreeNumber">
                                    </div>
                                    <div class="form-group">
                                          <label for="horoDegreeDetail">รายละเอียด</label>
                                          <textarea class="form-control" name="horoDegreeDetail" id="horoDegreeDetail"></textarea>
                                    </div>
                                    <div class="row">
                                          <div class="col-md-6">
                                                <div class="form-group">
                                                      <label for="horoDegreePercent">เปอร์เซ็นต์</label>
                                                      <input type="text" class="form-control" name="horoDegreePercent" id="horoDegreePercent">
                                                </div>
                                          </div>
                                          <div class="col-md-6">
                                                <div class="form-group has-danger">
                                                      <label for="horoDegreeType">ประเภท</label>
                                                      <select class="form-control" name="horoDegreeType" id="horoDegreeType">
                                                            <option value="" selected disabled>เลือกประเภท</option>
                                                      </select>
                                                </div>
                                          </div>
                                    </div>
                                    <div class="row">
                                          <div class="col-md-6">
                                                <div class="form-group has-danger">
                                                      <label for="horoDegreeEducation">การเรียน</label>
                                                      <select class="form-control" name="horoDegreeEducation" id="horoDegreeEducation">
                                                            <option value="" selected disabled>เลือกการเรียน</option>
                                                      </select>
                                                </div>
                                          </div>
                                          <div class="col-md-6">
                                                <div class="form-group has-danger">
                                                      <label for="horoDegreeWork">การงาน</label>
                                                      <select class="form-control" name="horoDegreeWork" id="horoDegreeWork">
                                                            <option value="" selected disabled>เลือกการงาน</option>
                                                      </select>
                                                </div>
                                          </div>
                                    </div>
                                    <div class="row">
                                          <div class="col-md-6">
                                                <div class="form-group has-danger">
                                                      <label for="horoDegreeFinance">การเงิน</label>
                                                      <select class="form-control" name="horoDegreeFinance" id="horoDegreeFinance">
                                                            <option value="" selected disabled>เลือกการเงิน</option>
                                                      </select>
                                                </div>
                                          </div>
                                          <div class="col-md-6">
                                                <div class="form-group has-danger">
                                                      <label for="horoDegreeLove">ความรัก</label>
                                                      <select class="form-control" name="horoDegreeLove" id="horoDegreeLove">
                                                            <option value="" selected disabled>เลือกความรัก</option>
                                                      </select>
                                                </div>
                                          </div>
                                    </div>
                                    <div class="row">
                                          <div class="col-md-6">
                                                <div class="form-group has-danger">
                                                      <label for="horoDegreeLucky">โชคลาภ</label>
                                                      <select class="form-control" name="horoDegreeLucky" id="horoDegreeLucky">
                                                            <option value="" selected disabled>เลือกโชคลาภ</option>
                                                      </select>
                                                </div>
                                          </div>
                                    </div>
                                    <div class="box-footer">
                                          <button type="submit" class="btn btn-success" id="saveHoroDegreeBtn" data-save-status="save" data-save-id="0">
                                                <span class="glyphicon glyphicon-plus"></span> บันทึก
                                          </button>
                                          <button type="reset" class="btn btn-inverse" id="resetBtn">
                                                <span class="glyphicon glyphicon-plus"></span> ยกเลิก
                                          </button>
                                    </div>
                              </form>
                        </div>
                  </div>
            </div>
      </div>
</div>
<!-- Data table -->
<script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
<!-- Select2 -->
<script src="assets/plugins/select2/dist/js/select2.min.js"></script>