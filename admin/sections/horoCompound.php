<!-- Select2 -->
<link href="assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet">
<div class="col-12">
      <div class="card">
            <div class="card-body">
                  <h4 class="card-title">เลขรวม</h4>
                  <div class="form-group text-right m-b-0" style="margin-top: -30px;">
                        <button class="btn btn-info" type="button" id="addHoroCompoundBtn"><span class="glyphicon glyphicon-pencil"></span>&nbsp;&nbsp;เพิ่มเลขรวม</button>
                        <button class="btn btn-info hide" type="button" id="viewHoroCompoundBtn"><span class="glyphicon glyphicon-eye-open"></span>&nbsp;&nbsp;รายการเลขรวม</button>
                  </div>
                  <div class="content-loader m-t-10">
                        <div id="viewContent" class="table-responsive">
                              <table id="horoCompoundTable" class="table table-bordered table-striped table-hover">
                                    <thead>
                                          <tr>
                                                <th class="text-center">หมายเลข</th>
                                                <th class="text-center">รายละเอียด</th>
                                                <th class="text-center">ธาตุ</th>
                                                <th></th>
                                          </tr>
                                    </thead>
                                    <tbody></tbody>
                              </table>
                        </div>
                        <div id="manageContent" class="hide">
                              <form id="horoCompoundForm" novalidate>
                                    <div class="form-group">
                                          <label for="horoCompoundNumber">หมายเลข</label>
                                          <input type="text" class="form-control" name="horoCompoundNumber" id="horoCompoundNumber">
                                    </div>
                                    <div class="form-group">
                                          <label for="horoCompoundDetail">รายละเอียด</label>
                                          <textarea class="form-control" name="horoCompoundDetail" id="horoCompoundDetail"></textarea>
                                    </div>
                                    <div class="row">
                                          <div class="col-md-6">
                                                <div class="form-group">
                                                      <label for="horoCompoundPercent">เปอร์เซ็นต์</label>
                                                      <input type="text" class="form-control" name="horoCompoundPercent" id="horoCompoundPercent">
                                                </div>
                                          </div>
                                          <div class="col-md-6">
                                                <div class="form-group has-danger">
                                                      <label for="horoCompoundType">ธาตุ</label>
                                                      <select class="form-control" name="horoCompoundType" id="horoCompoundType">
                                                            <option value="" selected disabled>เลือกประเภท</option>
                                                      </select>
                                                </div>
                                          </div>
                                    </div>
                                    <div class="box-footer">
                                          <button type="submit" class="btn btn-success" id="saveHoroCompoundBtn" data-save-status="save" data-save-id="0">
                                                <span class="glyphicon glyphicon-plus"></span> บันทึก
                                          </button>
                                          <button type="reset" class="btn btn-inverse" id="resetBtn">
                                                <span class="glyphicon glyphicon-plus"></span> ยกเลิก
                                          </button>
                                    </div>
                              </form>
                        </div>
                  </div>
            </div>
      </div>
</div>
<!-- Data table -->
<script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
<!-- Select2 -->
<script src="assets/plugins/select2/dist/js/select2.min.js"></script>