<!-- Select2 -->
<link href="assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet">
<!-- icheck -->
<link href="assets/plugins/icheck/skins/all.css" rel="stylesheet">
<div class="col-12">
      <div class="card">
            <div class="card-body">
                  <h4 class="card-title">จัดการเบอร์</h4>
                  <div class="form-group text-right m-b-0" style="margin-top: -30px;">
                        <button class="btn btn-info" type="button" id="addProductsBtn"><span class="glyphicon glyphicon-pencil"></span>&nbsp;&nbsp;เพิ่มเบอร์ขายดี</button>
                        <button class="btn btn-success" type="button" id="importProductsBtn"><span class="glyphicon glyphicon-pencil"></span>&nbsp;&nbsp;Import เบอร์ขายดี</button>
                        <button class="btn btn-info hide" type="button" id="viewProductsBtn"><span class="glyphicon glyphicon-eye-open"></span>&nbsp;&nbsp;รายการเบอร์ขายดี</button>
                        <input type="file" id="importProductsFile" accept=".csv, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" style="display: none;">
                  </div>
                  <div class="content-loader m-t-10">
                        <div id="viewContent" class="table-responsive">
                              <table id="productsTable" class="table table-bordered table-striped table-hover">
                                    <thead>
                                          <tr>
                                                <th class="text-center">เบอร์โทร</th>
                                                <th class="text-center">ราคา</th>
                                                <th class="text-center">เครือข่าย</th>
                                                <th class="text-center">หมวดหมู่</th>
                                                <th class="text-center">ความหมาย</th>
                                                <th></th>
                                          </tr>
                                    </thead>
                                    <tbody></tbody>
                              </table>
                        </div>
                        <div id="manageContent" class="hide">
                              <form id="productsForm" novalidate>
                                    <div class="form-group">
                                          <label for="productsNumber">เบอร์โทร</label>
                                          <input type="text" class="form-control" name="productsNumber" id="productsNumber">
                                    </div>
                                    <div class="form-group">
                                          <label for="productsPrice">ราคา</label>
                                          <input type="text" class="form-control" name="productsPrice" id="productsPrice">
                                    </div>
                                    <div class="row">
                                          <div class="col-md-6">
                                                <div class="form-group">
                                                      <label for="productsCarrier">เครือข่าย</label>
                                                      <select class="form-control" name="productsCarrier" id="productsCarrier">
                                                            <option value="" selected disabled>เลือกเครือข่าย</option>
                                                      </select>
                                                </div>
                                          </div>
                                          <div class="col-md-6">
                                                <div class="form-group has-danger">
                                                      <label for="productsCategories">หมวดหมู่</label>
                                                      <select class="form-control" name="productsCategories" id="productsCategories">
                                                            <!-- <option value="" selected disabled>เลือกหมวดหมู่</option> -->
                                                      </select>
                                                </div>
                                          </div>
                                    </div>
                                    <div class="form-group">
                                          <label for="productsDetail">ความหมาย</label>
                                          <textarea class="form-control" name="productsDetail" id="productsDetail"></textarea>
                                    </div>
                                    <div class="form-group">
                                          <div class="input-group">
                                                <ul class="icheck-list">
                                                      <li>
                                                            <input type="checkbox" class="check" name="productsPinAtHome" id="productsPinAtHome" data-checkbox="icheckbox_square-red">
                                                            <label for="productsPinAtHome">ปักหมุดหน้าแรก</label>
                                                      </li>
                                                      <li>
                                                            <input type="checkbox" class="check" name="productsSold" id="productsSold" data-checkbox="icheckbox_square-red">
                                                            <label for="productsSold">ขายแล้ว</label>
                                                      </li>
                                                </ul>
                                          </div>
                                    </div>
                                    <div class="box-footer">
                                          <button type="submit" class="btn btn-success" id="saveProductsBtn" data-save-status="save" data-save-id="0">
                                                <span class="glyphicon glyphicon-plus"></span> บันทึก
                                          </button>
                                          <button type="reset" class="btn btn-inverse" id="resetBtn">
                                                <span class="glyphicon glyphicon-plus"></span> ยกเลิก
                                          </button>
                                    </div>
                              </form>
                        </div>
                        <div id="importProductsPopup" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false" datastyle="display: none;">
                              <div class="modal-dialog">
                                    <div class="modal-content" style="width: 1000px; margin-left: -250px;">
                                          <div class="modal-header">
                                                <h4 class="modal-title">เพิ่มเบอร์ขายดี</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="$('#resetImportProductsBtn').trigger('click');">×</button>
                                          </div>
                                          <div class="modal-body">
                                                <form id="importProductsForm">
                                                      <table id="importProductsTable" class="table table-bordered table-striped table-hover">
                                                            <thead>
                                                                  <tr>
                                                                        <th class="text-center">เบอร์โทร</th>
                                                                        <th class="text-center">ราคา</th>
                                                                        <th class="text-center">เครือข่าย</th>
                                                                        <th class="text-center">หมวดหมู่</th>
                                                                        <th class="text-center">ขายแล้ว</th>
                                                                        <th></th>
                                                                  </tr>
                                                            </thead>
                                                            <tbody></tbody>
                                                      </table>
                                                </form>
                                          </div>
                                          <div class="modal-footer">
                                                <button type="button" class="btn btn-success waves-effect waves-light" id="saveImportProductsBtn">บันทึก</button>
                                                <button type="button" class="btn btn-reverse waves-effect" data-dismiss="modal" id="resetImportProductsBtn">ยกเลิก</button>
                                          </div>
                                    </div>
                              </div>
                        </div>
                  </div>
            </div>
      </div>
</div>
<!-- Data table -->
<script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/plugins/datatables/buttons/dataTables.buttons.min.js"></script>
<script src="assets/plugins/datatables/buttons/buttons.flash.min.js"></script>
<script src="assets/plugins/datatables/buttons/jszip.min.js"></script>
<script src="assets/plugins/datatables/buttons/pdfmake.min.js"></script>
<script src="assets/plugins/datatables/buttons/vfs_fonts.js"></script>
<script src="assets/plugins/datatables/buttons/buttons.html5.min.js"></script>
<script src="assets/plugins/datatables/buttons/buttons.print.min.js"></script>
<!-- Select2 -->
<script src="assets/plugins/select2/dist/js/select2.min.js"></script>
<!-- icheck -->
<script src="assets/plugins/icheck/icheck.min.js"></script>
<script src="assets/plugins/icheck/icheck.init.js"></script>
<!-- Papaparse-csv -->
<script src="assets/plugins/papaparse-csv/papaparse.js"></script>