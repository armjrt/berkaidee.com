<!-- Select2 -->
<link href="assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet">
<div class="col-12">
      <div class="card">
            <div class="card-body">
                  <h4 class="card-title">บทความ</h4>
                  <div class="form-group text-right m-b-0" style="margin-top: -30px;">
                        <button class="btn btn-info" type="button" id="addArticleBtn"><span class="glyphicon glyphicon-pencil"></span>&nbsp;&nbsp;เพิ่มบทความ</button>
                        <button class="btn btn-info hide" type="button" id="viewArticleBtn"><span class="glyphicon glyphicon-eye-open"></span>&nbsp;&nbsp;รายการบทความ</button>
                  </div>
                  <div class="content-loader m-t-10">
                        <div id="viewContent" class="table-responsive">
                              <table id="articleTable" class="table table-bordered table-striped table-hover">
                                    <thead>
                                          <tr>
                                                <th class="text-center">ชื่อบทความ</th>
                                                <th class="text-center">หมวดหมู่</th>
                                                <th class="text-center">วันที่สร้าง</th>
                                                <th class="text-center">จำนวนผู้เข้าชม</th>
                                                <th></th>
                                          </tr>
                                    </thead>
                                    <tbody></tbody>
                              </table>
                        </div>
                        <div id="manageContent" class="hide">
                              <form id="articleForm" novalidate>
                                    <div class="row">
                                          <div class="col-md-6">
                                                <div class="form-group">
                                                      <label for="articleName">ชื่อบทความ</label>
                                                      <input type="text" class="form-control" name="articleName" id="articleName">
                                                </div>
                                          </div>
                                          <div class="col-md-6">
                                                <div class="form-group has-danger">
                                                      <label for="articleCategories">หมวดหมู่</label>
                                                      <select class="form-control" name="articleCategories" id="articleCategories">
                                                            <option value="" selected disabled>เลือกหมวดหมู่</option>
                                                      </select>
                                                </div>
                                          </div>
                                    </div>
                                    <div class="form-group">
                                          <label for="articleContent">บทความ</label>
                                          <textarea id="articleContent" name="articleContent"></textarea>
                                    </div>
                                    <div class="form-group">
                                          <label for="articleImagePath">รูป Cover บทความ</label>
                                          <div class="card-columns el-element-overlay">
                                                <div class="card" style="width: 300px;">
                                                      <div class="el-card-item" style="padding-bottom: 0;">
                                                            <div class="el-card-avatar el-overlay-1 image-tumbnail">
                                                                  <a class="image-popup-vertical-fit" href="uploads/no-image-available.png"><img src="uploads/no-image-available.png" style="height: 250px;"></a>
                                                            </div>
                                                            <div class="el-card-content">
                                                                  <div class="image-upload-btn">
                                                                        <span class="btn btn-success">เลือกรูปภาพ
                                                                              <input type="file" id="imagePathFile" name="imagePathFile" accept="image/jpeg, image/png">
                                                                        </span>
                                                                  </div>
                                                            </div>
                                                      </div>
                                                </div>
                                          </div>
                                    </div>
                                    <div class="box-footer">
                                          <button type="submit" class="btn btn-success" id="saveArticleBtn" data-save-status="save" data-save-id="0">
                                                <span class="glyphicon glyphicon-plus"></span> บันทึก
                                          </button>
                                          <button type="reset" class="btn btn-inverse" id="resetBtn">
                                                <span class="glyphicon glyphicon-plus"></span> ยกเลิก
                                          </button>
                                    </div>
                              </form>
                        </div>
                  </div>
            </div>
      </div>
</div>
<!-- Data table -->
<script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
<!-- Select2 -->
<script src="assets/plugins/select2/dist/js/select2.min.js"></script>