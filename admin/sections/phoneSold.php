<div class="col-12">
      <div class="card">
            <div class="card-body">
                  <h4 class="card-title">เบอร์ที่ขายแล้ว</h4>
                  <div class="content-loader m-t-10">
                        <div id="viewContent" class="table-responsive">
                              <table id="phoneSoldTable" class="table table-bordered table-striped table-hover">
                                    <thead>
                                          <tr>
                                                <th class="text-center">เบอร์โทรศัพท์</th>
                                                <th class="text-center">หมวดหมู่</th>
                                                <th class="text-center">วันที่ขาย</th>
                                                <th></th>
                                          </tr>
                                    </thead>
                                    <tbody></tbody>
                              </table>
                        </div>
                  </div>
            </div>
      </div>
</div>
<!-- Data table -->
<script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/plugins/datatables/buttons/dataTables.buttons.min.js"></script>
<script src="assets/plugins/datatables/buttons/buttons.flash.min.js"></script>
<script src="assets/plugins/datatables/buttons/jszip.min.js"></script>
<script src="assets/plugins/datatables/buttons/pdfmake.min.js"></script>
<script src="assets/plugins/datatables/buttons/vfs_fonts.js"></script>
<script src="assets/plugins/datatables/buttons/buttons.html5.min.js"></script>
<script src="assets/plugins/datatables/buttons/buttons.print.min.js"></script>