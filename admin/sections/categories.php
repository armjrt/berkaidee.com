<div class="col-12">
      <div class="card">
            <div class="card-body">
                  <h4 class="card-title">เพิ่มหมวดหมู่เบอร์</h4>
                  <div class="form-group text-right m-b-0" style="margin-top: -30px;">
                        <button class="btn btn-info" type="button" id="addCategoriesBtn"><span class="glyphicon glyphicon-pencil"></span>&nbsp;&nbsp;เพิ่มหมวดหมู่เบอร์</button>
                        <button class="btn btn-info hide" type="button" id="viewCategoriesBtn"><span class="glyphicon glyphicon-eye-open"></span>&nbsp;&nbsp;รายการหมวดหมู่เบอร์</button>
                  </div>
                  <div class="content-loader m-t-10">
                        <div id="viewContent" class="table-responsive">
                              <table id="categoriesTable" class="table table-bordered table-striped table-hover">
                                    <thead>
                                          <tr>
                                                <th class="text-center">ชื่อหมวดหมู่เบอร์</th>
                                                <th></th>
                                          </tr>
                                    </thead>
                                    <tbody></tbody>
                              </table>
                        </div>
                        <div id="manageContent" class="hide">
                              <form id="categoriesForm" novalidate>
                                    <div class="form-group">
                                          <label for="categoriesName">ชื่อหมวดหมู่เบอร์</label>
                                          <input type="text" class="form-control" name="categoriesName" id="categoriesName">
                                    </div>
                                    <div class="box-footer">
                                          <button type="submit" class="btn btn-success" id="saveCategoriesBtn" data-save-status="save" data-save-id="0">
                                                <span class="glyphicon glyphicon-plus"></span> บันทึก
                                          </button>
                                          <button type="reset" class="btn btn-inverse" id="resetBtn">
                                                <span class="glyphicon glyphicon-plus"></span> ยกเลิก
                                          </button>
                                    </div>
                              </form>
                        </div>
                  </div>
            </div>
      </div>
</div>
<!-- Data table -->
<script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>