<div class="col-12">
      <div class="card">
            <div class="card-body">
                  <h4 class="card-title">แบนเนอร์โฆษณา</h4>
                  <div class="form-group text-right m-b-0" style="margin-top: -30px;">
                        <button class="btn btn-info" type="button" id="addBannerBtn"><span class="glyphicon glyphicon-pencil"></span>&nbsp;&nbsp;เพิ่มแบนเนอร์โฆษณา</button>
                        <button class="btn btn-info hide" type="button" id="viewBannerBtn"><span class="glyphicon glyphicon-eye-open"></span>&nbsp;&nbsp;รายการแบนเนอร์โฆษณา</button>
                  </div>
                  <div class="content-loader m-t-10">
                        <div id="viewContent" class="table-responsive">
                              <table id="bannerTable" class="table table-bordered table-striped table-hover">
                                    <thead>
                                          <tr>
                                                <th class="text-center">หัวข้อ</th>
                                                <th class="text-center">รายละเอียด</th>
                                                <th class="text-center">รูปแบนเนอร์โฆษณา</th>
                                                <th></th>
                                          </tr>
                                    </thead>
                                    <tbody></tbody>
                              </table>
                        </div>
                        <div id="manageContent" class="hide">
                              <form id="bannerForm" novalidate>
                                    <div class="form-group">
                                          <label for="bannerName">หัวข้อ</label>
                                          <input type="text" class="form-control" name="bannerName" id="bannerName">
                                    </div>
                                    <div class="form-group">
                                          <label for="bannerDetail">รายละเอียด</label>
                                          <textarea class="form-control" name="bannerDetail" id="bannerDetail"></textarea>
                                    </div>
                                    <div class="form-group">
                                          <label for="bannerImagePath">รูปแบนเนอร์โฆษณา</label>
                                          <div class="card-columns el-element-overlay">
                                                <div class="card" style="width: 300px;">
                                                      <div class="el-card-item" style="padding-bottom: 0;">
                                                            <div class="el-card-avatar el-overlay-1 image-tumbnail">
                                                                  <a class="image-popup-vertical-fit" href="uploads/no-image-available.png"><img src="uploads/no-image-available.png" style="height: 250px;"></a>
                                                            </div>
                                                            <div class="el-card-content">
                                                                  <div class="image-upload-btn">
                                                                        <span class="btn btn-success">เลือกรูปภาพ
                                                                              <input type="file" id="imagePathFile" name="imagePathFile" accept="image/jpeg, image/png">
                                                                        </span>
                                                                  </div>
                                                            </div>
                                                      </div>
                                                </div>
                                          </div>
                                    </div>
                                    <div class="box-footer">
                                          <button type="submit" class="btn btn-success" id="saveBannerBtn" data-save-status="save" data-save-id="0">
                                                <span class="glyphicon glyphicon-plus"></span> บันทึก
                                          </button>
                                          <button type="reset" class="btn btn-inverse" id="resetBtn">
                                                <span class="glyphicon glyphicon-plus"></span> ยกเลิก
                                          </button>
                                    </div>
                              </form>
                        </div>
                  </div>
            </div>
      </div>
</div>
<!-- Data table -->
<script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>