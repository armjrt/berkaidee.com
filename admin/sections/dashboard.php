<div class="col-lg-4 col-md-12">
    <a href="products" class="dashboard-link">
        <div class="card card-inverse card-info">
            <div class="card-body">
                <div class="d-flex">
                    <div class="m-r-20 align-self-center">
                        <h1 class="text-white"><i class="ti-pie-chart"></i></h1></div>
                    <div>
                        <h3 class="card-title">เบอร์หมวดหมู่หลัก</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-4 align-self-center">
                        <h2 class="font-light text-white" id="productsNums">0</h2>
                    </div>
                </div>
            </div>
        </div>
    </a>
</div>
<div class="col-lg-4 col-md-12">
    <a href="phone-sold" class="dashboard-link">
        <div class="card card-inverse card-success">
            <div class="card-body">
                <div class="d-flex">
                    <div class="m-r-20 align-self-center">
                        <h1 class="text-white"><i class="ti-pie-chart"></i></h1></div>
                    <div>
                        <h3 class="card-title">เบอร์ที่ขายแล้ว</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-4 align-self-center">
                        <h2 class="font-light text-white" id="soldPhoneNums">0</h2>
                    </div>
                </div>
            </div>
        </div>
    </a>
</div>
<div class="col-lg-4 col-md-12">
    <a href="ems-tracking" class="dashboard-link">
        <div class="card card-inverse card-danger">
            <div class="card-body">
                <div class="d-flex">
                    <div class="m-r-20 align-self-center">
                        <h1 class="text-white"><i class="ti-pie-chart"></i></h1></div>
                    <div>
                        <h3 class="card-title">EMS Tracking</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-4 align-self-center">
                        <h2 class="font-light text-white" id="emsTrackingNums">0</h2>
                    </div>
                </div>
            </div>
        </div>
    </a>
</div>