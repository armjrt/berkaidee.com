<div class="col-12">
      <div class="row">
            <div class="col-lg-4 col-xlg-3 col-md-5">
                  <div class="card">
                        <div class="card-body">
                              <center class="m-t-30">
                                    <img id="accountGalaryPathView" src="uploads/no-image-available.png" class="img-circle" width="150" height="150">
                                    <h4 class="card-title m-t-10"><span id="accountNameView"></span></h4>
                                    <h6 class="card-subtitle">เป็นสมาชิกตั้งแต่ <span id="accountJoinDateView"></span></h6>
                              </center>
                        </div>
                        <div><hr></div>
                        <div class="card-body">
                              <small class="text-muted">อีเมล์</small>
                              <h6><span id="accountEmailView"></span></h6>
                        </div>
                  </div>
            </div>
            <div class="col-lg-8 col-xlg-9 col-md-7">
                  <div class="card">
                        <div class="card-body">
                              <form id="accountForm" class="form-horizontal form-material">
                                    <div class="form-group">
                                          <label for="accountImagePath">รูปประจำตัว</label>
                                          <div class="card-columns el-element-overlay">
                                                <div class="card" style="width: 150px;">
                                                      <div class="el-card-item" style="padding-bottom: 0;">
                                                            <div class="el-card-avatar el-overlay-1 image-tumbnail">
                                                                  <a class="image-popup-vertical-fit" href="uploads/no-image-available.png"><img src="uploads/no-image-available.png" style="height: 150px;"></a>
                                                            </div>
                                                            <div class="el-card-content">
                                                                  <div class="image-upload-btn">
                                                                        <span class="btn btn-success">เลือกรูปภาพ
                                                                              <input type="file" id="imagePathFile" name="imagePathFile" accept="image/jpeg, image/png">
                                                                        </span>
                                                                  </div>
                                                            </div>
                                                      </div>
                                                </div>
                                          </div>
                                    </div>
                                    <div class="form-group">
                                          <label for="accountFullName" class="col-md-12">ชื่อ-นามสกุล</label>
                                          <div class="col-md-12">
                                                <input type="text" class="form-control form-control-line" name="accountFullName" id="accountFullName">
                                          </div>
                                    </div>
                                    <div class="row">
                                          <div class="col-md-6">
                                                <div class="form-group">
                                                      <label for="accountName" class="col-md-12">ชื่อผู้ใช้</label>
                                                      <div class="col-md-12">
                                                            <input type="text" class="form-control form-control-line" name="accountName" id="accountName">
                                                      </div>
                                                </div>
                                          </div>
                                          <div class="col-md-6">
                                                <div class="form-group">
                                                      <label for="accountTel" class="col-md-12">เบอร์ติดต่อ</label>
                                                      <div class="col-md-12">
                                                            <input type="text" class="form-control form-control-line" name="accountTel" id="accountTel">
                                                      </div>
                                                </div>
                                          </div>
                                    </div>
                                    <div class="row">
                                          <div class="col-md-6">
                                                <div class="form-group">
                                                      <label for="accountPassword" class="col-md-12">รหัสผ่านใหม่</label>
                                                      <div class="col-md-12">
                                                            <input type="password" class="form-control form-control-line" name="accountPassword" id="accountPassword">
                                                      </div>
                                                </div>
                                          </div>
                                          <div class="col-md-6">
                                                <div class="form-group">
                                                      <label for="accountPasswordConfirm" class="col-md-12">ยืนยันรหัสผ่านใหม่</label>
                                                      <div class="col-md-12">
                                                            <input type="password" class="form-control form-control-line" name="accountPasswordConfirm" id="accountPasswordConfirm">
                                                      </div>
                                                </div>
                                          </div>
                                    </div>
                                    <div class="form-group">
                                          <div class="col-sm-12">
                                                <button class="btn btn-success" id="saveAccountBtn" data-save-status="update" data-save-id="0">บันทึก</button>
                                          </div>
                                    </div>
                              </form>
                        </div>
                  </div>
            </div>
      </div>
</div>