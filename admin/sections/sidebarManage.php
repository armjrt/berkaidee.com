<!-- Select2 -->
<link href="assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet">
<div class="col-12">
      <div class="card">
            <div class="card-body">
                  <h4 class="card-title">จัดการเมนูบาร์</h4>
                  <ul class="nav nav-tabs customtab2" role="tablist" style="margin-top: 25px;">
                        <li class="nav-item"><a class="nav-link active load-sidebar-btn" data-sidebar-type="categories" data-toggle="tab" href="#categoriesTab" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span><span class="hidden-xs-down">หมวดหมู่เบอร์</span></a></li>
                        <li class="nav-item"><a class="nav-link load-sidebar-btn" data-sidebar-type="price" data-toggle="tab" href="#priceTab" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span><span class="hidden-xs-down">เบอร์ตามช่วงราคา</span></a></li>
                        <li class="nav-item"><a class="nav-link load-sidebar-btn" data-sidebar-type="compound" data-toggle="tab" href="#compoundTab" role="tab"><span class="hidden-sm-up"><i class="ti-email"></i></span><span class="hidden-xs-down">เบอร์ตามผลรวม</span></a></li>
                  </ul>
                  <div class="tab-content">
                        <div class="tab-pane active" id="categoriesTab" role="tabpanel">
                              <div class="form-group text-right m-b-0" style="margin-top: 10px;">
                                    <button class="btn btn-success add-sidebar-btn" data-sidebar-type="categories" type="button"><span class="glyphicon glyphicon-pencil"></span>&nbsp;&nbsp;เพิ่มรายการ</button>
                                    <button class="btn btn-success hide view-sidebar-btn" data-sidebar-type="categories" type="button"><span class="glyphicon glyphicon-eye-open"></span>&nbsp;&nbsp;รายการหมวดหมู่เบอร์</button>
                              </div>
                              <div class="content-loader m-t-10">
                                    <div id="viewContent" class="table-responsive">
                                          <table id="sidebarCategoriesTable" class="table table-bordered table-striped table-hover">
                                                <thead>
                                                      <tr>
                                                            <th class="text-center">ชื่อรายการ</th>
                                                            <th class="text-center">ประเภทหมวดหมู่เบอร์</th>
                                                            <th></th>
                                                      </tr>
                                                </thead>
                                                <tbody></tbody>
                                          </table>
                                    </div>
                                    <div id="manageContent" class="hide">
                                          <form id="sidebarCategoriesForm" novalidate>
                                                <div class="form-group">
                                                      <label for="sidebarCategoriesName">ชื่อรายการ</label>
                                                      <input type="text" class="form-control" name="sidebarCategoriesName" id="sidebarCategoriesName">
                                                </div>
                                                <div class="form-group has-danger">
                                                      <label for="sidebarCategoriesType">ประเภทหมวดหมู่เบอร์</label>
                                                      <select class="form-control" name="sidebarCategoriesType" id="sidebarCategoriesType"></select>
                                                </div>
                                                <div class="form-group has-danger">
                                                      <label for="sidebarCategoriesPiority">ลำดับรายการที่แสดง</label>
                                                      <select class="form-control" name="sidebarCategoriesPiority" id="sidebarCategoriesPiority"></select>
                                                </div>
                                                <div class="box-footer">
                                                      <button type="submit" class="btn btn-success save-sidebar-btn" data-save-status="save" data-sidebar-type="categories" data-save-id="0">
                                                            <span class="glyphicon glyphicon-plus"></span> บันทึก
                                                      </button>
                                                      <button type="reset" class="btn btn-inverse reset-btn" data-sidebar-type="categories">
                                                            <span class="glyphicon glyphicon-plus"></span> ยกเลิก
                                                      </button>
                                                </div>
                                          </form>
                                    </div>
                              </div>
                        </div>
                        <div class="tab-pane" id="priceTab" role="tabpanel">
                              <div class="form-group text-right m-b-0" style="margin-top: 10px;">
                                    <button class="btn btn-success add-sidebar-btn" data-sidebar-type="price" type="button"><span class="glyphicon glyphicon-pencil"></span>&nbsp;&nbsp;เพิ่มรายการ</button>
                                    <button class="btn btn-success hide view-sidebar-btn" data-sidebar-type="price" type="button"><span class="glyphicon glyphicon-eye-open"></span>&nbsp;&nbsp;รายการเบอร์ตามช่วงราคา</button>
                              </div>
                              <div class="content-loader m-t-10">
                                    <div id="viewContent" class="table-responsive">
                                          <table id="sidebarPriceTable" class="table table-bordered table-striped table-hover">
                                                <thead>
                                                      <tr>
                                                            <th class="text-center">ชื่อรายการ</th>
                                                            <th class="text-center">ช่วงราคา</th>
                                                            <th></th>
                                                      </tr>
                                                </thead>
                                                <tbody></tbody>
                                          </table>
                                    </div>
                                    <div id="manageContent" class="hide">
                                          <form id="sidebarPriceForm" novalidate>
                                                <div class="form-group">
                                                      <label for="sidebarPriceName">ชื่อรายการ</label>
                                                      <input type="text" class="form-control" name="sidebarPriceName" id="sidebarPriceName">
                                                </div>
                                                <div class="row">
                                                      <div class="col-md-6">
                                                            <div class="form-group">
                                                                  <label for="sidebarPriceBegin">ช่วงราคาเริ่มต้น</label>
                                                                  <input type="text" class="form-control" name="sidebarPriceBegin" id="sidebarPriceBegin">
                                                            </div>
                                                      </div>
                                                      <div class="col-md-6">
                                                            <div class="form-group">
                                                                  <label for="sidebarPriceEnd">ช่วงราคาสิ้นสุด</label>
                                                                  <input type="text" class="form-control" name="sidebarPriceEnd" id="sidebarPriceEnd">
                                                            </div>
                                                      </div>
                                                </div>
                                                <div class="form-group has-danger">
                                                      <label for="sidebarPricePiority">ลำดับรายการที่แสดง</label>
                                                      <select class="form-control" name="sidebarPricePiority" id="sidebarPricePiority"></select>
                                                </div>
                                                <div class="box-footer">
                                                      <button type="submit" class="btn btn-success save-sidebar-btn" data-save-status="save" data-sidebar-type="price" data-save-id="0">
                                                            <span class="glyphicon glyphicon-plus"></span> บันทึก
                                                      </button>
                                                      <button type="reset" class="btn btn-inverse reset-btn" data-sidebar-type="price">
                                                            <span class="glyphicon glyphicon-plus"></span> ยกเลิก
                                                      </button>
                                                </div>
                                          </form>
                                    </div>
                              </div>
                        </div>
                        <div class="tab-pane" id="compoundTab" role="tabpanel">
                              <div class="form-group text-right m-b-0" style="margin-top: 10px;">
                                    <button class="btn btn-success add-sidebar-btn" data-sidebar-type="compound" type="button"><span class="glyphicon glyphicon-pencil"></span>&nbsp;&nbsp;เพิ่มรายการ</button>
                                    <button class="btn btn-success hide view-sidebar-btn" data-sidebar-type="compound" type="button"><span class="glyphicon glyphicon-eye-open"></span>&nbsp;&nbsp;รายการเบอร์ตามผลรวม</button>
                              </div>
                              <div class="content-loader m-t-10">
                                    <div id="viewContent" class="table-responsive">
                                          <table id="sidebarCompoundTable" class="table table-bordered table-striped table-hover">
                                                <thead>
                                                      <tr>
                                                            <th class="text-center">ชื่อรายการ</th>
                                                            <th class="text-center">เลขผลรวม</th>
                                                            <th></th>
                                                      </tr>
                                                </thead>
                                                <tbody></tbody>
                                          </table>
                                    </div>
                                    <div id="manageContent" class="hide">
                                          <form id="sidebarCompoundForm" novalidate>
                                                <div class="form-group">
                                                      <label for="sidebarCompoundName">ชื่อรายการ</label>
                                                      <input type="text" class="form-control" name="sidebarCompoundName" id="sidebarCompoundName">
                                                </div>
                                                <div class="form-group">
                                                      <label for="sidebarCompoundNum">เลขผลรวม</label>
                                                      <input type="text" class="form-control" name="sidebarCompoundNum" id="sidebarCompoundNum">
                                                </div>
                                                <div class="form-group has-danger">
                                                      <label for="sidebarCompoundPiority">ลำดับรายการที่แสดง</label>
                                                      <select class="form-control" name="sidebarCompoundPiority" id="sidebarCompoundPiority"></select>
                                                </div>
                                                <div class="box-footer">
                                                      <button type="submit" class="btn btn-success save-sidebar-btn" data-save-status="save" data-sidebar-type="compound" data-save-id="0">
                                                            <span class="glyphicon glyphicon-plus"></span> บันทึก
                                                      </button>
                                                      <button type="reset" class="btn btn-inverse reset-btn" data-sidebar-type="compound">
                                                            <span class="glyphicon glyphicon-plus"></span> ยกเลิก
                                                      </button>
                                                </div>
                                          </form>
                                    </div>
                              </div>
                        </div>
                  </div>
            </div>
      </div>
</div>
<!-- Data table -->
<script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
<!-- Select2 -->
<script src="assets/plugins/select2/dist/js/select2.min.js"></script>