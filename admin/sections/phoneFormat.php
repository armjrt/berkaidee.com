<!-- Select2 -->
<link href="assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet">
<div class="col-12">
      <div class="card">
            <div class="card-body">
                  <h4 class="card-title">ตั้งค่ารูปแบบเบอร์</h4>
                  <div class="content-loader m-t-10">
                        <div id="viewContent">
                              <form id="phoneFormatForm" class="form-horizontal form-material">
                                    <div class="form-group" style="margin-top: 40px;">
                                          <div class="form-group has-danger">
                                                <label for="phoneFormat">รูปแบบเบอร์</label>
                                                <select class="form-control" name="phoneFormat" id="phoneFormat">
                                                      <option value="" selected disabled>เลือกรูปแบบเบอร์</option>
                                                </select>
                                          </div>
                                    </div>
                                    <div class="form-group">
                                          <div class="col-sm-12">
                                                <button type="submit" class="btn btn-success" id="savePhoneFormatBtn" data-save-status="update" data-save-id="0">บันทึก</button>
                                          </div>
                                    </div>
                              </form>
                        </div>
                  </div>
            </div>
      </div>
</div>
<!-- Select2 -->
<script src="assets/plugins/select2/dist/js/select2.min.js"></script>