<div class="col-12">
      <div class="card">
            <div class="card-body">
                  <h4 class="card-title">สถานะการจัดส่ง</h4>
                  <div class="form-group text-right m-b-0" style="margin-top: -30px;">
                        <button class="btn btn-info" type="button" id="addEmsTrackingBtn"><span class="glyphicon glyphicon-pencil"></span>&nbsp;&nbsp;เพิ่มสถานะการจัดส่ง</button>
                        <button class="btn btn-info hide" type="button" id="viewEmsTrackingBtn"><span class="glyphicon glyphicon-eye-open"></span>&nbsp;&nbsp;รายการสถานะการจัดส่ง</button>
                  </div>
                  <div class="content-loader m-t-10">
                        <div id="viewContent" class="table-responsive">
                              <table id="emsTrackingTable" class="table table-bordered table-striped table-hover">
                                    <thead>
                                          <tr>
                                                <th class="text-center">ชื่อ</th>
                                                <th class="text-center">หมายเลข EMS</th>
                                                <th class="text-center">วันที่จัดส่ง</th>
                                                <th></th>
                                          </tr>
                                    </thead>
                                    <tbody></tbody>
                              </table>
                        </div>
                        <div id="manageContent" class="hide">
                              <form id="emsTrackingForm" novalidate>
                                    <div class="form-group">
                                          <label for="emsTrackingName">ชื่อ</label>
                                          <input type="text" class="form-control" name="emsTrackingName" id="emsTrackingName">
                                    </div>
                                    <div class="form-group">
                                          <label for="emsTrackingNumber">หมายเลข EMS</label>
                                          <input type="text" class="form-control" name="emsTrackingNumber" id="emsTrackingNumber">
                                    </div>
                                    <div class="form-group">
                                          <label for="emsTrackingDate">วัน/เดือน/ปี</label>
                                          <input type="text" class="form-control input-medium datepicker" data-provide="datepicker" data-date-language="th-th" name="emsTrackingDate" id="emsTrackingDate">
                                    </div>
                                    <div class="box-footer">
                                          <button type="submit" class="btn btn-success" id="saveEmsTrackingBtn" data-save-status="save" data-save-id="0">
                                                <span class="glyphicon glyphicon-plus"></span> บันทึก
                                          </button>
                                          <button type="reset" class="btn btn-inverse" id="resetBtn">
                                                <span class="glyphicon glyphicon-plus"></span> ยกเลิก
                                          </button>
                                    </div>
                              </form>
                        </div>
                  </div>
            </div>
      </div>
</div>
<!-- Data table -->
<script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/plugins/datatables/buttons/dataTables.buttons.min.js"></script>
<script src="assets/plugins/datatables/buttons/buttons.flash.min.js"></script>
<script src="assets/plugins/datatables/buttons/jszip.min.js"></script>
<script src="assets/plugins/datatables/buttons/pdfmake.min.js"></script>
<script src="assets/plugins/datatables/buttons/vfs_fonts.js"></script>
<script src="assets/plugins/datatables/buttons/buttons.html5.min.js"></script>
<script src="assets/plugins/datatables/buttons/buttons.print.min.js"></script>