<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon-32x32.png">
<title>Berkaidee</title>
<!-- Bootstrap Core CSS -->
<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="assets/plugins/html5-editor/bootstrap-wysihtml5.css" rel="stylesheet">
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="assets/plugins/datepicker/datepicker3.css">
<!-- Popup CSS -->
<link href="assets/plugins/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
<!-- Chartist CSS -->
<link href="assets/plugins/chartist-js/dist/chartist.min.css" rel="stylesheet">
<link href="assets/plugins/chartist-js/dist/chartist-init.css" rel="stylesheet">
<link href="assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="assets/css/style.css" rel="stylesheet">
<!-- You can change the theme colors from here -->
<link href="assets/css/colors/blue.css" id="theme" rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<!-- All Jquery -->
<script src="assets/plugins/jquery/jquery.min.js"></script>
<!-- Custom JavaScript -->
<script src="scripts/service.js"></script>
<!-- Check Auth -->
<script type="text/javascript">
      var service = new Service();

      service.checkAuth();
</script>