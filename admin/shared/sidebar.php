<div class="scroll-sidebar">
    <nav class="sidebar-nav">
        <ul id="sidebarnav">
            <li class="nav-small-cap">PERSONAL</li>
            <li><a class="has-arrow waves-effect waves-dark" href="dashboard" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">แดชบอร์ด</span></a></li>
            <li><a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-phone-log"></i><span class="hide-menu">จัดการเบอร์ขายดี</span></a>
                <ul aria-expanded="false" class="collapse">
                    <li><a href="products">จัดการเบอร์</a></li>
                    <li><a href="categories">เพิ่มหมวดหมู่เบอร์</a></li>
                </ul>
            </li>
            <li><a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-bullseye"></i><span class="hide-menu">คำทำนาย</span></a>
                <ul aria-expanded="false" class="collapse">
                    <li><a href="horo-degree">เลขลำดับ</a></li>
                    <li><a href="horo-compound">เลขรวม</a></li>
                </ul>
            </li>
            <li><a class="has-arrow waves-effect waves-dark" href="banner" aria-expanded="false"><i class="mdi mdi-camera-burst"></i><span class="hide-menu">แบนเนอร์</span></a></li>
            <li><a class="has-arrow waves-effect waves-dark" href="ems-tracking" aria-expanded="false"><i class="mdi mdi-near-me"></i><span class="hide-menu">EMS Tracking</span></a></li>
            <li><a class="has-arrow waves-effect waves-dark" href="article" aria-expanded="false"><i class="mdi mdi-format-float-right"></i><span class="hide-menu">บทความ</span></a></li>
            <li><a class="has-arrow waves-effect waves-dark" href="sidebar-manage" aria-expanded="false"><i class="mdi mdi-format-list-bulleted"></i><span class="hide-menu">จัดการเมนูบาร์</span></a></li>
            <li><a class="has-arrow waves-effect waves-dark" href="phone-format" aria-expanded="false"><i class="mdi mdi-phone-classic"></i><span class="hide-menu">ตั้งค่ารูปแบบเบอร์</span></a></li>
            <li><a class="has-arrow waves-effect waves-dark" href="phone-sold" aria-expanded="false"><i class="mdi mdi-information"></i><span class="hide-menu">เบอร์ที่ขายแล้ว</span></a></li>
        </ul>
    </nav>
</div>