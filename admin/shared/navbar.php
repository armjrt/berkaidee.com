<nav class="navbar top-navbar navbar-expand-md navbar-light">
    <div class="navbar-header">
        <a class="navbar-brand" href="dashboard">
            <b>
              <img src="assets/images/logo-solid.png" alt="homepage" class="dark-logo" style="width: 50px; padding-right: 5px;">
              <img src="assets/images/logo-light.png" alt="homepage" class="light-logo" style="width: 50px; padding-right: 5px;">
            </b>
            <span style="padding-left: 5px; font-weight: bold; color: #FFFFFF;">Berkaidee</span>
            <span style="color: #FFFFFF;">Admin</span>
         </a>
    </div>
    <div class="navbar-collapse">
        <ul class="navbar-nav mr-auto mt-md-0">
            <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle text-muted text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="mdi mdi-message"></i>
                    <div class="notify"> <span class="heartbit"></span> <span class="point"></span> </div>
                </a>
                <div class="dropdown-menu mailbox animated slideInUp">
                    <ul>
                        <li>
                            <div class="drop-title">แจ้งเตือน</div>
                        </li>
                        <li>
                            <div class="message-center">
                                <a href="#">
                                    <div class="btn btn-danger btn-circle"><i class="fa fa-heart"></i></div>
                                    <div class="mail-contnet">
                                        <h5>สวัสดีครับ</h5> <span class="mail-desc">ยินดีต้อนรับเข้าสู่ระบบ Berkaidee Admin</div>
                                </a>
                            </div>
                        </li>
                        <li>
                            <a class="nav-link text-center" href="#"><strong>ดูการแจ้งเตือนทั้งหมด</strong> <i class="fa fa-angle-right"></i></a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
        <ul class="navbar-nav my-lg-0">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="uploads/no-image-available.png" alt="user" class="profile-pic user-galary-path" style="width: 30px; height: 30px;"></a>
                <div class="dropdown-menu dropdown-menu-right scale-up">
                    <ul class="dropdown-user">
                        <li>
                            <div class="dw-user-box">
                                <div class="u-img"><img src="uploads/no-image-available.png" class="user-galary-path" alt="user" style="height: 70px;"></div>
                                <div class="u-text">
                                    <h4><span id="username"></span></h4>
                                    <p class="text-muted"><span id="email"></span></p><a href="account" class="btn btn-rounded btn-danger btn-sm">ข้อมูลส่วนตัว</a></div>
                            </div>
                        </li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#" id="logoutBtn"><i class="fa fa-power-off" style="padding-right: 10px;"></i>ออกจากระบบ</a></li>
                    </ul>
                </div>
            </li>
        </ul>
    </div>
</nav>