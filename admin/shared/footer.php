<!-- Bootstrap tether Core JavaScript -->
<script src="assets/plugins/bootstrap/js/popper.min.js"></script>
<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- Slimscrollbar scrollbar JavaScript -->
<script src="assets/js/jquery.slimscroll.js"></script>
<!-- Wave Effects -->
<script src="assets/js/waves.js"></script>
<!-- Menu sidebar -->
<script src="assets/js/sidebarmenu.js"></script>
<!-- Stickey kit -->
<script src="assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
<script src="assets/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- Validation -->
<script src="assets/js/validation.min.js"></script>
<!-- This page plugins -->
<script src="assets/plugins/skycons/skycons.js"></script>
<!-- Style switcher -->
<script src="assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>
<!-- Magnific popup JavaScript -->
<script src="assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup.min.js"></script>
<script src="assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup-init.js"></script>
<!-- Bootstrap datepicker -->
<script src="assets/plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="assets/plugins/datepicker/bootstrap-datepicker-thai.js"></script>
<script src="assets/plugins/datepicker/locales/bootstrap-datepicker.th.js"></script>
<!-- Sweet Alert -->
<script src="assets/plugins/sweetalert/sweetalert2.all.js"></script>
<!-- TinyMCE -->
<script src="assets/plugins/tinymce/tinymce.min.js"></script>
<!--Custom JavaScript -->
<script src="assets/js/custom.min.js"></script>
<script src="scripts/script.js"></script>