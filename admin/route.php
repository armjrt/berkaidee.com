<?php
    class Route {
        protected $uriName = [
            [
                'url'=>'/',
                'templateUrl'=>'sections/dashboard.php'
            ],
            [
                'url'=>'/dashboard',
                'templateUrl'=>'sections/dashboard.php'
            ],
            [
                'url'=>'/products',
                'templateUrl'=>'sections/products.php'
            ],
            [
                'url'=>'/categories',
                'templateUrl'=>'sections/categories.php'
            ],
            [
                'url'=>'/horo-degree',
                'templateUrl'=>'sections/horoDegree.php'
            ],
            [
                'url'=>'/horo-compound',
                'templateUrl'=>'sections/horoCompound.php'
            ],
            [
                'url'=>'/banner',
                'templateUrl'=>'sections/banner.php'
            ],
            [
                'url'=>'/ems-tracking',
                'templateUrl'=>'sections/emsTracking.php'
            ],
            [
                'url'=>'/article',
                'templateUrl'=>'sections/article.php'
            ],
            [
                'url'=>'/sidebar-manage',
                'templateUrl'=>'sections/sidebarManage.php'
            ],
            [
                'url'=>'/phone-format',
                'templateUrl'=>'sections/phoneFormat.php'
            ],
            [
                'url'=>'/phone-sold',
                'templateUrl'=>'sections/phoneSold.php'
            ],
            [
                'url'=>'/account',
                'templateUrl'=>'sections/account.php'
            ]
        ];

        public function __construct() {
            $this->getRoute();
        }

        public function getRoute() {
            $urlFilterStatus = false;
            $uriGET = isset($_GET['uri']) ? '/'.$_GET['uri'] : '/';

            foreach ($this->uriName as $uriKey => $uriVal) {
                if ($uriVal['url'] == $uriGET) {
                    $urlFilterStatus = true;
                    require_once($uriVal['templateUrl']);

                    break;
                }
            }

            if (!$urlFilterStatus) {
              require_once('sections/dashboard.php');
            }
        }
    }

    $objRoute = new Route();
?>
