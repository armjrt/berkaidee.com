<?php
	class ArticleAPI {
		function __construct() {
			require_once('dbAPI.php');

			$db = new DBAPI();
			$this->db = $db;
			$this->db->connectDb();

			if (isset($_POST['apiParams'])) {
				$data = json_decode($_POST['apiParams'], true);
			} else {
				$data = json_decode(file_get_contents('php://input'), true);
			}
			
			$fn = $data['fn'];
			$this->$fn($data);
		}

		function loadDropdownData($params) {
			$resData = [];

			foreach ($params['filterData'] as $filter) {
				switch ($filter['filter']) {
					case 'categories':
						$sqlCmd = "SELECT catid AS id, name AS text, catid, name
								FROM neo_product_category
								ORDER BY catid";
						$query = $this->db->getListObj($sqlCmd);
						$resData['categoriesList'] = $query;

						break;
				}
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}

		function loadArticleData($params) {
			$resData = [];

			$sqlCmd = "SELECT a.article_id, a.article_title, a.detail, a.category, a.image_cover, a.date, a.user_view, npc.name AS categoriesName
					FROM article a
					INNER JOIN neo_product_category npc ON a.category = npc.catid
					ORDER BY a.article_id";
			$query = $this->db->getListObj($sqlCmd);

			$resData = [
				'status' => true,
				'msgInfo' => 'loadArticleData() is finished',
				'data' => $query
			];

			if (isset($params['msgInfo'])) {
				$resData['msgInfo'] = $params['msgInfo'];
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}

		function saveArticle($params) {
			$resData = [];
			
			if ($_FILES) {
				$targetDir = '../uploads/article/';
				$targetFileType = substr($_FILES[$params['insertArticle']['data'][0]['image_cover']]['name'], strrpos($_FILES[$params['insertArticle']['data'][0]['image_cover']]['name'], '.') + 1);
				$targetFileName = date('Y-m-d').'_article_'.$params['insertArticle']['data'][0]['image_cover'].'.'.$targetFileType;
				$targetFile = $targetDir.basename($targetFileName);
				
				if (move_uploaded_file($_FILES[$params['insertArticle']['data'][0]['image_cover']]['tmp_name'], $targetFile)) {
					$query['status'] = true;
					$params['insertArticle']['data'][0]['image_cover'] = 'uploads/article/'.$targetFileName;

					$query = $this->db->insertData($params['insertArticle']['tableName'], $params['insertArticle']['data']);
				} else {
					$query['status'] = false;
				}
			} else {
				$params['insertArticle']['data'][0]['image_cover'] = '';

				$query = $this->db->insertData($params['insertArticle']['tableName'], $params['insertArticle']['data']);
			}

			if ($query['status']) {
				$params['msgInfo'] = 'บันทึกข้อมูลเรียบร้อย';
				$this->loadArticleData($params);

				return true;
			} else {
				$resData = [
					'status' => false,
					'msgInfo' => 'ไม่สามารถบันทึกข้อมูลได้',
					'data' => new stdClass
				];
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}

		function updateArticle($params) {
			$resData = [];

			if ($_FILES) {
				$sqlCmd = "SELECT article_id, image_cover
						FROM article
						WHERE article_id = '".$params['updateArticle']['data'][0]['article_id']."'
						ORDER BY article_id";
				$query = $this->db->getObj($sqlCmd);
				
				if ($query['article_id'] && $query['image_cover']) {
					$targetRemovePath = '../'.$query['image_cover'];

					if (file_exists($targetRemovePath)) {
						if (unlink($targetRemovePath)) {
							$query['status'] = true;
						} else {
							$query['status'] = false;
						}
					} else {
						$query['status'] = true;
					}
				}

				$targetDir = '../uploads/article/';
				$targetFileType = substr($_FILES[$params['updateArticle']['data'][0]['image_cover']]['name'], strrpos($_FILES[$params['updateArticle']['data'][0]['image_cover']]['name'], '.') + 1);
				$targetFileName = date('Y-m-d').'_article_'.$params['updateArticle']['data'][0]['image_cover'].'.'.$targetFileType;
				$targetFile = $targetDir.basename($targetFileName);
				
				if (move_uploaded_file($_FILES[$params['updateArticle']['data'][0]['image_cover']]['tmp_name'], $targetFile)) {
					$query['status'] = true;
					$params['updateArticle']['data'][0]['image_cover'] = 'uploads/article/'.$targetFileName;

					$query = $this->db->updateData($params['updateArticle']['tableName'], $params['updateArticle']['data'], $params['updateArticle']['primaryKey']);
				} else {
					$query['status'] = false;
				}
			} else {
				// $params['updateArticle']['data'][0]['image_cover'] = '';

				$query = $this->db->updateData($params['updateArticle']['tableName'], $params['updateArticle']['data'], $params['updateArticle']['primaryKey']);
			}

			if ($query['status']) {
				$params['msgInfo'] = 'บันทึกข้อมูลเรียบร้อย';
				$this->loadArticleData($params);

				return true;
			} else {
				$resData = [
					'status' => false,
					'msgInfo' => 'ไม่สามารถบันทึกข้อมูลได้',
					'data' => new stdClass
				];
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}

		function removeArticle($params) {
			$resData = [];

			$sqlCmd = "SELECT article_id, image_cover
					FROM article
					WHERE article_id = '".$params['removeArticle'][0]['value']."'
					ORDER BY article_id";
			$query = $this->db->getObj($sqlCmd);
			
			if ($query['article_id'] && $query['image_cover']) {
				$targetRemovePath = '../'.$query['image_cover'];

				if (file_exists($targetRemovePath)) {
					if (unlink($targetRemovePath)) {
						$query['status'] = true;
					} else {
						$query['status'] = false;
					}
				} else {
					$query['status'] = true;
				}
			} else {
				$query['status'] = true;
			}

			if ($query['status']) {
				$query = $this->db->deleteData($params['removeArticle']);
			}

			if ($query['status']) {
				$params['msgInfo'] = 'ลบข้อมูลเรียบร้อย';
				$this->loadArticleData($params);

				return true;
			} else {
				$resData = [
					'status' => false,
					'msgInfo' => 'ไม่สามารถลบข้อมูลได้',
					'data' => new stdClass
				];
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}

		function uploadArticleImage($params) {
			$resData = [];

			if ($_FILES) {
				$targetDir = '../uploads/article-content/';
				$targetFileType = substr($_FILES[$params['image']]['name'], strrpos($_FILES[$params['image']]['name'], '.') + 1);
				$targetFileName = date('Y-m-d').'_article-content_'.$params['image'].'.'.$targetFileType;
				$targetFile = $targetDir.basename($targetFileName);
				
				if (move_uploaded_file($_FILES[$params['image']]['tmp_name'], $targetFile)) {
					$query['status'] = true;
				} else {
					$query['status'] = false;
				}
			} else {
				$query['status'] = true;
			}

			if ($query['status']) {
				$resData = [
					'status' => true,
					'msgInfo' => 'บันทึกข้อมูลเรียบร้อย',
					'data' => 'uploads/article-content/'.$targetFileName
				];
			} else {
				$resData = [
					'status' => false,
					'msgInfo' => 'ไม่สามารถบันทึกข้อมูลได้',
					'data' => new stdClass
				];
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}
	}

	$self = new ArticleAPI();
?>