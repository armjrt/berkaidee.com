<?php
	class CategoriesAPI {
		function __construct() {
			require_once('dbAPI.php');

			$db = new DBAPI();
			$this->db = $db;
			$this->db->connectDb();

			if (isset($_POST['apiParams'])) {
				$data = json_decode($_POST['apiParams'], true);
			} else {
				$data = json_decode(file_get_contents('php://input'), true);
			}
			
			$fn = $data['fn'];
			$this->$fn($data);
		}

		function loadCategoriesData($params) {
			$resData = [];

			$sqlCmd = "SELECT catid, name
					FROM neo_product_category
					WHERE catid NOT IN ('1', '2', '3', '4', '5', '6')
					ORDER BY catid";
			$query = $this->db->getListObj($sqlCmd);

			$resData = [
				'status' => true,
				'msgInfo' => 'loadCategoriesData() is finished',
				'data' => $query
			];

			if (isset($params['msgInfo'])) {
				$resData['msgInfo'] = $params['msgInfo'];
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}

		function saveCategories($params) {
			$resData = [];

			$sqlCmd = "SELECT COUNT(catid) AS lastData
					FROM neo_product_category";
			$query = $this->db->getObj($sqlCmd);

			if ($query['lastData']) {
				$params['insertCategories']['data'][0]['counts'] = (((int)$query['lastData']) + 1);

				$query = $this->db->insertData($params['insertCategories']['tableName'], $params['insertCategories']['data']);
			}

			if ($query['status']) {
				$params['msgInfo'] = 'บันทึกข้อมูลเรียบร้อย';
				$this->loadCategoriesData($params);

				return true;
			} else {
				$resData = [
					'status' => false,
					'msgInfo' => 'ไม่สามารถบันทึกข้อมูลได้',
					'data' => new stdClass
				];
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}

		function updateCategories($params) {
			$resData = [];

			$query = $this->db->updateData($params['updateCategories']['tableName'], $params['updateCategories']['data'], $params['updateCategories']['primaryKey']);

			if ($query['status']) {
				$params['msgInfo'] = 'บันทึกข้อมูลเรียบร้อย';
				$this->loadCategoriesData($params);

				return true;
			} else {
				$resData = [
					'status' => false,
					'msgInfo' => 'ไม่สามารถบันทึกข้อมูลได้',
					'data' => new stdClass
				];
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}

		function removeCategories($params) {
			$resData = [];

			$query = $this->db->deleteData($params['removeCategories']);

			if ($query['status']) {
				$params['msgInfo'] = 'ลบข้อมูลเรียบร้อย';
				$this->loadCategoriesData($params);

				return true;
			} else {
				$resData = [
					'status' => false,
					'msgInfo' => 'ไม่สามารถลบข้อมูลได้',
					'data' => new stdClass
				];
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}
	}

	$self = new CategoriesAPI();
?>