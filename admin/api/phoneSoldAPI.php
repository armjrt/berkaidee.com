<?php
	class CategoriesAPI {
		function __construct() {
			require_once('dbAPI.php');

			$db = new DBAPI();
			$this->db = $db;
			$this->db->connectDb();

			if (isset($_POST['apiParams'])) {
				$data = json_decode($_POST['apiParams'], true);
			} else {
				$data = json_decode(file_get_contents('php://input'), true);
			}
			
			$fn = $data['fn'];
			$this->$fn($data);
		}

		function loadPhoneSoldData($params) {
			$resData = [];

			$sqlCmd = "SELECT np.id, np.number1, np.sum1, np.ori_number1, np.sold_date, npc.catid, npc.name
					FROM neo_product np
					INNER JOIN neo_product_category_group npcg ON np.id = npcg.np_id
					INNER JOIN neo_product_category npc ON npcg.npc_id = npc.catid
					WHERE sold = '1'
					ORDER BY np.sold_date DESC";
			$query = $this->db->getListObj($sqlCmd);

			$resData = [
				'status' => true,
				'msgInfo' => 'loadPhoneSoldData() is finished',
				'data' => $query
			];

			if (isset($params['msgInfo'])) {
				$resData['msgInfo'] = $params['msgInfo'];
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}
	}

	$self = new CategoriesAPI();
?>