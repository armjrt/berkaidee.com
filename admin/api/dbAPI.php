<?php
	class DBAPI {
		//--Connect database
		function connectDb() {
			global $mysqli;

			if ($_SERVER['SERVER_NAME'] == 'localhost') {
				$configDb = array(
					'host'=>'localhost',
					'username'=>'root',
					'password'=>'',
					'dbName'=>'berkaide_test',
					'charset'=>'utf8'
				);
			} else {
				$configDb = array(
					'host'=>'localhost',
					'username'=>'berkaide_test',
					'password'=>'0QgFlt8U',
					'dbName'=>'berkaide_test',
					'charset'=>'utf8'
				);
				
			}

			$mysqli = mysqli_connect($configDb['host'], $configDb['username'], $configDb['password'], $configDb['dbName']);
			$mysqli->set_charset($configDb['charset']);

			if (mysqli_connect_error()) {
				die('<b>Connect error</b>: '.mysqli_connect_errno().'<br><b>Parse error</b>: '.mysqli_connect_error());
				exit();
			} else {
				return true;
			}
		}
		
		//--Query sql command
		function getQuery($sqlCmd) {
			global $mysqli;

			if ($mysqli->multi_query($sqlCmd)) {
				return true;
			} else {
				return false;
			}
		}

		//--Get object item
		function getObj($sqlCmd) {
			global $mysqli;
			$query = $mysqli->query($sqlCmd) or die('<b>SQL error</b>: \''.$sqlCmd.'\'<br><b>Parse error</b>: '.$mysqli->error);
			$item = $query->fetch_assoc();

			return $item;
		}

		//--Get array item
		function getListObj($sqlCmd) {
			global $mysqli;
			$itemList = array();
			$query = $mysqli->query($sqlCmd) or die('<b>SQL error</b>: \''.$sqlCmd.'\'<br><b>Parse error</b>: '.$mysqli->error);

			while ($item = $query->fetch_assoc()) {
				array_push($itemList, $item);
			}

			return $itemList;
		}

		//--Insert data function
		function insertData($tableName, $dataArr) {
			global $mysqli;

			if (count($dataArr) > 0) {
				$resData = [
					'status' => false,
					'lastInsertIDData' => []
				];
				
				foreach ($dataArr as $data) {
					$status = false;
					$fields = "";
					$values = "";
					$fieldIndex = 1;
					$insertChildData = [];
	
					foreach ($data as $key => $val) {
						if (is_array($val)) {
							array_push($insertChildData, $val);

							continue;
						}

						if ($fieldIndex != 1) {
							$fields .= ", ";
							$values .= ", ";
						}

						$fields .= "$key";

						if ($val == 'now') {
							$values .= "NOW()";
						} else {
							if (($tableName == 'users') && ($key == 'user_password')) {
								$values .= "MD5('$val')";
							} else {
								$values .= "'$val'";
							}
						}
						
						$fieldIndex++;
					}
	
					$sqlCmd = "INSERT INTO $tableName($fields) VALUES($values)";
					$status = $mysqli->query($sqlCmd);

					if ($status) {
						$lastInsertID = $mysqli->insert_id;
						array_push($resData['lastInsertIDData'], $lastInsertID);

						//--Insert child table
						if (count($insertChildData) > 0) {
							foreach ($insertChildData as $insertChild) {
								$tableNameChild = $insertChild['tableName'];
								$dataChild = $insertChild['data'];

								foreach ($dataChild as $data) {
									$status = false;
									$fields = $insertChild['primaryKey'];
									$values = "'$lastInsertID'";

									foreach ($data as $key => $val) {
										$fields .= ", $key";
				
										if ($val == 'now') {
											$values .= ", NOW()";
										} else {
											if (($tableNameChild == 'users') && ($key == 'user_password')) {
												$values .= ", MD5('$val')";
											} else {
												$values .= ", '$val'";
											}
										}
									}
					
									$sqlCmd = "INSERT INTO $tableNameChild($fields) VALUES($values)";
									$status = $mysqli->query($sqlCmd);
								}
							}
						}
					}
				}

				if ($status) {
					$resData['status'] = true;
				} else {
					$resData['status'] = false;
				}
			} else {
				$resData['status'] = true;
			}

			return $resData;
		}

		//--Update data function
		function updateData($tableName, $dataArr, $primaryKey) {
			global $mysqli;

			if (count($dataArr) > 0) {
				$resData = [
					'status' => false,
					'lastInsertIDData' => []
				];

				foreach ($dataArr as $data) {
					$status = false;
					$update = "";
					$pkValue = 0;
					$fieldIndex = 1;
					$updateChildData = [];
					
					foreach ($data as $key => $val) {
						if (is_array($val)) {
							array_push($updateChildData, $val);

							continue;
						}

						if ($key == $primaryKey) {
							$pkValue = $val;

							continue;
						}
	
						if ($fieldIndex != 1){
							$update .= ", ";
						}

						if ($val == 'now') {
							$update .= "$key = NOW()";
						} else {
							if (($tableName == 'users') && ($key == 'user_password')) {
								$update .= "$key = MD5('$val')";
							} else {
								$update .= "$key = '$val'";
							}
						}

						$fieldIndex++;
					}
	
					$sqlCmd = "UPDATE $tableName SET $update WHERE $primaryKey = '$pkValue'";
					$status = $mysqli->query($sqlCmd);

					if ($status) {
						//--Update child table
						if (count($updateChildData) > 0) {
							foreach ($updateChildData as $updateChild) {
								$tableNameChild = $updateChild['tableName'];
								$pkChild = $updateChild['primaryKey'];
								$dataChild = $updateChild['data'];

								foreach ($dataChild as $data) {
									$status = false;
									$update = "";
									$pkValue = 0;
									$fieldIndex = 1;

									foreach ($data as $key => $val) {
										if ($key == $pkChild) {
											$pkValue = $val;
				
											continue;
										}
					
										if ($fieldIndex != 1) {
											$update .= ", ";
										}
				
										if ($val == 'now') {
											$update .= "$key = NOW()";
										} else {
											if (($tableName == 'users') && ($key == 'user_password')) {
												$update .= "$key = MD5('$val')";
											} else {
												$update .= "$key = '$val'";
											}
										}
				
										$fieldIndex++;
									}
					
									$sqlCmd = "UPDATE $tableNameChild SET $update WHERE $pkChild = '$pkValue'";
									$status = $mysqli->query($sqlCmd);
								}
							}
						}
					}
				}
	
				if ($status) {
					$resData['status'] = true;
				} else {
					$resData['status'] = false;
				}
			} else {
				$resData['status'] = true;
			}

			return $resData;
		}

		//--Delete data function
		function deleteData($dataArr) {
			global $mysqli;

			if (count($dataArr) > 0) {
				$resData = [
					'status' => false
				];

				foreach ($dataArr as $data) {
					$status = false;
					$sqlCmd = "DELETE FROM ".$data['tableName']." WHERE ".$data['field']." = '".$data['value']."'";
					$status = $mysqli->query($sqlCmd);
				}

				if ($status) {
					$resData['status'] = true;
				} else {
					$resData['status'] = false;
				}
			} else {
				$resData['status'] = true;
			}

			return $resData;
		}

		//--Create and get session
		function getSession() {
			$session = array();

			if (!isset($_SESSION)) {
				session_start();
			}

			if (isset($_SESSION['ID'])) {
				$session['ID'] = $_SESSION['ID'];
				$session['username'] = $_SESSION['username'];
				$session['name'] = $_SESSION['name'];
			} else {
				$session['ID'] = '';
				$session['username'] = '';
				$session['name'] = '';
			}

			return $session;
		}

		//--Destroy session
		function destroySession() {
			if (!isset($_SESSION)) {
				session_start();
			}

			if (isset($_SESSION['user_session'])) {
				unset($_SESSION['user_session']);
				unset($_SESSION['user_email']);
				unset($_SESSION['user_name']);
				unset($_SESSION['user_galary_path']);
			}
		}
	}
?>