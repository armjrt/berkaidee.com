<?php
	class EmsTrackingAPI {
		function __construct() {
			require_once('dbAPI.php');

			$db = new DBAPI();
			$this->db = $db;
			$this->db->connectDb();

			if (isset($_POST['apiParams'])) {
				$data = json_decode($_POST['apiParams'], true);
			} else {
				$data = json_decode(file_get_contents('php://input'), true);
			}
			
			$fn = $data['fn'];
			$this->$fn($data);
		}

		function loadEmsTrackingData($params) {
			$resData = [];

			$sqlCmd = "SELECT ems_tracking_id, ems_tracking_name, ems_tracking_number, ems_tracking_date
					FROM ems_tracking
					ORDER BY ems_tracking_id";
			$query = $this->db->getListObj($sqlCmd);

			$resData = [
				'status' => true,
				'msgInfo' => 'loadEmsTrackingData() is finished',
				'data' => $query
			];

			if (isset($params['msgInfo'])) {
				$resData['msgInfo'] = $params['msgInfo'];
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}

		function saveEmsTracking($params) {
			$resData = [];

			$query = $this->db->insertData($params['insertEmsTracking']['tableName'], $params['insertEmsTracking']['data']);

			if ($query['status']) {
				$params['msgInfo'] = 'บันทึกข้อมูลเรียบร้อย';
				$this->loadEmsTrackingData($params);

				return true;
			} else {
				$resData = [
					'status' => false,
					'msgInfo' => 'ไม่สามารถบันทึกข้อมูลได้',
					'data' => new stdClass
				];
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}

		function updateEmsTracking($params) {
			$resData = [];

			$query = $this->db->updateData($params['updateEmsTracking']['tableName'], $params['updateEmsTracking']['data'], $params['updateEmsTracking']['primaryKey']);

			if ($query['status']) {
				$params['msgInfo'] = 'บันทึกข้อมูลเรียบร้อย';
				$this->loadEmsTrackingData($params);

				return true;
			} else {
				$resData = [
					'status' => false,
					'msgInfo' => 'ไม่สามารถบันทึกข้อมูลได้',
					'data' => new stdClass
				];
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}

		function removeEmsTracking($params) {
			$resData = [];

			$query = $this->db->deleteData($params['removeEmsTracking']);

			if ($query['status']) {
				$params['msgInfo'] = 'ลบข้อมูลเรียบร้อย';
				$this->loadEmsTrackingData($params);

				return true;
			} else {
				$resData = [
					'status' => false,
					'msgInfo' => 'ไม่สามารถลบข้อมูลได้',
					'data' => new stdClass
				];
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}
	}

	$self = new EmsTrackingAPI();
?>