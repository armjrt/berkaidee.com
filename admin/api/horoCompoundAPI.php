<?php
	class HoroCompoundAPI {
		function __construct() {
			require_once('dbAPI.php');

			$db = new DBAPI();
			$this->db = $db;
			$this->db->connectDb();

			if (isset($_POST['apiParams'])) {
				$data = json_decode($_POST['apiParams'], true);
			} else {
				$data = json_decode(file_get_contents('php://input'), true);
			}
			
			$fn = $data['fn'];
			$this->$fn($data);
		}

		function loadDropdownData($params) {
			$resData = [];

			foreach ($params['filterData'] as $filter) {
				switch ($filter['filter']) {
					case 'type':
						$resData['typeList'] = [
							[ 'id' => 'ดิน', 'text' => 'ดิน' ],
							[ 'id' => 'น้ำ', 'text' => 'น้ำ' ],
							[ 'id' => 'ลม', 'text' => 'ลม' ],
							[ 'id' => 'ไฟ', 'text' => 'ไฟ' ]
						];

						break;
				}
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}

		function loadHoroCompoundData($params) {
			$resData = [];

			$sqlCmd = "SELECT Id, number, detail, percent, mean
					FROM forcast_sum
					ORDER BY Id";
			$query = $this->db->getListObj($sqlCmd);

			$resData = [
				'status' => true,
				'msgInfo' => 'loadHoroCompoundData() is finished',
				'data' => $query
			];

			if (isset($params['msgInfo'])) {
				$resData['msgInfo'] = $params['msgInfo'];
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}

		function saveHoroCompound($params) {
			$resData = [];

			$query = $this->db->insertData($params['insertHoroCompound']['tableName'], $params['insertHoroCompound']['data']);

			if ($query['status']) {
				$params['msgInfo'] = 'บันทึกข้อมูลเรียบร้อย';
				$this->loadHoroCompoundData($params);

				return true;
			} else {
				$resData = [
					'status' => false,
					'msgInfo' => 'ไม่สามารถบันทึกข้อมูลได้',
					'data' => new stdClass
				];
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}

		function updateHoroCompound($params) {
			$resData = [];

			$query = $this->db->updateData($params['updateHoroCompound']['tableName'], $params['updateHoroCompound']['data'], $params['updateHoroCompound']['primaryKey']);

			if ($query['status']) {
				$params['msgInfo'] = 'บันทึกข้อมูลเรียบร้อย';
				$this->loadHoroCompoundData($params);

				return true;
			} else {
				$resData = [
					'status' => false,
					'msgInfo' => 'ไม่สามารถบันทึกข้อมูลได้',
					'data' => new stdClass
				];
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}

		function removeHoroCompound($params) {
			$resData = [];

			$query = $this->db->deleteData($params['removeHoroCompound']);

			if ($query['status']) {
				$params['msgInfo'] = 'ลบข้อมูลเรียบร้อย';
				$this->loadHoroCompoundData($params);

				return true;
			} else {
				$resData = [
					'status' => false,
					'msgInfo' => 'ไม่สามารถลบข้อมูลได้',
					'data' => new stdClass
				];
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}
	}

	$self = new HoroCompoundAPI();
?>