$(document).ready(function () {
      //--Set variables
      var apiUrl = '';
      var apiParams = {};
      var appPath = '/' + window.location.pathname.split('/')[1];
      var page = window.location.pathname.split('/')[(window.location.pathname.split('/').length - 1)];
      var credentialData = JSON.parse(sessionStorage.getItem('credentialData'));
      var windowHeight = $(window).height();
      var tbodyContent = '';
      var dropdownList = {};
      var detectData = [];
      var detectDetail = {};
      var importProductsData = [];

      //--Set initial
      setInit();

      //--Event
      $(document).on('click', '#addProductsBtn', function () {
            $('#importProductsFile').val('');
            $('#productsForm').find('#productsNumber, #productsPrice, #productsDetail').val('');
            $('#productsForm').find('#productsPinAtHome, #productsSold').iCheck('uncheck');
            $('#productsForm').find('#productsCarrier, #productsCategories').val('').trigger('change');
            $('#productsForm').find('label.error').hide();
            $('#productsForm').find('input, textarea, select').removeClass('error');
            $('#productsForm #saveProductsBtn').attr({
                  'data-save-status': 'save',
                  'data-save-id': 0
            });
            $('.content-loader').fadeOut('slow', function () {
                  $('.content-loader').fadeIn('slow');
                  $('#addProductsBtn, #importProductsBtn, #viewContent').addClass('hide');
                  $('#viewProductsBtn, #manageContent').removeClass('hide');
            });
      });

      $(document).on('click', '#addHoroDegreeBtn', function () {
            $('#horoDegreeForm').find('#horoDegreeNumber, #horoDegreeDetail, #horoDegreePercent').val('');
            $('#horoDegreeForm').find('#horoDegreeType, #horoDegreeEducation, #horoDegreeWork, #horoDegreeFinance, #horoDegreeLove, #horoDegreeLucky').val('').trigger('change');
            $('#horoDegreeForm').find('label.error').hide();
            $('#horoDegreeForm').find('input, textarea, select').removeClass('error');
            $('#horoDegreeForm #saveHoroDegreeBtn').attr({
                  'data-save-status': 'save',
                  'data-save-id': 0
            });
            $('.content-loader').fadeOut('slow', function () {
                  $('.content-loader').fadeIn('slow');
                  $('#addHoroDegreeBtn, #viewContent').addClass('hide');
                  $('#viewHoroDegreeBtn, #manageContent').removeClass('hide');
            });
      });

      $(document).on('click', '#addHoroCompoundBtn', function () {
            $('#horoCompoundForm').find('#horoCompoundNumber, #horoCompoundDetail, #horoCompoundPercent').val('');
            $('#horoCompoundForm').find('#horoCompoundType').val('').trigger('change');
            $('#horoCompoundForm').find('label.error').hide();
            $('#horoCompoundForm').find('input, textarea, select').removeClass('error');
            $('#horoCompoundForm #saveHoroCompoundBtn').attr({
                  'data-save-status': 'save',
                  'data-save-id': 0
            });
            $('.content-loader').fadeOut('slow', function () {
                  $('.content-loader').fadeIn('slow');
                  $('#addHoroCompoundBtn, #viewContent').addClass('hide');
                  $('#viewHoroCompoundBtn, #manageContent').removeClass('hide');
            });
      });

      $(document).on('click', '#addBannerBtn', function () {
            $('#bannerForm').find('#bannerName, #bannerDetail, #imagePathFile').val('');
            $('#bannerForm .image-tumbnail a').attr('href', 'uploads/no-image-available.png');
            $('#bannerForm .image-tumbnail img').attr('src', 'uploads/no-image-available.png');
            $('#bannerForm').find('label.error').hide();
            $('#bannerForm').find('input, textarea, select').removeClass('error');
            $('#bannerForm #saveBannerBtn').attr({
                  'data-save-status': 'save',
                  'data-save-id': 0
            });
            $('.content-loader').fadeOut('slow', function () {
                  $('.content-loader').fadeIn('slow');
                  $('#addBannerBtn, #viewContent').addClass('hide');
                  $('#viewBannerBtn, #manageContent').removeClass('hide');
            });
      });

      $(document).on('click', '#addEmsTrackingBtn', function () {
            $('#emsTrackingForm').find('#emsTrackingName, #emsTrackingNumber, #emsTrackingDate').val('');
            $('#emsTrackingForm').find('label.error').hide();
            $('#emsTrackingForm').find('input, textarea, select').removeClass('error');
            $('#emsTrackingForm #saveEmsTrackingBtn').attr({
                  'data-save-status': 'save',
                  'data-save-id': 0
            });
            $('.content-loader').fadeOut('slow', function () {
                  $('.content-loader').fadeIn('slow');
                  $('#addEmsTrackingBtn, #viewContent').addClass('hide');
                  $('#viewEmsTrackingBtn, #manageContent').removeClass('hide');
            });
      });

      $(document).on('click', '#addArticleBtn', function () {
            $('#articleForm').find('#articleName, #imagePathFile').val('');
            $('#articleForm').find('#articleCategories').val('').trigger('change');
            $('#articleForm .image-tumbnail a').attr('href', 'uploads/no-image-available.png');
            $('#articleForm .image-tumbnail img').attr('src', 'uploads/no-image-available.png');
            tinymce.get('articleContent').setContent('');

            $('#articleForm').find('label.error').hide();
            $('#articleForm').find('input, textarea, select').removeClass('error');
            $('#articleForm #saveArticleBtn').attr({
                  'data-save-status': 'save',
                  'data-save-id': 0
            });
            $('.content-loader').fadeOut('slow', function () {
                  $('.content-loader').fadeIn('slow');
                  $('#addArticleBtn, #viewContent').addClass('hide');
                  $('#viewArticleBtn, #manageContent').removeClass('hide');
            });
      });

      $(document).on('click', '#addCategoriesBtn', function () {
            if (detectData.length >= 20) {
                  swal({
                        html: '<p>หมวดหมู่เสริมมีได้เพียง 5 หมวดหมู่เท่านั้น</p>',
                        type: 'warning',
                        showConfirmButton: true,
                        showCancelButton: false,
                        showCloseButton: true,
                        confirmButtonText: 'ตกลง',
                        cancelButtonText: 'ยกเลิก',
                        confirmButtonClass: 'btn btn-primary',
                        cancelButtonClass: 'btn btn-reverse',
                        buttonsStyling: false,
                        allowOutsideClick: true
                  });

                  return true;
            }

            $('#categoriesForm').find('#categoriesName').val('');
            $('#categoriesForm').find('label.error').hide();
            $('#categoriesForm').find('input, textarea, select').removeClass('error');
            $('#categoriesForm #saveCategoriesBtn').attr({
                  'data-save-status': 'save',
                  'data-save-id': 0
            });
            $('.content-loader').fadeOut('slow', function () {
                  $('.content-loader').fadeIn('slow');
                  $('#addCategoriesBtn, #viewContent').addClass('hide');
                  $('#viewCategoriesBtn, #manageContent').removeClass('hide');
            });
      });

      $(document).on('click', '.add-sidebar-btn', function () {
            switch ($(this).attr('data-sidebar-type')) {
                  case 'categories':
                        setSidebarManagePiority('add', 'categories', 'sidebar_categories_name', 0);

                        $('#sidebarCategoriesForm').find('#sidebarCategoriesName').val('');
                        $('#sidebarCategoriesForm').find('#sidebarCategoriesType').val('').trigger('change');
                        $('#sidebarCategoriesForm').find('#sidebarCategoriesPiority').val($('#sidebarCategoriesForm').find('#sidebarCategoriesPiority option:last-child()')[0].value).trigger('change');
                        $('#sidebarCategoriesForm').find('label.error').hide();
                        $('#sidebarCategoriesForm').find('input, textarea, select').removeClass('error');
                        $('#sidebarCategoriesForm .save-sidebar-btn').attr({
                              'data-save-status': 'save',
                              'data-save-id': 0
                        });

                        break;
                  case 'price':
                        setSidebarManagePiority('add', 'price', 'sidebar_price_name', 0);

                        $('#sidebarPriceForm').find('#sidebarPriceName, #sidebarPriceBegin, #sidebarPriceEnd').val('');
                        $('#sidebarPriceForm').find('#sidebarPricePiority').val($('#sidebarPriceForm').find('#sidebarPricePiority option:last-child()')[0].value).trigger('change');
                        $('#sidebarPriceForm').find('label.error').hide();
                        $('#sidebarPriceForm').find('input, textarea, select').removeClass('error');
                        $('#sidebarPriceForm .save-sidebar-btn').attr({
                              'data-save-status': 'save',
                              'data-save-id': 0
                        });

                        break;
                  case 'compound':
                        setSidebarManagePiority('add', 'compound', 'sidebar_compound_name', 0);

                        $('#sidebarCompoundForm').find('#sidebarCompoundName, #sidebarCompoundNum').val('');
                        $('#sidebarCompoundForm').find('#sidebarCompoundPiority').val($('#sidebarCompoundForm').find('#sidebarCompoundPiority option:last-child()')[0].value).trigger('change');
                        $('#sidebarCompoundForm').find('label.error').hide();
                        $('#sidebarCompoundForm').find('input, textarea, select').removeClass('error');
                        $('#sidebarCompoundForm .save-sidebar-btn').attr({
                              'data-save-status': 'save',
                              'data-save-id': 0
                        });

                        break;
            }

            $(this).closest('.tab-pane').find('.content-loader').fadeOut('slow', function () {
                  $(this).closest('.tab-pane').find('.content-loader').fadeIn('slow');
                  $(this).closest('.tab-pane').find('.add-sidebar-btn, #viewContent').addClass('hide');
                  $(this).closest('.tab-pane').find('.view-sidebar-btn, #manageContent').removeClass('hide');
            });
      });

      $(document).on('click', '#viewProductsBtn', function () {
            $('#productsForm #saveProductsBtn').attr({
                  'data-save-status': 'save',
                  'data-save-id': 0
            });
            $('.content-loader').fadeOut('slow', function () {
                  $('.content-loader').fadeIn('slow');
                  $('#viewProductsBtn, #manageContent').addClass('hide');
                  $('#addProductsBtn, #importProductsBtn, #viewContent').removeClass('hide');
            });
      });

      $(document).on('click', '#viewHoroDegreeBtn', function () {
            $('#horoDegreeForm #saveHoroDegreeBtn').attr({
                  'data-save-status': 'save',
                  'data-save-id': 0
            });
            $('.content-loader').fadeOut('slow', function () {
                  $('.content-loader').fadeIn('slow');
                  $('#viewHoroDegreeBtn, #manageContent').addClass('hide');
                  $('#addHoroDegreeBtn, #viewContent').removeClass('hide');
            });
      });

      $(document).on('click', '#viewHoroCompoundBtn', function () {
            $('#horoCompoundForm #saveHoroCompoundBtn').attr({
                  'data-save-status': 'save',
                  'data-save-id': 0
            });
            $('.content-loader').fadeOut('slow', function () {
                  $('.content-loader').fadeIn('slow');
                  $('#viewHoroCompoundBtn, #manageContent').addClass('hide');
                  $('#addHoroCompoundBtn, #viewContent').removeClass('hide');
            });
      });

      $(document).on('click', '#viewBannerBtn', function () {
            $('#bannerForm #saveBannerBtn').attr({
                  'data-save-status': 'save',
                  'data-save-id': 0
            });
            $('.content-loader').fadeOut('slow', function () {
                  $('.content-loader').fadeIn('slow');
                  $('#viewBannerBtn, #manageContent').addClass('hide');
                  $('#addBannerBtn, #viewContent').removeClass('hide');
            });
      });

      $(document).on('click', '#viewEmsTrackingBtn', function () {
            $('#emsTrackingForm #saveEmsTrackingBtn').attr({
                  'data-save-status': 'save',
                  'data-save-id': 0
            });
            $('.content-loader').fadeOut('slow', function () {
                  $('.content-loader').fadeIn('slow');
                  $('#viewEmsTrackingBtn, #manageContent').addClass('hide');
                  $('#addEmsTrackingBtn, #viewContent').removeClass('hide');
            });
      });

      $(document).on('click', '#viewArticleBtn', function () {
            $('#articleForm #saveArticleBtn').attr({
                  'data-save-status': 'save',
                  'data-save-id': 0
            });
            $('.content-loader').fadeOut('slow', function () {
                  $('.content-loader').fadeIn('slow');
                  $('#viewArticleBtn, #manageContent').addClass('hide');
                  $('#addArticleBtn, #viewContent').removeClass('hide');
            });
      });

      $(document).on('click', '#viewCategoriesBtn', function () {
            $('#categoriesForm #saveCategoriesBtn').attr({
                  'data-save-status': 'save',
                  'data-save-id': 0
            });
            $('.content-loader').fadeOut('slow', function () {
                  $('.content-loader').fadeIn('slow');
                  $('#viewCategoriesBtn, #manageContent').addClass('hide');
                  $('#addCategoriesBtn, #viewContent').removeClass('hide');
            });
      });

      $(document).on('click', '.view-sidebar-btn', function () {
            switch ($(this).attr('data-sidebar-type')) {
                  case 'categories':
                        $('#sidebarCategoriesForm .save-sidebar-btn').attr({
                              'data-save-status': 'save',
                              'data-save-id': 0
                        });

                        break;
                  case 'price':
                        $('#sidebarPriceForm .save-sidebar-btn').attr({
                              'data-save-status': 'save',
                              'data-save-id': 0
                        });

                        break;
                  case 'compound':
                        $('#sidebarCompoundForm .save-sidebar-btn').attr({
                              'data-save-status': 'save',
                              'data-save-id': 0
                        });

                        break;
            }

            $(this).closest('.tab-pane').find('.content-loader').fadeOut('slow', function () {
                  $(this).closest('.tab-pane').find('.content-loader').fadeIn('slow');
                  $(this).closest('.tab-pane').find('.view-sidebar-btn, #manageContent').addClass('hide');
                  $(this).closest('.tab-pane').find('.add-sidebar-btn, #viewContent').removeClass('hide');
            });
      });

      $(document).on('click', '#updateProductsBtn', function () {
            detectDetail = detectDetailFilter(detectData, 'id', $(this).attr('data-id'));
            console.log('detectDetail', detectDetail);

            $('#addProductsBtn').trigger('click');
            $('#productsForm #productsNumber').val(detectDetail['ori_number1']);
            $('#productsForm #productsPrice').val(detectDetail['price']);
            $('#productsForm #productsDetail').val(detectDetail['product_detail']);
            $('#productsForm #productsCarrier').val(detectDetail['pic1']).trigger('change');

            var categoriesData = [];

            $(detectDetail['categoriesData']).each(function (index, categories) {
                  categoriesData.push(categories['categories_id']);
            });

            $('#productsForm #productsCategories').val(categoriesData).trigger('change');

            $('#productsForm').find('#productsPinAtHome').iCheck(((detectDetail['news'] == 1) ? 'check' : 'uncheck'));
            $('#productsForm').find('#productsSold').iCheck(((detectDetail['sold'] == 1) ? 'check' : 'uncheck'));
            $('#productsForm #saveProductsBtn').attr({
                  'data-save-status': 'update',
                  'data-save-id': detectDetail['id']
            });
      });

      $(document).on('click', '#updateHoroDegreeBtn', function () {
            detectDetail = detectDetailFilter(detectData, 'Id', $(this).attr('data-id'));
            console.log('detectDetail', detectDetail);

            $('#addHoroDegreeBtn').trigger('click');
            $('#horoDegreeForm #horoDegreeNumber').val(detectDetail['number']);
            $('#horoDegreeForm #horoDegreeDetail').val(detectDetail['detail']);
            $('#horoDegreeForm #horoDegreePercent').val(detectDetail['percent']);
            $('#horoDegreeForm #horoDegreeType').val(detectDetail['color']).trigger('change');
            $('#horoDegreeForm #horoDegreeEducation').val(detectDetail['education']).trigger('change');
            $('#horoDegreeForm #horoDegreeWork').val(detectDetail['work']).trigger('change');
            $('#horoDegreeForm #horoDegreeFinance').val(detectDetail['finance']).trigger('change');
            $('#horoDegreeForm #horoDegreeLove').val(detectDetail['love']).trigger('change');
            $('#horoDegreeForm #horoDegreeLucky').val(detectDetail['lucky']).trigger('change');
            $('#horoDegreeForm #saveHoroDegreeBtn').attr({
                  'data-save-status': 'update',
                  'data-save-id': detectDetail['Id']
            });
      });

      $(document).on('click', '#updateHoroCompoundBtn', function () {
            detectDetail = detectDetailFilter(detectData, 'Id', $(this).attr('data-id'));
            console.log('detectDetail', detectDetail);

            $('#addHoroCompoundBtn').trigger('click');
            $('#horoCompoundForm #horoCompoundNumber').val(detectDetail['number']);
            $('#horoCompoundForm #horoCompoundDetail').val(detectDetail['detail']);
            $('#horoCompoundForm #horoCompoundPercent').val(detectDetail['percent']);
            $('#horoCompoundForm #horoCompoundType').val(detectDetail['mean']).trigger('change');
            $('#horoCompoundForm #saveHoroCompoundBtn').attr({
                  'data-save-status': 'update',
                  'data-save-id': detectDetail['Id']
            });
      });

      $(document).on('click', '#updateBannerBtn', function () {
            detectDetail = detectDetailFilter(detectData, 'banner_id', $(this).attr('data-id'));
            console.log('detectDetail', detectDetail);

            $('#addBannerBtn').trigger('click');
            $('#bannerForm #bannerName').val(detectDetail['banner_name']);
            $('#bannerForm #bannerDetail').val(detectDetail['banner_detail']);
            $('#bannerForm #saveBannerBtn').attr({
                  'data-save-status': 'update',
                  'data-save-id': detectDetail['banner_id']
            });
            $('#bannerForm .image-tumbnail a').attr('href', (detectDetail['banner_image_path'] ? detectDetail['banner_image_path'] : 'uploads/no-image-available.png'));
            $('#bannerForm .image-tumbnail img').attr('src', (detectDetail['banner_image_path'] ? detectDetail['banner_image_path'] : 'uploads/no-image-available.png'));
      });

      $(document).on('click', '#updateEmsTrackingBtn', function () {
            detectDetail = detectDetailFilter(detectData, 'ems_tracking_id', $(this).attr('data-id'));
            console.log('detectDetail', detectDetail);

            $('#addEmsTrackingBtn').trigger('click');
            $('#emsTrackingForm #emsTrackingName').val(detectDetail['ems_tracking_name']);
            $('#emsTrackingForm #emsTrackingNumber').val(detectDetail['ems_tracking_number']);
            $('#emsTrackingForm #emsTrackingDate').datepicker('update', convertDateToDisplay(detectDetail['ems_tracking_date']));
            $('#emsTrackingForm #saveEmsTrackingBtn').attr({
                  'data-save-status': 'update',
                  'data-save-id': $(this).attr('data-id')
            });
      });

      $(document).on('click', '#updateArticleBtn', function () {
            detectDetail = detectDetailFilter(detectData, 'article_id', $(this).attr('data-id'));
            console.log('detectDetail', detectDetail);

            $('#addArticleBtn').trigger('click');
            $('#articleForm #articleName').val(detectDetail['article_title']);
            $('#articleForm #articleCategories').val(detectDetail['category']).trigger('change');
            $('#articleForm #saveArticleBtn').attr({
                  'data-save-status': 'update',
                  'data-save-id': detectDetail['article_id']
            });
            $('#articleForm .image-tumbnail a').attr('href', (detectDetail['image_cover'] ? detectDetail['image_cover'] : 'uploads/no-image-available.png'));
            $('#articleForm .image-tumbnail img').attr('src', (detectDetail['image_cover'] ? detectDetail['image_cover'] : 'uploads/no-image-available.png'));
            tinymce.get('articleContent').execCommand('mceInsertContent', false, detectDetail['detail']);
      });

      $(document).on('click', '#updateCategoriesBtn', function () {
            detectDetail = detectDetailFilter(detectData, 'catid', $(this).attr('data-id'));
            console.log('detectDetail', detectDetail);

            $('#addCategoriesBtn').trigger('click');
            $('#categoriesForm #categoriesName').val(detectDetail['name']);
            $('#categoriesForm #saveCategoriesBtn').attr({
                  'data-save-status': 'update',
                  'data-save-id': $(this).attr('data-id')
            });
      });

      $(document).on('click', '#updateSidebarManageWithCategoriesBtn', function (e) {
            detectDetail = detectDetailFilter(detectData, 'sidebar_categories_id', $(this).attr('data-id'));
            console.log('detectDetail', detectDetail);

            $(this).closest('.tab-pane').find('.add-sidebar-btn').trigger('click');
            $('#sidebarCategoriesForm #sidebarCategoriesName').val(detectDetail['sidebar_categories_name']);

            setSidebarManagePiority('update', 'categories', 'sidebar_categories_name', $(this).attr('data-index'));
            $('#sidebarCategoriesForm #sidebarCategoriesPiority').val(detectDetail['sidebar_categories_piority']).trigger('change');

            var categoriesData = [];

            $(detectDetail['categoriesData']).each(function (index, categories) {
                  categoriesData.push(categories['categories_id']);
            });

            $('#sidebarCategoriesForm #sidebarCategoriesType').val(categoriesData).trigger('change');
            $('#sidebarCategoriesForm .save-sidebar-btn').attr({
                  'data-save-status': 'update',
                  'data-save-id': detectDetail['sidebar_categories_id']
            });
      });

      $(document).on('click', '#updateSidebarManageWithPriceBtn', function (e) {
            detectDetail = detectDetailFilter(detectData, 'sidebar_price_id', $(this).attr('data-id'));
            console.log('detectDetail', detectDetail);

            $(this).closest('.tab-pane').find('.add-sidebar-btn').trigger('click');
            $('#sidebarPriceForm #sidebarPriceName').val(detectDetail['sidebar_price_name']);
            $('#sidebarPriceForm #sidebarPriceBegin').val(detectDetail['sidebar_price_begin']);
            $('#sidebarPriceForm #sidebarPriceEnd').val(detectDetail['sidebar_price_end']);

            setSidebarManagePiority('update', 'price', 'sidebar_price_name', $(this).attr('data-index'));
            $('#sidebarPriceForm #sidebarPricePiority').val(detectDetail['sidebar_price_piority']).trigger('change');
            $('#sidebarPriceForm .save-sidebar-btn').attr({
                  'data-save-status': 'update',
                  'data-save-id': detectDetail['sidebar_price_id']
            });
      });

      $(document).on('click', '#updateSidebarManageWithCompoundBtn', function (e) {
            detectDetail = detectDetailFilter(detectData, 'sidebar_compound_id', $(this).attr('data-id'));
            console.log('detectDetail', detectDetail);

            $(this).closest('.tab-pane').find('.add-sidebar-btn').trigger('click');
            $('#sidebarCompoundForm #sidebarCompoundName').val(detectDetail['sidebar_compound_name']);
            $('#sidebarCompoundForm #sidebarCompoundNum').val(detectDetail['sidebar_compound_num']);

            setSidebarManagePiority('update', 'price', 'sidebar_compound_name', $(this).attr('data-index'));
            $('#sidebarCompoundForm #sidebarCompoundPiority').val(detectDetail['sidebar_compound_piority']).trigger('change');
            $('#sidebarCompoundForm .save-sidebar-btn').attr({
                  'data-save-status': 'update',
                  'data-save-id': detectDetail['sidebar_compound_id']
            });
      });

      $(document).on('click', '#removeProductsBtn', function () {
            var detectID = $(this).attr('data-id');

            swal({
                  html: '<p>คุณต้องการตั้งค่าเป็นเบอร์ที่ขายแล้วใช่หรือไม่ ?</p>',
                  type: 'question',
                  showConfirmButton: true,
                  showCancelButton: true,
                  showCloseButton: true,
                  confirmButtonText: 'ตกลง',
                  cancelButtonText: 'ยกเลิก',
                  confirmButtonClass: 'btn btn-primary',
                  cancelButtonClass: 'btn btn-reverse',
                  buttonsStyling: false,
                  allowOutsideClick: true
            }).then(function (res) {
                  if (!res.hasOwnProperty('dismiss')) {
                        apiUrl = 'api/productsAPI.php';
                        apiParams = {
                              fn: 'updateProducts',
                              updateProducts: {
                                    tableName: 'neo_product',
                                    primaryKey: 'id',
                                    data: [{
                                          id: detectID,
                                          sold: '1',
                                          sold_date: 'now'
                                    }]
                              }
                        };
                        // console.log(apiParams);

                        Service.prototype.callAPI(apiUrl, apiParams, 'post', true).done(function (res) {
                              var data = JSON.parse(res);
                              // console.log(data);

                              if (data.status) {
                                    swal({
                                          html: '<p>' + data.msgInfo + '</p>',
                                          type: 'success',
                                          showConfirmButton: true,
                                          showCancelButton: false,
                                          showCloseButton: true,
                                          confirmButtonText: 'ตกลง',
                                          cancelButtonText: 'ยกเลิก',
                                          confirmButtonClass: 'btn btn-primary',
                                          cancelButtonClass: 'btn btn-reverse',
                                          buttonsStyling: false,
                                          allowOutsideClick: true
                                    });

                                    $('#viewProductsBtn').trigger('click');

                                    setProductsData(data.data);
                              } else {
                                    swal({
                                          html: '<p>' + data.msgInfo + '</p>',
                                          type: 'error',
                                          showConfirmButton: true,
                                          showCancelButton: false,
                                          showCloseButton: true,
                                          confirmButtonText: 'ตกลง',
                                          cancelButtonText: 'ยกเลิก',
                                          confirmButtonClass: 'btn btn-primary',
                                          cancelButtonClass: 'btn btn-reverse',
                                          buttonsStyling: false,
                                          allowOutsideClick: true
                                    });
                              }
                        });
                  }
            });
      });

      $(document).on('click', '#removeHoroDegreeBtn', function () {
            var detectID = $(this).attr('data-id');

            swal({
                  html: '<p>คุณต้องการลบรายการนี้ใช่หรือไม่ ?</p>',
                  type: 'question',
                  showConfirmButton: true,
                  showCancelButton: true,
                  showCloseButton: true,
                  confirmButtonText: 'ตกลง',
                  cancelButtonText: 'ยกเลิก',
                  confirmButtonClass: 'btn btn-primary',
                  cancelButtonClass: 'btn btn-reverse',
                  buttonsStyling: false,
                  allowOutsideClick: true
            }).then(function (res) {
                  if (!res.hasOwnProperty('dismiss')) {
                        apiUrl = 'api/horoDegreeAPI.php';
                        apiParams = {
                              fn: 'removeHoroDegree',
                              removeHoroDegree: [{
                                    tableName: 'forcast',
                                    field: 'Id',
                                    value: detectID
                              }]
                        };
                        // console.log(apiParams);

                        Service.prototype.callAPI(apiUrl, apiParams, 'post', true).done(function (res) {
                              var data = JSON.parse(res);
                              // console.log(data);

                              if (data.status) {
                                    swal({
                                          html: '<p>' + data.msgInfo + '</p>',
                                          type: 'success',
                                          showConfirmButton: true,
                                          showCancelButton: false,
                                          showCloseButton: true,
                                          confirmButtonText: 'ตกลง',
                                          cancelButtonText: 'ยกเลิก',
                                          confirmButtonClass: 'btn btn-primary',
                                          cancelButtonClass: 'btn btn-reverse',
                                          buttonsStyling: false,
                                          allowOutsideClick: true
                                    });

                                    $('#viewHoroDegreeBtn').trigger('click');

                                    setHoroDegreeData(data.data);
                              } else {
                                    swal({
                                          html: '<p>' + data.msgInfo + '</p>',
                                          type: 'error',
                                          showConfirmButton: true,
                                          showCancelButton: false,
                                          showCloseButton: true,
                                          confirmButtonText: 'ตกลง',
                                          cancelButtonText: 'ยกเลิก',
                                          confirmButtonClass: 'btn btn-primary',
                                          cancelButtonClass: 'btn btn-reverse',
                                          buttonsStyling: false,
                                          allowOutsideClick: true
                                    });
                              }
                        });
                  }
            });
      });

      $(document).on('click', '#removeHoroCompoundBtn', function () {
            var detectID = $(this).attr('data-id');

            swal({
                  html: '<p>คุณต้องการลบรายการนี้ใช่หรือไม่ ?</p>',
                  type: 'question',
                  showConfirmButton: true,
                  showCancelButton: true,
                  showCloseButton: true,
                  confirmButtonText: 'ตกลง',
                  cancelButtonText: 'ยกเลิก',
                  confirmButtonClass: 'btn btn-primary',
                  cancelButtonClass: 'btn btn-reverse',
                  buttonsStyling: false,
                  allowOutsideClick: true
            }).then(function (res) {
                  if (!res.hasOwnProperty('dismiss')) {
                        apiUrl = 'api/horoCompoundAPI.php';
                        apiParams = {
                              fn: 'removeHoroCompound',
                              removeHoroCompound: [{
                                    tableName: 'forcast_sum',
                                    field: 'Id',
                                    value: detectID
                              }]
                        };
                        // console.log(apiParams);

                        Service.prototype.callAPI(apiUrl, apiParams, 'post', true).done(function (res) {
                              var data = JSON.parse(res);
                              // console.log(data);

                              if (data.status) {
                                    swal({
                                          html: '<p>' + data.msgInfo + '</p>',
                                          type: 'success',
                                          showConfirmButton: true,
                                          showCancelButton: false,
                                          showCloseButton: true,
                                          confirmButtonText: 'ตกลง',
                                          cancelButtonText: 'ยกเลิก',
                                          confirmButtonClass: 'btn btn-primary',
                                          cancelButtonClass: 'btn btn-reverse',
                                          buttonsStyling: false,
                                          allowOutsideClick: true
                                    });

                                    $('#viewHoroCompoundBtn').trigger('click');

                                    setHoroCompoundData(data.data);
                              } else {
                                    swal({
                                          html: '<p>' + data.msgInfo + '</p>',
                                          type: 'error',
                                          showConfirmButton: true,
                                          showCancelButton: false,
                                          showCloseButton: true,
                                          confirmButtonText: 'ตกลง',
                                          cancelButtonText: 'ยกเลิก',
                                          confirmButtonClass: 'btn btn-primary',
                                          cancelButtonClass: 'btn btn-reverse',
                                          buttonsStyling: false,
                                          allowOutsideClick: true
                                    });
                              }
                        });
                  }
            });
      });

      $(document).on('click', '#removeBannerBtn', function () {
            var detectID = $(this).attr('data-id');

            swal({
                  html: '<p>คุณต้องการลบรายการนี้ใช่หรือไม่ ?</p>',
                  type: 'question',
                  showConfirmButton: true,
                  showCancelButton: true,
                  showCloseButton: true,
                  confirmButtonText: 'ตกลง',
                  cancelButtonText: 'ยกเลิก',
                  confirmButtonClass: 'btn btn-primary',
                  cancelButtonClass: 'btn btn-reverse',
                  buttonsStyling: false,
                  allowOutsideClick: true
            }).then(function (res) {
                  if (!res.hasOwnProperty('dismiss')) {
                        apiUrl = 'api/bannerAPI.php';
                        apiParams = {
                              fn: 'removeBanner',
                              removeBanner: [{
                                    tableName: 'banner',
                                    field: 'banner_id',
                                    value: detectID
                              }]
                        };
                        // console.log(apiParams);

                        Service.prototype.callAPI(apiUrl, apiParams, 'post', true).done(function (res) {
                              var data = JSON.parse(res);
                              // console.log(data);

                              if (data.status) {
                                    swal({
                                          html: '<p>' + data.msgInfo + '</p>',
                                          type: 'success',
                                          showConfirmButton: true,
                                          showCancelButton: false,
                                          showCloseButton: true,
                                          confirmButtonText: 'ตกลง',
                                          cancelButtonText: 'ยกเลิก',
                                          confirmButtonClass: 'btn btn-primary',
                                          cancelButtonClass: 'btn btn-reverse',
                                          buttonsStyling: false,
                                          allowOutsideClick: true
                                    });

                                    $('#viewBannerBtn').trigger('click');

                                    setBannerData(data.data);
                              } else {
                                    swal({
                                          html: '<p>' + data.msgInfo + '</p>',
                                          type: 'error',
                                          showConfirmButton: true,
                                          showCancelButton: false,
                                          showCloseButton: true,
                                          confirmButtonText: 'ตกลง',
                                          cancelButtonText: 'ยกเลิก',
                                          confirmButtonClass: 'btn btn-primary',
                                          cancelButtonClass: 'btn btn-reverse',
                                          buttonsStyling: false,
                                          allowOutsideClick: true
                                    });
                              }
                        });
                  }
            });
      });

      $(document).on('click', '#removeEmsTrackingBtn', function () {
            var detectID = $(this).attr('data-id');

            swal({
                  html: '<p>คุณต้องการลบรายการนี้ใช่หรือไม่ ?</p>',
                  type: 'question',
                  showConfirmButton: true,
                  showCancelButton: true,
                  showCloseButton: true,
                  confirmButtonText: 'ตกลง',
                  cancelButtonText: 'ยกเลิก',
                  confirmButtonClass: 'btn btn-primary',
                  cancelButtonClass: 'btn btn-reverse',
                  buttonsStyling: false,
                  allowOutsideClick: true
            }).then(function (res) {
                  if (!res.hasOwnProperty('dismiss')) {
                        apiUrl = 'api/emsTrackingAPI.php';
                        apiParams = {
                              fn: 'removeEmsTracking',
                              removeEmsTracking: [{
                                    tableName: 'ems_tracking',
                                    field: 'ems_tracking_id',
                                    value: detectID
                              }]
                        };
                        // console.log(apiParams);

                        Service.prototype.callAPI(apiUrl, apiParams, 'post', true).done(function (res) {
                              var data = JSON.parse(res);
                              // console.log(data);

                              if (data.status) {
                                    swal({
                                          html: '<p>' + data.msgInfo + '</p>',
                                          type: 'success',
                                          showConfirmButton: true,
                                          showCancelButton: false,
                                          showCloseButton: true,
                                          confirmButtonText: 'ตกลง',
                                          cancelButtonText: 'ยกเลิก',
                                          confirmButtonClass: 'btn btn-primary',
                                          cancelButtonClass: 'btn btn-reverse',
                                          buttonsStyling: false,
                                          allowOutsideClick: true
                                    });

                                    $('#viewEmsTrackingBtn').trigger('click');

                                    setEmsTrackingData(data.data);
                              } else {
                                    swal({
                                          html: '<p>' + data.msgInfo + '</p>',
                                          type: 'error',
                                          showConfirmButton: true,
                                          showCancelButton: false,
                                          showCloseButton: true,
                                          confirmButtonText: 'ตกลง',
                                          cancelButtonText: 'ยกเลิก',
                                          confirmButtonClass: 'btn btn-primary',
                                          cancelButtonClass: 'btn btn-reverse',
                                          buttonsStyling: false,
                                          allowOutsideClick: true
                                    });
                              }
                        });
                  }
            });
      });

      $(document).on('click', '#removeArticleBtn', function () {
            var detectID = $(this).attr('data-id');

            swal({
                  html: '<p>คุณต้องการลบรายการนี้ใช่หรือไม่ ?</p>',
                  type: 'question',
                  showConfirmButton: true,
                  showCancelButton: true,
                  showCloseButton: true,
                  confirmButtonText: 'ตกลง',
                  cancelButtonText: 'ยกเลิก',
                  confirmButtonClass: 'btn btn-primary',
                  cancelButtonClass: 'btn btn-reverse',
                  buttonsStyling: false,
                  allowOutsideClick: true
            }).then(function (res) {
                  if (!res.hasOwnProperty('dismiss')) {
                        apiUrl = 'api/articleAPI.php';
                        apiParams = {
                              fn: 'removeArticle',
                              removeArticle: [{
                                    tableName: 'article',
                                    field: 'article_id',
                                    value: detectID
                              }]
                        };
                        // console.log(apiParams);

                        Service.prototype.callAPI(apiUrl, apiParams, 'post', true).done(function (res) {
                              var data = JSON.parse(res);
                              // console.log(data);

                              if (data.status) {
                                    swal({
                                          html: '<p>' + data.msgInfo + '</p>',
                                          type: 'success',
                                          showConfirmButton: true,
                                          showCancelButton: false,
                                          showCloseButton: true,
                                          confirmButtonText: 'ตกลง',
                                          cancelButtonText: 'ยกเลิก',
                                          confirmButtonClass: 'btn btn-primary',
                                          cancelButtonClass: 'btn btn-reverse',
                                          buttonsStyling: false,
                                          allowOutsideClick: true
                                    });

                                    $('#viewArticleBtn').trigger('click');

                                    setArticleData(data.data);
                              } else {
                                    swal({
                                          html: '<p>' + data.msgInfo + '</p>',
                                          type: 'error',
                                          showConfirmButton: true,
                                          showCancelButton: false,
                                          showCloseButton: true,
                                          confirmButtonText: 'ตกลง',
                                          cancelButtonText: 'ยกเลิก',
                                          confirmButtonClass: 'btn btn-primary',
                                          cancelButtonClass: 'btn btn-reverse',
                                          buttonsStyling: false,
                                          allowOutsideClick: true
                                    });
                              }
                        });
                  }
            });
      });

      $(document).on('click', '#removeCategoriesBtn', function () {
            var detectID = $(this).attr('data-id');

            swal({
                  html: '<p>คุณต้องการลบรายการนี้ใช่หรือไม่ ?</p>',
                  type: 'question',
                  showConfirmButton: true,
                  showCancelButton: true,
                  showCloseButton: true,
                  confirmButtonText: 'ตกลง',
                  cancelButtonText: 'ยกเลิก',
                  confirmButtonClass: 'btn btn-primary',
                  cancelButtonClass: 'btn btn-reverse',
                  buttonsStyling: false,
                  allowOutsideClick: true
            }).then(function (res) {
                  if (!res.hasOwnProperty('dismiss')) {
                        apiUrl = 'api/categoriesAPI.php';
                        apiParams = {
                              fn: 'removeCategories',
                              removeCategories: [{
                                    tableName: 'neo_product_category',
                                    field: 'catid',
                                    value: detectID
                              }]
                        };
                        // console.log(apiParams);

                        Service.prototype.callAPI(apiUrl, apiParams, 'post', true).done(function (res) {
                              var data = JSON.parse(res);
                              // console.log(data);

                              if (data.status) {
                                    swal({
                                          html: '<p>' + data.msgInfo + '</p>',
                                          type: 'success',
                                          showConfirmButton: true,
                                          showCancelButton: false,
                                          showCloseButton: true,
                                          confirmButtonText: 'ตกลง',
                                          cancelButtonText: 'ยกเลิก',
                                          confirmButtonClass: 'btn btn-primary',
                                          cancelButtonClass: 'btn btn-reverse',
                                          buttonsStyling: false,
                                          allowOutsideClick: true
                                    });

                                    $('#viewCategoriesBtn').trigger('click');

                                    setCategoriesData(data.data);
                              } else {
                                    swal({
                                          html: '<p>' + data.msgInfo + '</p>',
                                          type: 'error',
                                          showConfirmButton: true,
                                          showCancelButton: false,
                                          showCloseButton: true,
                                          confirmButtonText: 'ตกลง',
                                          cancelButtonText: 'ยกเลิก',
                                          confirmButtonClass: 'btn btn-primary',
                                          cancelButtonClass: 'btn btn-reverse',
                                          buttonsStyling: false,
                                          allowOutsideClick: true
                                    });
                              }
                        });
                  }
            });
      });

      $(document).on('click', '#removeSidebarManageWithCategoriesBtn', function () {
            var detectID = $(this).attr('data-id');

            swal({
                  html: '<p>คุณต้องการลบรายการนี้ใช่หรือไม่ ?</p>',
                  type: 'question',
                  showConfirmButton: true,
                  showCancelButton: true,
                  showCloseButton: true,
                  confirmButtonText: 'ตกลง',
                  cancelButtonText: 'ยกเลิก',
                  confirmButtonClass: 'btn btn-primary',
                  cancelButtonClass: 'btn btn-reverse',
                  buttonsStyling: false,
                  allowOutsideClick: true
            }).then(function (res) {
                  if (!res.hasOwnProperty('dismiss')) {
                        apiUrl = 'api/sidebarManageAPI.php';
                        apiParams = {
                              fn: 'removeSidebarManage',
                              type: 'categories',
                              removeSidebarCategorie: [{
                                          tableName: 'sidebar_categories',
                                          field: 'sidebar_categories_id',
                                          value: detectID
                                    },
                                    {
                                          tableName: 'sidebar_categories_group',
                                          field: 'sc_id',
                                          value: detectID
                                    }
                              ]
                        };
                        // console.log(apiParams);

                        Service.prototype.callAPI(apiUrl, apiParams, 'post', true).done(function (res) {
                              var data = JSON.parse(res);
                              // console.log(data);

                              if (data.status) {
                                    swal({
                                          html: '<p>' + data.msgInfo + '</p>',
                                          type: 'success',
                                          showConfirmButton: true,
                                          showCancelButton: false,
                                          showCloseButton: true,
                                          confirmButtonText: 'ตกลง',
                                          cancelButtonText: 'ยกเลิก',
                                          confirmButtonClass: 'btn btn-primary',
                                          cancelButtonClass: 'btn btn-reverse',
                                          buttonsStyling: false,
                                          allowOutsideClick: true
                                    });

                                    $('.view-sidebar-btn[data-sidebar-type="categories"]').trigger('click');

                                    setSidebarManageWithCategoriesData(data.data);
                              } else {
                                    swal({
                                          html: '<p>' + data.msgInfo + '</p>',
                                          type: 'error',
                                          showConfirmButton: true,
                                          showCancelButton: false,
                                          showCloseButton: true,
                                          confirmButtonText: 'ตกลง',
                                          cancelButtonText: 'ยกเลิก',
                                          confirmButtonClass: 'btn btn-primary',
                                          cancelButtonClass: 'btn btn-reverse',
                                          buttonsStyling: false,
                                          allowOutsideClick: true
                                    });
                              }
                        });
                  }
            });
      });

      $(document).on('click', '#removeSidebarManageWithPriceBtn', function () {
            var detectID = $(this).attr('data-id');

            swal({
                  html: '<p>คุณต้องการลบรายการนี้ใช่หรือไม่ ?</p>',
                  type: 'question',
                  showConfirmButton: true,
                  showCancelButton: true,
                  showCloseButton: true,
                  confirmButtonText: 'ตกลง',
                  cancelButtonText: 'ยกเลิก',
                  confirmButtonClass: 'btn btn-primary',
                  cancelButtonClass: 'btn btn-reverse',
                  buttonsStyling: false,
                  allowOutsideClick: true
            }).then(function (res) {
                  if (!res.hasOwnProperty('dismiss')) {
                        apiUrl = 'api/sidebarManageAPI.php';
                        apiParams = {
                              fn: 'removeSidebarManage',
                              type: 'price',
                              removeSidebarPrice: [{
                                    tableName: 'sidebar_price',
                                    field: 'sidebar_price_id',
                                    value: detectID
                              }]
                        };
                        // console.log(apiParams);

                        Service.prototype.callAPI(apiUrl, apiParams, 'post', true).done(function (res) {
                              var data = JSON.parse(res);
                              // console.log(data);

                              if (data.status) {
                                    swal({
                                          html: '<p>' + data.msgInfo + '</p>',
                                          type: 'success',
                                          showConfirmButton: true,
                                          showCancelButton: false,
                                          showCloseButton: true,
                                          confirmButtonText: 'ตกลง',
                                          cancelButtonText: 'ยกเลิก',
                                          confirmButtonClass: 'btn btn-primary',
                                          cancelButtonClass: 'btn btn-reverse',
                                          buttonsStyling: false,
                                          allowOutsideClick: true
                                    });

                                    $('.view-sidebar-btn[data-sidebar-type="price"]').trigger('click');

                                    setSidebarManageWithPriceData(data.data);
                              } else {
                                    swal({
                                          html: '<p>' + data.msgInfo + '</p>',
                                          type: 'error',
                                          showConfirmButton: true,
                                          showCancelButton: false,
                                          showCloseButton: true,
                                          confirmButtonText: 'ตกลง',
                                          cancelButtonText: 'ยกเลิก',
                                          confirmButtonClass: 'btn btn-primary',
                                          cancelButtonClass: 'btn btn-reverse',
                                          buttonsStyling: false,
                                          allowOutsideClick: true
                                    });
                              }
                        });
                  }
            });
      });

      $(document).on('click', '#removeSidebarManageWithCompoundBtn', function () {
            var detectID = $(this).attr('data-id');

            swal({
                  html: '<p>คุณต้องการลบรายการนี้ใช่หรือไม่ ?</p>',
                  type: 'question',
                  showConfirmButton: true,
                  showCancelButton: true,
                  showCloseButton: true,
                  confirmButtonText: 'ตกลง',
                  cancelButtonText: 'ยกเลิก',
                  confirmButtonClass: 'btn btn-primary',
                  cancelButtonClass: 'btn btn-reverse',
                  buttonsStyling: false,
                  allowOutsideClick: true
            }).then(function (res) {
                  if (!res.hasOwnProperty('dismiss')) {
                        apiUrl = 'api/sidebarManageAPI.php';
                        apiParams = {
                              fn: 'removeSidebarManage',
                              type: 'compound',
                              removeSidebarCompound: [{
                                    tableName: 'sidebar_compound',
                                    field: 'sidebar_compound_id',
                                    value: detectID
                              }]
                        };
                        // console.log(apiParams);

                        Service.prototype.callAPI(apiUrl, apiParams, 'post', true).done(function (res) {
                              var data = JSON.parse(res);
                              // console.log(data);

                              if (data.status) {
                                    swal({
                                          html: '<p>' + data.msgInfo + '</p>',
                                          type: 'success',
                                          showConfirmButton: true,
                                          showCancelButton: false,
                                          showCloseButton: true,
                                          confirmButtonText: 'ตกลง',
                                          cancelButtonText: 'ยกเลิก',
                                          confirmButtonClass: 'btn btn-primary',
                                          cancelButtonClass: 'btn btn-reverse',
                                          buttonsStyling: false,
                                          allowOutsideClick: true
                                    });

                                    $('.view-sidebar-btn[data-sidebar-type="compound"]').trigger('click');

                                    setSidebarManageWithCompoundData(data.data);
                              } else {
                                    swal({
                                          html: '<p>' + data.msgInfo + '</p>',
                                          type: 'error',
                                          showConfirmButton: true,
                                          showCancelButton: false,
                                          showCloseButton: true,
                                          confirmButtonText: 'ตกลง',
                                          cancelButtonText: 'ยกเลิก',
                                          confirmButtonClass: 'btn btn-primary',
                                          cancelButtonClass: 'btn btn-reverse',
                                          buttonsStyling: false,
                                          allowOutsideClick: true
                                    });
                              }
                        });
                  }
            });
      });

      $(document).on('click', '#restorePhoneBtn', function () {
            var detectID = $(this).attr('data-id');

            swal({
                  html: '<p>คุณต้องการใช้งานเบอร์นี้ใช่หรือไม่ ?</p>',
                  type: 'question',
                  showConfirmButton: true,
                  showCancelButton: true,
                  showCloseButton: true,
                  confirmButtonText: 'ตกลง',
                  cancelButtonText: 'ยกเลิก',
                  confirmButtonClass: 'btn btn-primary',
                  cancelButtonClass: 'btn btn-reverse',
                  buttonsStyling: false,
                  allowOutsideClick: true
            }).then(function (res) {
                  if (!res.hasOwnProperty('dismiss')) {
                        apiUrl = 'api/productsAPI.php';
                        apiParams = {
                              fn: 'updateProducts',
                              updateProducts: {
                                    tableName: 'neo_product',
                                    primaryKey: 'id',
                                    data: [{
                                          id: detectID,
                                          sold: '0',
                                          sold_date: '0000-00-00 00:00:00'
                                    }]
                              }
                        };
                        // console.log(apiParams);

                        Service.prototype.callAPI(apiUrl, apiParams, 'post', true).done(function (res) {
                              var data = JSON.parse(res);
                              // console.log(data);

                              if (data.status) {
                                    swal({
                                          html: '<p>' + data.msgInfo + '</p>',
                                          type: 'success',
                                          showConfirmButton: true,
                                          showCancelButton: false,
                                          showCloseButton: true,
                                          confirmButtonText: 'ตกลง',
                                          cancelButtonText: 'ยกเลิก',
                                          confirmButtonClass: 'btn btn-primary',
                                          cancelButtonClass: 'btn btn-reverse',
                                          buttonsStyling: false,
                                          allowOutsideClick: true
                                    });

                                    loadPhoneSoldData();
                              } else {
                                    swal({
                                          html: '<p>' + data.msgInfo + '</p>',
                                          type: 'error',
                                          showConfirmButton: true,
                                          showCancelButton: false,
                                          showCloseButton: true,
                                          confirmButtonText: 'ตกลง',
                                          cancelButtonText: 'ยกเลิก',
                                          confirmButtonClass: 'btn btn-primary',
                                          cancelButtonClass: 'btn btn-reverse',
                                          buttonsStyling: false,
                                          allowOutsideClick: true
                                    });
                              }
                        });
                  }
            });
      });

      $(document).on('click', '.load-sidebar-btn', function () {
            switch ($(this).attr('data-sidebar-type')) {
                  case 'categories':
                        loadSidebarManageData('categories');
                        break;
                  case 'price':
                        loadSidebarManageData('price');
                        break;
                  case 'compound':
                        loadSidebarManageData('compound');
                        break;
            }
      });

      $(document).on('change', 'input[name="imagePathFile"]', function (e) {
            var regExp = /^([ก-๙a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.png)$/;

            if (regExp.test(($(e.target)[0].files[0]['name']).toLowerCase())) {
                  var reader = new FileReader();
                  reader.readAsDataURL($(e.target)[0].files[0]);
                  reader.onload = function (evt) {
                        $('.image-tumbnail a').attr('href', evt.target['result']);
                        $('.image-tumbnail img').attr('src', evt.target['result']);
                  }
            }

            if ($(this).val()) {
                  $(this).next('label.error').hide();
            }
      });

      $(document).on('click', '#productsForm #resetBtn', function () {
            $('#productsForm').find('#productsCarrier, #productsCategories').val('').trigger('change');
      });

      $(document).on('click', '#horoDegreeForm #resetBtn', function () {
            $('#horoDegreeForm').find('#horoDegreeType, #horoDegreeEducation, #horoDegreeWork, #horoDegreeFinance, #horoDegreeLove, #horoDegreeLucky').val('').trigger('change');
      });

      $(document).on('click', '#horoCompoundForm #resetBtn', function () {
            $('#horoCompoundForm').find('#horoCompoundType').val('').trigger('change');
      });

      $(document).on('click', '#bannerForm #resetBtn', function () {
            $('#bannerForm .image-tumbnail a').attr('href', 'uploads/no-image-available.png');
            $('#bannerForm .image-tumbnail img').attr('src', 'uploads/no-image-available.png');
      });

      $(document).on('click', '#articleForm #resetBtn', function () {
            $('#articleForm #articleCategories').val('').trigger('change');
            $('#articleForm .image-tumbnail a').attr('href', 'uploads/no-image-available.png');
            $('#articleForm .image-tumbnail img').attr('src', 'uploads/no-image-available.png');
      });

      $(document).on('click', '#importProductsBtn', function () {
            $('#importProductsFile').trigger('click');
      });

      $(document).on('change', '#importProductsFile', function () {
            $(this).parse({
                  config: {
                        skipEmptyLines: true,
                        delimiter: ',',
                        encoding: 'TIS-620',
                        complete: function (csvFileData) {
                              var csvFile = csvFileData.data;
                              var products = {};

                              $(csvFile).each(function (index, data) {
                                    if (index == 0) {
                                          return true;
                                    }

                                    products = {};

                                    //--Detect field 'Phone Number'
                                    products['productsNumber'] = data[0].replace(/[-]/g, '');
                                    products['productsNumberStr'] = products['productsNumber'].substr(0, 3) + '-' + products['productsNumber'].substr(3, 3) + '-' + products['productsNumber'].substr(6);

                                    //--Detect field 'Price'
                                    products['productsPrice'] = data[1].replace(/[,]/g, '');

                                    //--Detect field 'Carrier'
                                    for (var i = 0; i < dropdownList['carrierList'].length; i++) {
                                          if (dropdownList['carrierList'][i]['carrier_name'].toLowerCase() == data[2].toLowerCase()) {
                                                products['productsCarrier'] = dropdownList['carrierList'][i]['carrier_id'];
                                                products['carrier_name'] = dropdownList['carrierList'][i]['carrier_name'];

                                                break;
                                          }
                                    }

                                    //--Detect field 'Categories'
                                    products['categoriesData'] = [];
                                    data[3] = data[3].toLowerCase().split(/[(,)|(, )]/g);

                                    for (var i = 0; i < dropdownList['categoriesList'].length; i++) {
                                          for (var j = 0; j < data[3].length; j++) {
                                                if (dropdownList['categoriesList'][i]['name'].toLowerCase() == data[3][j].trim()) {
                                                      products['categoriesData'].push({
                                                            categories_id: dropdownList['categoriesList'][i]['catid'],
                                                            categories_name: dropdownList['categoriesList'][i]['name']
                                                      });

                                                      break;
                                                }
                                          }
                                    }

                                    //--Detect field 'Pin at Home'
                                    products['productsPinAtHome'] = (data[4].toLowerCase() == 'y') ? '1' : '0';

                                    //--Detect field 'Sold'
                                    products['productsSold'] = (data[5].toLowerCase() == 'y') ? '1' : '0';

                                    //--Add products data
                                    importProductsData.push(products);
                              });

                              setImportProductsFile();
                        }
                  }
            });
      });

      $(document).on('click', '#removeImportProductsBtn', function (e) {
            var productsSelected = $(this).attr('data-id');

            swal({
                  html: '<p>คุณต้องการลบรายการนี้ใช่หรือไม่ ?</p>',
                  type: 'question',
                  showConfirmButton: true,
                  showCancelButton: true,
                  showCloseButton: true,
                  confirmButtonText: 'ตกลง',
                  cancelButtonText: 'ยกเลิก',
                  confirmButtonClass: 'btn btn-primary',
                  cancelButtonClass: 'btn btn-reverse',
                  buttonsStyling: false,
                  allowOutsideClick: true
            }).then(function (res) {
                  if (!res.hasOwnProperty('dismiss')) {
                        importProductsData.splice(productsSelected, 1);

                        if (importProductsData.length) {
                              setImportProductsFile();
                        } else {
                              $('#resetImportProductsBtn').trigger('click');
                        }
                  }
            });
      });

      $(document).on('click', '#resetImportProductsBtn', function () {
            $('#importProductsFile').val('');

            importProductsData = [];
      });

      $(document).on('click', '#saveImportProductsBtn', function () {
            saveProducts(true);
      });

      $(document).on('click', '#showResetPasswordBtn', function (e) {
            e.preventDefault();

            var popupContent = '<p>กรุณากรอกอีเมล์ของท่าน</p> \
                                    <div class="form-group"> \
                                          <div class="col-xs-12"> \
                                                <input class="form-control" type="text" id="emailReset" name="emailReset"> \
                                                <div class="text-left error">อีเมล์ห้ามว่าง</div> \
                                          </div> \
                                    </div>';


            swal({
                  title: 'กรุณากรอกอีเมล์ของท่าน',
                  input: 'email',
                  showConfirmButton: true,
                  showCancelButton: true,
                  showCloseButton: true,
                  confirmButtonText: 'ตกลง',
                  cancelButtonText: 'ยกเลิก',
                  confirmButtonClass: 'btn btn-primary',
                  cancelButtonClass: 'btn btn-reverse',
                  buttonsStyling: false,
                  allowOutsideClick: true,
                  showLoaderOnConfirm: true,
                  preConfirm: function (email) {
                        return new Promise(function (resolve, reject) {
                              apiUrl = 'api/userAPI.php';
                              apiParams = {
                                    fn: 'checkEmailForResetPassword',
                                    email: email
                              };

                              Service.prototype.callAPI(apiUrl, apiParams, 'post', true).done(function (res) {
                                    var data = JSON.parse(res);
                                    // console.log(data);

                                    if (data.status) {
                                          resolve();
                                    } else {
                                          swal.showValidationError(data.msgInfo);

                                          $('.swal2-confirm').removeAttr('disabled');
                                          $('.swal2-cancel').removeAttr('disabled');
                                          $('.swal2-buttonswrapper').removeClass('swal2-loading');
                                    }
                              });
                        })
                  },
                  allowOutsideClick: function () {
                        return !swal.isLoading();
                  }
            }).then(function (result) {
                  if (result.value) {
                        apiUrl = 'api/userAPI.php';
                        apiParams = {
                              fn: 'sendMailForResetPassword',
                              email: result.value
                        };

                        Service.prototype.callAPI(apiUrl, apiParams, 'post', true).done(function (res) {
                              console.log(res);
                              var data = JSON.parse(res);
                              console.log(data);

                              if (data.status) {
                                    swal({
                                          html: '<p>' + data.msgInfo + '</p>',
                                          type: 'success',
                                          showConfirmButton: true,
                                          showCancelButton: false,
                                          showCloseButton: true,
                                          confirmButtonText: 'ตกลง',
                                          cancelButtonText: 'ยกเลิก',
                                          confirmButtonClass: 'btn btn-primary',
                                          cancelButtonClass: 'btn btn-reverse',
                                          buttonsStyling: false,
                                          allowOutsideClick: true
                                    });
                              }
                        });
                  }
            });
      });

      $(document).on('click', '#logoutBtn', function (e) {
            e.preventDefault();

            apiUrl = 'api/userAPI.php';
            apiParams = {
                  fn: 'logout'
            };

            Service.prototype.callAPI(apiUrl, apiParams, 'post').done(function (res) {
                  var data = JSON.parse(res);
                  // console.log(data);

                  if (data.status) {
                        sessionStorage.removeItem('credentialData');

                        window.open('login.php', '_self');
                  }
            });
      });

      $(document).on('keypress', function (e) {
            if ((page == 'login.php') && ((e.which == 13) || (e.keyCode == 13))) {
                  login();
            }
      });

      //--Function
      function setInit() {
            if (credentialData) {
                  Service.prototype.detectBreadcrumb();

                  $('.navbar-nav #username').text(credentialData['user_name']);
                  $('.navbar-nav #email').text(credentialData['user_email']);
                  $('.navbar-nav .user-galary-path').attr('src', (credentialData['user_galary_path']) ? credentialData['user_galary_path'] : 'uploads/no-image-available.png');
            }

            //--Detect menu
            switch (page) {
                  case 'login.php':
                        $('#loginForm').validate({
                              rules: {
                                    email: {
                                          required: true,
                                          email: true
                                    },
                                    password: {
                                          required: true
                                    }
                              },
                              messages: {
                                    email: {
                                          email: 'อีเมล์ไม่ถูกต้อง',
                                          required: 'อีเมล์ห้ามว่าง'
                                    },
                                    password: {
                                          required: 'รหัสผ่านห้ามว่าง'
                                    }
                              },
                              submitHandler: login
                        });

                        $('#loginForm').find('#email, #password').val('');
                        $('#loginForm').find('#email').focus();

                        break;
                  case 'resetPassword.php':
                        $('.login-box').addClass('hide').find('#passwordData').text('');

                        if (!window.location.search) {
                              window.open('login.php', '_self');
                        } else {
                              var queryParams = (window.location.search.replace('?', '')).split('&');
                              var dateReset = (new Date().getFullYear() + '-' +
                                    (((new Date().getMonth() + 1).toString().length == 1) ? '0' + (new Date().getMonth() + 1) : (new Date().getMonth() + 1)) + '-' +
                                    (((new Date().getDate()).toString().length == 1) ? '0' + new Date().getDate() : new Date().getDate()) + ' ' +
                                    (((new Date().getHours()).toString().length == 1) ? '0' + new Date().getHours() : new Date().getHours()) + ':' +
                                    (((new Date().getMinutes()).toString().length == 1) ? '0' + new Date().getMinutes() : new Date().getMinutes()) + ':' +
                                    (((new Date().getSeconds()).toString().length == 1) ? '0' + new Date().getSeconds() : new Date().getSeconds()));
                              apiUrl = 'api/userAPI.php';
                              apiParams = {
                                    fn: 'checkResetRequest',
                                    userID: queryParams[0].split('id=')[1] || 0,
                                    resetReq: queryParams[1].split('reset_request=')[1] || '',
                                    dateReset: dateReset
                              };

                              Service.prototype.callAPI(apiUrl, apiParams, 'post', true).done(function (res) {
                                    var data = JSON.parse(res);
                                    // console.log(data);

                                    if (data.status) {
                                          $('.login-box').removeClass('hide').find('#passwordData').text(data.data);
                                    } else {
                                          $('.login-box').addClass('hide').find('#passwordData').text('');
                                          window.open('login.php', '_self');
                                    }
                              });
                        }

                        break;
                  case 'products':
                        $('#productsForm').validate({
                              rules: {
                                    productsNumber: {
                                          required: true,
                                          number: true,
                                          maxlength: 10,
                                          minlength: 10
                                    },
                                    productsPrice: {
                                          required: true,
                                          number: true
                                    },
                                    productsCarrier: {
                                          required: true
                                    },
                                    productsCategories: {
                                          required: true
                                    }
                              },
                              messages: {
                                    productsNumber: {
                                          required: 'เบอร์โทรห้ามว่าง',
                                          number: 'ต้องเป็นตัวเลขเท่านั้น',
                                          maxlength: 'เบอร์โทรต้องมี 10 หลัก',
                                          minlength: 'เบอร์โทรต้องมี 10 หลัก'
                                    },
                                    productsPrice: {
                                          required: 'ราคาห้ามว่าง',
                                          number: 'ต้องเป็นตัวเลขเท่านั้น'
                                    },
                                    productsCarrier: {
                                          required: 'เครือข่ายห้ามว่าง'
                                    },
                                    productsCategories: {
                                          required: 'หมวดหมู่ห้ามว่าง'
                                    }
                              },
                              submitHandler: saveProducts
                        });

                        loadProductsData();

                        loadDropdownData('api/productsAPI.php', [{
                                    filter: 'carrier',
                                    value: ''
                              },
                              {
                                    filter: 'categories',
                                    value: ''
                              }
                        ], function (data) {
                              $('#productsForm').find('#productsCarrier, #productsCategories').find('option:not(:first-child)').remove();
                              $('#productsForm #productsCarrier').select2({
                                          data: data['carrierList'],
                                          language: {
                                                noResults: function (params) {
                                                      return 'ไม่พบรายการ';
                                                }
                                          },
                                          tags: false
                                    })
                                    .on('select2:select', function (e) {
                                          $('#productsForm #productsCarrier').val(e.params.data['id']).trigger('change');
                                    });

                              $('#productsForm #productsCategories').select2({
                                          data: data['categoriesList'],
                                          language: {
                                                noResults: function (params) {
                                                      return 'ไม่พบรายการ';
                                                }
                                          },
                                          tags: true,
                                          multiple: true
                                    })
                                    .on('select2:select', function (e) {
                                          $('#productsForm #productsCategories').trigger('change');
                                    });
                        });

                        break;
                  case 'horo-degree':
                        $('#horoDegreeForm').validate({
                              rules: {
                                    horoDegreeNumber: {
                                          required: true
                                    },
                                    horoDegreeDetail: {
                                          required: true
                                    },
                                    horoDegreePercent: {
                                          required: true,
                                          number: true,
                                          max: 100
                                    },
                                    horoDegreeType: {
                                          required: true
                                    },
                                    horoDegreeEducation: {
                                          required: true
                                    },
                                    horoDegreeWork: {
                                          required: true
                                    },
                                    horoDegreeFinance: {
                                          required: true
                                    },
                                    horoDegreeLove: {
                                          required: true
                                    },
                                    horoDegreeLucky: {
                                          required: true
                                    }
                              },
                              messages: {
                                    horoDegreeNumber: {
                                          required: 'หมายเลขห้ามว่าง'
                                    },
                                    horoDegreeDetail: {
                                          required: 'รายละเอียดห้ามว่าง'
                                    },
                                    horoDegreePercent: {
                                          required: 'เปอร์เซ็นต์ห้ามว่าง',
                                          number: 'ต้องเป็นตัวเลขเท่านั้น',
                                          max: 'ต้องไม่เกิน 100'
                                    },
                                    horoDegreeType: {
                                          required: 'ประเภทห้ามว่าง'
                                    },
                                    horoDegreeEducation: {
                                          required: 'การเรียนห้ามว่าง'
                                    },
                                    horoDegreeWork: {
                                          required: 'การงานห้ามว่าง'
                                    },
                                    horoDegreeFinance: {
                                          required: 'การเงินห้ามว่าง'
                                    },
                                    horoDegreeLove: {
                                          required: 'ความรักห้ามว่าง'
                                    },
                                    horoDegreeLucky: {
                                          required: 'โชคลาภห้ามว่าง'
                                    }
                              },
                              submitHandler: saveHoroDegree
                        });

                        loadHoroDegreeData();

                        loadDropdownData('api/horoDegreeAPI.php', [{
                                    filter: 'type',
                                    value: ''
                              },
                              {
                                    filter: 'education',
                                    value: ''
                              },
                              {
                                    filter: 'work',
                                    value: ''
                              },
                              {
                                    filter: 'finance',
                                    value: ''
                              },
                              {
                                    filter: 'love',
                                    value: ''
                              },
                              {
                                    filter: 'lucky',
                                    value: ''
                              }
                        ], function (data) {
                              $('#horoDegreeForm').find('#horoDegreeType, #horoDegreeEducation, #horoDegreeWork, #horoDegreeFinance, #horoDegreeLove, #horoDegreeLucky').find('option:not(:first-child)').remove();
                              $('#horoDegreeForm #horoDegreeType').select2({
                                          data: data['typeList'],
                                          language: {
                                                noResults: function (params) {
                                                      return 'ไม่พบรายการ';
                                                }
                                          },
                                          tags: false
                                    })
                                    .on('select2:select', function (e) {
                                          $('#horoDegreeForm #horoDegreeType').val(e.params.data['id']).trigger('change');
                                    });

                              $('#horoDegreeForm #horoDegreeEducation').select2({
                                          data: data['educationList'],
                                          language: {
                                                noResults: function (params) {
                                                      return 'ไม่พบรายการ';
                                                }
                                          },
                                          tags: false
                                    })
                                    .on('select2:select', function (e) {
                                          $('#horoDegreeForm #horoDegreeEducation').val(e.params.data['id']).trigger('change');
                                    });

                              $('#horoDegreeForm #horoDegreeWork').select2({
                                          data: data['workList'],
                                          language: {
                                                noResults: function (params) {
                                                      return 'ไม่พบรายการ';
                                                }
                                          },
                                          tags: false
                                    })
                                    .on('select2:select', function (e) {
                                          $('#horoDegreeForm #horoDegreeWork').val(e.params.data['id']).trigger('change');
                                    });

                              $('#horoDegreeForm #horoDegreeFinance').select2({
                                          data: data['financeList'],
                                          language: {
                                                noResults: function (params) {
                                                      return 'ไม่พบรายการ';
                                                }
                                          },
                                          tags: false
                                    })
                                    .on('select2:select', function (e) {
                                          $('#horoDegreeForm #horoDegreeFinance').val(e.params.data['id']).trigger('change');
                                    });

                              $('#horoDegreeForm #horoDegreeLove').select2({
                                          data: data['loveList'],
                                          language: {
                                                noResults: function (params) {
                                                      return 'ไม่พบรายการ';
                                                }
                                          },
                                          tags: false
                                    })
                                    .on('select2:select', function (e) {
                                          $('#horoDegreeForm #horoDegreeLove').val(e.params.data['id']).trigger('change');
                                    });

                              $('#horoDegreeForm #horoDegreeLucky').select2({
                                          data: data['luckyList'],
                                          language: {
                                                noResults: function (params) {
                                                      return 'ไม่พบรายการ';
                                                }
                                          },
                                          tags: false
                                    })
                                    .on('select2:select', function (e) {
                                          $('#horoDegreeForm #horoDegreeLucky').val(e.params.data['id']).trigger('change');
                                    });
                        });

                        break;
                  case 'horo-compound':
                        $('#horoCompoundForm').validate({
                              rules: {
                                    horoCompoundNumber: {
                                          required: true
                                    },
                                    horoCompoundDetail: {
                                          required: true
                                    },
                                    horoCompoundPercent: {
                                          required: true,
                                          number: true,
                                          max: 100
                                    },
                                    horoCompoundType: {
                                          required: true
                                    }
                              },
                              messages: {
                                    horoCompoundNumber: {
                                          required: 'หมายเลขห้ามว่าง'
                                    },
                                    horoCompoundDetail: {
                                          required: 'รายละเอียดห้ามว่าง'
                                    },
                                    horoCompoundPercent: {
                                          required: 'เปอร์เซ็นต์ห้ามว่าง',
                                          number: 'ต้องเป็นตัวเลขเท่านั้น',
                                          max: 'ต้องไม่เกิน 100'
                                    },
                                    horoCompoundType: {
                                          required: 'ธาตุห้ามว่าง'
                                    }
                              },
                              submitHandler: saveHoroCompound
                        });

                        loadHoroCompoundData();

                        loadDropdownData('api/horoCompoundAPI.php', [{
                              filter: 'type',
                              value: ''
                        }], function (data) {
                              $('#horoCompoundForm').find('#horoCompoundType').find('option:not(:first-child)').remove();
                              $('#horoCompoundForm #horoCompoundType').select2({
                                          data: data['typeList'],
                                          language: {
                                                noResults: function (params) {
                                                      return 'ไม่พบรายการ';
                                                }
                                          },
                                          tags: false
                                    })
                                    .on('select2:select', function (e) {
                                          $('#horoCompoundForm #horoCompoundType').val(e.params.data['id']).trigger('change');
                                    });
                        });

                        break;
                  case 'banner':
                        $('#bannerForm').validate({
                              rules: {
                                    bannerName: {
                                          required: true
                                    }
                              },
                              messages: {
                                    bannerName: {
                                          required: 'หัวข้อห้ามว่าง'
                                    }
                              },
                              submitHandler: saveBanner
                        });

                        loadBannerData();

                        break;
                  case 'ems-tracking':
                        $('.datepicker').datepicker({
                                    format: "dd/mm/yyyy",
                              })
                              .change(function (e) {
                                    $(this).datepicker('hide');
                                    $(this).trigger('blur');
                              });

                        $('#emsTrackingForm').validate({
                              rules: {
                                    emsTrackingName: {
                                          required: true
                                    },
                                    emsTrackingNumber: {
                                          required: true
                                    },
                                    emsTrackingDate: {
                                          required: true
                                    }
                              },
                              messages: {
                                    emsTrackingName: {
                                          required: 'ชื่อห้ามว่าง'
                                    },
                                    emsTrackingNumber: {
                                          required: 'หมายเลข EMS ห้ามว่าง'
                                    },
                                    emsTrackingDate: {
                                          required: 'วัน/เดือน/ปี ห้ามว่าง'
                                    }
                              },
                              submitHandler: saveEmsTracking
                        });

                        loadEmsTrackingData();

                        break;
                  case 'categories':
                        $('#categoriesForm').validate({
                              rules: {
                                    categoriesName: {
                                          required: true
                                    }
                              },
                              messages: {
                                    categoriesName: {
                                          required: 'ชื่อหมวดหมู่ห้ามว่าง'
                                    }
                              },
                              submitHandler: saveCategories
                        });

                        loadCategoriesData();

                        break;
                  case 'article':
                        $('#articleForm').validate({
                              rules: {
                                    articleName: {
                                          required: true
                                    },
                                    articleCategories: {
                                          required: true
                                    }
                              },
                              messages: {
                                    articleName: {
                                          required: 'ชื่อบทความห้ามว่าง'
                                    },
                                    articleCategories: {
                                          required: 'หมวดหมู่ห้ามว่าง'
                                    }
                              },
                              submitHandler: saveArticle
                        });

                        loadArticleData();

                        loadDropdownData('api/articleAPI.php', [{
                              filter: 'categories',
                              value: ''
                        }], function (data) {
                              $('#articleForm #articleCategories').find('option:not(:first-child)').remove();
                              $('#articleForm #articleCategories').select2({
                                          data: data['categoriesList'],
                                          language: {
                                                noResults: function (params) {
                                                      return 'ไม่พบรายการ';
                                                }
                                          },
                                          tags: false
                                    })
                                    .on('select2:select', function (e) {
                                          $('#articleForm #articleCategories').val(e.params.data['id']).trigger('change');
                                    });
                        });

                        if ($('#articleContent').length > 0) {
                              tinymce.init({
                                    selector: 'textarea#articleContent',
                                    theme: 'modern',
                                    height: 300,
                                    plugins: [
                                          'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
                                          'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
                                          'save table contextmenu directionality emoticons template paste textcolor'
                                    ],
                                    toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons | removeformat',
                                    file_picker_callback: function (cb, value, meta) {
                                          var input = document.createElement('input');
                                          input.setAttribute('type', 'file');
                                          input.setAttribute('accept', 'image/*');
                                          input.onchange = function() {
                                                var file = this.files[0];
                                                var reader = new FileReader();

                                                reader.onload = function() {
                                                      var formData = new FormData();
                                                      var imagePathFileRef = Math.random().toString(36).substr(2);

                                                      apiUrl = 'api/articleAPI.php';
                                                      apiParams = {
                                                            fn: 'uploadArticleImage',
                                                            image: imagePathFileRef
                                                      };

                                                      formData.append(imagePathFileRef, file);
                                                      formData.append('apiParams', JSON.stringify(apiParams));

                                                      Service.prototype.callAPIWithFormData(apiUrl, formData, false).done(function (res) {
                                                            var data = JSON.parse(res);

                                                            if (data.status) {
                                                                  cb(data.data, { title: file.name });
                                                            }
                                                      });

                                                };
                                                reader.readAsDataURL(file);
                                          };
                                          input.click();
                                    }
                              });
                        }

                        break;
                  case 'sidebar-manage':
                        loadSidebarManageData('categories');

                        break;
                  case 'phone-format':
                        $('#phoneFormatForm').validate({
                              rules: {
                                    phoneFormat: {
                                          required: true
                                    }
                              },
                              messages: {
                                    phoneFormat: {
                                          required: 'รูปแบบเบอร์ห้ามว่าง'
                                    }
                              },
                              submitHandler: savePhoneFormat
                        });

                        loadPhoneFormatData();

                        break;
                  case 'phone-sold':
                        loadPhoneSoldData();

                        break;
                  case 'account':
                        $('#accountForm').validate({
                              rules: {
                                    accountName: {
                                          required: true
                                    }
                              },
                              messages: {
                                    accountName: {
                                          required: 'ชื่อผู้ใช้ห้ามว่าง'
                                    }
                              },
                              submitHandler: saveAccount
                        });

                        loadAccountData();

                        break;
                  default:
                        loadDashboardData();

                        break;
            }
      }

      function login() {
            apiUrl = 'api/userAPI.php';
            apiParams = {
                  fn: 'login',
                  email: $('#loginForm #email').val(),
                  password: $('#loginForm #password').val()
            };

            Service.prototype.callAPI(apiUrl, apiParams, 'post').done(function (res) {
                  var data = JSON.parse(res);
                  console.log(data);

                  if (data.status) {
                        sessionStorage.setItem('credentialData', JSON.stringify(data.data));

                        window.open(appPath + '/dashboard', '_self');
                  } else {
                        swal({
                              html: '<p>' + data.msgInfo + '</p>',
                              type: 'error',
                              showConfirmButton: true,
                              showCancelButton: false,
                              showCloseButton: true,
                              confirmButtonText: 'ตกลง',
                              cancelButtonText: 'ยกเลิก',
                              confirmButtonClass: 'btn btn-primary',
                              cancelButtonClass: 'btn btn-reverse',
                              buttonsStyling: false,
                              allowOutsideClick: true
                        });

                        $('#loginForm').find('#email, #password').val('');
                  }
            });
      }

      function loadDropdownData(apiUrl, filterData, cbFunc) {
            if (apiUrl && filterData.length) {
                  apiUrl = apiUrl;
                  apiParams = {
                        fn: 'loadDropdownData',
                        filterData: filterData
                  };

                  Service.prototype.callAPI(apiUrl, apiParams, 'post', true).done(function (res) {
                        var data = JSON.parse(res);
                        // console.log(data);
                        dropdownList = data;
                        cbFunc(data);
                  });
            } else {
                  cbFunc([]);
            }
      }

      function loadDashboardData() {
            apiUrl = 'api/dashboardAPI.php';
            apiParams = {
                  fn: 'loadDashboardData'
            };

            Service.prototype.callAPI(apiUrl, apiParams, 'post', true).done(function (res) {
                  var data = JSON.parse(res);
                  // console.log(data);

                  setDashboardData(data);
            });
      }

      function loadProductsData() {
            apiUrl = 'api/productsAPI.php';
            apiParams = {
                  fn: 'loadProductsData'
            };

            Service.prototype.callAPI(apiUrl, apiParams, 'post', true).done(function (res) {
                  var data = JSON.parse(res);
                  console.log(data);

                  if (data.status) {
                        detectData = data.data;
                        setProductsData(data.data);
                  }
            });
      }

      function loadHoroDegreeData() {
            apiUrl = 'api/horoDegreeAPI.php';
            apiParams = {
                  fn: 'loadHoroDegreeData'
            };

            Service.prototype.callAPI(apiUrl, apiParams, 'post', true).done(function (res) {
                  var data = JSON.parse(res);
                  console.log(data);

                  if (data.status) {
                        detectData = data.data;
                        setHoroDegreeData(data.data);
                  }
            });
      }

      function loadHoroCompoundData() {
            apiUrl = 'api/horoCompoundAPI.php';
            apiParams = {
                  fn: 'loadHoroCompoundData'
            };

            Service.prototype.callAPI(apiUrl, apiParams, 'post', true).done(function (res) {
                  var data = JSON.parse(res);
                  console.log(data);

                  if (data.status) {
                        detectData = data.data;
                        setHoroCompoundData(data.data);
                  }
            });
      }

      function loadBannerData() {
            apiUrl = 'api/bannerAPI.php';
            apiParams = {
                  fn: 'loadBannerData'
            };

            Service.prototype.callAPI(apiUrl, apiParams, 'post', true).done(function (res) {
                  var data = JSON.parse(res);
                  console.log(data);

                  if (data.status) {
                        detectData = data.data;
                        setBannerData(data.data);
                  }
            });
      }

      function loadEmsTrackingData() {
            apiUrl = 'api/emsTrackingAPI.php';
            apiParams = {
                  fn: 'loadEmsTrackingData'
            };

            Service.prototype.callAPI(apiUrl, apiParams, 'post', true).done(function (res) {
                  var data = JSON.parse(res);
                  console.log(data);

                  if (data.status) {
                        detectData = data.data;
                        setEmsTrackingData(data.data);
                  }
            });
      }

      function loadArticleData() {
            apiUrl = 'api/articleAPI.php';
            apiParams = {
                  fn: 'loadArticleData'
            };

            Service.prototype.callAPI(apiUrl, apiParams, 'post', true).done(function (res) {
                  var data = JSON.parse(res);
                  console.log(data);

                  if (data.status) {
                        detectData = data.data;
                        setArticleData(data.data);
                  }
            });
      }

      function loadCategoriesData() {
            apiUrl = 'api/categoriesAPI.php';
            apiParams = {
                  fn: 'loadCategoriesData'
            };

            Service.prototype.callAPI(apiUrl, apiParams, 'post', true).done(function (res) {
                  var data = JSON.parse(res);
                  console.log(data);

                  if (data.status) {
                        detectData = data.data;
                        setCategoriesData(data.data);
                  }
            });
      }

      function loadSidebarManageData(type) {
            apiUrl = 'api/sidebarManageAPI.php';
            apiParams = {
                  fn: 'loadSidebarManageData',
                  type: type
            };

            Service.prototype.callAPI(apiUrl, apiParams, 'post', true).done(function (res) {
                  var data = JSON.parse(res);
                  console.log(data);

                  if (data.status) {
                        detectData = data.data;

                        switch (type) {
                              case 'categories':
                                    setSidebarManageWithCategoriesData(data.data);
                                    break;
                              case 'price':
                                    setSidebarManageWithPriceData(data.data);
                                    break;
                              case 'compound':
                                    setSidebarManageWithCompoundData(data.data);
                                    break;
                        }
                  }
            });
      }

      function loadPhoneFormatData() {
            apiUrl = 'api/settingAPI.php';
            apiParams = {
                  fn: 'loadPhoneFormatData'
            };

            Service.prototype.callAPI(apiUrl, apiParams, 'post', true).done(function (res) {
                  var data = JSON.parse(res);
                  console.log(data);

                  if (data.status) {
                        detectData = data.data;
                        setPhoneFormatData(data.data);
                  }
            });
      }

      function loadPhoneSoldData() {
            apiUrl = 'api/phoneSoldAPI.php';
            apiParams = {
                  fn: 'loadPhoneSoldData'
            };

            Service.prototype.callAPI(apiUrl, apiParams, 'post', true).done(function (res) {
                  var data = JSON.parse(res);
                  console.log(data);

                  if (data.status) {
                        detectData = data.data;
                        setPhoneSoldData(data.data);
                  }
            });
      }

      function loadAccountData() {
            apiUrl = 'api/accountAPI.php';
            apiParams = {
                  fn: 'loadAccountData',
                  userID: credentialData['user_session']
            };

            Service.prototype.callAPI(apiUrl, apiParams, 'post', true).done(function (res) {
                  var data = JSON.parse(res);
                  console.log(data);

                  if (data.status) {
                        detectData = data.data;
                        setAccountData(data.data);
                  }
            });
      }

      function setDashboardData(dashboardData) {
            detectData = dashboardData;

            $('#productsNums').text(Number(detectData['productsNums'].toLocaleString()).toLocaleString('en', {
                  minimumFractionDigits: 0
            }));
            $('#soldPhoneNums').text(Number(detectData['soldPhoneNums'].toLocaleString()).toLocaleString('en', {
                  minimumFractionDigits: 0
            }));
            $('#emsTrackingNums').text(Number(detectData['emsTrackingNums'].toLocaleString()).toLocaleString('en', {
                  minimumFractionDigits: 0
            }));
      }

      function setProductsData(productsData) {
            detectData = productsData;

            var optionsDT = {
                  dom: 'Bfrtip',
                  paging: true,
                  lengthChange: false,
                  searching: true,
                  ordering: true,
                  info: true,
                  autoWidth: true,
                  destroy: true,
                  columnDefs: [{
                        orderable: false,
                        targets: [5]
                  }],
                  lengthMenu: [
                        [10, 20, 30, 50, -1],
                        [10, 20, 30, 50, 'ทั้งหมด']
                  ],
                  buttons: ['excel', 'print'],
                  order: [0, 'asc'],
                  language: {
                        loadingRecords: 'กำลังโหลดข้อมูล...',
                        processing: 'กำลังดำเนินการ...',
                        search: '_INPUT_',
                        searchPlaceholder: 'ค้นหา',
                        zeroRecords: 'ไม่พบข้อมูล',
                        lengthMenu: 'แสดง _MENU_ รายการ',
                        info: '_START_ ถึง _END_ จาก _TOTAL_ รายการ',
                        infoEmpty: '',
                        infoFiltered: '',
                        paginate: {
                              first: '<<',
                              last: '>>',
                              next: '>',
                              previous: '<'
                        }
                  }
            };

            $('#productsTable').DataTable().destroy();
            $('#productsTable tbody').empty();
            tbodyContent = '';

            if (productsData.length) {
                  var categoriesNameData = '';

                  $(productsData).each(function (index, products) {
                        categoriesNameData = '';

                        $(products['categoriesData']).each(function (index, categories) {
                              categoriesNameData += ((index == 0) ? categories['categories_name'] : ', ' + categories['categories_name']);
                        });

                        tbodyContent += '<tr> \
                                          <td>' + products['number1'] + ' <span style="color: red;">(' + products['sum1'] + ') <span class="hide">' + products['ori_number1'] + '</span></td> \
                                          <td>' + Number(products['price'].toLocaleString()).toLocaleString('en', {
                              minimumFractionDigits: 0
                        }) + '</td> \
                                          <td>' + products['carrier_name'] + '</td> \
                                          <td>' + categoriesNameData + '</td> \
                                          <td>' + (products['product_detail'] ? products['product_detail'] : '-') + '</td> \
                                          <td class="text-center" style="width: 100px;"> \
                                                <a href="#" class="btn btn-sm btn-success" id="updateProductsBtn" data-id="' + products['id'] + '" data-toggle="tooltip" data-placement="top" data-original-title="แก้ไข"> \
                                                      <i class="fa fa-pencil-square-o"></i></a> \
                                                <a href="#" class="btn btn-sm btn-danger" id="removeProductsBtn" data-id="' + products['id'] + '" data-toggle="tooltip" data-placement="top" data-original-title="ลบ"> \
                                                      <i class="fa fa-trash-o"></i></a> \
                                          </td> \
                                    </tr>';
                  });

                  $('#productsTable tbody').append(tbodyContent);
            }

            $('#productsTable').DataTable(optionsDT);
      }

      function setHoroDegreeData(horoDegreeData) {
            detectData = horoDegreeData;

            var optionsDT = {
                  dom: 'Bfrtip',
                  paging: true,
                  lengthChange: false,
                  searching: true,
                  ordering: true,
                  info: true,
                  autoWidth: true,
                  destroy: true,
                  columnDefs: [{
                        orderable: false,
                        targets: [2]
                  }],
                  lengthMenu: [
                        [10, 20, 30, 50, -1],
                        [10, 20, 30, 50, 'ทั้งหมด']
                  ],
                  buttons: [],
                  order: [0, 'asc'],
                  language: {
                        loadingRecords: 'กำลังโหลดข้อมูล...',
                        processing: 'กำลังดำเนินการ...',
                        search: '_INPUT_',
                        searchPlaceholder: 'ค้นหา',
                        zeroRecords: 'ไม่พบข้อมูล',
                        lengthMenu: 'แสดง _MENU_ รายการ',
                        info: '_START_ ถึง _END_ จาก _TOTAL_ รายการ',
                        infoEmpty: '',
                        infoFiltered: '',
                        paginate: {
                              first: '<<',
                              last: '>>',
                              next: '>',
                              previous: '<'
                        }
                  }
            };

            $('#horoDegreeTable').DataTable().destroy();
            $('#horoDegreeTable tbody').empty();
            tbodyContent = '';

            if (horoDegreeData.length) {
                  $(horoDegreeData).each(function (index, horoDegree) {
                        tbodyContent += '<tr> \
                                          <td class="text-center">' + horoDegree['number'] + '</td> \
                                          <td>' + horoDegree['detail'] + '</td> \
                                          <td class="text-center" style="width: 100px;"> \
                                                <a href="#" class="btn btn-sm btn-success" id="updateHoroDegreeBtn" data-id="' + horoDegree['Id'] + '" data-toggle="tooltip" data-placement="top" data-original-title="แก้ไข"> \
                                                      <i class="fa fa-pencil-square-o"></i></a> \
                                                <a href="#" class="btn btn-sm btn-danger" id="removeHoroDegreeBtn" data-id="' + horoDegree['Id'] + '" data-toggle="tooltip" data-placement="top" data-original-title="ลบ"> \
                                                      <i class="fa fa-trash-o"></i></a> \
                                          </td> \
                                    </tr>';
                  });

                  $('#horoDegreeTable tbody').append(tbodyContent);
            }

            $('#horoDegreeTable').DataTable(optionsDT);
      }

      function setHoroCompoundData(horoCompoundData) {
            detectData = horoCompoundData;

            var optionsDT = {
                  dom: 'Bfrtip',
                  paging: true,
                  lengthChange: false,
                  searching: true,
                  ordering: true,
                  info: true,
                  autoWidth: true,
                  destroy: true,
                  columnDefs: [{
                        orderable: false,
                        targets: [2]
                  }],
                  lengthMenu: [
                        [10, 20, 30, 50, -1],
                        [10, 20, 30, 50, 'ทั้งหมด']
                  ],
                  buttons: [],
                  order: [0, 'asc'],
                  language: {
                        loadingRecords: 'กำลังโหลดข้อมูล...',
                        processing: 'กำลังดำเนินการ...',
                        search: '_INPUT_',
                        searchPlaceholder: 'ค้นหา',
                        zeroRecords: 'ไม่พบข้อมูล',
                        lengthMenu: 'แสดง _MENU_ รายการ',
                        info: '_START_ ถึง _END_ จาก _TOTAL_ รายการ',
                        infoEmpty: '',
                        infoFiltered: '',
                        paginate: {
                              first: '<<',
                              last: '>>',
                              next: '>',
                              previous: '<'
                        }
                  }
            };

            $('#horoCompoundTable').DataTable().destroy();
            $('#horoCompoundTable tbody').empty();
            tbodyContent = '';

            if (horoCompoundData.length) {
                  $(horoCompoundData).each(function (index, horoCompound) {
                        tbodyContent += '<tr> \
                                          <td class="text-center">' + horoCompound['number'] + '</td> \
                                          <td>' + horoCompound['detail'] + '</td> \
                                          <td class="text-center">' + horoCompound['mean'] + '</td> \
                                          <td class="text-center" style="width: 100px;"> \
                                                <a href="#" class="btn btn-sm btn-success" id="updateHoroCompoundBtn" data-id="' + horoCompound['Id'] + '" data-toggle="tooltip" data-placement="top" data-original-title="แก้ไข"> \
                                                      <i class="fa fa-pencil-square-o"></i></a> \
                                                <a href="#" class="btn btn-sm btn-danger" id="removeHoroCompoundBtn" data-id="' + horoCompound['Id'] + '" data-toggle="tooltip" data-placement="top" data-original-title="ลบ"> \
                                                      <i class="fa fa-trash-o"></i></a> \
                                          </td> \
                                    </tr>';
                  });

                  $('#horoCompoundTable tbody').append(tbodyContent);
            }

            $('#horoCompoundTable').DataTable(optionsDT);
      }

      function setBannerData(bannerData) {
            detectData = bannerData;

            var optionsDT = {
                  dom: 'Bfrtip',
                  paging: true,
                  lengthChange: false,
                  searching: true,
                  ordering: true,
                  info: true,
                  autoWidth: true,
                  destroy: true,
                  columnDefs: [{
                        orderable: false,
                        targets: [2, 3]
                  }],
                  lengthMenu: [
                        [10, 20, 30, 50, -1],
                        [10, 20, 30, 50, 'ทั้งหมด']
                  ],
                  buttons: [],
                  order: [0, 'asc'],
                  language: {
                        loadingRecords: 'กำลังโหลดข้อมูล...',
                        processing: 'กำลังดำเนินการ...',
                        search: '_INPUT_',
                        searchPlaceholder: 'ค้นหา',
                        zeroRecords: 'ไม่พบข้อมูล',
                        lengthMenu: 'แสดง _MENU_ รายการ',
                        info: '_START_ ถึง _END_ จาก _TOTAL_ รายการ',
                        infoEmpty: '',
                        infoFiltered: '',
                        paginate: {
                              first: '<<',
                              last: '>>',
                              next: '>',
                              previous: '<'
                        }
                  }
            };

            $('#bannerTable').DataTable().destroy();
            $('#bannerTable tbody').empty();
            tbodyContent = '';

            if (bannerData.length) {
                  $(bannerData).each(function (index, banner) {
                        tbodyContent += '<tr> \
                                          <td>' + banner['banner_name'] + '</td> \
                                          <td>' + banner['banner_detail'] + '</td> \
                                          <td class="text-center"><img src="' + (banner['banner_image_path'] ? banner['banner_image_path'] : 'uploads/no-image-available.png') + '" style="width: 100px; height: 80px;"></td> \
                                          <td class="text-center" style="width: 100px;"> \
                                                <a href="#" class="btn btn-sm btn-success" id="updateBannerBtn" data-id="' + banner['banner_id'] + '" data-toggle="tooltip" data-placement="top" data-original-title="แก้ไข"> \
                                                      <i class="fa fa-pencil-square-o"></i></a> \
                                                <a href="#" class="btn btn-sm btn-danger" id="removeBannerBtn" data-id="' + banner['banner_id'] + '" data-toggle="tooltip" data-placement="top" data-original-title="ลบ"> \
                                                      <i class="fa fa-trash-o"></i></a> \
                                          </td> \
                                    </tr>';
                  });

                  $('#bannerTable tbody').append(tbodyContent);
            }

            $('#bannerTable').DataTable(optionsDT);
      }

      function setEmsTrackingData(emsTrackingData) {
            detectData = emsTrackingData;

            var optionsDT = {
                  dom: 'Bfrtip',
                  paging: true,
                  lengthChange: false,
                  searching: true,
                  ordering: true,
                  info: true,
                  autoWidth: true,
                  destroy: true,
                  columnDefs: [{
                        orderable: false,
                        targets: [3]
                  }],
                  lengthMenu: [
                        [10, 20, 30, 50, -1],
                        [10, 20, 30, 50, 'ทั้งหมด']
                  ],
                  buttons: ['excel', 'print'],
                  order: [0, 'asc'],
                  language: {
                        loadingRecords: 'กำลังโหลดข้อมูล...',
                        processing: 'กำลังดำเนินการ...',
                        search: '_INPUT_',
                        searchPlaceholder: 'ค้นหา',
                        zeroRecords: 'ไม่พบข้อมูล',
                        lengthMenu: 'แสดง _MENU_ รายการ',
                        info: '_START_ ถึง _END_ จาก _TOTAL_ รายการ',
                        infoEmpty: '',
                        infoFiltered: '',
                        paginate: {
                              first: '<<',
                              last: '>>',
                              next: '>',
                              previous: '<'
                        }
                  }
            };

            $('#emsTrackingTable').DataTable().destroy();
            $('#emsTrackingTable tbody').empty();
            tbodyContent = '';

            if (emsTrackingData.length) {
                  $(emsTrackingData).each(function (index, emsTracking) {
                        tbodyContent += '<tr> \
                                          <td>' + emsTracking['ems_tracking_name'] + '</td> \
                                          <td>' + emsTracking['ems_tracking_number'] + '</td> \
                                          <td>' + convertDateToDisplay(emsTracking['ems_tracking_date']) + '</td> \
                                          <td class="text-center" style="width: 100px;"> \
                                                <a href="#" class="btn btn-sm btn-success" id="updateEmsTrackingBtn" data-id="' + emsTracking['ems_tracking_id'] + '" data-toggle="tooltip" data-placement="top" data-original-title="แก้ไข"> \
                                                      <i class="fa fa-pencil-square-o"></i></a> \
                                                <a href="#" class="btn btn-sm btn-danger" id="removeEmsTrackingBtn" data-id="' + emsTracking['ems_tracking_id'] + '" data-toggle="tooltip" data-placement="top" data-original-title="ลบ"> \
                                                      <i class="fa fa-trash-o"></i></a> \
                                          </td> \
                                    </tr>';
                  });

                  $('#emsTrackingTable tbody').append(tbodyContent);
            }

            $('#emsTrackingTable').DataTable(optionsDT);
      }

      function setArticleData(articleData) {
            detectData = articleData;

            var optionsDT = {
                  dom: 'Bfrtip',
                  paging: true,
                  lengthChange: false,
                  searching: true,
                  ordering: true,
                  info: true,
                  autoWidth: true,
                  destroy: true,
                  columnDefs: [{
                        orderable: false,
                        targets: [4]
                  }],
                  lengthMenu: [
                        [10, 20, 30, 50, -1],
                        [10, 20, 30, 50, 'ทั้งหมด']
                  ],
                  buttons: [],
                  order: [0, 'asc'],
                  language: {
                        loadingRecords: 'กำลังโหลดข้อมูล...',
                        processing: 'กำลังดำเนินการ...',
                        search: '_INPUT_',
                        searchPlaceholder: 'ค้นหา',
                        zeroRecords: 'ไม่พบข้อมูล',
                        lengthMenu: 'แสดง _MENU_ รายการ',
                        info: '_START_ ถึง _END_ จาก _TOTAL_ รายการ',
                        infoEmpty: '',
                        infoFiltered: '',
                        paginate: {
                              first: '<<',
                              last: '>>',
                              next: '>',
                              previous: '<'
                        }
                  }
            };

            $('#articleTable').DataTable().destroy();
            $('#articleTable tbody').empty();
            tbodyContent = '';

            if (articleData.length) {
                  $(articleData).each(function (index, article) {
                        tbodyContent += '<tr> \
                                          <td>' + article['article_title'] + '</td> \
                                          <td>' + article['categoriesName'] + '</td> \
                                          <td>' + convertDateToDisplay(article['date']) + '</td> \
                                          <td>' + Number(article['user_view'].toLocaleString()).toLocaleString('en', {
                              minimumFractionDigits: 0
                        }) + '</td> \
                                          <td class="text-center" style="width: 100px;"> \
                                                <a href="#" class="btn btn-sm btn-success" id="updateArticleBtn" data-id="' + article['article_id'] + '" data-toggle="tooltip" data-placement="top" data-original-title="แก้ไข"> \
                                                      <i class="fa fa-pencil-square-o"></i></a> \
                                                <a href="#" class="btn btn-sm btn-danger" id="removeArticleBtn" data-id="' + article['article_id'] + '" data-toggle="tooltip" data-placement="top" data-original-title="ลบ"> \
                                                      <i class="fa fa-trash-o"></i></a> \
                                          </td> \
                                    </tr>';
                  });

                  $('#articleTable tbody').append(tbodyContent);
            }

            $('#articleTable').DataTable(optionsDT);
      }

      function setCategoriesData(categoriesData) {
            detectData = categoriesData;

            var optionsDT = {
                  dom: 'Bfrtip',
                  paging: true,
                  lengthChange: false,
                  searching: true,
                  ordering: true,
                  info: true,
                  autoWidth: true,
                  destroy: true,
                  columnDefs: [{
                        orderable: false,
                        targets: [1]
                  }],
                  lengthMenu: [
                        [10, 20, 30, 50, -1],
                        [10, 20, 30, 50, 'ทั้งหมด']
                  ],
                  buttons: [],
                  order: [0, 'asc'],
                  language: {
                        loadingRecords: 'กำลังโหลดข้อมูล...',
                        processing: 'กำลังดำเนินการ...',
                        search: '_INPUT_',
                        searchPlaceholder: 'ค้นหา',
                        zeroRecords: 'ไม่พบข้อมูล',
                        lengthMenu: 'แสดง _MENU_ รายการ',
                        info: '_START_ ถึง _END_ จาก _TOTAL_ รายการ',
                        infoEmpty: '',
                        infoFiltered: '',
                        paginate: {
                              first: '<<',
                              last: '>>',
                              next: '>',
                              previous: '<'
                        }
                  }
            };

            $('#categoriesTable').DataTable().destroy();
            $('#categoriesTable tbody').empty();
            tbodyContent = '';

            if (categoriesData.length) {
                  $(categoriesData).each(function (index, categories) {
                        tbodyContent += '<tr> \
                                          <td>' + categories['name'] + '</td> \
                                          <td class="text-center" style="width: 100px;"> \
                                                <a href="#" class="btn btn-sm btn-success" id="updateCategoriesBtn" data-id="' + categories['catid'] + '" data-toggle="tooltip" data-placement="top" data-original-title="แก้ไข"> \
                                                      <i class="fa fa-pencil-square-o"></i></a> \
                                                <a href="#" class="btn btn-sm btn-danger" id="removeCategoriesBtn" data-id="' + categories['catid'] + '" data-toggle="tooltip" data-placement="top" data-original-title="ลบ"> \
                                                      <i class="fa fa-trash-o"></i></a> \
                                          </td> \
                                    </tr>';
                  });

                  $('#categoriesTable tbody').append(tbodyContent);
            }

            $('#categoriesTable').DataTable(optionsDT);
      }

      function setSidebarManageWithCategoriesData(sidebarCategoriesData) {
            detectData = sidebarCategoriesData;

            var optionsDT = {
                  dom: 'Bfrtip',
                  paging: true,
                  lengthChange: false,
                  searching: true,
                  ordering: true,
                  info: true,
                  autoWidth: true,
                  destroy: true,
                  columnDefs: [{
                        orderable: false,
                        targets: [2]
                  }],
                  lengthMenu: [
                        [10, 20, 30, 50, -1],
                        [10, 20, 30, 50, 'ทั้งหมด']
                  ],
                  buttons: [],
                  order: [0, 'asc'],
                  language: {
                        loadingRecords: 'กำลังโหลดข้อมูล...',
                        processing: 'กำลังดำเนินการ...',
                        search: '_INPUT_',
                        searchPlaceholder: 'ค้นหา',
                        zeroRecords: 'ไม่พบข้อมูล',
                        lengthMenu: 'แสดง _MENU_ รายการ',
                        info: '_START_ ถึง _END_ จาก _TOTAL_ รายการ',
                        infoEmpty: '',
                        infoFiltered: '',
                        paginate: {
                              first: '<<',
                              last: '>>',
                              next: '>',
                              previous: '<'
                        }
                  }
            };

            $('#sidebarCategoriesTable').DataTable().destroy();
            $('#sidebarCategoriesTable tbody').empty();
            tbodyContent = '';

            if (sidebarCategoriesData.length) {
                  var categoriesNameData = '';

                  $(sidebarCategoriesData).each(function (index, sidebarCategories) {
                        categoriesNameData = '';

                        $(sidebarCategories['categoriesData']).each(function (index, categories) {
                              categoriesNameData += ((index == 0) ? categories['categories_name'] : ', ' + categories['categories_name']);
                        });

                        tbodyContent += '<tr> \
                                          <td>' + sidebarCategories['sidebar_categories_name'] + '</td> \
                                          <td>' + categoriesNameData + '</td> \
                                          <td class="text-center" style="width: 100px;"> \
                                                <a href="#" class="btn btn-sm btn-success" id="updateSidebarManageWithCategoriesBtn" data-id="' + sidebarCategories['sidebar_categories_id'] + '" data-index="' + index + '" data-toggle="tooltip" data-placement="top" data-original-title="แก้ไข"> \
                                                      <i class="fa fa-pencil-square-o"></i></a> \
                                                <a href="#" class="btn btn-sm btn-danger" id="removeSidebarManageWithCategoriesBtn" data-id="' + sidebarCategories['sidebar_categories_id'] + '" data-index="' + index + '" data-toggle="tooltip" data-placement="top" data-original-title="ลบ"> \
                                                      <i class="fa fa-trash-o"></i></a> \
                                          </td> \
                                    </tr>';
                  });

                  $('#sidebarCategoriesTable tbody').append(tbodyContent);
            }

            $('#sidebarCategoriesTable').DataTable(optionsDT);

            //--Set validate form
            $('#sidebarCategoriesForm').validate({
                  rules: {
                        sidebarCategoriesName: {
                              required: true
                        },
                        sidebarCategoriesType: {
                              required: true
                        }
                  },
                  messages: {
                        sidebarCategoriesName: {
                              required: 'ชื่อรายการห้ามว่าง'
                        },
                        sidebarCategoriesType: {
                              required: 'ประเภทหมวดหมู่เบอร์ห้ามว่าง'
                        }
                  },
                  submitHandler: saveSidebarManageWithCategories
            });

            //--Set dropdown
            loadDropdownData('api/sidebarManageAPI.php', [{
                  filter: 'categories',
                  value: ''
            }], function (data) {
                  $('#sidebarCategoriesForm').find('#sidebarCategoriesType').find('option').remove();
                  $('#sidebarCategoriesForm #sidebarCategoriesType').select2({
                              data: data['categoriesList'],
                              language: {
                                    noResults: function (params) {
                                          return 'ไม่พบรายการ';
                                    }
                              },
                              tags: true,
                              multiple: true
                        })
                        .on('select2:select', function (e) {
                              $('#sidebarCategoriesForm #sidebarCategoriesType').trigger('change');
                        });

                  setSidebarManagePiority('add', 'categories', 'sidebar_categories_name', 0);
            });
      }

      function setSidebarManageWithPriceData(sidebarPriceData) {
            detectData = sidebarPriceData;

            var optionsDT = {
                  dom: 'Bfrtip',
                  paging: true,
                  lengthChange: false,
                  searching: true,
                  ordering: true,
                  info: true,
                  autoWidth: true,
                  destroy: true,
                  columnDefs: [{
                        orderable: false,
                        targets: [2]
                  }],
                  lengthMenu: [
                        [10, 20, 30, 50, -1],
                        [10, 20, 30, 50, 'ทั้งหมด']
                  ],
                  buttons: [],
                  order: [0, 'asc'],
                  language: {
                        loadingRecords: 'กำลังโหลดข้อมูล...',
                        processing: 'กำลังดำเนินการ...',
                        search: '_INPUT_',
                        searchPlaceholder: 'ค้นหา',
                        zeroRecords: 'ไม่พบข้อมูล',
                        lengthMenu: 'แสดง _MENU_ รายการ',
                        info: '_START_ ถึง _END_ จาก _TOTAL_ รายการ',
                        infoEmpty: '',
                        infoFiltered: '',
                        paginate: {
                              first: '<<',
                              last: '>>',
                              next: '>',
                              previous: '<'
                        }
                  }
            };

            $('#sidebarPriceTable').DataTable().destroy();
            $('#sidebarPriceTable tbody').empty();
            tbodyContent = '';

            if (sidebarPriceData.length) {
                  $(sidebarPriceData).each(function (index, sidebarPrice) {
                        tbodyContent += '<tr> \
                                          <td>' + sidebarPrice['sidebar_price_name'] + '</td>';

                        if ((sidebarPrice['sidebar_price_begin'] == 0) && (sidebarPrice['sidebar_price_end'] != 0)) {
                              tbodyContent += '<td>ราคาไม่เกิน ' + Number(sidebarPrice['sidebar_price_end'].toLocaleString()).toLocaleString('en', {
                                    minimumFractionDigits: 0
                              }) + '</td>';
                        } else if ((sidebarPrice['sidebar_price_begin'] != 0) && (sidebarPrice['sidebar_price_end'] == 0)) {
                              tbodyContent += '<td>ตั้งแต่ ' + Number(sidebarPrice['sidebar_price_begin'].toLocaleString()).toLocaleString('en', {
                                    minimumFractionDigits: 0
                              }) + ' ขึ้นไป</td>';
                        } else {
                              tbodyContent += '<td>' + Number(sidebarPrice['sidebar_price_begin'].toLocaleString()).toLocaleString('en', {
                                    minimumFractionDigits: 0
                              }) + ' - ' + Number(sidebarPrice['sidebar_price_end'].toLocaleString()).toLocaleString('en', {
                                    minimumFractionDigits: 0
                              }) + '</td>';
                        }

                        tbodyContent += '<td class="text-center" style="width: 100px;"> \
                                                <a href="#" class="btn btn-sm btn-success" id="updateSidebarManageWithPriceBtn" data-id="' + sidebarPrice['sidebar_price_id'] + '" data-index="' + index + '" data-toggle="tooltip" data-placement="top" data-original-title="แก้ไข"> \
                                                      <i class="fa fa-pencil-square-o"></i></a> \
                                                <a href="#" class="btn btn-sm btn-danger" id="removeSidebarManageWithPriceBtn" data-id="' + sidebarPrice['sidebar_price_id'] + '" data-index="' + index + '" data-toggle="tooltip" data-placement="top" data-original-title="ลบ"> \
                                                      <i class="fa fa-trash-o"></i></a> \
                                          </td> \
                                    </tr>';
                  });

                  $('#sidebarPriceTable tbody').append(tbodyContent);
            }

            $('#sidebarPriceTable').DataTable(optionsDT);

            //--Set validate form
            $('#sidebarPriceForm').validate({
                  rules: {
                        sidebarPriceName: {
                              required: true
                        },
                        sidebarPriceBegin: {
                              required: true,
                              number: true
                        },
                        sidebarPriceEnd: {
                              required: true,
                              number: true
                        }
                  },
                  messages: {
                        sidebarPriceName: {
                              required: 'ชื่อรายการห้ามว่าง'
                        },
                        sidebarPriceBegin: {
                              required: 'ช่วงราคาเริ่มต้นห้ามว่าง',
                              number: 'ต้องเป็นตัวเลขเท่านั้น'
                        },
                        sidebarPriceEnd: {
                              required: 'ช่วงราคาสิ้นสุดห้ามว่าง',
                              number: 'ต้องเป็นตัวเลขเท่านั้น'
                        }
                  },
                  submitHandler: saveSidebarManageWithPrice
            });
      }

      function setSidebarManageWithCompoundData(sidebarCompoundData) {
            detectData = sidebarCompoundData;

            var optionsDT = {
                  dom: 'Bfrtip',
                  paging: true,
                  lengthChange: false,
                  searching: true,
                  ordering: true,
                  info: true,
                  autoWidth: true,
                  destroy: true,
                  columnDefs: [{
                        orderable: false,
                        targets: [2]
                  }],
                  lengthMenu: [
                        [10, 20, 30, 50, -1],
                        [10, 20, 30, 50, 'ทั้งหมด']
                  ],
                  buttons: [],
                  order: [0, 'asc'],
                  language: {
                        loadingRecords: 'กำลังโหลดข้อมูล...',
                        processing: 'กำลังดำเนินการ...',
                        search: '_INPUT_',
                        searchPlaceholder: 'ค้นหา',
                        zeroRecords: 'ไม่พบข้อมูล',
                        lengthMenu: 'แสดง _MENU_ รายการ',
                        info: '_START_ ถึง _END_ จาก _TOTAL_ รายการ',
                        infoEmpty: '',
                        infoFiltered: '',
                        paginate: {
                              first: '<<',
                              last: '>>',
                              next: '>',
                              previous: '<'
                        }
                  }
            };

            $('#sidebarCompoundTable').DataTable().destroy();
            $('#sidebarCompoundTable tbody').empty();
            tbodyContent = '';

            if (sidebarCompoundData.length) {
                  $(sidebarCompoundData).each(function (index, sidebarCompound) {
                        tbodyContent += '<tr> \
                                          <td>' + sidebarCompound['sidebar_compound_name'] + '</td> \
                                          <td>' + sidebarCompound['sidebar_compound_num'] + '</td> \
                                          <td class="text-center" style="width: 100px;"> \
                                                <a href="#" class="btn btn-sm btn-success" id="updateSidebarManageWithCompoundBtn" data-id="' + sidebarCompound['sidebar_compound_id'] + '" data-index="' + index + '" data-toggle="tooltip" data-placement="top" data-original-title="แก้ไข"> \
                                                      <i class="fa fa-pencil-square-o"></i></a> \
                                                <a href="#" class="btn btn-sm btn-danger" id="removeSidebarManageWithCompoundBtn" data-id="' + sidebarCompound['sidebar_compound_id'] + '" data-index="' + index + '" data-toggle="tooltip" data-placement="top" data-original-title="ลบ"> \
                                                      <i class="fa fa-trash-o"></i></a> \
                                          </td> \
                                    </tr>';
                  });

                  $('#sidebarCompoundTable tbody').append(tbodyContent);
            }

            $('#sidebarCompoundTable').DataTable(optionsDT);

            //--Set validate form
            $('#sidebarCompoundForm').validate({
                  rules: {
                        sidebarCompoundName: {
                              required: true
                        },
                        sidebarCompoundNum: {
                              required: true,
                              number: true
                        }
                  },
                  messages: {
                        sidebarCompoundName: {
                              required: 'ชื่อรายการห้ามว่าง'
                        },
                        sidebarCompoundNum: {
                              required: 'เลขผลรวมห้ามว่าง',
                              number: 'ต้องเป็นตัวเลขเท่านั้น'
                        }
                  },
                  submitHandler: saveSidebarManageWithCompound
            });
      }

      function setSidebarManagePiority(mode, type, filterKey, dataIndex) {
            var piorityList = [];
            // console.log(mode, type, filterKey, dataIndex);

            if (mode == 'add') {
                  if (!detectData.length) {
                        piorityList.push({
                              id: 1,
                              text: 'ลำดับแรก',
                              disabled: false
                        });
                  } else {
                        $(detectData).each(function (index, detect) {
                              if (index == 0) {
                                    piorityList.push({
                                          id: (index + 1),
                                          text: 'ลำดับแรก',
                                          disabled: false
                                    });
                              }

                              piorityList.push({
                                    id: (index + 2),
                                    text: 'หลัง ' + detect[filterKey],
                                    disabled: false
                              });
                        });
                  }
            } else if (mode == 'update') {
                  if (!detectData.length) {
                        piorityList.push({
                              id: 1,
                              text: 'ลำดับแรก',
                              disabled: false
                        });
                  } else {
                        $(detectData).each(function (index, detect) {
                              var status = false;

                              if (index == 0) {
                                    status = (dataIndex == 0) ? true : false;
                              } else {
                                    if (dataIndex == index) {
                                          piorityList[index]['disabled'] = true;
                                          status = true;
                                    }
                              }

                              if (index == 0) {
                                    piorityList.push({
                                          id: (index + 1),
                                          text: 'ลำดับแรก',
                                          disabled: status
                                    });
                              }

                              piorityList.push({
                                    id: (index + 2),
                                    text: 'หลัง ' + detect[filterKey],
                                    disabled: status
                              });
                        });
                  }
            }

            switch (type) {
                  case 'categories':
                        $('#sidebarCategoriesForm').find('#sidebarCategoriesPiority').find('option').remove();
                        $('#sidebarCategoriesForm #sidebarCategoriesPiority').select2({
                                    data: piorityList,
                                    language: {
                                          noResults: function (params) {
                                                return 'ไม่พบรายการ';
                                          }
                                    },
                                    tags: false
                              })
                              .on('select2:select', function (e) {
                                    $('#sidebarCategoriesForm #sidebarCategoriesPiority').val(e.params.data['id']).trigger('change');
                              });

                        break;
                  case 'price':
                        $('#sidebarPriceForm').find('#sidebarPricePiority').find('option').remove();
                        $('#sidebarPriceForm #sidebarPricePiority').select2({
                                    data: piorityList,
                                    language: {
                                          noResults: function (params) {
                                                return 'ไม่พบรายการ';
                                          }
                                    },
                                    tags: false
                              })
                              .on('select2:select', function (e) {
                                    $('#sidebarPriceForm #sidebarPricePiority').val(e.params.data['id']).trigger('change');
                              });

                        break;
                  case 'compound':
                        $('#sidebarCompoundForm').find('#sidebarCompoundPiority').find('option').remove();
                        $('#sidebarCompoundForm #sidebarCompoundPiority').select2({
                                    data: piorityList,
                                    language: {
                                          noResults: function (params) {
                                                return 'ไม่พบรายการ';
                                          }
                                    },
                                    tags: false
                              })
                              .on('select2:select', function (e) {
                                    $('#sidebarCompoundForm #sidebarCompoundPiority').val(e.params.data['id']).trigger('change');
                              });

                        break;
            }
      }

      function setPhoneFormatData(phoneFormatData) {
            detectData = phoneFormatData;
            console.log('detectData', detectData);

            loadDropdownData('api/settingAPI.php', [{
                  filter: 'phoneFormatCategories',
                  value: ''
            }], function (data) {
                  $('#phoneFormatForm').find('#phoneFormat').find('option:not(:first-child)').remove();
                  $('#phoneFormatForm #phoneFormat').select2({
                              data: data['phoneFormatCategoriesList'],
                              language: {
                                    noResults: function (params) {
                                          return 'ไม่พบรายการ';
                                    }
                              },
                              tags: false
                        })
                        .on('select2:select', function (e) {
                              $('#phoneFormatForm #phoneFormat').val(e.params.data['id']).trigger('change');
                        });

                  $('#phoneFormatForm #phoneFormat').val(detectData['phone_format']).trigger('change');
            });

            $('#phoneFormatForm #savePhoneFormatBtn').attr('data-save-id', detectData['Id']);
      }

      function setPhoneSoldData(phoneSoldData) {
            detectData = phoneSoldData;

            var optionsDT = {
                  dom: 'Bfrtip',
                  paging: true,
                  lengthChange: false,
                  searching: true,
                  ordering: true,
                  info: true,
                  autoWidth: true,
                  destroy: true,
                  columnDefs: [{
                        orderable: false,
                        targets: [3]
                  }],
                  lengthMenu: [
                        [10, 20, 30, 50, -1],
                        [10, 20, 30, 50, 'ทั้งหมด']
                  ],
                  buttons: ['excel', 'print'],
                  order: [2, 'desc'],
                  language: {
                        loadingRecords: 'กำลังโหลดข้อมูล...',
                        processing: 'กำลังดำเนินการ...',
                        search: '_INPUT_',
                        searchPlaceholder: 'ค้นหา',
                        zeroRecords: 'ไม่พบข้อมูล',
                        lengthMenu: 'แสดง _MENU_ รายการ',
                        info: '_START_ ถึง _END_ จาก _TOTAL_ รายการ',
                        infoEmpty: '',
                        infoFiltered: '',
                        paginate: {
                              first: '<<',
                              last: '>>',
                              next: '>',
                              previous: '<'
                        }
                  }
            };

            $('#phoneSoldTable').DataTable().destroy();
            $('#phoneSoldTable tbody').empty();
            tbodyContent = '';

            if (phoneSoldData.length) {
                  var soldDate = '';

                  $(phoneSoldData).each(function (index, phoneSold) {
                        soldDate = (((new Date(phoneSold['sold_date']).getDate()).toString().length == 1) ? '0' + new Date(phoneSold['sold_date']).getDate() : new Date(phoneSold['sold_date']).getDate()) + '/' +
                              (((new Date(phoneSold['sold_date']).getMonth() + 1).toString().length == 1) ? '0' + (new Date(phoneSold['sold_date']).getMonth() + 1) : (new Date(phoneSold['sold_date']).getMonth() + 1)) + '/' +
                              ((new Date(phoneSold['sold_date']).getFullYear() + 543) + ' ' +
                                    (((new Date(phoneSold['sold_date']).getHours()).toString().length == 1) ? '0' + new Date(phoneSold['sold_date']).getHours() : new Date(phoneSold['sold_date']).getHours()) + ':' +
                                    (((new Date(phoneSold['sold_date']).getMinutes()).toString().length == 1) ? '0' + new Date(phoneSold['sold_date']).getMinutes() : new Date(phoneSold['sold_date']).getMinutes()) + ' น.');

                        tbodyContent += '<tr> \
                                          <td>' + phoneSold['number1'] + ' <span style="color: red;">(' + phoneSold['sum1'] + ')</span> <span class="hide">' + phoneSold['ori_number1'] + '</span></td> \
                                          <td>' + phoneSold['name'] + '</td> \
                                          <td>' + soldDate + '</td> \
                                          <td class="text-center" style="width: 100px;"> \
                                                <a href="#" class="btn btn-sm btn-success" id="restorePhoneBtn" data-id="' + phoneSold['id'] + '" data-toggle="tooltip" data-placement="top" data-original-title="กู้คืน"> \
                                                      <i class="fa fa-pencil-square-o"></i></a> \
                                          </td> \
                                    </tr>';
                  });

                  $('#phoneSoldTable tbody').append(tbodyContent);
            }

            $('#phoneSoldTable').DataTable(optionsDT);
      }

      function setAccountData(accountData) {
            detectData = accountData;
            console.log('detectData', detectData);

            $('#accountForm').find('#imagePathFile, #accountPassword, #accountPasswordConfirm').val('');

            $('#accountGalaryPathView').attr('src', (detectData['user_galary_path'] ? detectData['user_galary_path'] : 'uploads/no-image-available.png'));
            $('#accountNameView').text(detectData['user_fullname']);
            $('#accountJoinDateView').text(convertDateToDisplay('2018-09-27'));
            $('#accountEmailView').text(detectData['user_email']);
            $('#accountForm #accountFullName').val(detectData['user_fullname']);
            $('#accountForm #accountName').val(detectData['user_name']);
            $('#accountForm #accountTel').val(detectData['user_tel']);
            $('#accountForm .image-tumbnail a').attr('href', (detectData['user_galary_path'] ? detectData['user_galary_path'] : 'uploads/no-image-available.png'));
            $('#accountForm .image-tumbnail img').attr('src', (detectData['user_galary_path'] ? detectData['user_galary_path'] : 'uploads/no-image-available.png'));
            $('#accountForm #saveAccountBtn').attr('data-save-id', detectData['user_id']);
      }

      function setImportProductsFile() {
            console.log(importProductsData);

            var optionsDT = {
                  dom: 'Bfrtip',
                  paging: true,
                  lengthChange: false,
                  searching: true,
                  ordering: true,
                  info: true,
                  autoWidth: true,
                  destroy: true,
                  columnDefs: [{
                        orderable: false,
                        targets: [5]
                  }],
                  lengthMenu: [
                        [10, 20, 30, 50, -1],
                        [10, 20, 30, 50, 'ทั้งหมด']
                  ],
                  buttons: [],
                  order: [0, 'asc'],
                  language: {
                        loadingRecords: 'กำลังโหลดข้อมูล...',
                        processing: 'กำลังดำเนินการ...',
                        search: '_INPUT_',
                        searchPlaceholder: 'ค้นหา',
                        zeroRecords: 'ไม่พบข้อมูล',
                        lengthMenu: 'แสดง _MENU_ รายการ',
                        info: '_START_ ถึง _END_ จาก _TOTAL_ รายการ',
                        infoEmpty: '',
                        infoFiltered: '',
                        paginate: {
                              first: '<<',
                              last: '>>',
                              next: '>',
                              previous: '<'
                        }
                  }
            };

            $('#importProductsTable').DataTable().destroy();
            $('#importProductsTable tbody').empty();
            tbodyContent = '';
            var categoriesNameData = '';

            if (importProductsData.length) {
                  $(importProductsData).each(function (index, importProducts) {
                        categoriesNameData = '';

                        $(importProducts['categoriesData']).each(function (index, categories) {
                              categoriesNameData += ((index == 0) ? categories['categories_name'] : ', ' + categories['categories_name']);
                        });

                        tbodyContent += '<tr> \
                                          <td>' + importProducts['productsNumberStr'] + ' <span style="color: red;">(' + sumPhoneNumber(importProducts['productsNumber']) + ')</td> \
                                          <td>' + Number(importProducts['productsPrice'].toLocaleString()).toLocaleString('en', {
                              minimumFractionDigits: 0
                        }) + '</td> \
                                          <td>' + importProducts['carrier_name'] + '</td> \
                                          <td>' + categoriesNameData + '</td> \
                                          <td class="text-center"><img src="assets/images/' + ((importProducts['productsSold'] == 1) ? 'yes.png' : 'no.png') + '" style="width: 20px;"></td> \
                                          <td class="text-center" style="width: 100px;"> \
                                                <a href="#" class="btn btn-sm btn-danger" id="removeImportProductsBtn" data-id="' + index + '" data-toggle="tooltip" data-placement="top" data-original-title="ลบ"> \
                                                      <i class="fa fa-trash-o"></i></a> \
                                          </td> \
                                    </tr>';
                  });

                  $('#importProductsTable tbody').append(tbodyContent);
            }

            $('#importProductsTable').DataTable(optionsDT);
            $('#importProductsPopup').modal('show');
      }

      function saveProducts(importProductsStatus) {
            apiUrl = 'api/productsAPI.php';
            importProductsStatus = (importProductsStatus == true) ? importProductsStatus : false;

            if (!importProductsStatus) {
                  var productsNumber = $('#productsForm #productsNumber').val().substr(0, 3) + '-' + $('#productsForm #productsNumber').val().substr(3, 3) + '-' + $('#productsForm #productsNumber').val().substr(6, 4);

                  if ($('#productsForm #saveProductsBtn').attr('data-save-status') == 'save') {
                        var insertNeoProductCategoryGroupData = [];

                        $($('#productsForm #productsCategories').val()).each(function (index, categories) {
                              insertNeoProductCategoryGroupData.push({
                                    npc_id: categories
                              });
                        });

                        apiParams = {
                              fn: 'saveProducts',
                              insertProducts: {
                                    tableName: 'neo_product',
                                    data: [{
                                          number1: productsNumber,
                                          pic1: $('#productsForm #productsCarrier').val(),
                                          price: $('#productsForm #productsPrice').val(),
                                          counts: '0',
                                          sum1: sumPhoneNumber($('#productsForm #productsNumber').val()),
                                          ori_number1: $('#productsForm #productsNumber').val().replace('-', ''),
                                          product_detail: $('#productsForm #productsDetail').val(),
                                          date: 'now',
                                          statussum: 'y',
                                          news: ($('#productsForm #productsPinAtHome').is(':checked') ? '1' : '0'),
                                          sold: ($('#productsForm #productsSold').is(':checked') ? '1' : '0'),
                                          sold_date: ($('#productsForm #productsSold').is(':checked') ? 'now' : ''),
                                          insertNeoProductCategoryGroup: {
                                                tableName: 'neo_product_category_group',
                                                primaryKey: 'np_id',
                                                data: insertNeoProductCategoryGroupData
                                          }
                                    }]
                              }
                        };
                  } else {
                        apiParams = {
                              fn: 'updateProducts',
                              updateProducts: {
                                    tableName: 'neo_product',
                                    primaryKey: 'id',
                                    data: [{
                                          id: $('#productsForm #saveProductsBtn').attr('data-save-id'),
                                          number1: productsNumber,
                                          pic1: $('#productsForm #productsCarrier').val(),
                                          price: $('#productsForm #productsPrice').val(),
                                          counts: '0',
                                          sum1: sumPhoneNumber($('#productsForm #productsNumber').val()),
                                          ori_number1: $('#productsForm #productsNumber').val().replace('-', ''),
                                          product_detail: $('#productsForm #productsDetail').val(),
                                          statussum: 'y',
                                          news: ($('#productsForm #productsPinAtHome').is(':checked') ? '1' : '0'),
                                          sold: ($('#productsForm #productsSold').is(':checked') ? '1' : '0'),
                                          sold_date: ($('#productsForm #productsSold').is(':checked') ? 'now' : '')
                                    }]
                              },
                              deleteNeoProductCategoryGroup: [{
                                    tableName: 'neo_product_category_group',
                                    field: 'np_id',
                                    value: $('#productsForm #saveProductsBtn').attr('data-save-id')
                              }],
                              insertNeoProductCategoryGroup: {
                                    tableName: 'neo_product_category_group',
                                    data: []
                              }
                        };

                        $($('#productsForm #productsCategories').val()).each(function (index, categories) {
                              apiParams['insertNeoProductCategoryGroup']['data'].push({
                                    np_id: $('#productsForm #saveProductsBtn').attr('data-save-id'),
                                    npc_id: categories
                              });
                        });
                  }
            } else {
                  apiParams = {
                        fn: 'saveProducts',
                        insertProducts: {
                              tableName: 'neo_product',
                              data: []
                        }
                  };

                  $(importProductsData).each(function (index, importProducts) {
                        var insertNeoProductCategoryGroupData = [];

                        $(importProducts['categoriesData']).each(function (index, categories) {
                              insertNeoProductCategoryGroupData.push({
                                    npc_id: categories['categories_id']
                              });
                        });

                        apiParams['insertProducts']['data'].push({
                              number1: importProducts['productsNumberStr'],
                              pic1: importProducts['productsCarrier'],
                              price: importProducts['productsPrice'],
                              counts: '0',
                              sum1: sumPhoneNumber(importProducts['productsNumber']),
                              ori_number1: importProducts['productsNumber'],
                              product_detail: '',
                              date: 'now',
                              statussum: 'y',
                              news: importProducts['productsPinAtHome'],
                              sold: importProducts['productsSold'],
                              sold_date: ((importProducts['productsSold'] == 1) ? 'now' : ''),
                              insertNeoProductCategoryGroup: {
                                    tableName: 'neo_product_category_group',
                                    primaryKey: 'np_id',
                                    data: insertNeoProductCategoryGroupData
                              }
                        });
                  });
            }
            console.log(apiParams);

            Service.prototype.callAPI(apiUrl, apiParams, 'post', true).done(function (res) {
                  var data = JSON.parse(res);
                  // console.log(data);

                  if (data.status) {
                        swal({
                              html: '<p>' + data.msgInfo + '</p>',
                              type: 'success',
                              showConfirmButton: true,
                              showCancelButton: false,
                              showCloseButton: true,
                              confirmButtonText: 'ตกลง',
                              cancelButtonText: 'ยกเลิก',
                              confirmButtonClass: 'btn btn-primary',
                              cancelButtonClass: 'btn btn-reverse',
                              buttonsStyling: false,
                              allowOutsideClick: true
                        });

                        $('#viewProductsBtn').trigger('click');
                        $('#importProductsPopup').modal('hide');
                        $('#resetImportProductsBtn').trigger('click');

                        setProductsData(data.data);
                  } else {
                        swal({
                              html: '<p>' + data.msgInfo + '</p>',
                              type: 'error',
                              showConfirmButton: true,
                              showCancelButton: false,
                              showCloseButton: true,
                              confirmButtonText: 'ตกลง',
                              cancelButtonText: 'ยกเลิก',
                              confirmButtonClass: 'btn btn-primary',
                              cancelButtonClass: 'btn btn-reverse',
                              buttonsStyling: false,
                              allowOutsideClick: true
                        });
                  }
            });
      }

      function saveHoroDegree() {
            apiUrl = 'api/horoDegreeAPI.php';

            if ($('#horoDegreeForm #saveHoroDegreeBtn').attr('data-save-status') == 'save') {
                  apiParams = {
                        fn: 'saveHoroDegree',
                        insertHoroDegree: {
                              tableName: 'forcast',
                              data: [{
                                    number: $('#horoDegreeForm #horoDegreeNumber').val(),
                                    detail: $('#horoDegreeForm #horoDegreeDetail').val(),
                                    percent: $('#horoDegreeForm #horoDegreePercent').val(),
                                    color: $('#horoDegreeForm #horoDegreeType').val(),
                                    education: $('#horoDegreeForm #horoDegreeEducation').val(),
                                    work: $('#horoDegreeForm #horoDegreeWork').val(),
                                    finance: $('#horoDegreeForm #horoDegreeFinance').val(),
                                    love: $('#horoDegreeForm #horoDegreeLove').val(),
                                    lucky: $('#horoDegreeForm #horoDegreeLucky').val()
                              }]
                        }
                  };
            } else {
                  apiParams = {
                        fn: 'updateHoroDegree',
                        updateHoroDegree: {
                              tableName: 'forcast',
                              primaryKey: 'Id',
                              data: [{
                                    Id: $('#horoDegreeForm #saveHoroDegreeBtn').attr('data-save-id'),
                                    number: $('#horoDegreeForm #horoDegreeNumber').val(),
                                    detail: $('#horoDegreeForm #horoDegreeDetail').val(),
                                    percent: $('#horoDegreeForm #horoDegreePercent').val(),
                                    color: $('#horoDegreeForm #horoDegreeType').val(),
                                    education: $('#horoDegreeForm #horoDegreeEducation').val(),
                                    work: $('#horoDegreeForm #horoDegreeWork').val(),
                                    finance: $('#horoDegreeForm #horoDegreeFinance').val(),
                                    love: $('#horoDegreeForm #horoDegreeLove').val(),
                                    lucky: $('#horoDegreeForm #horoDegreeLucky').val()
                              }]
                        }
                  };
            }
            // console.log(apiParams);

            Service.prototype.callAPI(apiUrl, apiParams, 'post', true).done(function (res) {
                  var data = JSON.parse(res);
                  // console.log(data);

                  if (data.status) {
                        swal({
                              html: '<p>' + data.msgInfo + '</p>',
                              type: 'success',
                              showConfirmButton: true,
                              showCancelButton: false,
                              showCloseButton: true,
                              confirmButtonText: 'ตกลง',
                              cancelButtonText: 'ยกเลิก',
                              confirmButtonClass: 'btn btn-primary',
                              cancelButtonClass: 'btn btn-reverse',
                              buttonsStyling: false,
                              allowOutsideClick: true
                        });

                        $('#viewHoroDegreeBtn').trigger('click');

                        setHoroDegreeData(data.data);
                  } else {
                        swal({
                              html: '<p>' + data.msgInfo + '</p>',
                              type: 'error',
                              showConfirmButton: true,
                              showCancelButton: false,
                              showCloseButton: true,
                              confirmButtonText: 'ตกลง',
                              cancelButtonText: 'ยกเลิก',
                              confirmButtonClass: 'btn btn-primary',
                              cancelButtonClass: 'btn btn-reverse',
                              buttonsStyling: false,
                              allowOutsideClick: true
                        });
                  }
            });
      }

      function saveHoroCompound() {
            apiUrl = 'api/horoCompoundAPI.php';

            if ($('#horoCompoundForm #saveHoroCompoundBtn').attr('data-save-status') == 'save') {
                  apiParams = {
                        fn: 'saveHoroCompound',
                        insertHoroCompound: {
                              tableName: 'forcast_sum',
                              data: [{
                                    number: $('#horoCompoundForm #horoCompoundNumber').val(),
                                    detail: $('#horoCompoundForm #horoCompoundDetail').val(),
                                    percent: $('#horoCompoundForm #horoCompoundPercent').val(),
                                    mean: $('#horoCompoundForm #horoCompoundType').val()
                              }]
                        }
                  };
            } else {
                  apiParams = {
                        fn: 'updateHoroCompound',
                        updateHoroCompound: {
                              tableName: 'forcast_sum',
                              primaryKey: 'Id',
                              data: [{
                                    Id: $('#horoCompoundForm #saveHoroCompoundBtn').attr('data-save-id'),
                                    number: $('#horoCompoundForm #horoCompoundNumber').val(),
                                    detail: $('#horoCompoundForm #horoCompoundDetail').val(),
                                    percent: $('#horoCompoundForm #horoCompoundPercent').val(),
                                    mean: $('#horoCompoundForm #horoCompoundType').val()
                              }]
                        }
                  };
            }
            // console.log(apiParams);

            Service.prototype.callAPI(apiUrl, apiParams, 'post', true).done(function (res) {
                  var data = JSON.parse(res);
                  // console.log(data);

                  if (data.status) {
                        swal({
                              html: '<p>' + data.msgInfo + '</p>',
                              type: 'success',
                              showConfirmButton: true,
                              showCancelButton: false,
                              showCloseButton: true,
                              confirmButtonText: 'ตกลง',
                              cancelButtonText: 'ยกเลิก',
                              confirmButtonClass: 'btn btn-primary',
                              cancelButtonClass: 'btn btn-reverse',
                              buttonsStyling: false,
                              allowOutsideClick: true
                        });

                        $('#viewHoroCompoundBtn').trigger('click');

                        setHoroCompoundData(data.data);
                  } else {
                        swal({
                              html: '<p>' + data.msgInfo + '</p>',
                              type: 'error',
                              showConfirmButton: true,
                              showCancelButton: false,
                              showCloseButton: true,
                              confirmButtonText: 'ตกลง',
                              cancelButtonText: 'ยกเลิก',
                              confirmButtonClass: 'btn btn-primary',
                              cancelButtonClass: 'btn btn-reverse',
                              buttonsStyling: false,
                              allowOutsideClick: true
                        });
                  }
            });
      }

      function saveBanner() {
            var formData = new FormData();
            var imagePathFileRef = Math.random().toString(36).substr(2);
            apiUrl = 'api/bannerAPI.php';

            if ($('#bannerForm #saveBannerBtn').attr('data-save-status') == 'save') {
                  apiParams = {
                        fn: 'saveBanner',
                        insertBanner: {
                              tableName: 'banner',
                              data: [{
                                    banner_name: $('#bannerForm #bannerName').val(),
                                    banner_detail: $('#bannerForm #bannerDetail').val(),
                                    banner_image_path: ''
                              }]
                        }
                  };

                  //--Detect field 'image_cover'
                  if ($('input[name="imagePathFile"]')[0].files[0]) {
                        apiParams['insertBanner']['data'][0]['banner_image_path'] = imagePathFileRef;
                        formData.append(imagePathFileRef, $('input[name="imagePathFile"]')[0].files[0]);
                  }
            } else {
                  apiParams = {
                        fn: 'updateBanner',
                        updateBanner: {
                              tableName: 'banner',
                              primaryKey: 'banner_id',
                              data: [{
                                    banner_id: $('#bannerForm #saveBannerBtn').attr('data-save-id'),
                                    banner_name: $('#bannerForm #bannerName').val(),
                                    banner_detail: $('#bannerForm #bannerDetail').val()
                              }]
                        }
                  };

                  //--Detect field 'banner_image_path'
                  if ($('input[name="imagePathFile"]')[0].files[0]) {
                        apiParams['updateBanner']['data'][0]['banner_image_path'] = imagePathFileRef;
                        formData.append(imagePathFileRef, $('input[name="imagePathFile"]')[0].files[0]);
                  }
            }

            formData.append('apiParams', JSON.stringify(apiParams));
            // console.log(apiParams);

            Service.prototype.callAPIWithFormData(apiUrl, formData, true).done(function (res) {
                  var data = JSON.parse(res);
                  // console.log(data);

                  if (data.status) {
                        swal({
                              html: '<p>' + data.msgInfo + '</p>',
                              type: 'success',
                              showConfirmButton: true,
                              showCancelButton: false,
                              showCloseButton: true,
                              confirmButtonText: 'ตกลง',
                              cancelButtonText: 'ยกเลิก',
                              confirmButtonClass: 'btn btn-primary',
                              cancelButtonClass: 'btn btn-reverse',
                              buttonsStyling: false,
                              allowOutsideClick: true
                        });

                        $('#viewBannerBtn').trigger('click');

                        setBannerData(data.data);
                  } else {
                        swal({
                              html: '<p>' + data.msgInfo + '</p>',
                              type: 'error',
                              showConfirmButton: true,
                              showCancelButton: false,
                              showCloseButton: true,
                              confirmButtonText: 'ตกลง',
                              cancelButtonText: 'ยกเลิก',
                              confirmButtonClass: 'btn btn-primary',
                              cancelButtonClass: 'btn btn-reverse',
                              buttonsStyling: false,
                              allowOutsideClick: true
                        });
                  }
            });
      }

      function saveEmsTracking() {
            apiUrl = 'api/emsTrackingAPI.php';

            if ($('#emsTrackingForm #saveEmsTrackingBtn').attr('data-save-status') == 'save') {
                  apiParams = {
                        fn: 'saveEmsTracking',
                        insertEmsTracking: {
                              tableName: 'ems_tracking',
                              data: [{
                                    ems_tracking_name: $('#emsTrackingForm #emsTrackingName').val(),
                                    ems_tracking_number: $('#emsTrackingForm #emsTrackingNumber').val(),
                                    ems_tracking_date: convertDateToDB($('#emsTrackingForm #emsTrackingDate').val())
                              }]
                        }
                  };
            } else {
                  apiParams = {
                        fn: 'updateEmsTracking',
                        updateEmsTracking: {
                              tableName: 'ems_tracking',
                              primaryKey: 'ems_tracking_id',
                              data: [{
                                    ems_tracking_id: $('#emsTrackingForm #saveEmsTrackingBtn').attr('data-save-id'),
                                    ems_tracking_name: $('#emsTrackingForm #emsTrackingName').val(),
                                    ems_tracking_number: $('#emsTrackingForm #emsTrackingNumber').val(),
                                    ems_tracking_date: convertDateToDB($('#emsTrackingForm #emsTrackingDate').val())
                              }]
                        }
                  };
            }
            // console.log(apiParams);

            Service.prototype.callAPI(apiUrl, apiParams, 'post', true).done(function (res) {
                  var data = JSON.parse(res);
                  // console.log(data);

                  if (data.status) {
                        swal({
                              html: '<p>' + data.msgInfo + '</p>',
                              type: 'success',
                              showConfirmButton: true,
                              showCancelButton: false,
                              showCloseButton: true,
                              confirmButtonText: 'ตกลง',
                              cancelButtonText: 'ยกเลิก',
                              confirmButtonClass: 'btn btn-primary',
                              cancelButtonClass: 'btn btn-reverse',
                              buttonsStyling: false,
                              allowOutsideClick: true
                        });

                        $('#viewEmsTrackingBtn').trigger('click');

                        setEmsTrackingData(data.data);
                  } else {
                        swal({
                              html: '<p>' + data.msgInfo + '</p>',
                              type: 'error',
                              showConfirmButton: true,
                              showCancelButton: false,
                              showCloseButton: true,
                              confirmButtonText: 'ตกลง',
                              cancelButtonText: 'ยกเลิก',
                              confirmButtonClass: 'btn btn-primary',
                              cancelButtonClass: 'btn btn-reverse',
                              buttonsStyling: false,
                              allowOutsideClick: true
                        });
                  }
            });
      }

      function saveArticle() {
            var formData = new FormData();
            var imagePathFileRef = Math.random().toString(36).substr(2);
            apiUrl = 'api/articleAPI.php';

            if ($('#articleForm #saveArticleBtn').attr('data-save-status') == 'save') {
                  apiParams = {
                        fn: 'saveArticle',
                        insertArticle: {
                              tableName: 'article',
                              data: [{
                                    article_title: $('#articleForm #articleName').val(),
                                    detail: tinyMCE.get('articleContent').getContent(),
                                    category: $('#articleForm #articleCategories').val(),
                                    image_cover: '',
                                    date: 'now'
                              }]
                        }
                  };

                  //--Detect field 'image_cover'
                  if ($('input[name="imagePathFile"]')[0].files[0]) {
                        apiParams['insertArticle']['data'][0]['image_cover'] = imagePathFileRef;
                        formData.append(imagePathFileRef, $('input[name="imagePathFile"]')[0].files[0]);
                  }
            } else {
                  apiParams = {
                        fn: 'updateArticle',
                        updateArticle: {
                              tableName: 'article',
                              primaryKey: 'article_id',
                              data: [{
                                    article_id: $('#articleForm #saveArticleBtn').attr('data-save-id'),
                                    article_title: $('#articleForm #articleName').val(),
                                    detail: tinyMCE.get('articleContent').getContent(),
                                    category: $('#articleForm #articleCategories').val()
                              }]
                        }
                  };

                  //--Detect field 'image_cover'
                  if ($('input[name="imagePathFile"]')[0].files[0]) {
                        apiParams['updateArticle']['data'][0]['image_cover'] = imagePathFileRef;
                        formData.append(imagePathFileRef, $('input[name="imagePathFile"]')[0].files[0]);
                  }
            }

            formData.append('apiParams', JSON.stringify(apiParams));
            console.log(apiParams);

            Service.prototype.callAPIWithFormData(apiUrl, formData, true).done(function (res) {
                  var data = JSON.parse(res);
                  console.log(data);

                  if (data.status) {
                        swal({
                              html: '<p>' + data.msgInfo + '</p>',
                              type: 'success',
                              showConfirmButton: true,
                              showCancelButton: false,
                              showCloseButton: true,
                              confirmButtonText: 'ตกลง',
                              cancelButtonText: 'ยกเลิก',
                              confirmButtonClass: 'btn btn-primary',
                              cancelButtonClass: 'btn btn-reverse',
                              buttonsStyling: false,
                              allowOutsideClick: true
                        });

                        $('#viewArticleBtn').trigger('click');

                        setArticleData(data.data);
                  } else {
                        swal({
                              html: '<p>' + data.msgInfo + '</p>',
                              type: 'error',
                              showConfirmButton: true,
                              showCancelButton: false,
                              showCloseButton: true,
                              confirmButtonText: 'ตกลง',
                              cancelButtonText: 'ยกเลิก',
                              confirmButtonClass: 'btn btn-primary',
                              cancelButtonClass: 'btn btn-reverse',
                              buttonsStyling: false,
                              allowOutsideClick: true
                        });
                  }
            });
      }

      function savePhoneFormat() {
            apiUrl = 'api/settingAPI.php';
            apiParams = {
                  fn: 'updatePhoneFormat',
                  updatePhoneFormat: {
                        tableName: 'setting',
                        primaryKey: 'Id',
                        data: [{
                              Id: $('#phoneFormatForm #savePhoneFormatBtn').attr('data-save-id'),
                              phone_format: $('#phoneFormatForm #phoneFormat').val()
                        }]
                  }
            };
            // console.log(apiParams);

            Service.prototype.callAPI(apiUrl, apiParams, 'post', true).done(function (res) {
                  var data = JSON.parse(res);
                  console.log(data);

                  if (data.status) {
                        swal({
                              html: '<p>' + data.msgInfo + '</p>',
                              type: 'success',
                              showConfirmButton: true,
                              showCancelButton: false,
                              showCloseButton: true,
                              confirmButtonText: 'ตกลง',
                              cancelButtonText: 'ยกเลิก',
                              confirmButtonClass: 'btn btn-primary',
                              cancelButtonClass: 'btn btn-reverse',
                              buttonsStyling: false,
                              allowOutsideClick: true
                        });

                        setPhoneFormatData(data.data);
                  } else {
                        swal({
                              html: '<p>' + data.msgInfo + '</p>',
                              type: 'error',
                              showConfirmButton: true,
                              showCancelButton: false,
                              showCloseButton: true,
                              confirmButtonText: 'ตกลง',
                              cancelButtonText: 'ยกเลิก',
                              confirmButtonClass: 'btn btn-primary',
                              cancelButtonClass: 'btn btn-reverse',
                              buttonsStyling: false,
                              allowOutsideClick: true
                        });
                  }
            });
      }

      function saveAccount() {
            var formData = new FormData();
            var imagePathFileRef = Math.random().toString(36).substr(2);
            apiUrl = 'api/accountAPI.php';
            apiParams = {
                  fn: 'updateAccount',
                  userID: credentialData['user_session'],
                  updateAccount: {
                        tableName: 'users',
                        primaryKey: 'user_id',
                        data: [{
                              user_id: $('#accountForm #saveAccountBtn').attr('data-save-id'),
                              user_name: $('#accountForm #accountName').val(),
                              user_fullname: $('#accountForm #accountFullName').val(),
                              user_tel: $('#accountForm #accountTel').val()
                        }]
                  }
            };

            //--Detect field 'user_galary_path'
            if ($('input[name="imagePathFile"]')[0].files[0]) {
                  apiParams['updateAccount']['data'][0]['user_galary_path'] = imagePathFileRef;
                  formData.append(imagePathFileRef, $('input[name="imagePathFile"]')[0].files[0]);
            }

            //--Detect field 'user_password'
            if ($('#accountForm #accountPassword').val() || $('#accountForm #accountPasswordConfirm').val()) {
                  if ($('#accountForm #accountPassword').val() !== $('#accountForm #accountPasswordConfirm').val()) {
                        swal({
                              html: '<p>รหัสผ่านต้องตรงกัน</p>',
                              type: 'warning',
                              showConfirmButton: true,
                              showCancelButton: false,
                              showCloseButton: true,
                              confirmButtonText: 'ตกลง',
                              cancelButtonText: 'ยกเลิก',
                              confirmButtonClass: 'btn btn-primary',
                              cancelButtonClass: 'btn btn-reverse',
                              buttonsStyling: false,
                              allowOutsideClick: true
                        });

                        return false;
                  } else {
                        apiParams['updateAccount']['data'][0]['user_password'] = $('#accountForm #accountPasswordConfirm').val();
                  }
            }

            formData.append('apiParams', JSON.stringify(apiParams));
            console.log(apiParams);

            Service.prototype.callAPIWithFormData(apiUrl, formData, true).done(function (res) {
                  var data = JSON.parse(res);
                  console.log(data);

                  if (data.status) {
                        swal({
                              html: '<p>' + data.msgInfo + '</p>',
                              type: 'success',
                              showConfirmButton: true,
                              showCancelButton: false,
                              showCloseButton: true,
                              confirmButtonText: 'ตกลง',
                              cancelButtonText: 'ยกเลิก',
                              confirmButtonClass: 'btn btn-primary',
                              cancelButtonClass: 'btn btn-reverse',
                              buttonsStyling: false,
                              allowOutsideClick: true
                        });

                        //--Set credential data
                        var credentialDataOptions = {
                              user_session: data.data['user_id'],
                              user_email: data.data['user_email'],
                              user_name: data.data['user_name'],
                              user_galary_path: data.data['user_galary_path']
                        };
                        sessionStorage.setItem('credentialData', JSON.stringify(credentialDataOptions));
                        $('.navbar-nav #username').text(data.data['user_name']);
                        $('.navbar-nav #email').text(data.data['user_email']);
                        $('.navbar-nav .user-galary-path').attr('src', (data.data['user_galary_path']) ? data.data['user_galary_path'] : 'uploads/no-image-available.png');

                        setAccountData(data.data);
                  } else {
                        swal({
                              html: '<p>' + data.msgInfo + '</p>',
                              type: 'error',
                              showConfirmButton: true,
                              showCancelButton: false,
                              showCloseButton: true,
                              confirmButtonText: 'ตกลง',
                              cancelButtonText: 'ยกเลิก',
                              confirmButtonClass: 'btn btn-primary',
                              cancelButtonClass: 'btn btn-reverse',
                              buttonsStyling: false,
                              allowOutsideClick: true
                        });
                  }
            });
      }

      function saveCategories() {
            apiUrl = 'api/categoriesAPI.php';

            if ($('#categoriesForm #saveCategoriesBtn').attr('data-save-status') == 'save') {
                  apiParams = {
                        fn: 'saveCategories',
                        insertCategories: {
                              tableName: 'neo_product_category',
                              data: [{
                                    name: $('#categoriesForm #categoriesName').val(),
                                    status: 'y',
                                    counts: '',
                                    type: '1',
                                    home: 'y'
                              }]
                        }
                  };
            } else {
                  apiParams = {
                        fn: 'updateCategories',
                        updateCategories: {
                              tableName: 'neo_product_category',
                              primaryKey: 'catid',
                              data: [{
                                    catid: $('#categoriesForm #saveCategoriesBtn').attr('data-save-id'),
                                    name: $('#categoriesForm #categoriesName').val()
                              }]
                        }
                  };
            }
            // console.log(apiParams);

            Service.prototype.callAPI(apiUrl, apiParams, 'post', true).done(function (res) {
                  var data = JSON.parse(res);
                  // console.log(data);

                  if (data.status) {
                        swal({
                              html: '<p>' + data.msgInfo + '</p>',
                              type: 'success',
                              showConfirmButton: true,
                              showCancelButton: false,
                              showCloseButton: true,
                              confirmButtonText: 'ตกลง',
                              cancelButtonText: 'ยกเลิก',
                              confirmButtonClass: 'btn btn-primary',
                              cancelButtonClass: 'btn btn-reverse',
                              buttonsStyling: false,
                              allowOutsideClick: true
                        });

                        $('#viewCategoriesBtn').trigger('click');

                        setCategoriesData(data.data);
                  } else {
                        swal({
                              html: '<p>' + data.msgInfo + '</p>',
                              type: 'error',
                              showConfirmButton: true,
                              showCancelButton: false,
                              showCloseButton: true,
                              confirmButtonText: 'ตกลง',
                              cancelButtonText: 'ยกเลิก',
                              confirmButtonClass: 'btn btn-primary',
                              cancelButtonClass: 'btn btn-reverse',
                              buttonsStyling: false,
                              allowOutsideClick: true
                        });
                  }
            });
      }

      function saveSidebarManageWithCategories() {
            apiUrl = 'api/sidebarManageAPI.php';

            if ($('#sidebarCategoriesForm .save-sidebar-btn').attr('data-save-status') == 'save') {
                  var insertSidebarCategoriesGroupData = [];

                  $($('#sidebarCategoriesForm #sidebarCategoriesType').val()).each(function (index, categories) {
                        insertSidebarCategoriesGroupData.push({
                              npc_id: categories
                        });
                  });

                  apiParams = {
                        fn: 'saveSidebarManage',
                        type: 'categories',
                        insertSidebarCategories: {
                              tableName: 'sidebar_categories',
                              data: [{
                                    sidebar_categories_name: $('#sidebarCategoriesForm #sidebarCategoriesName').val(),
                                    sidebar_categories_piority: $('#sidebarCategoriesForm #sidebarCategoriesPiority').val(),
                                    insertSidebarCategoriesGroup: {
                                          tableName: 'sidebar_categories_group',
                                          primaryKey: 'sc_id',
                                          data: insertSidebarCategoriesGroupData
                                    }
                              }]
                        }
                  };
            } else {
                  apiParams = {
                        fn: 'updateSidebarManage',
                        type: 'categories',
                        updateSidebarCategories: {
                              tableName: 'sidebar_categories',
                              primaryKey: 'sidebar_categories_id',
                              data: [{
                                    sidebar_categories_id: $('#sidebarCategoriesForm .save-sidebar-btn').attr('data-save-id'),
                                    sidebar_categories_name: $('#sidebarCategoriesForm #sidebarCategoriesName').val(),
                                    sidebar_categories_piority: $('#sidebarCategoriesForm #sidebarCategoriesPiority').val()
                              }]
                        },
                        deleteSidebarCategoriesGroup: [{
                              tableName: 'sidebar_categories_group',
                              field: 'sc_id',
                              value: $('#sidebarCategoriesForm .save-sidebar-btn').attr('data-save-id')
                        }],
                        insertSidebarCategoriesGroup: {
                              tableName: 'sidebar_categories_group',
                              data: []
                        }
                  };

                  $($('#sidebarCategoriesForm #sidebarCategoriesType').val()).each(function (index, categories) {
                        apiParams['insertSidebarCategoriesGroup']['data'].push({
                              sc_id: $('#sidebarCategoriesForm .save-sidebar-btn').attr('data-save-id'),
                              npc_id: categories
                        });
                  });
            }
            console.log(apiParams);

            Service.prototype.callAPI(apiUrl, apiParams, 'post', true).done(function (res) {
                  console.log(res);
                  var data = JSON.parse(res);
                  // console.log(data);

                  if (data.status) {
                        swal({
                              html: '<p>' + data.msgInfo + '</p>',
                              type: 'success',
                              showConfirmButton: true,
                              showCancelButton: false,
                              showCloseButton: true,
                              confirmButtonText: 'ตกลง',
                              cancelButtonText: 'ยกเลิก',
                              confirmButtonClass: 'btn btn-primary',
                              cancelButtonClass: 'btn btn-reverse',
                              buttonsStyling: false,
                              allowOutsideClick: true
                        });

                        $('.view-sidebar-btn[data-sidebar-type="categories"]').trigger('click');

                        setSidebarManageWithCategoriesData(data.data);
                  } else {
                        swal({
                              html: '<p>' + data.msgInfo + '</p>',
                              type: 'error',
                              showConfirmButton: true,
                              showCancelButton: false,
                              showCloseButton: true,
                              confirmButtonText: 'ตกลง',
                              cancelButtonText: 'ยกเลิก',
                              confirmButtonClass: 'btn btn-primary',
                              cancelButtonClass: 'btn btn-reverse',
                              buttonsStyling: false,
                              allowOutsideClick: true
                        });
                  }
            });
      }

      function saveSidebarManageWithPrice() {
            apiUrl = 'api/sidebarManageAPI.php';

            if ($('#sidebarPriceForm .save-sidebar-btn').attr('data-save-status') == 'save') {
                  apiParams = {
                        fn: 'saveSidebarManage',
                        type: 'price',
                        insertSidebarPrice: {
                              tableName: 'sidebar_price',
                              data: [{
                                    sidebar_price_name: $('#sidebarPriceForm #sidebarPriceName').val(),
                                    sidebar_price_begin: $('#sidebarPriceForm #sidebarPriceBegin').val(),
                                    sidebar_price_end: $('#sidebarPriceForm #sidebarPriceEnd').val(),
                                    sidebar_price_piority: $('#sidebarPriceForm #sidebarPricePiority').val()
                              }]
                        }
                  };
            } else {
                  apiParams = {
                        fn: 'updateSidebarManage',
                        type: 'price',
                        updateSidebarPrice: {
                              tableName: 'sidebar_price',
                              primaryKey: 'sidebar_price_id',
                              data: [{
                                    sidebar_price_id: $('#sidebarPriceForm .save-sidebar-btn').attr('data-save-id'),
                                    sidebar_price_name: $('#sidebarPriceForm #sidebarPriceName').val(),
                                    sidebar_price_begin: $('#sidebarPriceForm #sidebarPriceBegin').val(),
                                    sidebar_price_end: $('#sidebarPriceForm #sidebarPriceEnd').val(),
                                    sidebar_price_piority: $('#sidebarPriceForm #sidebarPricePiority').val()
                              }]
                        }
                  };
            }
            console.log(apiParams);

            Service.prototype.callAPI(apiUrl, apiParams, 'post', true).done(function (res) {
                  console.log(res);
                  var data = JSON.parse(res);
                  // console.log(data);

                  if (data.status) {
                        swal({
                              html: '<p>' + data.msgInfo + '</p>',
                              type: 'success',
                              showConfirmButton: true,
                              showCancelButton: false,
                              showCloseButton: true,
                              confirmButtonText: 'ตกลง',
                              cancelButtonText: 'ยกเลิก',
                              confirmButtonClass: 'btn btn-primary',
                              cancelButtonClass: 'btn btn-reverse',
                              buttonsStyling: false,
                              allowOutsideClick: true
                        });

                        $('.view-sidebar-btn[data-sidebar-type="price"]').trigger('click');

                        setSidebarManageWithPriceData(data.data);
                  } else {
                        swal({
                              html: '<p>' + data.msgInfo + '</p>',
                              type: 'error',
                              showConfirmButton: true,
                              showCancelButton: false,
                              showCloseButton: true,
                              confirmButtonText: 'ตกลง',
                              cancelButtonText: 'ยกเลิก',
                              confirmButtonClass: 'btn btn-primary',
                              cancelButtonClass: 'btn btn-reverse',
                              buttonsStyling: false,
                              allowOutsideClick: true
                        });
                  }
            });
      }

      function saveSidebarManageWithCompound() {
            apiUrl = 'api/sidebarManageAPI.php';

            if ($('#sidebarCompoundForm .save-sidebar-btn').attr('data-save-status') == 'save') {
                  apiParams = {
                        fn: 'saveSidebarManage',
                        type: 'compound',
                        insertSidebarCompound: {
                              tableName: 'sidebar_compound',
                              data: [{
                                    sidebar_compound_name: $('#sidebarCompoundForm #sidebarCompoundName').val(),
                                    sidebar_compound_num: $('#sidebarCompoundForm #sidebarCompoundNum').val(),
                                    sidebar_compound_piority: $('#sidebarCompoundForm #sidebarCompoundPiority').val()
                              }]
                        }
                  };
            } else {
                  apiParams = {
                        fn: 'updateSidebarManage',
                        type: 'compound',
                        updateSidebarCompound: {
                              tableName: 'sidebar_compound',
                              primaryKey: 'sidebar_compound_id',
                              data: [{
                                    sidebar_compound_id: $('#sidebarCompoundForm .save-sidebar-btn').attr('data-save-id'),
                                    sidebar_compound_name: $('#sidebarCompoundForm #sidebarCompoundName').val(),
                                    sidebar_compound_num: $('#sidebarCompoundForm #sidebarCompoundNum').val(),
                                    sidebar_compound_piority: $('#sidebarCompoundForm #sidebarCompoundPiority').val()
                              }]
                        }
                  };
            }
            console.log(apiParams);

            Service.prototype.callAPI(apiUrl, apiParams, 'post', true).done(function (res) {
                  console.log(res);
                  var data = JSON.parse(res);
                  // console.log(data);

                  if (data.status) {
                        swal({
                              html: '<p>' + data.msgInfo + '</p>',
                              type: 'success',
                              showConfirmButton: true,
                              showCancelButton: false,
                              showCloseButton: true,
                              confirmButtonText: 'ตกลง',
                              cancelButtonText: 'ยกเลิก',
                              confirmButtonClass: 'btn btn-primary',
                              cancelButtonClass: 'btn btn-reverse',
                              buttonsStyling: false,
                              allowOutsideClick: true
                        });

                        $('.view-sidebar-btn[data-sidebar-type="compound"]').trigger('click');

                        setSidebarManageWithCompoundData(data.data);
                  } else {
                        swal({
                              html: '<p>' + data.msgInfo + '</p>',
                              type: 'error',
                              showConfirmButton: true,
                              showCancelButton: false,
                              showCloseButton: true,
                              confirmButtonText: 'ตกลง',
                              cancelButtonText: 'ยกเลิก',
                              confirmButtonClass: 'btn btn-primary',
                              cancelButtonClass: 'btn btn-reverse',
                              buttonsStyling: false,
                              allowOutsideClick: true
                        });
                  }
            });
      }

      function detectDetailFilter(filterData, filterKey, filterID) {
            var detectDetail = {};

            $(filterData).each(function (index, data) {
                  if (filterID == data[filterKey]) {
                        detectDetail = data;
                  }
            });

            return detectDetail;
      }

      function sumPhoneNumber(phoneNumber) {
            var phoneNumberSum = 0;

            if (phoneNumber) {
                  phoneNumber = phoneNumber.replace('-', '');

                  for (var i = 0; i < phoneNumber.length; i++) {
                        phoneNumberSum += parseInt(phoneNumber[i]);
                  }
            }

            return phoneNumberSum;
      }

      function convertDateToDB(date) {
            if ((/^(\d{2}\/\d{2}\/\d{4})$/g).test(date)) {
                  var newDate = (Number(date.split('/')[2]) - 543) + '-' + date.split('/')[1] + '-' + date.split('/')[0];

                  return newDate;
            } else {
                  return '';
            }
      }

      function convertDateToDisplay(date) {
            if ((/^(\d{4}\-\d{2}\-\d{2})$/g).test(date)) {
                  var newDate = date.split('-')[2] + '/' + date.split('-')[1] + '/' + (Number(date.split('-')[0]) + 543);

                  return newDate;
            } else {
                  return '';
            }
      }
});