<?php
	class DashboardAPI {
		function __construct() {
			require_once('dbAPI.php');

			$db = new DBAPI();
			$this->db = $db;
			$this->db->connectDb();

			if (isset($_POST['apiParams'])) {
				$data = json_decode($_POST['apiParams'], true);
			} else {
				$data = json_decode(file_get_contents('php://input'), true);
			}
			
			$fn = $data['fn'];
			$this->$fn($data);
		}

		function loadDashboardData($params) {
			$resData = [];

			//--เบอร์หมวดหมู่หลัก
			$sqlCmd = "SELECT COUNT(id) AS productsNums
					FROM neo_product";
			$query = $this->db->getObj($sqlCmd);
			$resData['productsNums'] = $query['productsNums'];

			//--เบอร์ที่ขายแล้ว
			$sqlCmd = "SELECT COUNT(id) AS soldPhoneNums
					FROM neo_product
					WHERE sold = '1'";
			$query = $this->db->getObj($sqlCmd);
			$resData['soldPhoneNums'] = $query['soldPhoneNums'];

			//--EMS Tracking
			$sqlCmd = "SELECT COUNT(ems_tracking_id) AS emsTrackingNums
					FROM ems_tracking";
			$query = $this->db->getObj($sqlCmd);
			$resData['emsTrackingNums'] = $query['emsTrackingNums'];

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}
	}

	$self = new DashboardAPI();
?>