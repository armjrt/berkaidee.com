<?php
	class HoroDegreeAPI {
		function __construct() {
			require_once('dbAPI.php');

			$db = new DBAPI();
			$this->db = $db;
			$this->db->connectDb();

			if (isset($_POST['apiParams'])) {
				$data = json_decode($_POST['apiParams'], true);
			} else {
				$data = json_decode(file_get_contents('php://input'), true);
			}
			
			$fn = $data['fn'];
			$this->$fn($data);
		}

		function loadDropdownData($params) {
			$resData = [];

			foreach ($params['filterData'] as $filter) {
				switch ($filter['filter']) {
					case 'type':
						$resData['typeList'] = [
							[ 'id' => 'g', 'text' => 'เลขดี' ],
							[ 'id' => 'r', 'text' => 'เลขปานกลาง' ],
							[ 'id' => 'b', 'text' => 'เลขไม่ดี' ]
						];

						break;
					case 'education':
					case 'work':
					case 'finance':
					case 'love':
					case 'lucky':
						$resData[$filter['filter'].'List'] = [
							[ 'id' => '0', 'text' => '0 ดาว' ],
							[ 'id' => '0.5', 'text' => '0.5 ดาว' ],
							[ 'id' => '1', 'text' => '1 ดาว' ],
							[ 'id' => '1.5', 'text' => '1.5 ดาว' ],
							[ 'id' => '2', 'text' => '2 ดาว' ],
							[ 'id' => '2.5', 'text' => '2.5 ดาว' ],
							[ 'id' => '3', 'text' => '3 ดาว' ],
							[ 'id' => '3.5', 'text' => '3.5 ดาว' ],
							[ 'id' => '4', 'text' => '4 ดาว' ],
							[ 'id' => '4.5', 'text' => '4.5 ดาว' ],
							[ 'id' => '5', 'text' => '5 ดาว' ]
						];

						break;
				}
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}

		function loadHoroDegreeData($params) {
			$resData = [];

			$sqlCmd = "SELECT Id, number, detail, percent, color, education, work, finance, love, lucky
					FROM forcast
					ORDER BY Id";
			$query = $this->db->getListObj($sqlCmd);

			$resData = [
				'status' => true,
				'msgInfo' => 'loadHoroDegreeData() is finished',
				'data' => $query
			];

			if (isset($params['msgInfo'])) {
				$resData['msgInfo'] = $params['msgInfo'];
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}

		function saveHoroDegree($params) {
			$resData = [];

			$query = $this->db->insertData($params['insertHoroDegree']['tableName'], $params['insertHoroDegree']['data']);

			if ($query['status']) {
				$params['msgInfo'] = 'บันทึกข้อมูลเรียบร้อย';
				$this->loadHoroDegreeData($params);

				return true;
			} else {
				$resData = [
					'status' => false,
					'msgInfo' => 'ไม่สามารถบันทึกข้อมูลได้',
					'data' => new stdClass
				];
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}

		function updateHoroDegree($params) {
			$resData = [];

			$query = $this->db->updateData($params['updateHoroDegree']['tableName'], $params['updateHoroDegree']['data'], $params['updateHoroDegree']['primaryKey']);

			if ($query['status']) {
				$params['msgInfo'] = 'บันทึกข้อมูลเรียบร้อย';
				$this->loadHoroDegreeData($params);

				return true;
			} else {
				$resData = [
					'status' => false,
					'msgInfo' => 'ไม่สามารถบันทึกข้อมูลได้',
					'data' => new stdClass
				];
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}

		function removeHoroDegree($params) {
			$resData = [];

			$query = $this->db->deleteData($params['removeHoroDegree']);

			if ($query['status']) {
				$params['msgInfo'] = 'ลบข้อมูลเรียบร้อย';
				$this->loadHoroDegreeData($params);

				return true;
			} else {
				$resData = [
					'status' => false,
					'msgInfo' => 'ไม่สามารถลบข้อมูลได้',
					'data' => new stdClass
				];
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}
	}

	$self = new HoroDegreeAPI();
?>