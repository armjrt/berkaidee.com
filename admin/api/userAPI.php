<?php
	class UserAPI {
		function __construct() {
			date_default_timezone_set('Asia/Bangkok');
			require_once('dbAPI.php');

			$db = new DBAPI();
			$this->db = $db;
			$this->db->connectDb();

			if (preg_match("/(localhost)/", $_SERVER['SERVER_NAME'])) {
				$this->appPath = 'http://'.$_SERVER['SERVER_NAME'].'/berkaidee';
			} else {
				$this->appPath = 'http://'.$_SERVER['SERVER_NAME'];
			}

			$data = json_decode(file_get_contents('php://input'), true);
			
			$fn = $data['fn'];
			$this->$fn($data);
		}

		function getSession() {
			$resData = [];
			$resData = $this->db->getSession();

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}

		function login($params) {
			$resData = [];

			$sqlCmd = "SELECT user_id, user_email, user_name, user_galary_path
					FROM users
					WHERE user_email = '".$params['email']."'
					AND user_password = MD5('".$params['password']."')";
			$item = $this->db->getObj($sqlCmd);

			if ($item) {
				if (!isset($_SESSION)) {
					session_start();
				}

				$_SESSION = [
					"user_session" => $item['user_id'],
					"user_email" => $item['user_email'],
					"user_name" => $item['user_name'],
					"user_galary_path" => $item['user_galary_path']
				];
				$resData = [
					'status' => true,
					'msgInfo' => 'บัญชีผู้ใช้ถูกต้อง',
					'data' => [
						'user_session' => $item['user_id'],
						'user_email' => $item['user_email'],
						'user_name' => $item['user_name'],
						'user_galary_path' => $item['user_galary_path']
					]
				];
			} else {
				$resData = [
					'status' => false,
					'msgInfo' => 'บัญชีผู้ใช้ไม่ถูกต้อง',
					'data' => new stdClass
				];
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}

		function logout() {
			$resData = [];
			$this->db->destroySession();

			$resData = [
				'status' => true,
				'msgInfo' => 'ออกจากระบบเรียบร้อย',
				'data' => new stdClass
			];

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}

		function checkEmailForResetPassword($params) {
			$resData = [];

			$sqlCmd = "SELECT user_id, user_email
					FROM users
					WHERE user_email = '".$params['email']."'";
			$query = $this->db->getObj($sqlCmd);

			if ($query['user_id']) {
				$resData = [
					'status' => true,
					'msgInfo' => 'checkEmailForResetPassword() is finished',
					'data' => $query
				];
			} else {
				$resData = [
					'status' => false,
					'msgInfo' => 'ไม่พบอีเมล์นี้ในระบบ',
					'data' => new stdClass()
				];
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}

		function sendMailForResetPassword($params) {
			$resData = [];
			$sendMailStatus = false;
			$dateCurr = date('d').'/'.date('m').'/'.substr((date('Y') + 543), 2).' '.date('H:i').' น.';

			//--Check is exists email
			$sqlCmd = "SELECT user_id, user_name, user_email, user_fullname
					FROM users
					WHERE user_email = '".$params['email']."'";
			$query = $this->db->getObj($sqlCmd);
			$accountData = $query;

			//--Save request for reset password
			if ($accountData['user_id']) {
				$accountData['dateReq'] = date('Y-m-d H:i:s');
				$accountData['dateExp'] = date('Y-m-d H:i:s', strtotime('+1 day'));
				$resetRequest = base64_encode(json_encode($accountData, JSON_UNESCAPED_UNICODE));

				$sqlCmd = "UPDATE users
						SET user_reset_req = '".$resetRequest."'
						WHERE user_id = '".$accountData['user_id']."'";
				$query = $this->db->getQuery($sqlCmd);
			} else {
				$resData['msgInfo'] = 'ไม่พบอีเมล์นี้ในระบบ, กรุณาลองอีกครั้ง';

				echo json_encode($resData, JSON_UNESCAPED_UNICODE);

				return false;
			}

			//--Send email confirm
			if ($query) {
				require_once('../assets/plugins/php-mailer/PHPMailerAutoload.php');

				$mail = new PHPMailer;
				$mail->CharSet = 'utf-8';
				$mail->IsSMTP();
				$mail->SMTPDebug = 0;
				$mail->Debugoutput = 'html';
				$mail->Host = 'smtp.gmail.com'; //--smtp.live.com | smtp.gmail.com
				$mail->Port = 465; //--587 | 465 | 25
				$mail->SMTPSecure = 'ssl'; //--tls | ssl
				$mail->SMTPAuth = true;
				$mail->AuthType = 'LOGIN';
				$mail->SMTPOptions = [
					'ssl' => [
						'verify_peer' => false,
						'verify_peer_name' => false,
						'allow_self_signed' => true
					]
				];
				$mail->Username = 'itsara.ra.cs@gmail.com';
				$mail->Password = base64_decode('MDk3MDEyNTA5MA==');
				$mail->SetFrom('itsara.ra.cs@gmail.com', 'Berkaidee');
				$mail->Subject = 'ยืนยันการรีเซ็ตรหัสผ่านเข้าใช้งาน Berkaidee ('.$dateCurr.')';

				$mail->AddEmbeddedImage('../assets/images/logo-solid.png', 'berkaideeIcon');

				$body = '<!DOCTYPE>
						<html>
							<body>
								<style type="text/css">
								body {
									background-color : #eff3f8;
									padding: 0;
									margin: 0;
								}

								html {
									-webkit-text-size-adjust: none;
									-ms-text-size-adjust: none;
								}

								@media only screen and (max-device-width: 680px),
								only screen and (max-width: 680px) {
									*[class="table_width_100"] {
										width: 96% !important;
									}
									*[class="border-right_mob"] {
										border-right: 1px solid #dddddd;
									}
									*[class="mob_100"] {
										width: 100% !important;
									}
									*[class="mob_center"] {
										text-align: center !important;
									}
									*[class="mob_center_bl"] {
										float: none !important;
										display: block !important;
										margin: 0px auto;
									}
									.iage_footer a {
										text-decoration: none;
										color: #929ca8;
									}
									img.mob_display_none {
										width: 0px !important;
										height: 0px !important;
										display: none !important;
									}
									img.mob_width_50 {
										width: 40% !important;
										height: auto !important;
									}
								}

								.table_width_100 {
									width: 680px;
								}
								</style>
								<div id="mailsub" class="notification" align="center">
								<table width="100%" border="0" cellspacing="0" cellpadding="0" style="min-width: 320px;">
									<tr>
										<td align="center" bgcolor="#eff3f8">
										<table border="0" cellspacing="0" cellpadding="0" class="table_width_100" width="100%" style="max-width: 680px; min-width: 300px;">
											<tr>
												<td>
												<!-- padding -->
												<div style="height: 100px; line-height: 80px; font-size: 10px;"> </div>
												</td>
											</tr>
											<!--header -->
											<tr>
												<td align="center" bgcolor="#ffffff">
												<!-- padding -->
												<div style="height: 30px; line-height: 30px; font-size: 10px;"> </div>
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td align="left">
														<div class="mob_center_bl" style="float: left; display: inline-block; width: 115px;">
															<table class="mob_center" width="115" border="0" cellspacing="0" cellpadding="0" align="left" style="border-collapse: collapse;">
																<tr>
																<td align="left" valign="middle">
																	<!-- padding -->
																	<div style="height: 20px; line-height: 20px; font-size: 10px;"> </div>
																	<table width="115" border="0" cellspacing="0" cellpadding="0">
																		<tr>
																		<td align="left" valign="top" class="mob_center">
																			<a href="'.$this->appPath.'" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 13px;">
																				<font face="Arial, Helvetica, sans-seri; font-size: 13px;" size="3" color="#596167">
																				<img src="cid:berkaideeIcon" width="" height="50" alt="Berkaidee" border="0" style="display: block;" />
																				</font>
																			</a>
																		</td>
																		</tr>
																	</table>
																</td>
																</tr>
															</table>
														</div>
														<div class="mob_center_bl" style="float: right; display: inline-block; width: 88px;">
															<table width="88" border="0" cellspacing="0" cellpadding="0" align="right" style="border-collapse: collapse;">
																<tr>
																<td align="right" valign="middle">
																	<!-- padding -->
																	<div style="height: 20px; line-height: 20px; font-size: 10px;"> </div>
																	<table width="100%" border="0" cellspacing="0" cellpadding="0">
																		<tr>
																		<td align="right">
																			<!--social -->

																			<!--social END-->
																		</td>
																		</tr>
																	</table>
																</td>
																</tr>
															</table>
														</div>
														<!-- Item END-->
														</td>
													</tr>
												</table>
												<!-- padding -->

												</td>
											</tr>
											<!--header END-->

											<!--content 1 -->
											<tr>
												<td align="center" bgcolor="#fff">
												<table width="90%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td align="center">
														<!-- padding -->

														<!-- padding -->
														<div style="height: 40px; line-height: 40px; font-size: 10px;"> </div>
														</td>
													</tr>
													<tr>
														<td align="left">
														<div style="line-height: 24px;">
															<font face="Arial, Helvetica, sans-serif" size="4" color="#57697e" style="font-size: 14px;">
																<span style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #57697e; line-height : 24px; font-weight:bold;">
																เรียนคุณ '.$accountData['user_fullname'].'
																<br>
																<br>
																</span>
																<span>
																	มีผู้ร้องขอให้ทำการรีเซ็ตรหัสผ่านใหม่ ที่ใช้สำหรับเข้าสู่ระบบ Berkaidee หากท่านเป็นผู้ร้องขอด้วยตนเอง
																	กรุณาคลิก link ด้านล่างนี้เพื่อทำการยืนยันการเปลี่ยนแปลงรหัสผ่านใหม่ภายใน 24 ชั่วโมง<br><br>
																	<a href="'.$this->appPath.'/admin/resetPassword.php?id='.$accountData['user_id'].'&reset_request='.$resetRequest.'">
																		'.$this->appPath.'/admin/resetPassword.php?id='.$accountData['user_id'].'&reset_request='.$resetRequest.'</a><br><br>
																	หากท่านไม่ได้ทำการร้องขอให้เปลี่ยนรหัสผ่านใหม่ด้วยตนเอง กรุณาเพิกเฉยต่อ email ฉบับนี้
																</span>
																<div style="height: 10px; line-height: 10px; font-size: 10px;"></div>
															</font>
														</div>

														<!-- padding -->
														<div style="height: 20px; line-height: 20px; font-size: 10px;"> </div>
														</td>
													</tr>

												</table>
												</td>
											</tr>
											<!--content 1 END-->
											<tr>
												<td>
												<!-- padding -->
												<div style="height: 100px; line-height: 80px; font-size: 10px;"> </div>
												</td>
											</tr>
										</table>
										</td>
									</tr>
								</table>
								</div>
							</body>
						</html>';

				$mail->MsgHTML($body);

				$mail->AddAddress($params['email']);

				if (!$mail->Send()) {
					$sendMailStatus = false;

					echo $mail->ErrorInfo;
				} else {
					$sendMailStatus = true;
				}
			}

			//--Send response data
			if ($sendMailStatus) {
				$resData = [
					'status' => true,
					'msgInfo' => 'ระบบได้ส่งลิ้งค์ยืนยันการรีเซ็ตรหัสผ่านแล้ว กรุณาตรวจสอบอีเมล์ของท่าน'
				];
			} else {
				$resData['msgInfo'] = 'ไม่สามารถดำเนินการได้, กรุณาลองอีกครั้ง';
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}

		function checkResetRequest($params) {
			$resData = [
				'status' => false,
				'msgInfo' => 'ไม่พบข้อมูลสำหรับการรีเซ็ตรหัสผ่าน'
			];
			$accountData = [];

			//--Check request for reset password
			$sqlCmd = "SELECT user_id, user_name, user_email, user_fullname, user_reset_req
					FROM users
					WHERE user_id = '".$params['userID']."'
					AND user_reset_req = '".$params['resetReq']."'";
			$query = $this->db->getObj($sqlCmd);

			//--Send response data
			if ($query['user_id']) {
				$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
				$newPassword = substr(str_shuffle($chars), 0, 8);

				$sqlCmd = "UPDATE users SET
							user_reset_req = '',
							user_password = MD5('".$newPassword."')
						WHERE user_id = '".$params['userID']."'";
				$query = $this->db->getQuery($sqlCmd);

				if ($query) {
					$resData = [
						'status' => true,
						'msgInfo' => 'รีเซ็ตรหัสผ่านเรียบร้อย',
						'data' => $newPassword
					];
				} else {
					$resData['msgInfo'] = 'ลบข้อมูลการรีเซ็ตรหัสผ่านไม่ได้';
				}
			} else {
				$sqlCmd = "SELECT user_id, user_name, user_email, user_fullname, user_reset_req
						FROM users
						WHERE user_id = '".$params['userID']."'";
				$query = $this->db->getObj($sqlCmd);
				$accountData = json_decode(base64_decode($query['user_reset_req']), true);

				if ($query['user_id']) {
					if (!((strtotime($params['dateReset']) > strtotime($accountData['dateReq'])) && (strtotime($params['dateReset']) < strtotime($accountData['dateExp'])))) {
						$sqlCmd = "UPDATE users
								SET user_reset_req = ''
								WHERE user_id = '".$params['userID']."'";
						$query = $this->db->getQuery($sqlCmd);

						if (!$query) {
							$resData['msgInfo'] = 'ลบข้อมูลการรีเซ็ตรหัสผ่านไม่ได้';
						}
					}
				}
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}
	}

	$self = new UserAPI();
?>