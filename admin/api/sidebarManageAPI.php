<?php
	class SidebarManageAPI {
		function __construct() {
			require_once('dbAPI.php');

			$db = new DBAPI();
			$this->db = $db;
			$this->db->connectDb();

			if (isset($_POST['apiParams'])) {
				$data = json_decode($_POST['apiParams'], true);
			} else {
				$data = json_decode(file_get_contents('php://input'), true);
			}
			
			$fn = $data['fn'];
			$this->$fn($data);
		}

		function loadDropdownData($params) {
			$resData = [];

			foreach ($params['filterData'] as $filter) {
				switch ($filter['filter']) {
					case 'categories':
						$sqlCmd = "SELECT catid AS id, name AS text, catid, name
								FROM neo_product_category
								ORDER BY catid";
						$query = $this->db->getListObj($sqlCmd);
						$resData['categoriesList'] = $query;

						break;
				}
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}

		function loadSidebarManageData($params) {
			$resData = [];
			$sidebarData;

			//--Check type for load data
			switch ($params['type']) {
				case 'categories':
					//--Get 'sidebar_categories'
					$sqlCmd = "SELECT sidebar_categories_id, sidebar_categories_name, sidebar_categories_piority
							FROM sidebar_categories
							ORDER BY sidebar_categories_piority";
					$sidebarData = $this->db->getListObj($sqlCmd);

					//--Get 'sidebar_categories_group'
					foreach ($sidebarData as $key => $val) {
						$sqlCmd = "SELECT scg.npc_id AS categories_id, npc.name AS categories_name
								FROM sidebar_categories_group scg
								INNER JOIN neo_product_category npc ON scg.npc_id = npc.catid
								WHERE scg.sc_id = '".$val['sidebar_categories_id']."'
								ORDER BY scg.npc_id";
						$categoriesData = $this->db->getListObj($sqlCmd);

						$sidebarData[$key]['categoriesData'] = $categoriesData;
					}

					break;
				case 'price':
					//--Get 'sidebar_price'
					$sqlCmd = "SELECT sidebar_price_id, sidebar_price_name, sidebar_price_begin, sidebar_price_end, sidebar_price_piority
							FROM sidebar_price
							ORDER BY sidebar_price_piority";
					$sidebarData = $this->db->getListObj($sqlCmd);

					break;
				case 'compound':
					//--Get 'sidebar_compound'
					$sqlCmd = "SELECT sidebar_compound_id, sidebar_compound_name, sidebar_compound_num, sidebar_compound_piority
							FROM sidebar_compound
							ORDER BY sidebar_compound_piority";
					$sidebarData = $this->db->getListObj($sqlCmd);

					break;
			}

			$resData = [
				'status' => true,
				'msgInfo' => 'loadSidebarManageData() is finished',
				'data' => $sidebarData
			];

			if (isset($params['msgInfo'])) {
				$resData['msgInfo'] = $params['msgInfo'];
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}

		function saveSidebarManage($params) {
			$resData = [];

			switch ($params['type']) {
				case 'categories':
					//--Insert table 'sidebar_categories', 'sidebar_categories_group'
					$query = $this->db->insertData($params['insertSidebarCategories']['tableName'], $params['insertSidebarCategories']['data']);

					break;
				case 'price':
					//--Insert table 'sidebar_price'
					$query = $this->db->insertData($params['insertSidebarPrice']['tableName'], $params['insertSidebarPrice']['data']);

					break;
				case 'compound':
					//--Insert table 'sidebar_compound'
					$query = $this->db->insertData($params['insertSidebarCompound']['tableName'], $params['insertSidebarCompound']['data']);

					break;
			}

			if ($query['status']) {
				$params['msgInfo'] = 'บันทึกข้อมูลเรียบร้อย';
				$this->loadSidebarManageData($params);

				return true;
			} else {
				$resData = [
					'status' => false,
					'msgInfo' => 'ไม่สามารถบันทึกข้อมูลได้',
					'data' => new stdClass
				];
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}

		function updateSidebarManage($params) {
			$resData = [];

			switch ($params['type']) {
				case 'categories':
					//--Refresh piority
					$piorityNew = $params['updateSidebarCategories']['data'][0]['sidebar_categories_piority'];
					$sqlCmd = "SELECT sidebar_categories_id, sidebar_categories_name, sidebar_categories_piority
							FROM sidebar_categories
							WHERE sidebar_categories_id = '".$params['updateSidebarCategories']['data'][0]['sidebar_categories_id']."'";
					$query = $this->db->getObj($sqlCmd);
					$piorityOld = $query['sidebar_categories_piority'];

					if ($piorityOld != $piorityNew) {
						if ($piorityOld > $piorityNew) {
							$sqlCmd = "SELECT sidebar_categories_id, sidebar_categories_name, sidebar_categories_piority
									FROM sidebar_categories
									WHERE sidebar_categories_piority BETWEEN '".$piorityNew."' AND '".($piorityOld - 1)."'";
							$piorityData = $this->db->getListObj($sqlCmd);

							foreach ($piorityData as $piority) {
								$sqlCmd = "UPDATE sidebar_categories
										SET sidebar_categories_piority = '".((int)$piority['sidebar_categories_piority'] + 1)."'
										WHERE sidebar_categories_id = '".$piority['sidebar_categories_id']."'";
								$query = $this->db->getQuery($sqlCmd);
							}

							$params['updateSidebarCategories']['data'][0]['sidebar_categories_piority'] = $piorityNew;
						} else if ($piorityOld < $piorityNew) {
							$sqlCmd = "SELECT sidebar_categories_id, sidebar_categories_name, sidebar_categories_piority
									FROM sidebar_categories
									WHERE sidebar_categories_piority BETWEEN '".($piorityOld + 1)."' AND '".$piorityNew."'";
							$piorityData = $this->db->getListObj($sqlCmd);

							foreach ($piorityData as $piority) {
								$sqlCmd = "UPDATE sidebar_categories
										SET sidebar_categories_piority = '".((int)$piority['sidebar_categories_piority'] - 1)."'
										WHERE sidebar_categories_id = '".$piority['sidebar_categories_id']."'";
								$query = $this->db->getQuery($sqlCmd);
							}

							$params['updateSidebarCategories']['data'][0]['sidebar_categories_piority'] = ($piorityNew - 1);
						}
					} else {
						$query = true;
					}

					if ($query) {
						//--Update table 'sidebar_categories', 'sidebar_categories_group'
						$query = $this->db->updateData($params['updateSidebarCategories']['tableName'], $params['updateSidebarCategories']['data'], $params['updateSidebarCategories']['primaryKey']);
					}

					//--Update table 'sidebar_categories_group'
					if ($query['status']) {
						$query = $this->db->deleteData($params['deleteSidebarCategoriesGroup']);
					}

					if ($query['status']) {
						$query = $this->db->insertData($params['insertSidebarCategoriesGroup']['tableName'], $params['insertSidebarCategoriesGroup']['data']);
					}

					break;
				case 'price':
					//--Update table 'sidebar_price'
					$query = $this->db->updateData($params['updateSidebarPrice']['tableName'], $params['updateSidebarPrice']['data'], $params['updateSidebarPrice']['primaryKey']);

					break;
				case 'compound':
					//--Update table 'sidebar_compound'
					$query = $this->db->updateData($params['updateSidebarCompound']['tableName'], $params['updateSidebarCompound']['data'], $params['updateSidebarCompound']['primaryKey']);

					break;
			}

			if ($query['status']) {
				$params['msgInfo'] = 'บันทึกข้อมูลเรียบร้อย';
				$this->loadSidebarManageData($params);

				return true;
			} else {
				$resData = [
					'status' => false,
					'msgInfo' => 'ไม่สามารถบันทึกข้อมูลได้',
					'data' => new stdClass
				];
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}

		function removeSidebarManage($params) {
			$resData = [];

			switch ($params['type']) {
				case 'categories':
					//--Delete table 'sidebar_categories', 'sidebar_categories_group'
					$query = $this->db->deleteData($params['removeSidebarCategorie']);

					break;
				case 'price':
					//--Delete table 'sidebar_price'
					$query = $this->db->deleteData($params['removeSidebarPrice']);

					break;
				case 'compound':
					//--Delete table 'sidebar_compound'
					$query = $this->db->deleteData($params['removeSidebarCompound']);

					break;
			}

			if ($query['status']) {
				$params['msgInfo'] = 'ลบข้อมูลเรียบร้อย';
				$this->loadSidebarManageData($params);

				return true;
			} else {
				$resData = [
					'status' => false,
					'msgInfo' => 'ไม่สามารถลบข้อมูลได้',
					'data' => new stdClass
				];
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}
	}

	$self = new SidebarManageAPI();
?>