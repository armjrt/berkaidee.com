<?php
	class ProductsAPI {
		function __construct() {
			require_once('dbAPI.php');

			$db = new DBAPI();
			$this->db = $db;
			$this->db->connectDb();

			if (isset($_POST['apiParams'])) {
				$data = json_decode($_POST['apiParams'], true);
			} else {
				$data = json_decode(file_get_contents('php://input'), true);
			}
			
			$fn = $data['fn'];
			$this->$fn($data);
		}

		function loadDropdownData($params) {
			$resData = [];

			foreach ($params['filterData'] as $filter) {
				switch ($filter['filter']) {
					case 'carrier':
						$sqlCmd = "SELECT carrier_id AS id, carrier_name AS text, carrier_id, carrier_name
								FROM carrier
								ORDER BY carrier_id";
						$query = $this->db->getListObj($sqlCmd);
						$resData['carrierList'] = $query;

						break;
					case 'categories':
						$sqlCmd = "SELECT catid AS id, name AS text, catid, name
								FROM neo_product_category
								ORDER BY catid";
						$query = $this->db->getListObj($sqlCmd);
						$resData['categoriesList'] = $query;

						break;
				}
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}

		function loadProductsData($params) {
			$resData = [];

			//--Get products
			$sqlCmd = "SELECT np.id, np.number1, np.pic1, np.price, np.sum1, np.ori_number1, np.product_detail, np.news, np.sold, c.carrier_name
					FROM neo_product np
					INNER JOIN carrier c ON np.pic1 = c.carrier_id
					WHERE sold != '1'
					ORDER BY np.id";
			$productsData = $this->db->getListObj($sqlCmd);

			//--Get catgories of products
			foreach ($productsData as $key => $val) {
				$sqlCmd = "SELECT npcg.npc_id AS categories_id, npc.name AS categories_name
						FROM neo_product_category_group npcg
						INNER JOIN neo_product_category npc ON npcg.npc_id = npc.catid
						WHERE npcg.np_id = '".$val['id']."'
						ORDER BY npcg.npc_id";
				$categoriesData = $this->db->getListObj($sqlCmd);

				$productsData[$key]['categoriesData'] = $categoriesData;
			}

			$resData = [
				'status' => true,
				'msgInfo' => 'loadProductsData() is finished',
				'data' => $productsData
			];

			if (isset($params['msgInfo'])) {
				$resData['msgInfo'] = $params['msgInfo'];
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}

		function saveProducts($params) {
			$resData = [];

			//--Insert table 'neo_product', 'neo_product_category_group'
			$query = $this->db->insertData($params['insertProducts']['tableName'], $params['insertProducts']['data']);

			if ($query['status']) {
				$params['msgInfo'] = 'บันทึกข้อมูลเรียบร้อย';
				$this->loadProductsData($params);

				return true;
			} else {
				$resData = [
					'status' => false,
					'msgInfo' => 'ไม่สามารถบันทึกข้อมูลได้',
					'data' => new stdClass
				];
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}

		function updateProducts($params) {
			$resData = [];

			//--Update table 'neo_product'
			$query = $this->db->updateData($params['updateProducts']['tableName'], $params['updateProducts']['data'], $params['updateProducts']['primaryKey']);

			//--Update table 'neo_product_category_group'
			if ($query['status'] && isset($params['deleteNeoProductCategoryGroup'])) {
				$query = $this->db->deleteData($params['deleteNeoProductCategoryGroup']);
			}

			if ($query['status'] && isset($params['insertNeoProductCategoryGroup'])) {
				$query = $this->db->insertData($params['insertNeoProductCategoryGroup']['tableName'], $params['insertNeoProductCategoryGroup']['data']);
			}

			if ($query['status']) {
				$params['msgInfo'] = 'บันทึกข้อมูลเรียบร้อย';
				$this->loadProductsData($params);

				return true;
			} else {
				$resData = [
					'status' => false,
					'msgInfo' => 'ไม่สามารถบันทึกข้อมูลได้',
					'data' => new stdClass
				];
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}

		function removeProducts($params) {
			$resData = [];

			$query = $this->db->deleteData($params['removeProducts']);

			if ($query['status']) {
				$params['msgInfo'] = 'ลบข้อมูลเรียบร้อย';
				$this->loadProductsData($params);

				return true;
			} else {
				$resData = [
					'status' => false,
					'msgInfo' => 'ไม่สามารถลบข้อมูลได้',
					'data' => new stdClass
				];
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}
	}

	$self = new ProductsAPI();
?>