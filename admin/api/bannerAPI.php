<?php
	class BannerAPI {
		function __construct() {
			require_once('dbAPI.php');

			$db = new DBAPI();
			$this->db = $db;
			$this->db->connectDb();

			if (isset($_POST['apiParams'])) {
				$data = json_decode($_POST['apiParams'], true);
			} else {
				$data = json_decode(file_get_contents('php://input'), true);
			}
			
			$fn = $data['fn'];
			$this->$fn($data);
		}

		function loadBannerData($params) {
			$resData = [];

			$sqlCmd = "SELECT banner_id, banner_name, banner_detail, banner_image_path
					FROM banner
					ORDER BY banner_id";
			$query = $this->db->getListObj($sqlCmd);

			$resData = [
				'status' => true,
				'msgInfo' => 'loadBannerData() is finished',
				'data' => $query
			];

			if (isset($params['msgInfo'])) {
				$resData['msgInfo'] = $params['msgInfo'];
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}

		function saveBanner($params) {
			$resData = [];
			
			if ($_FILES) {
				$targetDir = '../uploads/banner/';
				$targetFileType = substr($_FILES[$params['insertBanner']['data'][0]['banner_image_path']]['name'], strrpos($_FILES[$params['insertBanner']['data'][0]['banner_image_path']]['name'], '.') + 1);
				$targetFileName = date('Y-m-d').'_banner_'.$params['insertBanner']['data'][0]['banner_image_path'].'.'.$targetFileType;
				$targetFile = $targetDir.basename($targetFileName);
				
				if (move_uploaded_file($_FILES[$params['insertBanner']['data'][0]['banner_image_path']]['tmp_name'], $targetFile)) {
					$query['status'] = true;
					$params['insertBanner']['data'][0]['banner_image_path'] = 'uploads/banner/'.$targetFileName;

					$query = $this->db->insertData($params['insertBanner']['tableName'], $params['insertBanner']['data']);
				} else {
					$query['status'] = false;
				}
			} else {
				$params['insertBanner']['data'][0]['banner_image_path'] = '';

				$query = $this->db->insertData($params['insertBanner']['tableName'], $params['insertBanner']['data']);
			}

			if ($query['status']) {
				$params['msgInfo'] = 'บันทึกข้อมูลเรียบร้อย';
				$this->loadBannerData($params);

				return true;
			} else {
				$resData = [
					'status' => false,
					'msgInfo' => 'ไม่สามารถบันทึกข้อมูลได้',
					'data' => []
				];
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}

		function updateBanner($params) {
			$resData = [];

			if ($_FILES) {
				$sqlCmd = "SELECT banner_id, banner_image_path
						FROM banner
						WHERE banner_id = '".$params['updateBanner']['data'][0]['banner_id']."'
						ORDER BY banner_id";
				$query = $this->db->getObj($sqlCmd);
				
				if ($query['banner_id'] && $query['banner_image_path']) {
					$targetRemovePath = '../'.$query['banner_image_path'];

					if (file_exists($targetRemovePath)) {
						if (unlink($targetRemovePath)) {
							$query['status'] = true;
						} else {
							$query['status'] = false;
						}
					} else {
						$query['status'] = true;
					}
				}

				$targetDir = '../uploads/banner/';
				$targetFileType = substr($_FILES[$params['updateBanner']['data'][0]['banner_image_path']]['name'], strrpos($_FILES[$params['updateBanner']['data'][0]['banner_image_path']]['name'], '.') + 1);
				$targetFileName = date('Y-m-d').'_banner_'.$params['updateBanner']['data'][0]['banner_image_path'].'.'.$targetFileType;
				$targetFile = $targetDir.basename($targetFileName);
				
				if (move_uploaded_file($_FILES[$params['updateBanner']['data'][0]['banner_image_path']]['tmp_name'], $targetFile)) {
					$query['status'] = true;
					$params['updateBanner']['data'][0]['banner_image_path'] = 'uploads/banner/'.$targetFileName;

					$query = $this->db->updateData($params['updateBanner']['tableName'], $params['updateBanner']['data'], $params['updateBanner']['primaryKey']);
				} else {
					$query['status'] = false;
				}
			} else {
				// $params['updateBanner']['data'][0]['banner_image_path'] = '';

				$query = $this->db->updateData($params['updateBanner']['tableName'], $params['updateBanner']['data'], $params['updateBanner']['primaryKey']);
			}

			if ($query['status']) {
				$params['msgInfo'] = 'บันทึกข้อมูลเรียบร้อย';
				$this->loadBannerData($params);

				return true;
			} else {
				$resData = [
					'status' => false,
					'msgInfo' => 'ไม่สามารถบันทึกข้อมูลได้',
					'data' => []
				];
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}

		function removeBanner($params) {
			$resData = [];

			$sqlCmd = "SELECT banner_id, banner_image_path
					FROM banner
					WHERE banner_id = '".$params['removeBanner'][0]['value']."'
					ORDER BY banner_id";
			$query = $this->db->getObj($sqlCmd);
			
			if ($query['banner_id'] && $query['banner_image_path']) {
				$targetRemovePath = '../'.$query['banner_image_path'];

				if (file_exists($targetRemovePath)) {
					if (unlink($targetRemovePath)) {
						$query['status'] = true;
					} else {
						$query['status'] = false;
					}
				} else {
					$query['status'] = true;
				}
			} else {
				$query['status'] = true;
			}

			if ($query['status']) {
				$query = $this->db->deleteData($params['removeBanner']);
			}

			if ($query['status']) {
				$params['msgInfo'] = 'ลบข้อมูลเรียบร้อย';
				$this->loadBannerData($params);

				return true;
			} else {
				$resData = [
					'status' => false,
					'msgInfo' => 'ไม่สามารถลบข้อมูลได้',
					'data' => []
				];
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}
	}

	$self = new BannerAPI();
?>