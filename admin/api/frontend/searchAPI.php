<?php
	class SearchAPI {
		function __construct() {
			require_once('../dbAPI.php');

			$db = new DBAPI();
			$this->db = $db;
			$this->db->connectDb();

			if (isset($_POST['apiParams'])) {
				$data = json_decode($_POST['apiParams'], true);
			} else {
				$data = json_decode(file_get_contents('php://input'), true);
			}
			
			$fn = $data['fn'];
			$this->$fn($data);
		}

		function searchData($params) {
			$resData = [
				'status' => false,
				'msgInfo' => 'searchData() is failed',
				'data' => [],
				'total' => 0,
				'totalPage' => 0
			];
			$filter = $params['filter'];

			//--Set pagination
			$page = isset($params['page']) ? $params['page'] : 1;
			$perPage = 50;
			$start = (($page - 1) * $perPage);
			$filter['start'] = $start;
			$filter['perPage'] = $perPage;

			$sqlCmd = $this->getSqlWithSearchData($filter, true);
			$data = $this->db->getListObj($sqlCmd);
			
			$sqlCmd = $this->getSqlWithSearchData($filter, false);
			$total = $this->db->getObj($sqlCmd);
			$totalPage = ceil($total['total'] / $perPage);

			$resData = [
				'status' => true,
				'msgInfo' => 'searchData() is finished',
				'data' => $data,
				'total' => (int)$total['total'],
				'totalPage' => $totalPage
			];

			if (isset($params['msgInfo'])) {
				$resData['msgInfo'] = $params['msgInfo'];
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}

		function loadSidebarData($params) {
			$resData = [
				'status' => false,
				'msgInfo' => 'loadSidebarData() is failed',
				'data' => [],
				'total' => 0,
				'totalPage' => 0
			];

			//--Set pagination
			$page = isset($params['page']) ? $params['page'] : 1;
			$perPage = 50;
			$start = (($page - 1) * $perPage);
			$params['start'] = $start;
			$params['perPage'] = $perPage;

			switch ($params['type']) {
				case 'categories':
					$sqlCmd = $this->getSqlWithLoadSidebarCategoriesData($params, true);
					$data = $this->db->getListObj($sqlCmd);
					
					$sqlCmd = $this->getSqlWithLoadSidebarCategoriesData($params, false);
					$total = $this->db->getObj($sqlCmd);
					$totalPage = ceil($total['total'] / $perPage);
					$title = 'เบอร์ทั้งหมด';

					if ($params['sidebarCategoriesID'] != 0) {
						$sqlCmd = "SELECT sidebar_categories_name
							FROM sidebar_categories
							WHERE sidebar_categories_id = '".$params['sidebarCategoriesID']."'";
						$title = $this->db->getObj($sqlCmd)['sidebar_categories_name'];
					}

					break;
				case 'price':
					$sqlCmd = $this->getSqlWithLoadSidebarPriceData($params, true);
					$data = $this->db->getListObj($sqlCmd);
					
					$sqlCmd = $this->getSqlWithLoadSidebarPriceData($params, false);
					$total = $this->db->getObj($sqlCmd);
					$totalPage = ceil($total['total'] / $perPage);

					$sqlCmd = "SELECT sidebar_price_name
							FROM sidebar_price
							WHERE sidebar_price_id = '".$params['sidebarPriceID']."'";
					$title = $this->db->getObj($sqlCmd)['sidebar_price_name'];
					
					break;
				case 'compound':
					$sqlCmd = $this->getSqlWithLoadSidebarCompoundData($params, true);
					$data = $this->db->getListObj($sqlCmd);
					
					$sqlCmd = $this->getSqlWithLoadSidebarCompoundData($params, false);
					$total = $this->db->getObj($sqlCmd);
					$totalPage = ceil($total['total'] / $perPage);

					$sqlCmd = "SELECT sidebar_compound_name
							FROM sidebar_compound
							WHERE sidebar_compound_id = '".$params['sidebarCompoundID']."'";
					$title = $this->db->getObj($sqlCmd)['sidebar_compound_name'];
					
					break;
			}
			
			$resData = [
				'status' => true,
				'msgInfo' => 'loadSidebarData() is finished',
				'data' => $data,
				'title' => $title,
				'total' => (int)$total['total'],
				'totalPage' => $totalPage
			];

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}

		function getSqlWithSearchData($filter = [], $limitStatus = false) {
			$sqlCmd = "SELECT ";

			if ($limitStatus) {
				$sqlCmd .= "np.id, np.number1, np.pic1, np.price, np.sum1, np.ori_number1, np.product_detail,
						c.carrier_name, c.carrier_image_path,
						fs.detail ";
			} else {
				$sqlCmd .= "COUNT(np.id) AS total ";
			}

			$sqlCmd .= "FROM neo_product np
					INNER JOIN carrier c ON np.pic1 = c.carrier_id
					INNER JOIN forcast_sum fs ON np.sum1 = fs.number
					WHERE np.sold = '0' ";

			//--ค้นหาบางเลข
			if ($filter['numberFilter']) {
				$sqlCmd .= "AND np.ori_number1 LIKE '%".$filter['numberFilter']."%' ";
			} else {
				//--ระบุเบอร์ที่ต้องการค้นหา
				$sqlCmd .= "AND np.ori_number1 LIKE '".$filter['numberPhone']."' ";

				//--เลือกเฉพาะเลข
				if (array_search(true, $filter['numberFavourite'])) {
					for ($i = 0; $i < count($filter['numberFavourite']); $i++) {
						if ($filter['numberFavourite'][$i]) {
							$sqlCmd .= "AND np.ori_number1 LIKE '___%".$i."%' ";
						} else {
							// $sqlCmd .= "AND np.ori_number1 NOT LIKE '___%".$i."%' ";
						}
					}
				}
			}

			//--งบประมาณ
			if ($filter['budget']) {
				$budgetBegin = explode('-', $filter['budget'])[0];
				$budgetEnd = explode('-', $filter['budget'])[1];

				if ($budgetEnd == 0) {
					$sqlCmd .= "AND np.price >= '".$budgetBegin."' ";
				} else {
					$sqlCmd .= "AND np.price BETWEEN '".$budgetBegin."' AND '".$budgetEnd."' ";
				}
			}

			//--เครือข่าย
			if ($filter['carrier']) {
				$sqlCmd .= "AND np.pic1 = '".$filter['carrier']."' ";
			}

			//--ผลรวม
			if ($filter['compound']) {
				$sqlCmd .= "AND np.sum1 = '".$filter['compound']."' ";
			}

			$sqlCmd .= "ORDER BY np.price DESC ";

			if ($limitStatus) {
				$sqlCmd .= "LIMIT ".$filter['start'].", ".$filter['perPage']."";
			}
			// echo $sqlCmd;

			return $sqlCmd;
		}

		function getSqlWithLoadSidebarCategoriesData($filter = [], $limitStatus = false) {
			//--Get categories
			if ($filter['sidebarCategoriesID'] == 0) {
				$sqlCmd = "SELECT catid AS npc_id
						FROM neo_product_category";
			} else {
				$sqlCmd = "SELECT sc.sidebar_categories_id, scg.npc_id
						FROM sidebar_categories sc
						INNER JOIN sidebar_categories_group scg ON sc.sidebar_categories_id = scg.sc_id
						WHERE sc.sidebar_categories_id = '".$filter['sidebarCategoriesID']."'";
			}

			$categoriesGroup = $this->db->getListObj($sqlCmd);
			$sqlCmd = "SELECT ";

			if ($limitStatus) {
				$sqlCmd .= "np.id, np.number1, np.pic1, np.price, np.sum1, np.ori_number1, np.product_detail, np.sold,
						fs.detail, c.carrier_name, c.carrier_image_path ";
			} else {
				$sqlCmd .= "COUNT(np.id) AS total ";
			}

			$sqlCmd .= "FROM neo_product np
					INNER JOIN forcast_sum fs ON np.sum1 = fs.number
					INNER JOIN carrier c ON np.pic1 = c.carrier_id
					INNER JOIN neo_product_category_group npcg ON np.id = npcg.np_id
					WHERE sold = '0'
					AND npcg.npc_id IN (";

					for ($i = 0; $i < count($categoriesGroup); $i++) {
						if ($i != 0) {
							$sqlCmd .= ", ";
						}

						$sqlCmd .= "'".$categoriesGroup[$i]['npc_id']."'";
					}

					$sqlCmd .= ")
					ORDER BY np.price DESC ";

			if ($limitStatus) {
				$sqlCmd .= "LIMIT ".$filter['start'].", ".$filter['perPage']."";
			}
			// echo $sqlCmd;

			return $sqlCmd;
		}

		function getSqlWithLoadSidebarPriceData($filter = [], $limitStatus = false) {
			//--Get price range
			$sqlCmd = "SELECT sidebar_price_id, sidebar_price_begin, sidebar_price_end
					FROM sidebar_price
					WHERE sidebar_price_id = '".$filter['sidebarPriceID']."'";
			$priceRange = $this->db->getObj($sqlCmd);

			//--Get products
			$sqlCmd = "SELECT ";

			if ($limitStatus) {
				$sqlCmd .= "np.id, np.number1, np.pic1, np.price, np.sum1, np.ori_number1, np.product_detail, np.sold,
						fs.detail, c.carrier_name, c.carrier_image_path ";
			} else {
				$sqlCmd .= "COUNT(np.id) AS total ";
			}

			$sqlCmd .= "FROM neo_product np
					INNER JOIN forcast_sum fs ON np.sum1 = fs.number
					INNER JOIN carrier c ON np.pic1 = c.carrier_id
					WHERE sold != '1' ";

			if ($priceRange['sidebar_price_end'] == 0) {
				$sqlCmd .= "AND np.price >= ".$priceRange['sidebar_price_begin']." ";
			} else {
				$sqlCmd .= "AND np.price BETWEEN ".$priceRange['sidebar_price_begin']." AND ".$priceRange['sidebar_price_end']." ";
			}
			
			$sqlCmd .= "ORDER BY np.price DESC ";

			if ($limitStatus) {
				$sqlCmd .= "LIMIT ".$filter['start'].", ".$filter['perPage']."";
			}
			// echo $sqlCmd;

			return $sqlCmd;
		}

		function getSqlWithLoadSidebarCompoundData($filter = [], $limitStatus = false) {
			//--Get compound number
			$sqlCmd = "SELECT sidebar_compound_id, sidebar_compound_num
					FROM sidebar_compound
					WHERE sidebar_compound_id = '".$filter['sidebarCompoundID']."'";
			$compoundNum = $this->db->getObj($sqlCmd);

			//--Get products
			$sqlCmd = "SELECT ";

			if ($limitStatus) {
				$sqlCmd .= "np.id, np.number1, np.pic1, np.price, np.sum1, np.ori_number1, np.product_detail, np.sold,
						fs.detail, c.carrier_name, c.carrier_image_path ";
			} else {
				$sqlCmd .= "COUNT(np.id) AS total ";
			}

			$sqlCmd .= "FROM neo_product np
					INNER JOIN forcast_sum fs ON np.sum1 = fs.number
					INNER JOIN carrier c ON np.pic1 = c.carrier_id
					WHERE sold != '1'
					AND np.sum1 = '".$compoundNum['sidebar_compound_num']."'
					ORDER BY np.price DESC ";

			if ($limitStatus) {
				$sqlCmd .= "LIMIT ".$filter['start'].", ".$filter['perPage']."";
			}
			// echo $sqlCmd;

			return $sqlCmd;
		}
	}

	$self = new SearchAPI();
?>