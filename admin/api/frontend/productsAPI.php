<?php
	class ProductsAPI {
		function __construct() {
			require_once('../dbAPI.php');

			$db = new DBAPI();
			$this->db = $db;
			$this->db->connectDb();

			if (isset($_POST['apiParams'])) {
				$data = json_decode($_POST['apiParams'], true);
			} else {
				$data = json_decode(file_get_contents('php://input'), true);
			}
			
			$fn = $data['fn'];
			$this->$fn($data);
		}

		function loadProductsData($params) {
			$resData = [
				'status' => false,
				'msgInfo' => 'loadProductsData() is failed',
				'data' => []
			];

			//--Get products with check sum of forcast
			$sqlCmd = "SELECT '".$params['productsNumber']."' AS ori_number1, '".$params['productsNumberSum']."' AS sum1, detail, percent, education, work, finance, love, lucky
					FROM forcast
					WHERE number = '".(strlen((string)$params['productsNumberSum']) == 1 ? '0'.$params['productsNumberSum'] : $params['productsNumberSum'])."'";
			$productsData = $this->db->getObj($sqlCmd);

			//--Get products with check sum of forcast_sum
			$sqlCmd = "SELECT detail, mean
					FROM forcast_sum
					WHERE number = '".$params['productsNumberSum']."'";
			$query = $this->db->getObj($sqlCmd);
			$productsData['detail_sum'] = $query['detail'];
			$productsData['mean'] = $query['mean'];

			//--Get horo compound of products
			$productsFilter = 0;
			//--ทำนายชุดที่ 1
			$productsFilter = substr($productsData['ori_number1'], 3, 2);
			$sqlCmd = "SELECT number, detail, percent
					FROM forcast
					WHERE number = '$productsFilter'";
			$productsData['horoCompound_1'] = $this->db->getObj($sqlCmd);

			//--ทำนายชุดที่ 2
			$productsFilter = substr($productsData['ori_number1'], 4, 2);
			$sqlCmd = "SELECT number, detail, percent
					FROM forcast
					WHERE number = '$productsFilter'";
			$productsData['horoCompound_2'] = $this->db->getObj($sqlCmd);

			//--ทำนายชุดที่ 3
			$productsFilter = substr($productsData['ori_number1'], 5, 2);
			$sqlCmd = "SELECT number, detail, percent
					FROM forcast
					WHERE number = '$productsFilter'";
			$productsData['horoCompound_3'] = $this->db->getObj($sqlCmd);

			//--ทำนายชุดที่ 4
			$productsFilter = substr($productsData['ori_number1'], 6, 2);
			$sqlCmd = "SELECT number, detail, percent
					FROM forcast
					WHERE number = '$productsFilter'";
			$productsData['horoCompound_4'] = $this->db->getObj($sqlCmd);

			//--ทำนายชุดที่ 5
			$productsFilter = substr($productsData['ori_number1'], 7, 2);
			$sqlCmd = "SELECT number, detail, percent
					FROM forcast
					WHERE number = '$productsFilter'";
			$productsData['horoCompound_5'] = $this->db->getObj($sqlCmd);

			//--ทำนายชุดที่ 6
			$productsFilter = substr($productsData['ori_number1'], 8, 2);
			$sqlCmd = "SELECT number, detail, percent
					FROM forcast
					WHERE number = '$productsFilter'";
			$productsData['horoCompound_6'] = $this->db->getObj($sqlCmd);

			$resData = [
				'status' => true,
				'msgInfo' => 'loadProductsData() is finished',
				'data' => $productsData
			];

			if (isset($params['msgInfo'])) {
				$resData['msgInfo'] = $params['msgInfo'];
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}

		function loadProductsDataWithDB($params) {
			$resData = [];

			//--Get products
			$sqlCmd = "SELECT np.id, np.number1, np.pic1, np.price, np.sum1, np.ori_number1, np.sold, c.carrier_name,
						f.detail, f.percent, f.education, f.work, f.finance, f.love, f.lucky,
						fs.detail AS detail_sum, fs.mean
					FROM neo_product np
					INNER JOIN carrier c ON np.pic1 = c.carrier_id
					INNER JOIN forcast f ON np.sum1 = f.number
					INNER JOIN forcast_sum fs ON np.sum1 = fs.number ";

					if ($params['productsNumber']) {
						$sqlCmd .= "WHERE np.ori_number1 = '".$params['productsNumber']."' ";
					}
					
					$sqlCmd .= "ORDER BY np.id";
			$productsData = $this->db->getListObj($sqlCmd);

			//--Get horo compound of products
			foreach ($productsData as $key => $val) {
				$productsFilter = 0;

				//--ทำนายชุดที่ 1
				$productsFilter = substr($val['ori_number1'], 3, 2);
				$sqlCmd = "SELECT number, detail, percent
						FROM forcast
						WHERE number = '$productsFilter'";
				$productsData[$key]['horoCompound_1'] = $this->db->getObj($sqlCmd);

				//--ทำนายชุดที่ 2
				$productsFilter = substr($val['ori_number1'], 4, 2);
				$sqlCmd = "SELECT number, detail, percent
						FROM forcast
						WHERE number = '$productsFilter'";
				$productsData[$key]['horoCompound_2'] = $this->db->getObj($sqlCmd);

				//--ทำนายชุดที่ 3
				$productsFilter = substr($val['ori_number1'], 5, 2);
				$sqlCmd = "SELECT number, detail, percent
						FROM forcast
						WHERE number = '$productsFilter'";
				$productsData[$key]['horoCompound_3'] = $this->db->getObj($sqlCmd);

				//--ทำนายชุดที่ 4
				$productsFilter = substr($val['ori_number1'], 6, 2);
				$sqlCmd = "SELECT number, detail, percent
						FROM forcast
						WHERE number = '$productsFilter'";
				$productsData[$key]['horoCompound_4'] = $this->db->getObj($sqlCmd);

				//--ทำนายชุดที่ 5
				$productsFilter = substr($val['ori_number1'], 7, 2);
				$sqlCmd = "SELECT number, detail, percent
						FROM forcast
						WHERE number = '$productsFilter'";
				$productsData[$key]['horoCompound_5'] = $this->db->getObj($sqlCmd);

				//--ทำนายชุดที่ 6
				$productsFilter = substr($val['ori_number1'], 8, 2);
				$sqlCmd = "SELECT number, detail, percent
						FROM forcast
						WHERE number = '$productsFilter'";
				$productsData[$key]['horoCompound_6'] = $this->db->getObj($sqlCmd);
			}

			//--Get catgories of products
			foreach ($productsData as $key => $val) {
				$sqlCmd = "SELECT npcg.npc_id AS categories_id, npc.name AS categories_name
						FROM neo_product_category_group npcg
						INNER JOIN neo_product_category npc ON npcg.npc_id = npc.catid
						WHERE npcg.np_id = '".$val['id']."'
						ORDER BY npcg.npc_id";
				$categoriesData = $this->db->getListObj($sqlCmd);

				$productsData[$key]['categoriesData'] = $categoriesData;
			}

			$resData = [
				'status' => true,
				'msgInfo' => 'loadProductsData() is finished',
				'data' => $productsData
			];

			if (isset($params['msgInfo'])) {
				$resData['msgInfo'] = $params['msgInfo'];
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}
	}

	$self = new ProductsAPI();
?>