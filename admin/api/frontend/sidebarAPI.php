<?php
	class SidebarAPI {
		function __construct() {
			require_once('../dbAPI.php');

			$db = new DBAPI();
			$this->db = $db;
			$this->db->connectDb();

			if (isset($_POST['apiParams'])) {
				$data = json_decode($_POST['apiParams'], true);
			} else {
				$data = json_decode(file_get_contents('php://input'), true);
			}
			
			$fn = $data['fn'];
			$this->$fn($data);
		}

		function loadSidebarData($params) {
			$resData = [];

			//--รูปแบบเบอร์
			$sqlCmd = "SELECT pfc.pfc_format
					FROM setting st
					INNER JOIN phone_format_categories pfc ON st.phone_format = pfc.pfc_id";
			$query = $this->db->getObj($sqlCmd);
			$resData['phoneFormat'] = $query['pfc_format'];

			//--หมวดหมู่เบอร์
			$sqlCmd = "SELECT sidebar_categories_id, sidebar_categories_name, sidebar_categories_piority
					FROM sidebar_categories
					ORDER BY sidebar_categories_piority";
			$query = $this->db->getListObj($sqlCmd);
			$resData['categoriesData'] = $query;

			//--เบอร์ตามช่วงราคา
			$sqlCmd = "SELECT sidebar_price_id, sidebar_price_name, sidebar_price_begin, sidebar_price_end, sidebar_price_piority
					FROM sidebar_price
					ORDER BY sidebar_price_piority";
			$query = $this->db->getListObj($sqlCmd);
			$resData['priceData'] = $query;

			//--เบอร์ตามผลรวม
			$sqlCmd = "SELECT sidebar_compound_id, sidebar_compound_name, sidebar_compound_num, sidebar_compound_piority
					FROM sidebar_compound
					ORDER BY sidebar_compound_piority";
			$query = $this->db->getListObj($sqlCmd);
			$resData['compoundData'] = $query;

			//--สถานะการจัดส่ง EMS
			$sqlCmd = "SELECT ems_tracking_id, ems_tracking_name, ems_tracking_number, ems_tracking_date
					FROM ems_tracking
					WHERE ems_tracking_date = CURDATE()
					LIMIT 5";
			$query = $this->db->getListObj($sqlCmd);
			$resData['emsTrackingData'] = $query;

			//--บทความแนะนำ
			$sqlCmd = "SELECT article_id, article_title, image_cover
					FROM article
					ORDER BY article_id DESC
					LIMIT 5";
			$query = $this->db->getListObj($sqlCmd);
			$resData['articleData'] = $query;

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}
	}

	$self = new SidebarAPI();
?>