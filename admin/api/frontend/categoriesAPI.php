<?php
	class CategoriesAPI {
		function __construct() {
			require_once('../dbAPI.php');

			$db = new DBAPI();
			$this->db = $db;
			$this->db->connectDb();

			if (isset($_POST['apiParams'])) {
				$data = json_decode($_POST['apiParams'], true);
			} else {
				$data = json_decode(file_get_contents('php://input'), true);
			}
			
			$fn = $data['fn'];
			$this->$fn($data);
		}

		function loadCategoriesData($params) {
			$resData = [
				'status' => false,
				'msgInfo' => 'loadCategoriesData() is failed',
				'data' => [],
				'total' => 0,
				'totalPage' => 0
			];

			//--Set pagination
			$page = isset($params['page']) ? $params['page'] : 1;
			$perPage = 50;
			$start = (($page - 1) * $perPage);
			$params['start'] = $start;
			$params['perPage'] = $perPage;

			$sqlCmd = $this->getSqlWithLoadCategoriesData($params, true);
			$data = $this->db->getListObj($sqlCmd);
			
			$sqlCmd = $this->getSqlWithLoadCategoriesData($params, false);
			$total = $this->db->getObj($sqlCmd);
			$totalPage = ceil($total['total'] / $perPage);
			// $total['total'] = 100;
			// $totalPage = ceil($total['total'] / $perPage);

			$resData = [
				'status' => true,
				'msgInfo' => 'loadCategoriesData() is finished',
				'data' => $data,
				'total' => (int)$total['total'],
				'totalPage' => $totalPage
			];

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}
// 		SELECT np.id, np.number1, np.pic1, np.price, np.sum1, np.ori_number1, np.product_detail, npcg.npc_id,c.carrier_name, c.carrier_image_path, fs.detail FROM neo_product np
// INNER JOIN carrier c ON np.pic1 = c.carrier_id
// INNER JOIN forcast_sum fs ON np.sum1 = fs.number
// INNER JOIN neo_product_category_group npcg ON np.id = npcg.np_id
// WHERE np.sold = '0' AND npcg.npc_id = '1' ORDER BY np.price DESC

		function getSqlWithLoadCategoriesData($filter = [], $limitStatus = false) {
			$sqlCmd = "SELECT ";

			if ($limitStatus) {
				$sqlCmd .= "np.id, np.number1, np.pic1, np.price, np.sum1, np.ori_number1, np.product_detail, npcg.npc_id,c.carrier_name, c.carrier_image_path, fs.detail ";
			} else {
				$sqlCmd .= "COUNT(np.id) AS total ";
			}

			$sqlCmd .= "FROM neo_product np
					INNER JOIN carrier c ON np.pic1 = c.carrier_id
					INNER JOIN forcast_sum fs ON np.sum1 = fs.number
					INNER JOIN neo_product_category_group npcg ON np.id = npcg.np_id
					WHERE np.sold = '0' ";

			if ($filter['categoriesID'] != 0) {
				$sqlCmd .= "AND npcg.npc_id = '".$filter['categoriesID']."'
						ORDER BY np.price DESC ";
			} else {
				if ($limitStatus) {
					$sqlCmd .= "GROUP BY np.id
							ORDER BY np.id DESC ";
				}
			}

			if ($limitStatus) {
				$sqlCmd .= "LIMIT ".$filter['start'].", ".$filter['perPage']."";
			}
			// echo $sqlCmd;

			return $sqlCmd;
		}
	}

	$self = new CategoriesAPI();
?>