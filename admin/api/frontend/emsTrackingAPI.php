<?php
	class EmsTrackingAPI {
		function __construct() {
			require_once('../dbAPI.php');

			$db = new DBAPI();
			$this->db = $db;
			$this->db->connectDb();

			if (isset($_POST['apiParams'])) {
				$data = json_decode($_POST['apiParams'], true);
			} else {
				$data = json_decode(file_get_contents('php://input'), true);
			}
			
			$fn = $data['fn'];
			$this->$fn($data);
		}

		function loadEmsTrackingData($params) {
			$resData = [
				'status' => false,
				'msgInfo' => 'loadEmsTrackingData() is failed',
				'data' => [],
				'total' => 0,
				'totalPage' => 0
			];

			//--Set pagination
			$page = isset($params['page']) ? $params['page'] : 1;
			$perPage = 10;
			$start = (($page - 1) * $perPage);
			$params['start'] = $start;
			$params['perPage'] = $perPage;

			$sqlCmd = $this->getSqlWithLoadEmsTrackingData($params, true);
			$data = $this->db->getListObj($sqlCmd);
			
			$sqlCmd = $this->getSqlWithLoadEmsTrackingData($params, false);
			$total = $this->db->getObj($sqlCmd);
			$totalPage = ceil($total['total'] / $perPage);

			$resData = [
				'status' => true,
				'msgInfo' => 'loadEmsTrackingData() is finished',
				'data' => $data,
				'total' => (int)$total['total'],
				'totalPage' => $totalPage
			];

			if (isset($params['msgInfo'])) {
				$resData['msgInfo'] = $params['msgInfo'];
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}

		function getSqlWithLoadEmsTrackingData($filter = [], $limitStatus = false) {
			//--Get categories
			$sqlCmd = "SELECT ";

			if ($limitStatus) {
				$sqlCmd .= "ems_tracking_id, ems_tracking_name, ems_tracking_number, ems_tracking_date ";
			} else {
				$sqlCmd .= "COUNT(ems_tracking_id) AS total ";
			}

			$sqlCmd .= "FROM ems_tracking
					ORDER BY ems_tracking_id DESC ";

			if ($limitStatus) {
				$sqlCmd .= "LIMIT ".$filter['start'].", ".$filter['perPage']."";
			}
			// echo $sqlCmd;

			return $sqlCmd;
		}
	}

	$self = new EmsTrackingAPI();
?>