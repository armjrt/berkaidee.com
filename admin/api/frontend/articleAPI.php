<?php
	class ArticleAPI {
		function __construct() {
			require_once('../dbAPI.php');

			$db = new DBAPI();
			$this->db = $db;
			$this->db->connectDb();

			if (isset($_POST['apiParams'])) {
				$data = json_decode($_POST['apiParams'], true);
			} else {
				$data = json_decode(file_get_contents('php://input'), true);
			}
			
			$fn = $data['fn'];
			$this->$fn($data);
		}

		function loadArticleData($params) {
			$resData = [
				'status' => false,
				'msgInfo' => 'loadArticleData() is failed',
				'data' => [],
				'total' => 0,
				'totalPage' => 0
			];

			//--Set pagination
			$page = isset($params['page']) ? $params['page'] : 1;
			$perPage = 8;
			$start = (($page - 1) * $perPage);
			$params['start'] = $start;
			$params['perPage'] = $perPage;

			$sqlCmd = $this->getSqlWithLoadArticleData($params, true);
			$data = $this->db->getListObj($sqlCmd);
			
			$sqlCmd = $this->getSqlWithLoadArticleData($params, false);
			$total = $this->db->getObj($sqlCmd);
			$totalPage = ceil($total['total'] / $perPage);

			$resData = [
				'status' => true,
				'msgInfo' => 'loadArticleData() is finished',
				'data' => $data,
				'total' => (int)$total['total'],
				'totalPage' => $totalPage
			];

			if (isset($params['msgInfo'])) {
				$resData['msgInfo'] = $params['msgInfo'];
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}

		function loadArticleDetailData($params) {
			$resData = [
				'status' => false,
				'msgInfo' => 'loadArticleDetailData() is failed',
				'data' => []
			];

			$sqlCmd = "SELECT a.article_id, a.article_title, a.detail, a.category, a.image_cover, a.date, a.user_view, npc.name AS categories_name
					FROM article a
					INNER JOIN neo_product_category npc ON a.category = npc.catid
					WHERE a.article_id = '".$params['articleID']."'";
			$query = $this->db->getObj($sqlCmd);

			//--Change path for view on webpage
			$query['detail'] = str_replace('src="uploads/article-content/', 'src="admin/uploads/article-content/', $query['detail']);

			$resData = [
				'status' => true,
				'msgInfo' => 'loadArticleDetailData() is finished',
				'data' => $query
			];

			if (isset($params['msgInfo'])) {
				$resData['msgInfo'] = $params['msgInfo'];
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}

		function getSqlWithLoadArticleData($filter = [], $limitStatus = false) {
			//--Get categories
			$sqlCmd = "SELECT ";

			if ($limitStatus) {
				$sqlCmd .= "a.article_id, a.article_title, a.detail, a.category, a.image_cover, a.date, a.user_view, npc.name AS categories_name ";
			} else {
				$sqlCmd .= "COUNT(a.article_id) AS total ";
			}

			$sqlCmd .= "FROM article a
					INNER JOIN neo_product_category npc ON a.category = npc.catid
					ORDER BY a.article_id DESC ";

			if ($limitStatus) {
				$sqlCmd .= "LIMIT ".$filter['start'].", ".$filter['perPage']."";
			}
			// echo $sqlCmd;

			return $sqlCmd;
		}
	}

	$self = new ArticleAPI();
?>