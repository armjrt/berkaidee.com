<?php
	class HomeAPI {
		function __construct() {
			require_once('../dbAPI.php');

			$db = new DBAPI();
			$this->db = $db;
			$this->db->connectDb();

			if (isset($_POST['apiParams'])) {
				$data = json_decode($_POST['apiParams'], true);
			} else {
				$data = json_decode(file_get_contents('php://input'), true);
			}
			
			$fn = $data['fn'];
			$this->$fn($data);
		}

		function loadHomeData($params) {
			$resData = [];

			//--แบนเนอร์
			$sqlCmd = "SELECT banner_id, banner_name, banner_detail, banner_image_path
					FROM banner
					ORDER BY banner_id";
			$query = $this->db->getListObj($sqlCmd);
			$resData['bannerData'] = $query;

			//--เบอร์มาใหม่
			$sqlCmd = "SELECT np.id, np.number1, np.pic1, np.price, np.sum1, np.ori_number1, np.product_detail, c.carrier_name, c.carrier_image_path, f.detail , npcg.npc_id , npc.icon_type , npc.name
					FROM neo_product np
					INNER JOIN carrier c ON np.pic1 = c.carrier_id
					INNER JOIN forcast f ON np.sum1 = f.number
					INNER JOIN neo_product_category_group npcg ON np.id = npcg.np_id
					INNER JOIN neo_product_category npc ON npcg.npc_id = npc.catid
					ORDER BY np.id DESC
					LIMIT 50";
			$query = $this->db->getListObj($sqlCmd);
			$resData['productsData'] = $query;

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}
	}

	$self = new HomeAPI();
?>