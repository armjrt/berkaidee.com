<?php
	class ContactUsAPI {
		function __construct() {
			require_once('../dbAPI.php');

			$db = new DBAPI();
			$this->db = $db;
			$this->db->connectDb();

			if (isset($_POST['apiParams'])) {
				$data = json_decode($_POST['apiParams'], true);
			} else {
				$data = json_decode(file_get_contents('php://input'), true);
			}
			
			$fn = $data['fn'];
			$this->$fn($data);
		}

		function sendMailToAdmin($params) {
			$resData = [
				'status' => false,
				'msgInfo' => 'sendMailToAdmin() is failed',
				'data' => ''
			];

			$mailTo = 'berkaidee@gmail.com';
			$subject = 'ติดต่อ Berkaidee จากคุณ '.$params['name'].' ('.$params['tel'].')';
			$header = 'From: '.$params['email'];
			$mailMessage = $params['detail'];
			$status = @mail($mailTo, $subject, $mailMessage, $header);

			if ($status) {
				$resData = [
					'status' => true,
					'msgInfo' => 'sendMailToAdmin() is finished',
					'data' => ''
				];
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}
	}

	$self = new ContactUsAPI();
?>