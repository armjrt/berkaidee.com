<?php
	class OrderProductAPI {
		function __construct() {
			require_once('../dbAPI.php');

			$db = new DBAPI();
			$this->db = $db;
			$this->db->connectDb();

			if (isset($_POST['apiParams'])) {
				$data = json_decode($_POST['apiParams'], true);
			} else {
				$data = json_decode(file_get_contents('php://input'), true);
			}
			
			$fn = $data['fn'];
			$this->$fn($data);
		}

		function loadOrderProductData($params) {
			$resData = [
				'status' => false,
				'msgInfo' => 'loadOrderProductData() is failed',
				'data' => []
			];

			$sqlCmd = "SELECT id, number1, ori_number1, price
					FROM neo_product
					WHERE id = '".$params['productID']."'";
			$data = $this->db->getObj($sqlCmd);

			$resData = [
				'status' => true,
				'msgInfo' => 'loadOrderProductData() is finished',
				'data' => $data
			];

			if (isset($params['msgInfo'])) {
				$resData['msgInfo'] = $params['msgInfo'];
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}
	}

	$self = new OrderProductAPI();
?>