<?php
	class AccountAPI {
		function __construct() {
			require_once('dbAPI.php');

			$db = new DBAPI();
			$this->db = $db;
			$this->db->connectDb();

			if (isset($_POST['apiParams'])) {
				$data = json_decode($_POST['apiParams'], true);
			} else {
				$data = json_decode(file_get_contents('php://input'), true);
			}
			
			$fn = $data['fn'];
			$this->$fn($data);
		}

		function loadAccountData($params) {
			$resData = [];

			$sqlCmd = "SELECT user_id, user_name, user_email, joining_date, user_fullname, user_tel, user_galary_path
					FROM users
					WHERE user_id = '".$params['userID']."'
					AND status = 'y'";
			$query = $this->db->getObj($sqlCmd);

			$resData = [
				'status' => true,
				'msgInfo' => 'loadAccountData() is finished',
				'data' => $query
			];

			if (isset($params['msgInfo'])) {
				$resData['msgInfo'] = $params['msgInfo'];
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}

		function updateAccount($params) {
			$resData = [];

			if ($_FILES) {
				$sqlCmd = "SELECT user_id, user_galary_path
						FROM users
						WHERE user_id = '".$params['updateAccount']['data'][0]['user_id']."'";
				$query = $this->db->getObj($sqlCmd);
				
				if ($query['user_id'] && $query['user_galary_path']) {
					$targetRemovePath = '../'.$query['user_galary_path'];

					if (file_exists($targetRemovePath)) {
						if (unlink($targetRemovePath)) {
							$query['status'] = true;
						} else {
							$query['status'] = false;
						}
					} else {
						$query['status'] = true;
					}
				}

				$targetDir = '../uploads/user/';
				$targetFileType = substr($_FILES[$params['updateAccount']['data'][0]['user_galary_path']]['name'], strrpos($_FILES[$params['updateAccount']['data'][0]['user_galary_path']]['name'], '.') + 1);
				$targetFileName = date('Y-m-d').'_user_'.$params['updateAccount']['data'][0]['user_galary_path'].'.'.$targetFileType;
				$targetFile = $targetDir.basename($targetFileName);
				
				if (move_uploaded_file($_FILES[$params['updateAccount']['data'][0]['user_galary_path']]['tmp_name'], $targetFile)) {
					$query['status'] = true;
					$params['updateAccount']['data'][0]['user_galary_path'] = 'uploads/user/'.$targetFileName;

					$query = $this->db->updateData($params['updateAccount']['tableName'], $params['updateAccount']['data'], $params['updateAccount']['primaryKey']);
				} else {
					$query['status'] = false;
				}
			} else {
				$query = $this->db->updateData($params['updateAccount']['tableName'], $params['updateAccount']['data'], $params['updateAccount']['primaryKey']);
			}

			if ($query['status']) {
				$params['msgInfo'] = 'บันทึกข้อมูลเรียบร้อย';
				$this->loadAccountData($params);

				return true;
			} else {
				$resData = [
					'status' => false,
					'msgInfo' => 'ไม่สามารถบันทึกข้อมูลได้',
					'data' => new stdClass
				];
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}
	}

	$self = new AccountAPI();
?>