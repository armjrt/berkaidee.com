<?php
	class SettingAPI {
		function __construct() {
			require_once('dbAPI.php');

			$db = new DBAPI();
			$this->db = $db;
			$this->db->connectDb();

			if (isset($_POST['apiParams'])) {
				$data = json_decode($_POST['apiParams'], true);
			} else {
				$data = json_decode(file_get_contents('php://input'), true);
			}
			
			$fn = $data['fn'];
			$this->$fn($data);
		}

		function loadDropdownData($params) {
			$resData = [];

			foreach ($params['filterData'] as $filter) {
				switch ($filter['filter']) {
					case 'phoneFormatCategories':
						$sqlCmd = "SELECT pfc_id AS id, pfc_format AS text, pfc_id, pfc_format
								FROM phone_format_categories
								ORDER BY pfc_format";
						$query = $this->db->getListObj($sqlCmd);
						$resData['phoneFormatCategoriesList'] = $query;

						break;
				}
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}

		function loadPhoneFormatData($params) {
			$resData = [];

			$sqlCmd = "SELECT Id, phone_format
					FROM setting";
			$query = $this->db->getObj($sqlCmd);

			$resData = [
				'status' => true,
				'msgInfo' => 'loadPhoneFormatData() is finished',
				'data' => $query
			];

			if (isset($params['msgInfo'])) {
				$resData['msgInfo'] = $params['msgInfo'];
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}

		function updatePhoneFormat($params) {
			$resData = [];

			$query = $this->db->updateData($params['updatePhoneFormat']['tableName'], $params['updatePhoneFormat']['data'], $params['updatePhoneFormat']['primaryKey']);

			if ($query['status']) {
				$params['msgInfo'] = 'บันทึกข้อมูลเรียบร้อย';
				$this->loadPhoneFormatData($params);

				return true;
			} else {
				$resData = [
					'status' => false,
					'msgInfo' => 'ไม่สามารถบันทึกข้อมูลได้',
					'data' => new stdClass
				];
			}

			echo json_encode($resData, JSON_UNESCAPED_UNICODE);
		}
	}

	$self = new SettingAPI();
?>