function Service() {
    console.log('This is Service');
}

Service.prototype.appPath = '/'+ window.location.pathname.split('/')[1];

Service.prototype.loadingContent = `<div class="loading" style="position: absolute;
                                        top: 0;
                                        width: 100%;
                                        height: 100%;
                                        text-align: center;
                                        background-color: #666;
                                        background-repeat: no-repeat;
                                        background-position: center;
                                        z-index: 10000000;
                                        opacity: 0.4;
                                        color: #FFFFFF;
                                        filter: alpha(opacity=40);">
                                    <i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw" style="top: 38%; position: absolute; color: #FFF; font-size: 5em;"></i>
                                    <h4 class="text-disabled" style="position: absolute;
                                        top: 50%;
                                        left: 53%;
                                        margin-top: 20px;
                                        transform: translate(-50%, -50%); color:#FFF; font-size:2em;">กรุณารอสักครู่...</h4>
                                    </div>`;

Service.prototype.callAPI = function(apiUrl, apiParams, apiType, requireLoading) {
    var self = this;
    var options = {};

    if (apiType == 'post') {
        options = {
            type: 'post',
            url: apiUrl,
            data: JSON.stringify(apiParams),
            cache: false,
            beforeSend: function() {
                if (requireLoading) {
                    $('body').append(self.loadingContent);
                }
            },
            success: function() {
                if (requireLoading) {
                    $('body .loading').remove();
                }
            }
        };
    } else {
        options = {
            type: 'get',
            url: apiUrl,
            async: true,
            crossDomain: true,
            beforeSend: function() {
                if (requireLoading) {
                    $('body').append(self.loadingContent);
                }
            },
            success: function() {
                if (requireLoading) {
                    $('body .loading').remove();
                }
            }
        };
    }

    return $.ajax(options);
}

Service.prototype.callAPIWithFormData = function(apiUrl, apiParams, requireLoading) {
    var self = this;
    var options = {};

    options = {
        type: 'post',
        url: apiUrl,
        data: apiParams,
        processData: false,
        contentType: false,
        beforeSend: function() {
            if (requireLoading) {
                $('body').append(self.loadingContent);
            }
        },
        success: function() {
            if (requireLoading) {
                $('body .loading').remove();
            }
        }
    };

    return $.ajax(options);
}

Service.prototype.checkAuth = function() {
    var credentialData = JSON.parse(sessionStorage.getItem('credentialData'));

    if (!credentialData) {
        window.open('login.php', '_self');
    }
}

Service.prototype.detectBreadcrumb = function() {
    var page = window.location.pathname.split('/')[(window.location.pathname.split('/').length - 1)];

    switch (page) {
        case 'dashboard':
            $('.menuName').text('แดชบอร์ด');
            $('.breadcrumb').addClass('hide');

            break;
        case 'products':
            $('.menuName').text('จัดการเบอร์ขายดี (จัดการเบอร์)');

            break;
        case 'categories':
            $('.menuName').text('จัดการเบอร์ขายดี (เพิ่มหมวดหมู่เบอร์)');

            break;
        case 'horo-degree':
            $('.menuName').text('คำทำนาย (เลขลำดับ)');

            break;
        case 'horo-compound':
            $('.menuName').text('คำทำนาย (เลขรวม)');

            break;
        case 'banner':
            $('#menuName').text('แบนเนอร์');

            break;
        case 'ems-tracking':
            $('.menuName').text('EMS Tracking');

            break;
        case 'article':
            $('.menuName').text('บทความ');

            break;
        case 'sidebar-manage':
            $('.menuName').text('จัดการเมนูบาร์');

            break;
        case 'phone-format':
            $('.menuName').text('ตั้งค่ารูปแบบเบอร์');

            break;
        case 'phone-sold':
            $('.menuName').text('เบอร์ที่ขายแล้ว');

            break;
        case 'account':
            $('.menuName').text('ข้อมูลส่วนตัว');

            break;
        default:
            $('.menuName').text('แดชบอร์ด');
            $('.breadcrumb').addClass('hide');

            break;
    }
}