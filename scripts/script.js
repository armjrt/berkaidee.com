$(document).ready(function () {
      //--Set variables
      var apiUrl = '';
      var apiParams = {};
      var appPath = '/' + window.location.pathname.split('/')[1];
      var page = window.location.pathname.split('/')[(window.location.pathname.split('/').length - 1)];
      var queryParams = (window.location.search.replace('?', '')).split('&');
      var pageCurr = ((queryParams.length == 1) ? queryParams[0].split('page=')[1] : queryParams[1].split('page=')[1]) || 1;
      var windowHeight = $(window).height();
      var htmlContent = '';
      var dropdownList = {};
      var detectData = [];
      var detectDetail = {};
      var phoneFormat = 'XXX-XXX-XXXX';

      //--Set initial
      setInit();

      //--Event
      $(document).on('keyup', '#numberSearchFilter', function (e) {
            if (!(/[0-9]/g).test($(this).val().substr(($(this).val().length - 1)))) {
                  $(this).val($(this).val().slice(0, -1));
            }
      });

      $(document).on('click', '#numberSearchFilterBtn', function (e) {
            e.preventDefault();

            if ($('#numberSearchFilter').val().length == 10) {
                  window.open('horo.php?number=' + $('#numberSearchFilter').val(), '_self');
            } else {
                  swal({
                        html: '<p>เบอร์โทรต้องมี 10 หลัก</p>',
                        showConfirmButton: true,
                        showCancelButton: false,
                        showCloseButton: true,
                        confirmButtonText: 'ตกลง',
                        cancelButtonText: 'ยกเลิก',
                        confirmButtonClass: 'btn btn-primary',
                        cancelButtonClass: 'btn btn-reverse',
                        buttonsStyling: false,
                        allowOutsideClick: true
                  });
            }
      });

      $(document).on('click', '#searchBtn', function (e) {
            e.preventDefault();

            var numberPhone = ($('#searchForm').find('#num').val() ? $('#searchForm').find('#num').val() : '_') +
                  ($('#searchForm').find('#num0').val() ? $('#searchForm').find('#num0').val() : '_') +
                  ($('#searchForm').find('#num1').val() ? $('#searchForm').find('#num1').val() : '_') +
                  ($('#searchForm').find('#num2').val() ? $('#searchForm').find('#num2').val() : '_') +
                  ($('#searchForm').find('#num3').val() ? $('#searchForm').find('#num3').val() : '_') +
                  ($('#searchForm').find('#num4').val() ? $('#searchForm').find('#num4').val() : '_') +
                  ($('#searchForm').find('#num5').val() ? $('#searchForm').find('#num5').val() : '_') +
                  ($('#searchForm').find('#num6').val() ? $('#searchForm').find('#num6').val() : '_') +
                  ($('#searchForm').find('#num7').val() ? $('#searchForm').find('#num7').val() : '_') +
                  ($('#searchForm').find('#num8').val() ? $('#searchForm').find('#num8').val() : '_');
            var numberFavourite = [
                  ($('#searchForm').find('#n0').is(':checked') ? true : false),
                  ($('#searchForm').find('#n1').is(':checked') ? true : false),
                  ($('#searchForm').find('#n2').is(':checked') ? true : false),
                  ($('#searchForm').find('#n3').is(':checked') ? true : false),
                  ($('#searchForm').find('#n4').is(':checked') ? true : false),
                  ($('#searchForm').find('#n5').is(':checked') ? true : false),
                  ($('#searchForm').find('#n6').is(':checked') ? true : false),
                  ($('#searchForm').find('#n7').is(':checked') ? true : false),
                  ($('#searchForm').find('#n8').is(':checked') ? true : false),
                  ($('#searchForm').find('#n9').is(':checked') ? true : false)
            ];
            var budget = $('#budget').val();
            var carrier = $('#system').val();
            var compound = $('#all').val();
            var numberFilter = $('#some').val();

            apiUrl = 'admin/api/frontend/searchAPI.php';
            apiParams = {
                  fn: 'searchData',
                  page: pageCurr,
                  filter: {
                        numberPhone: ((numberPhone != 0) ? numberPhone : ''), //--ระบุเบอร์ที่ต้องการค้นหา
                        numberFavourite: numberFavourite, //--เลือกเฉพาะเลข
                        budget: budget, //--งบประมาณ
                        carrier: carrier, //--เครือข่าย
                        compound: compound, //--ผลรวม
                        numberFilter: numberFilter //--ค้นหาบางเลข
                  }
            };
            console.log(apiParams);

            Service.prototype.callAPI(apiUrl, apiParams, 'post', true).done(function (res) {
                  var data = JSON.parse(res);
                  // console.log(data);

                  htmlContent = '';

                  if (data.data.length) {
                        $(data.data).each(function (index, products) {
                              htmlContent += 
                              '<div class="col-lg-6 col-md-12 my-2">'+
                                    '<div class="styleNumberList h-100">'
                                          +'<div class="row">'
                                                +'<div class="col-7 col-md-8 col-lg-8 pl-3">'
                                                      +'<p class="text-number font-28">'+products['ori_number1']+'</p>' 
                                                     
                                                +'</div>'
                                                +'<div class="col-5 col-sm-3 col-lg-3 text-center">'
                                                      +'<div>'
                                                      +'<img class="carrier-logo " src="admin/' 
                                                            +products['carrier_image_path'] + '" alt="' + products['carrier_name'] + '">' 
                                                      +'</div>'
                                                +'</div>'
                                                +'<div class="col-12">'
                                                      +'<div >'
                                                       +'<p>ราคา:<span class="stylePriceNumberList text-price">'+Number(products['price']).toLocaleString("en",{ minimumFractionDigits: 0}) +
                                                      '</span> ฿ <span class="sum"> (ผลรวม '+ 
                                                            '<span class="styleSumPoint text-total">'+ 
                                                                  products['sum1']
                                                      +     '</span> )'
                                                      +'</span> &nbsp;&nbsp;&nbsp;'+products['name']+'</p>'
                              
                                                      +'</div>'
                                                +'</div>'
                                          +'</div>'
                                          +'<div class="row d-flex justify-content-between">'
                                                +'<div class="col-5 col-lg-6 ">'
                                                      +'<div class="btn-buy h-100 d-flex justify-content-center align-items-center">'
                                                                + ' <a href="horo.php?number='+products['ori_number1']+'" class="btn btn-primary">'
                                                                + '<i class="fas fa-search iconSearch"></i>ทำนายเบอร์</a>'
                                                      +'</div>'
                                                +'</div>'
                                                +'<div class="col-5 col-lg-6 text-center">'
                                                      + '<div class="btn-buy">'
                                                            +'<a href="howto.php" class="btn btn-predict mt-0"><i class="fas fa-shopping-basket iconBasket"></i>'
                                                            +'สั่งซื้อ'
                                                            +'</a>'
                                                      +'</div>'
                                                +'</div>'+
                                          '</div>'
                                          +
                                    '</div>'
                                +'</div>'
                          
                              // '<div class="col-lg-6">'+
                              //       '<div class="styleNumberList">'
                              //       +   
                              //             '<div class="row">'
                              //             +
                              //                   '<div class="col-lg-7 pl-3">'
                              //                   +
                              //                         '</p>'+products['ori_number1']+'</p>' 
                              //                   +
                              //                         '<p>ราคา:<span class="stylePriceNumberList">'+Number(products['price']).toLocaleString("en",{ minimumFractionDigits: 0}) +
                              //                         '</span>฿ <span class="sum"> (ผลรวม'+ 
                              //                               '<span class="styleSumPoint">'+ 
                              //                                     products['sum1'] 
                              //                         +     '</span>)'
                              //                         +
                              //                         '</span></p>'
                              //                          + '<div class="btn-buy"> \
                              //                                     <a href="howto.php" class="btn btn-primary">สั่งซื้อ</a> \
                              //                            </div>'
                              //                   +
                              //                   '</div>'
                              //                   +
                              //                   '<div class="col-lg-5">'
                              //                   +
                              //                         '<img class="mr-3 carrier-logo" src="admin/' 
                              //                         + products['carrier_image_path'] + '" alt="' + products['carrier_name'] + '">'
                              //                   +
                              //                   '</div>'
                              //                   +
                              //             '</div>'
                              //             +
                              //       '</div>'
                              //   +'</div>'
                              // '<tr> \
                              //                         <td> \
                              //                               <img class="mr-3 carrier-logo" src="admin/' + products['carrier_image_path'] + '" alt="' + products['carrier_name'] + '"> \
                              //                         </td> \
                              //                         <td width="25%"> \
                              //                               <div class="showber"> \
                              //                                     <div class="mt-0 mb-1"> \
                              //                                           <span>' + getPhoneFormat(products['ori_number1']) + '</span> \
                              //                                           <span class="sum">(ผลรวม ' + products['sum1'] + ')</span> \
                              //                                     </div> \
                              //                               </div> \
                              //                         </td> \
                              //                         <td width="10%"> \
                              //                               <div class="berprice">' + Number(products['price']).toLocaleString('en', {
                              //       minimumFractionDigits: 0
                              // }) + ' ฿</div> \
                              //                         </td> \
                              //                         <td> \
                              //                               <div class="berdetail"> \
                              //                                     <p>' + products['product_detail'] + '</p> \
                              //                               </div> \
                              //                         </td> \
                              //                         <td> \
                              //                               <div class="btn-buy"> \
                              //                                     <a href="tel:+66814562456" class="btn btn-primary">สั่งซื้อ</a> \
                              //                               </div> \
                              //                         </td> \
                              //                   </tr>';
                        });

                        $('#productsTable').empty().append(htmlContent);
                        $('#pagination').css({
                              'display': 'block'
                        });
                        $('#pagination ul').empty().append(getPagination('search.php?', pageCurr, 2, data.totalPage));
                        $('#pagination div').text('ผลการค้นหาทั้งหมด ' + Number(data.total).toLocaleString('en', {
                              minimumFractionDigits: 0
                        }) + ' รายการ');
                  } else {
                        htmlContent += '<tr><td class="text-center"><h4 style="margin: 20px auto;">ไม่พบข้อมูลที่ตรงกัน</h4></td></tr>';
                        $('#productsTable tbody').empty().append(htmlContent);

                        $('#pagination').css({
                              'display': 'none'
                        });
                  }
            });
      });

      //--Function
      function setInit() {
            //--Set sidebar initial
            if (page != 'login.php') {
                  loadSidebarData();
            }
      }

      function detectMenu() {
            switch (page) {
                  case 'index.php':
                        loadHomeData();

                        break;
                  case 'horo.php':
                        setHoroData();

                        break;
                  case 'categories.php':
                        var categoriesID = (queryParams[0].split('categories_id=')[1] || 0);

                        switch (parseInt(categoriesID)) {
                              case 0:
                                    $('.page-header').html('เบอร์มาใหม่');
                                    $('.breadcrumb li.active').text('เบอร์มาใหม่');

                                    break;
                              case 1:
                                    $('.page-header').html('เบอร์มังกร <h5 style="display: inline-block;">(เงินก้อนใหญ่ มีอำนาจบารมี มั่งคั่งร่ำรวย)</h5>');
                                    $('.breadcrumb li.active').text('เบอร์มังกร');

                                    break;
                              case 2:
                                    $('.page-header').html('เบอร์หงษ์ <h5 style="display: inline-block;">(289 มีอำนาจบารมี เงินก้อนใหญ่ ธุรกิจมั่นคง)</h5>');
                                    $('.breadcrumb li.active').text('เบอร์หงษ์');

                                    break;
                              case 3:
                                    $('.page-header').html('เบอร์เศรษฐี <h5 style="display: inline-block;">(การแข่งขัน ค้าขายรุ่งเรือง มีอำนาจบารมี)</h5>');
                                    $('.breadcrumb li.active').text('เบอร์เศรษฐี');

                                    break;
                              case 4:
                                    $('.page-header').html('เบอร์ Platinum <h5 style="display: inline-block;">(เบอร์มงคลระดับ VIP)</h5>');
                                    $('.breadcrumb li.active').text('เบอร์ Platinum');

                                    break;
                              case 5:
                                    $('.page-header').html('เบอร์ Gold <h5 style="display: inline-block;">(เบอร์มงคลคัดพิเศษ)</h5>');
                                    $('.breadcrumb li.active').text('เบอร์ Gold');

                                    break;
                              case 6:
                                    $('.page-header').html('เบอร์ Silver <h5 style="display: inline-block;">(เบอร์ดีเลขมงคล)</h5>');
                                    $('.breadcrumb li.active').text('เบอร์ Silver');

                                    break;
                              default:
                                    $('.page-header').html('หมวดหมู่เบอร์</h5>');
                                    $('.breadcrumb li.active').text('หมวดหมู่เบอร์');

                                    break;
                        }

                        setCategoriesData();

                        break;
                  case 'search.php':
                        setSearchData();

                        break;
                  case 'ems-tracking.php':
                        setEmsTrackingData();

                        break;
                  case 'article.php':
                        setArticleData();

                        break;
                  case 'article-detail.php':
                        setArticleDetailData();

                        break;
                  case 'search-categories.php':
                        setSearchCategoriesData();

                        break;
                  case 'search-price.php':
                        setSearchPriceData();

                        break;
                  case 'search-compound.php':
                        setSearchCompoundData();

                        break;
                  case 'order-product.php':
                        setOrderProductData();

                        break;
                  case 'contact-us.php':
                        $('#contactUsForm').validate({
                              rules: {
                                    contactName: {
                                          required: true
                                    },
                                    contactEmail: {
                                          email: true,
                                          required: true
                                    },
                                    contactTel: {
                                          required: true,
                                          number: true,
                                          maxlength: 10,
                                          minlength: 10
                                    }
                              },
                              messages: {
                                    contactName: {
                                          required: 'โปรดระบุชื่อ-สกุลของท่าน'
                                    },
                                    contactEmail: {
                                          email: 'อีเมล์ของท่านไม่ถูกต้อง',
                                          required: 'โปรดระบุอีเมล์ของท่าน'
                                    },
                                    contactTel: {
                                          required: 'โปรดระบุเบอร์โทรศัพท์ของท่าน',
                                          number: 'เบอร์โทรศัพท์ต้องเป็นตัวเลขเท่านั้น',
                                          maxlength: 'เบอร์โทรศัพท์ต้องมี 10 หลัก',
                                          minlength: 'เบอร์โทรศัพท์ต้องมี 10 หลัก'
                                    }
                              },
                              submitHandler: sendEmailToAdmin
                        });

                        break;
                  default:
                        loadHomeData();

                        break;
            }
      }

      function loadDropdownData(apiUrl, filterData, cbFunc) {
            if (apiUrl && filterData.length) {
                  apiUrl = apiUrl;
                  apiParams = {
                        fn: 'loadDropdownData',
                        filterData: filterData
                  };

                  Service.prototype.callAPI(apiUrl, apiParams, 'post', true).done(function (res) {
                        var data = JSON.parse(res);
                        // console.log(data);
                        dropdownList = data;
                        cbFunc(data);
                  });
            } else {
                  cbFunc([]);
            }
      }

      function loadSidebarData() {
            apiUrl = 'admin/api/frontend/sidebarAPI.php';
            apiParams = {
                  fn: 'loadSidebarData'
            };

            Service.prototype.callAPI(apiUrl, apiParams, 'post', true).done(function (res) {
                  var data = JSON.parse(res);
                  // console.log(data);

                  setSidebarData(data);
            });
      }

      function loadHomeData() {
            apiUrl = 'admin/api/frontend/homeAPI.php';
            apiParams = {
                  fn: 'loadHomeData'
            };

            Service.prototype.callAPI(apiUrl, apiParams, 'post', true).done(function (res) {
                  var data = JSON.parse(res);
                  console.log(data);

                  setHomeData(data);
            });
      }

      function setSidebarData(sidebarData) {
            //--รูปแบบเบอร์
            phoneFormat = sidebarData['phoneFormat'];
            detectMenu();

            //--หมวดหมู่เบอร์
            htmlContent = '<li> \
                                    <a href="search-categories.php?categories_id=0"> \
                                          <i class="fas fa-angle-right"></i> เบอร์ทั้งหมด</a> \
                              </li>';

            $(sidebarData['categoriesData']).each(function (index, categories) {
                  htmlContent += '<li> \
                                          <a href="search-categories.php?categories_id=' + categories['sidebar_categories_id'] + '"> \
                                                <i class="fas fa-angle-right"></i> ' + categories['sidebar_categories_name'] + '</a> \
                                    </li>';

            });

            $('ul#categoriesList').empty().append(htmlContent);

            //--เบอร์ตามช่วงราคา
            htmlContent = '';

            $(sidebarData['priceData']).each(function (index, price) {
                  htmlContent += '<li> \
                                          <a href="search-price.php?price_id=' + price['sidebar_price_id'] + '"> \
                                                <i class="fas fa-angle-right"></i> ' + price['sidebar_price_name'] + '</a> \
                                    </li>';
            });

            $('ul#priceList').empty().append(htmlContent);

            //--เบอร์ตามผลรวม
            htmlContent = '';

            $(sidebarData['compoundData']).each(function (index, compound) {
                  htmlContent += '<li> \
                                          <a href="search-compound.php?compound_id=' + compound['sidebar_compound_id'] + '"> \
                                                <i class="fas fa-angle-right"></i> ' + compound['sidebar_compound_name'] + '</a> \
                                    </li>';
            });

            $('ul#compoundList').empty().append(htmlContent);

            //--สถานะการจัดส่ง EMS
            htmlContent = '<span class="deliver-date"><i class="far fa-calendar"></i> ' + getDateCurrent() + '</span>';

            $(sidebarData['emsTrackingData']).each(function (index, emsTracking) {
                  htmlContent += '<li> \
                                          <a href="http://emsbot.com/#/?s=' + emsTracking['ems_tracking_number'] + '"> \
                                                <i class="fab fa-telegram-plane"></i> ' + emsTracking['ems_tracking_number'] + ' ' + emsTracking['ems_tracking_name'] + '</a> \
                                    </li>';
            });

            $('ul#emsTrackingList').empty().append(htmlContent);

            //--บทความแนะนำ
            htmlContent = '';

            $(sidebarData['articleData']).each(function (index, article) {
                  htmlContent += '<li class="media"> \
                                          <a href="article-detail.php?article_id=' + article['article_id'] + '"> \
                                                <img class="img-fluid" src="' + (article['image_cover'] ? 'admin/' + article['image_cover'] : 'admin/uploads/no-image-available.png') + '" style="width: 100px; height 100px;"> \
                                                <div class="media-body"> \
                                                      <h5 class="mt-0 mb-1">' + article['article_title'] + '</h5> \
                                                      <p></p> \
                                                </div> \
                                          </a> \
                                    </li>';
            });

            $('ul#articleList').empty().append(htmlContent);
      }

      function setHomeData(homeData) {
            //--แบนเนอร์
            htmlContent = '';

            $(homeData['bannerData']).each(function (index, banner) {
                  htmlContent += '<div class="carousel-item ' + ((index == 0) ? 'active' : '') + '" style="background-image: url(admin/' + banner['banner_image_path'] + ')"> \
                                          <div class="carousel-caption d-none d-md-block"> \
                                                <h1>' + banner['banner_name'] + '</h1> \
                                                <p>' + banner['banner_detail'] + '</p> \
                                          </div> \
                                    </div>';

            });

            $('#bannerList').empty().append(htmlContent);

            //--เบอร์มาใหม่
            htmlContent = '';

            $(homeData['productsData']).each(function (index, products) {
                  htmlContent += 
                  '<div class="col-lg-6 col-md-12 my-2">'+
                                    '<div class="styleNumberList h-100">'
                                          +'<div class="row">'
                                                +'<div class="col-7 col-md-8 col-lg-8 pl-3">'
                                                      +'<p class="text-number font-28">'+products['ori_number1']+'</p>' 
                                                     
                                                +'</div>'
                                                +'<div class="col-5 col-sm-3 col-lg-3 text-center">'
                                                      +'<div>'
                                                      +'<img class="carrier-logo " src="admin/' 
                                                            +products['carrier_image_path'] + '" alt="' + products['carrier_name'] + '">' 
                                                      +'</div>'
                                                +'</div>'
                                                +'<div class="col-12">'
                                                      +'<div >'
                                                       +'<p>ราคา:<span class="stylePriceNumberList text-price">'+Number(products['price']).toLocaleString("en",{ minimumFractionDigits: 0}) +
                                                      '</span> ฿ <span class="sum"> (ผลรวม '+ 
                                                            '<span class="styleSumPoint text-total">'+ 
                                                                  products['sum1']
                                                      +     '</span> )'
                                                      +'</span> &nbsp;&nbsp;&nbsp;'+products['name']+'</p>'
                              
                                                      +'</div>'
                                                +'</div>'
                                          +'</div>'
                                          +'<div class="row d-flex justify-content-between">'
                                                +'<div class="col-5 col-lg-6 ">'
                                                      +'<div class="btn-buy h-100 d-flex justify-content-center align-items-center">'
                                                                + ' <a href="horo.php?number='+products['ori_number1']+'" class="btn btn-primary">'
                                                                + '<i class="fas fa-search iconSearch"></i>ทำนายเบอร์</a>'
                                                      +'</div>'
                                                +'</div>'
                                                +'<div class="col-5 col-lg-6 text-center">'
                                                      + '<div class="btn-buy">'
                                                            +'<a href="howto.php" class="btn btn-predict mt-0"><i class="fas fa-shopping-basket iconBasket"></i>'
                                                            +'สั่งซื้อ'
                                                            +'</a>'
                                                      +'</div>'
                                                +'</div>'+
                                          '</div>'
                                          +
                                    '</div>'
                                +'</div>'
                          });
            //       '<tr> \
            //                               <td> \
            //                                     <img class="mr-3 carrier-logo" src="admin/' + products['carrier_image_path'] + '" alt="' + products['carrier_name'] + '"> \
            //                               </td> \
            //                               <td width="25%"> \
            //                                     <div class="showber"> \
            //                                           <div class="mt-0 mb-1"> \
            //                                                 <span>' + getPhoneFormat(products['ori_number1']) + '</span> \
            //                                                 <span class="sum">(ผลรวม ' + products['sum1'] + ')</span> \
            //                                           </div> \
            //                                     </div> \
            //                               </td> \
            //                               <td width="10%"> \
            //                                     <div class="berprice">' + Number(products['price']).toLocaleString('en', {
            //             minimumFractionDigits: 0
            //       }) + ' ฿</div> \
            //                               </td> \
            //                               <td> \
            //                                     <div class="berdetail"> \
            //                                           <p>' + products['product_detail'] + '</p> \
            //                                     </div> \
            //                               </td> \
            //                               <td> \
            //                                     <div class="btn-buy"> \
            //                                           <a href="tel:+66814562456" class="btn btn-primary">สั่งซื้อ</a> \
            //                                     </div> \
            //                               </td> \
            //                         </tr>';
            // });

            $('#productsTable').empty().append(htmlContent);//tbody
      }

      function setHoroData() {
            if (queryParams[0]) {
                  var productsNumber = queryParams[0].split('number=')[1];

                  apiUrl = 'admin/api/frontend/productsAPI.php';
                  apiParams = {
                        fn: 'loadProductsData',
                        productsNumber: productsNumber,
                        productsNumberSum: getPhoneNumberSum(productsNumber)
                  };
                  // console.log(apiParams);

                  Service.prototype.callAPI(apiUrl, apiParams, 'post', true).done(function (res) {
                        var data = JSON.parse(res);
                        console.log(data);

                        if (data.status) {
                              var products = data.data;
                              htmlContent = '<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab"> \
                                                      <div class="row"> \
                                                            <div class="col-md-4 offset-md-4"> \
                                                                  <div class="number-header text-center">' + getPhoneFormat(products['ori_number1']) + '</div> \
                                                                  <div class="horo-title">ความเฉลี่ยความมงคล</div> \
                                                                  <div class="horo-percent text-center">' + Number(products['percent']).toLocaleString('en', {
                                    minimumFractionDigits: 0
                              }) + '%</div> \
                                                                  <div class="primary-horo-rating">' + getCalibratePercentContent(products['percent']) + '</div> \
                                                            </div> \
                                                      </div> \
                                                      <div class="sec-b"> \
                                                            <div class="col-md-4 offset-md-4"> \
                                                                  <img src="images/grass.svg" class="element"> \
                                                                  <h4 class="text-center">เบอร์โทรศัพท์ตรงกับธาตุ ' + products['mean'] + '</h4> \
                                                                  <div class="horo-title">คำวิเคราะห์แต่ละด้าน</div> \
                                                                  <table border="0" cellpadding="0" cellspacing="0" style="width:100%" class="table-element"> \
                                                                        <tbody> \
                                                                        <tr> \
                                                                              <td width="50%" align="left"><i class="fa fa-book"></i> การเรียน</td> \
                                                                              <td width="50%" align="right">' + getCalibrateScoreContent(products['education']) + '</td> \
                                                                        </tr> \
                                                                        <tr> \
                                                                              <td width="50%" align="left"><i class="fa fa-briefcase"></i> การงาน</td> \
                                                                              <td width="50%" align="right">' + getCalibrateScoreContent(products['work']) + '</td> \
                                                                        </tr> \
                                                                        <tr> \
                                                                              <td width="50%" align="left"><i class="fas fa-money-bill-alt"></i> การเงิน</td> \
                                                                              <td width="50%" align="right">' + getCalibrateScoreContent(products['finance']) + '</td> \
                                                                        </tr> \
                                                                        <tr> \
                                                                              <td width="50%" align="left"><i class="fa fa-heart"></i> ความรัก</td> \
                                                                              <td width="50%" align="right">' + getCalibrateScoreContent(products['love']) + '</td> \
                                                                        </tr> \
                                                                        <tr> \
                                                                              <td width="50%" align="left"><i class="fa fa-balance-scale"></i> โชคลาภ</td> \
                                                                              <td width="50%" align="right">' + getCalibrateScoreContent(products['lucky']) + '</td> \
                                                                        </tr> \
                                                                        </tbody> \
                                                                  </table> \
                                                                  <h4 class="text-center">เบอร์โทรศัพท์ของคุณมีผลรวมเท่ากับ</h4> \
                                                                  <h1 class="text-center">' + products['sum1'] + '</h1> \
                                                            </div> \
                                                            <div class="col-md-8 offset-md-2"> \
                                                                  <p>' + products['detail_sum'] + '</p> \
                                                            </div> \
                                                      </div> \
                                                </div> \
                                                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab"> \
                                                      <div class="number-header text-center">' + getPhoneFormat(products['ori_number1']) + '</div> \
                                                      <div class="col-md-4 offset-md-4"> \
                                                            <div class="horo-title">น้ำหนักคำทำนายรวม</div> \
                                                            <div class="horo-percent text-center">' + Number(products['percent']).toLocaleString('en', {
                                    minimumFractionDigits: 0
                              }) + '%</div> \
                                                            <p>86-100% = เบอร์ดีมาก<br> \
                                                                  71-85% = เบอร์ดี<br> \
                                                                  61-70% = เบอร์ดีพอใช้<br> \
                                                                  50-60% = เบอร์พอใช้<br> \
                                                                  น้อยกว่า 49% = เบอร์ไม่ดี \
                                                            </p> \
                                                      </div> \
                                                      <div class="sec-b"> \
                                                            <div class="col-md-4 offset-md-4 mb-3"> \
                                                                  <div class="horo-title">วิธีการคำนวณแบบคู่ลำดับมงคล</div> \
                                                            </div>';

                              for (var i = 1; i <= 6; i++) {
                                    htmlContent += '<div class="col-md-8 offset-md-2"> \
                                                                                    <h4 class="text-center">ทำนายชุดที่ ' + i + ' (' + products['horoCompound_' + i]['number'] + ')</h4> \
                                                                                    <h3 class="text-center">' + getHoroCompoundNumberFormat(products['ori_number1'], i) + '</h3> \
                                                                                    <p>' + products['horoCompound_' + i]['detail'] + '</p> \
                                                                                    <p>น้ำหนักคำทำนาย ' + products['horoCompound_' + i]['percent'] + '%</p> \
                                                                              </div>';
                              }

                              htmlContent += '</div> \
                                                </div>';
                              $('#horoContent').empty().append(htmlContent);
                        } else {
                              $('#horoContent').empty().append('<h4 style="margin: 20px auto;">ไม่สามารถทำนายเบอร์ได้ เนื่องจากไม่พบข้อมูลในระบบ</h4>');
                        }
                  });
            } else {
                  $('#horoContent').empty().append('<h4 style="margin: 20px auto;">ไม่สามารถทำนายเบอร์ได้ เนื่องจากไม่พบข้อมูลในระบบ</h4>');
            }
      }

      function setCategoriesData() {
            apiUrl = 'admin/api/frontend/categoriesAPI.php';
            apiParams = {
                  fn: 'loadCategoriesData',
                  page: pageCurr,
                  categoriesID: (queryParams[0].split('categories_id=')[1] || 0)
            };
            // console.log(apiParams);

            Service.prototype.callAPI(apiUrl, apiParams, 'post', true).done(function (res) {
                  var data = JSON.parse(res);
                  console.log(data);

                  if (data.status) {
                        htmlContent = '';

                        if (data.data.length) {
                              $(data.data).each(function (index, products) {
                                    htmlContent += 
                 '<div class="col-lg-6 col-md-12 my-2">'+
                                    '<div class="styleNumberList h-100">'
                                          +'<div class="row">'
                                                +'<div class="col-7 col-md-8 col-lg-8 pl-3">'
                                                      +'<p class="text-number font-28">'+products['ori_number1']+'</p>' 
                                                     
                                                +'</div>'
                                                +'<div class="col-5 col-sm-3 col-lg-3 text-center">'
                                                      +'<div>'
                                                      +'<img class="carrier-logo " src="admin/' 
                                                            +products['carrier_image_path'] + '" alt="' + products['carrier_name'] + '">' 
                                                      +'</div>'
                                                +'</div>'
                                                +'<div class="col-12">'
                                                      +'<div >'
                                                       +'<p>ราคา:<span class="stylePriceNumberList text-price">'+Number(products['price']).toLocaleString("en",{ minimumFractionDigits: 0}) +
                                                      '</span> ฿ <span class="sum"> (ผลรวม '+ 
                                                            '<span class="styleSumPoint text-total">'+ 
                                                                  products['sum1']
                                                      +     '</span> )'
                                                      +'</span> &nbsp;&nbsp;&nbsp;'+products['name']+'</p>'
                              
                                                      +'</div>'
                                                +'</div>'
                                          +'</div>'
                                          +'<div class="row d-flex justify-content-between">'
                                                +'<div class="col-5 col-lg-6 ">'
                                                      +'<div class="btn-buy h-100 d-flex justify-content-center align-items-center">'
                                                                + ' <a href="horo.php?number='+products['ori_number1']+'" class="btn btn-primary">'
                                                                + '<i class="fas fa-search iconSearch"></i>ทำนายเบอร์</a>'
                                                      +'</div>'
                                                +'</div>'
                                                +'<div class="col-5 col-lg-6 text-center">'
                                                      + '<div class="btn-buy">'
                                                            +'<a href="howto.php" class="btn btn-predict mt-0"><i class="fas fa-shopping-basket iconBasket"></i>'
                                                            +'สั่งซื้อ'
                                                            +'</a>'
                                                      +'</div>'
                                                +'</div>'+
                                          '</div>'
                                          +
                                    '</div>'
                                +'</div>'
                          });

                              //       '<tr> \
                              //                               <td> \
                              //                                     <img class="mr-3 carrier-logo" src="admin/' + products['carrier_image_path'] + '" alt="' + products['carrier_name'] + '"> \
                              //                               </td> \
                              //                               <td width="25%"> \
                              //                                     <div class="showber"> \
                              //                                           <div class="mt-0 mb-1"> \
                              //                                                 <span>' + getPhoneFormat(products['ori_number1']) + '</span> \
                              //                                                 <span class="sum">(ผลรวม ' + products['sum1'] + ')</span> \
                              //                                           </div> \
                              //                                     </div> \
                              //                               </td> \
                              //                               <td width="10%"> \
                              //                                     <div class="berprice">' + Number(products['price']).toLocaleString('en', {
                              //             minimumFractionDigits: 0
                              //       }) + ' ฿</div> \
                              //                               </td> \
                              //                               <td> \
                              //                                     <div class="berdetail"> \
                              //                                           <p>' + products['product_detail'] + '</p> \
                              //                                     </div> \
                              //                               </td> \
                              //                               <td> \
                              //                                     <div class="btn-buy"> \
                              //                                           <a href="tel:+66814562456" class="btn btn-primary">สั่งซื้อ</a> \
                              //                                     </div> \
                              //                               </td> \
                              //                         </tr>';
                              // });

                              $('#productsTable').empty().append(htmlContent);
                              $('#pagination').css({
                                    'display': 'block'
                              });
                              $('#pagination ul').empty().append(getPagination('categories.php?categories_id=' + apiParams['categoriesID'] + '&', pageCurr, 2, data.totalPage));
                              $('#pagination div').text('ผลการค้นหาทั้งหมด ' + Number(data.total).toLocaleString('en', {
                                    minimumFractionDigits: 0
                              }) + ' รายการ');
                        } else {
                              htmlContent += '<tr><td class="text-center"><h4 style="margin: 20px auto;">ไม่พบข้อมูลที่ตรงกัน</h4></td></tr>';
                              $('#productsTable tbody').empty().append(htmlContent);

                              $('#pagination').css({
                                    'display': 'none'
                              });
                        }
                  }
            });
      }

      function setSearchData() {
            setTimeout(function () {
                  $('#searchBtn').trigger('click');
            });
      }

      function setEmsTrackingData() {
            apiUrl = 'admin/api/frontend/emsTrackingAPI.php';
            apiParams = {
                  fn: 'loadEmsTrackingData',
                  page: pageCurr
            };
            // console.log(apiParams);

            Service.prototype.callAPI(apiUrl, apiParams, 'post', true).done(function (res) {
                  var data = JSON.parse(res);
                  console.log(data);

                  if (data.status) {
                        htmlContent = '';

                        if (data.data.length) {
                              $(data.data).each(function (index, emsTracking) {
                                    htmlContent += '<tr> \
                                                            <td>' + convertDateToDisplay(emsTracking['ems_tracking_date']) + '</td> \
                                                            <td><a href="http://emsbot.com/#/?s=' + emsTracking['ems_tracking_number'] + '" style="color: #212529;">' + emsTracking['ems_tracking_number'] + '</a></td> \
                                                            <td>' + emsTracking['ems_tracking_name'] + '</td> \
                                                      </tr>';
                              });

                              $('#emsTrackingContent table').removeClass('hide').addClass('show');
                              $('#emsTrackingContent table tbody').empty().append(htmlContent);
                              $('#pagination').css({
                                    'display': 'block'
                              });
                              $('#pagination ul').empty().append(getPagination('ems-tracking.php?', pageCurr, 2, data.totalPage));
                              $('#pagination div').text('ผลการค้นหาทั้งหมด ' + Number(data.total).toLocaleString('en', {
                                    minimumFractionDigits: 0
                              }) + ' รายการ');

                              $('.share-content').jsSocials({
                                    shares: [{
                                                share: 'email'
                                          },
                                          {
                                                share: 'facebook',
                                                logo: 'fab fa-facebook'
                                          },
                                          {
                                                share: 'twitter',
                                                logo: 'fab fa-twitter'
                                          },
                                          {
                                                share: 'googleplus',
                                                logo: 'fab fa-google'
                                          }
                                    ]
                              });
                        } else {
                              htmlContent = '<div class="card mb-4"> \
                                                      <h4 style="margin: 20px auto;">ไม่พบข้อมูลที่ตรงกัน</h4> \
                                                </div>';
                              $('#emsTrackingContent table').removeClass('show').addClass('hide');
                              $('#emsTrackingContent').empty().append(htmlContent);
                              $('.breadcrumb li.active').hide();
                        }
                  }
            });
      }

      function setArticleData() {
            apiUrl = 'admin/api/frontend/articleAPI.php';
            apiParams = {
                  fn: 'loadArticleData',
                  page: pageCurr
            };
            // console.log(apiParams);

            Service.prototype.callAPI(apiUrl, apiParams, 'post', true).done(function (res) {
                  var data = JSON.parse(res);
                  console.log(data);

                  if (data.status) {
                        htmlContent = '';

                        if (data.data.length) {
                              $(data.data).each(function (index, article) {
                                    htmlContent += '<a href="article-detail.php?article_id=' + article['article_id'] + '"> \
                                                            <div class="card mb-4"> \
                                                                  <img class="card-img-top mw-100" src="admin/' + (article['image_cover'] ? article['image_cover'] : 'uploads/no-image-available.png') + '" alt="' + article['article_title'] + '" "> \
                                                                  <div class="card-body"> \
                                                                        <h4 class="card-title">' + article['article_title'] + '</h4> \
                                                                        <div class="share"> \
                                                                              <span class="datepost">หมวดหมู่ : ' + article['categories_name'] + ' |</span> \
                                                                              <span class="datepost">โพสต์เมื่อ : ' + convertDateToDisplay(article['date']) + '</span> \
                                                                        </div> \
                                                                        <!--<p class="card-text">' + article['detail'] + '</p>--> \
                                                                        <a href="article-detail.php?article_id=' + article['article_id'] + '" class="btn btn-primary">อ่านต่อ <i class="fas fa-angle-double-right"></i></a> \
                                                                  </div> \
                                                            </div> \
                                                      </a>';
                              });

                              $('#articleContent').empty().append(htmlContent);
                              $('#pagination').css({
                                    'display': 'block'
                              });
                              $('#pagination ul').empty().append(getPagination('article.php?', pageCurr, 2, data.totalPage));
                              $('#pagination div').text('ผลการค้นหาทั้งหมด ' + Number(data.total).toLocaleString('en', {
                                    minimumFractionDigits: 0
                              }) + ' รายการ');
                        } else {
                              htmlContent = '<div class="card mb-4"> \
                                                      <h4 style="margin: 20px auto;">ไม่พบข้อมูลที่ตรงกัน</h4> \
                                                </div>';
                              $('#articleContent').empty().append(htmlContent);
                              $('.breadcrumb li.active').hide();
                        }
                  }
            });
      }

      function setArticleDetailData() {
            apiUrl = 'admin/api/frontend/articleAPI.php';
            apiParams = {
                  fn: 'loadArticleDetailData',
                  page: pageCurr,
                  articleID: (queryParams[0].split('article_id=')[1] || 0)
            };
            console.log(apiParams);

            Service.prototype.callAPI(apiUrl, apiParams, 'post', true).done(function (res) {
                  var data = JSON.parse(res);
                  console.log(data);

                  if (data.status && data.data) {
                        var article = data.data;

                        $('.page-header').html(article['article_title']);
                        $('.breadcrumb li.active').show().text(article['article_title']);

                        htmlContent = '<div class="card mb-4"> \
                                          <img class="card-img-top" src="admin/' + (article['image_cover'] ? article['image_cover'] : 'uploads/no-image-available.png') + '" alt="' + article['article_title'] + '"> \
                                          <div class="card-body"> \
                                                <h4 class="card-title">' + article['article_title'] + '</h4> \
                                                <div class="share"> \
                                                      <span class="datepost">หมวดหมู่ : ' + article['categories_name'] + ' |</span> \
                                                      <span class="datepost">โพสต์เมื่อ : ' + convertDateToDisplay(article['date']) + ' |</span> \
                                                      <div class="share-content" style="font-size: 13px;"></div> \
                                                </div> \
                                                <p class="card-text">' + article['detail'] + '</p> \
                                          </div> \
                                    </div>';

                        $('#articleContent').empty().append(htmlContent);
                        $('.share-content').jsSocials({
                              showLabel: true,
                              showCount: false,
                              shareIn: 'popup',
                              shares: [{
                                          share: 'email'
                                    },
                                    {
                                          share: 'facebook',
                                          logo: 'fab fa-facebook'
                                    },
                                    {
                                          share: 'twitter',
                                          logo: 'fab fa-twitter'
                                    },
                                    {
                                          share: 'googleplus',
                                          logo: 'fab fa-google'
                                    }
                              ]
                        });
                  } else {
                        htmlContent = '<div class="card mb-4"> \
                                                <h4 style="margin: 20px auto;">ไม่พบข้อมูลที่ตรงกัน</h4> \
                                          </div>';
                        $('#articleContent').empty().append(htmlContent);
                        $('.breadcrumb li.active').hide();
                  }
            });
      }

      function setSearchCategoriesData() {
            apiUrl = 'admin/api/frontend/searchAPI.php';
            apiParams = {
                  fn: 'loadSidebarData',
                  page: pageCurr,
                  type: 'categories',
                  sidebarCategoriesID: (queryParams[0].split('categories_id=')[1] || 0)
            };
            // console.log(apiParams);

            Service.prototype.callAPI(apiUrl, apiParams, 'post', true).done(function (res) {
                  var data = JSON.parse(res);
                  console.log(data);

                  if (data.status) {
                        $('.page-header').html('หมวดหมู่เบอร์ <h5 style="display: inline-block;">(' + data.title + ')</h5>');
                        $('.breadcrumb li.active').text('หมวดหมู่เบอร์ (' + data.title + ')');

                        htmlContent = '';

                        if (data.data.length) {
                              $(data.data).each(function (index, products) {
                                   htmlContent += 
                  '<div class="col-lg-6 col-md-12 my-2">'+
                                    '<div class="styleNumberList h-100">'
                                          +'<div class="row">'
                                                +'<div class="col-7 col-md-8 col-lg-8 pl-3">'
                                                      +'<p class="text-number font-28">'+products['ori_number1']+'</p>' 
                                                     
                                                +'</div>'
                                                +'<div class="col-5 col-sm-3 col-lg-3 text-center">'
                                                      +'<div>'
                                                      +'<img class="carrier-logo " src="admin/' 
                                                            +products['carrier_image_path'] + '" alt="' + products['carrier_name'] + '">' 
                                                      +'</div>'
                                                +'</div>'
                                                +'<div class="col-12">'
                                                      +'<div >'
                                                       +'<p>ราคา:<span class="stylePriceNumberList text-price">'+Number(products['price']).toLocaleString("en",{ minimumFractionDigits: 0}) +
                                                      '</span> ฿ <span class="sum"> (ผลรวม '+ 
                                                            '<span class="styleSumPoint text-total">'+ 
                                                                  products['sum1']
                                                      +     '</span> )'
                                                      +'</span> &nbsp;&nbsp;&nbsp;'+products['name']+'</p>'
                              
                                                      +'</div>'
                                                +'</div>'
                                          +'</div>'
                                          +'<div class="row d-flex justify-content-between">'
                                                +'<div class="col-5 col-lg-6 ">'
                                                      +'<div class="btn-buy h-100 d-flex justify-content-center align-items-center">'
                                                                + ' <a href="horo.php?number='+products['ori_number1']+'" class="btn btn-primary">'
                                                                + '<i class="fas fa-search iconSearch"></i>ทำนายเบอร์</a>'
                                                      +'</div>'
                                                +'</div>'
                                                +'<div class="col-5 col-lg-6 text-center">'
                                                      + '<div class="btn-buy">'
                                                            +'<a href="howto.php" class="btn btn-predict mt-0"><i class="fas fa-shopping-basket iconBasket"></i>'
                                                            +'สั่งซื้อ'
                                                            +'</a>'
                                                      +'</div>'
                                                +'</div>'+
                                          '</div>'
                                          +
                                    '</div>'
                                +'</div>'
                          });
                              //       '<tr> \
                              //                               <td> \
                              //                                     <img class="mr-3 carrier-logo" src="admin/' + products['carrier_image_path'] + '" alt="' + products['carrier_name'] + '"> \
                              //                               </td> \
                              //                               <td width="25%"> \
                              //                                     <div class="showber"> \
                              //                                           <div class="mt-0 mb-1"> \
                              //                                                 <span>' + getPhoneFormat(products['ori_number1']) + '</span> \
                              //                                                 <span class="sum">(ผลรวม ' + products['sum1'] + ')</span> \
                              //                                           </div> \
                              //                                     </div> \
                              //                               </td> \
                              //                               <td width="10%"> \
                              //                                     <div class="berprice">' + Number(products['price']).toLocaleString('en', {
                              //             minimumFractionDigits: 0
                              //       }) + ' ฿</div> \
                              //                               </td> \
                              //                               <td> \
                              //                                     <div class="berdetail"> \
                              //                                           <p>' + products['product_detail'] + '</p> \
                              //                                     </div> \
                              //                               </td> \
                              //                               <td> \
                              //                                     <div class="btn-buy"> \
                              //                                           <a href="tel:+66814562456" class="btn btn-primary">สั่งซื้อ</a> \
                              //                                     </div> \
                              //                               </td> \
                              //                         </tr>';
                              // });

                              $('#productsTable').empty().append(htmlContent);
                              $('#pagination').css({
                                    'display': 'block'
                              });
                              $('#pagination ul').empty().append(getPagination('search-categories.php?categories_id=' + apiParams['sidebarCategoriesID'] + '&', pageCurr, 2, data.totalPage));
                              $('#pagination div').text('ผลการค้นหาทั้งหมด ' + Number(data.total).toLocaleString('en', {
                                    minimumFractionDigits: 0
                              }) + ' รายการ');
                        } else {
                              htmlContent += '<tr><td class="text-center"><h4 style="margin: 20px auto;">ไม่พบข้อมูลที่ตรงกัน</h4></td></tr>';
                              $('#productsTable').empty().append(htmlContent);

                              $('#pagination').css({
                                    'display': 'none'
                              });
                        }
                  }
            });
      }

      function setSearchPriceData() {
            apiUrl = 'admin/api/frontend/searchAPI.php';
            apiParams = {
                  fn: 'loadSidebarData',
                  page: pageCurr,
                  type: 'price',
                  sidebarPriceID: (queryParams[0].split('price_id=')[1] || 0)
            };
            // console.log(apiParams);

            Service.prototype.callAPI(apiUrl, apiParams, 'post', true).done(function (res) {
                  var data = JSON.parse(res);
                  console.log(data);

                  if (data.status) {
                        $('.page-header').html('เบอร์ตามช่วงราคา <h5 style="display: inline-block;">(' + data.title + ')</h5>');
                        $('.breadcrumb li.active').text('เบอร์ตามช่วงราคา (' + data.title + ')');

                        htmlContent = '';

                        if (data.data.length) {
                              $(data.data).each(function (index, products) {
                                    htmlContent += 
                              '<div class="col-lg-6 col-md-12 my-2">'+
                                    '<div class="styleNumberList h-100">'
                                          +'<div class="row">'
                                                +'<div class="col-7 col-md-8 col-lg-8 pl-3">'
                                                      +'<p class="text-number font-28">'+products['ori_number1']+'</p>' 
                                                     
                                                +'</div>'
                                                +'<div class="col-5 col-sm-3 col-lg-3 text-center">'
                                                      +'<div>'
                                                      +'<img class="carrier-logo " src="admin/' 
                                                            +products['carrier_image_path'] + '" alt="' + products['carrier_name'] + '">' 
                                                      +'</div>'
                                                +'</div>'
                                                +'<div class="col-12">'
                                                      +'<div >'
                                                       +'<p>ราคา:<span class="stylePriceNumberList text-price">'+Number(products['price']).toLocaleString("en",{ minimumFractionDigits: 0}) +
                                                      '</span> ฿ <span class="sum"> (ผลรวม '+ 
                                                            '<span class="styleSumPoint text-total">'+ 
                                                                  products['sum1']
                                                      +     '</span> )'
                                                      +'</span> &nbsp;&nbsp;&nbsp;'+products['name']+'</p>'
                              
                                                      +'</div>'
                                                +'</div>'
                                          +'</div>'
                                          +'<div class="row d-flex justify-content-between">'
                                                +'<div class="col-5 col-lg-6 ">'
                                                      +'<div class="btn-buy h-100 d-flex justify-content-center align-items-center">'
                                                                + ' <a href="horo.php?number='+products['ori_number1']+'" class="btn btn-primary">'
                                                                + '<i class="fas fa-search iconSearch"></i>ทำนายเบอร์</a>'
                                                      +'</div>'
                                                +'</div>'
                                                +'<div class="col-5 col-lg-6 text-center">'
                                                      + '<div class="btn-buy">'
                                                            +'<a href="howto.php" class="btn btn-predict mt-0"><i class="fas fa-shopping-basket iconBasket"></i>'
                                                            +'สั่งซื้อ'
                                                            +'</a>'
                                                      +'</div>'
                                                +'</div>'+
                                          '</div>'
                                          +
                                    '</div>'
                                +'</div>'
                          });
                              //       '<tr> \
                              //                               <td> \
                              //                                     <img class="mr-3 carrier-logo" src="admin/' + products['carrier_image_path'] + '" alt="' + products['carrier_name'] + '"> \
                              //                               </td> \
                              //                               <td width="25%"> \
                              //                                     <div class="showber"> \
                              //                                           <div class="mt-0 mb-1"> \
                              //                                                 <span>' + getPhoneFormat(products['ori_number1']) + '</span> \
                              //                                                 <span class="sum">(ผลรวม ' + products['sum1'] + ')</span> \
                              //                                           </div> \
                              //                                     </div> \
                              //                               </td> \
                              //                               <td width="10%"> \
                              //                                     <div class="berprice">' + Number(products['price']).toLocaleString('en', {
                              //             minimumFractionDigits: 0
                              //       }) + ' ฿</div> \
                              //                               </td> \
                              //                               <td> \
                              //                                     <div class="berdetail"> \
                              //                                           <p>' + products['product_detail'] + '</p> \
                              //                                     </div> \
                              //                               </td> \
                              //                               <td> \
                              //                                     <div class="btn-buy"> \
                              //                                           <a href="tel:+66814562456" class="btn btn-primary">สั่งซื้อ</a> \
                              //                                     </div> \
                              //                               </td> \
                              //                         </tr>';
                              // });

                              $('#productsTable').empty().append(htmlContent);
                              $('#pagination').css({
                                    'display': 'block'
                              });
                              $('#pagination ul').empty().append(getPagination('search-price.php?price_id=' + apiParams['sidebarPriceID'] + '&', pageCurr, 2, data.totalPage));
                              $('#pagination div').text('ผลการค้นหาทั้งหมด ' + Number(data.total).toLocaleString('en', {
                                    minimumFractionDigits: 0
                              }) + ' รายการ');
                        } else {
                              htmlContent += '<tr><td class="text-center"><h4 style="margin: 20px auto;">ไม่พบข้อมูลที่ตรงกัน</h4></td></tr>';
                              $('#productsTable').empty().append(htmlContent);

                              $('#pagination').css({
                                    'display': 'none'
                              });
                        }
                  }
            });
      }

      function setSearchCompoundData() {
            apiUrl = 'admin/api/frontend/searchAPI.php';
            apiParams = {
                  fn: 'loadSidebarData',
                  page: pageCurr,
                  type: 'compound',
                  sidebarCompoundID: (queryParams[0].split('compound_id=')[1] || 0)
            };
            // console.log(apiParams);

            Service.prototype.callAPI(apiUrl, apiParams, 'post', true).done(function (res) {
                  var data = JSON.parse(res);
                  console.log(data);

                  if (data.status) {
                        $('.page-header').html('เบอร์ตามผลรวม <h5 style="display: inline-block;">(' + data.title + ')</h5>');
                        $('.breadcrumb li.active').text('เบอร์ตามผลรวม (' + data.title + ')');

                        htmlContent = '';

                        if (data.data.length) {
                              $(data.data).each(function (index, products) {
                                    htmlContent += 
                              '<div class="col-lg-6 col-md-12 my-2">'+
                                    '<div class="styleNumberList h-100">'
                                          +'<div class="row">'
                                                +'<div class="col-7 col-md-8 col-lg-8 pl-3">'
                                                      +'<p class="text-number font-28">'+products['ori_number1']+'</p>' 
                                                     
                                                +'</div>'
                                                +'<div class="col-5 col-sm-3 col-lg-3 text-center">'
                                                      +'<div>'
                                                      +'<img class="carrier-logo " src="admin/' 
                                                            +products['carrier_image_path'] + '" alt="' + products['carrier_name'] + '">' 
                                                      +'</div>'
                                                +'</div>'
                                                +'<div class="col-12">'
                                                      +'<div >'
                                                       +'<p>ราคา:<span class="stylePriceNumberList text-price">'+Number(products['price']).toLocaleString("en",{ minimumFractionDigits: 0}) +
                                                      '</span> ฿ <span class="sum"> (ผลรวม '+ 
                                                            '<span class="styleSumPoint text-total">'+ 
                                                                  products['sum1']
                                                      +     '</span> )'
                                                      +'</span> &nbsp;&nbsp;&nbsp;'+products['name']+'</p>'
                              
                                                      +'</div>'
                                                +'</div>'
                                          +'</div>'
                                          +'<div class="row d-flex justify-content-between">'
                                                +'<div class="col-5 col-lg-6 ">'
                                                      +'<div class="btn-buy h-100 d-flex justify-content-center align-items-center">'
                                                                + ' <a href="horo.php?number='+products['ori_number1']+'" class="btn btn-primary">'
                                                                + '<i class="fas fa-search iconSearch"></i>ทำนายเบอร์</a>'
                                                      +'</div>'
                                                +'</div>'
                                                +'<div class="col-5 col-lg-6 text-center">'
                                                      + '<div class="btn-buy">'
                                                            +'<a href="howto.php" class="btn btn-predict mt-0"><i class="fas fa-shopping-basket iconBasket"></i>'
                                                            +'สั่งซื้อ'
                                                            +'</a>'
                                                      +'</div>'
                                                +'</div>'+
                                          '</div>'
                                          +
                                    '</div>'
                                +'</div>'
                          });
                              //       '<tr> \
                              //                               <td> \
                              //                                     <img class="mr-3 carrier-logo" src="admin/' + products['carrier_image_path'] + '" alt="' + products['carrier_name'] + '"> \
                              //                               </td> \
                              //                               <td width="25%"> \
                              //                                     <div class="showber"> \
                              //                                           <div class="mt-0 mb-1"> \
                              //                                                 <span>' + getPhoneFormat(products['ori_number1']) + '</span> \
                              //                                                 <span class="sum">(ผลรวม ' + products['sum1'] + ')</span> \
                              //                                           </div> \
                              //                                     </div> \
                              //                               </td> \
                              //                               <td width="10%"> \
                              //                                     <div class="berprice">' + Number(products['price']).toLocaleString('en', {
                              //             minimumFractionDigits: 0
                              //       }) + ' ฿</div> \
                              //                               </td> \
                              //                               <td> \
                              //                                     <div class="berdetail"> \
                              //                                           <p>' + products['product_detail'] + '</p> \
                              //                                     </div> \
                              //                               </td> \
                              //                               <td> \
                              //                                     <div class="btn-buy"> \
                              //                                           <a href="tel:+66814562456" class="btn btn-primary">สั่งซื้อ</a> \
                              //                                     </div> \
                              //                               </td> \
                              //                         </tr>';
                              // });

                              $('#productsTable').empty().append(htmlContent);
                              $('#pagination').css({
                                    'display': 'block'
                              });
                              $('#pagination ul').empty().append(getPagination('search-compound.php?compound_id=' + apiParams['sidebarCompoundID'] + '&', pageCurr, 2, data.totalPage));
                              $('#pagination div').text('ผลการค้นหาทั้งหมด ' + Number(data.total).toLocaleString('en', {
                                    minimumFractionDigits: 0
                              }) + ' รายการ');
                        } else {
                              htmlContent += '<tr><td class="text-center"><h4 style="margin: 20px auto;">ไม่พบข้อมูลที่ตรงกัน</h4></td></tr>';
                              $('#productsTable').empty().append(htmlContent);

                              $('#pagination').css({
                                    'display': 'none'
                              });
                        }
                  }
            });
      }

      function setOrderProductData() {
            apiUrl = 'admin/api/frontend/orderProductAPI.php';
            apiParams = {
                  fn: 'loadOrderProductData',
                  productID: (queryParams[0].split('product_id=')[1] || 0)
            };

            Service.prototype.callAPI(apiUrl, apiParams, 'post', true).done(function (res) {
                  var data = JSON.parse(res);
                  console.log(data);
            });
      }

      function sendEmailToAdmin() {
            if (grecaptcha.getResponse() == '') {
                  swal({
                        html: '<p>กรุณาระบุตัวตน (ติ๊กเครื่องหมายถูกหน้า I\'m not a robot)</p>',
                        type: 'warning',
                        showConfirmButton: true,
                        showCancelButton: false,
                        showCloseButton: true,
                        confirmButtonText: 'ตกลง',
                        cancelButtonText: 'ยกเลิก',
                        confirmButtonClass: 'btn btn-primary',
                        cancelButtonClass: 'btn btn-reverse',
                        buttonsStyling: false,
                        allowOutsideClick: true
                  });
            } else {
                  apiUrl = 'admin/api/frontend/contactUsAPI.php';
                  apiParams = {
                        fn: 'sendMailToAdmin',
                        name: $('#contactUsForm #contactName').val() || '',
                        email: $('#contactUsForm #contactEmail').val() || '',
                        tel: $('#contactUsForm #contactTel').val() || '',
                        detail: $('#contactUsForm #contactDetail').val() || '',
                  };
                  console.log(apiParams);

                  Service.prototype.callAPI(apiUrl, apiParams, 'post', true).done(function (res) {
                        var data = JSON.parse(res);
                        console.log(data);

                        if (data.status) {
                              swal({
                                    html: '<p>ส่งข้อความเรียบร้อย</p>',
                                    type: 'success',
                                    showConfirmButton: true,
                                    showCancelButton: false,
                                    showCloseButton: true,
                                    confirmButtonText: 'ตกลง',
                                    cancelButtonText: 'ยกเลิก',
                                    confirmButtonClass: 'btn btn-primary',
                                    cancelButtonClass: 'btn btn-reverse',
                                    buttonsStyling: false,
                                    allowOutsideClick: true
                              });

                              $('#contactUsForm').find('input, textarea').val('');
                        } else {
                              swal({
                                    html: '<p>ไม่สามารถส่งข้อความได้</p>',
                                    type: 'error',
                                    showConfirmButton: true,
                                    showCancelButton: false,
                                    showCloseButton: true,
                                    confirmButtonText: 'ตกลง',
                                    cancelButtonText: 'ยกเลิก',
                                    confirmButtonClass: 'btn btn-primary',
                                    cancelButtonClass: 'btn btn-reverse',
                                    buttonsStyling: false,
                                    allowOutsideClick: true
                              });
                        }
                  });
            }
      }

      function detectDetailFilter(filterData, filterKey, filterID) {
            var detectDetail = {};

            $(filterData).each(function (index, data) {
                  if (filterID == data[filterKey]) {
                        detectDetail = data;
                  }
            });

            return detectDetail;
      }

      function sumPhoneNumber(phoneNumber) {
            var phoneNumberSum = 0;

            if (phoneNumber) {
                  phoneNumber = phoneNumber.replace('-', '');

                  for (var i = 0; i < phoneNumber.length; i++) {
                        phoneNumberSum += parseInt(phoneNumber[i]);
                  }
            }

            return phoneNumberSum;
      }

      function convertDateToDB(date) {
            if ((/^(\d{2}\/\d{2}\/\d{4})$/g).test(date)) {
                  var newDate = (Number(date.split('/')[2]) - 543) + '-' + date.split('/')[1] + '-' + date.split('/')[0];

                  return newDate;
            } else {
                  return '';
            }
      }

      function convertDateToDisplay(date) {
            if ((/^(\d{4}\-\d{2}\-\d{2})$/g).test(date)) {
                  var newDate = date.split('-')[2] + '/' + date.split('-')[1] + '/' + (Number(date.split('-')[0]) + 543);

                  return newDate;
            } else {
                  return '';
            }
      }

      function getDateCurrent() {
            var monthData = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฏาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'];
            var dateCurr = new Date().getDate() + ' ';

            $(monthData).each(function (index, month) {
                  if (index == (new Date().getMonth())) {
                        dateCurr += month + ' ';

                        return true;
                  }
            });

            dateCurr += (new Date().getFullYear() + 543);

            return dateCurr;
      }

      function getCalibratePercentContent(percent) {
            htmlContent = '';

            if (percent < 49) {
                  htmlContent += '<i class="fa fa-star"></i> \
                                    <i class="far fa-star"></i> \
                                    <i class="far fa-star"></i> \
                                    <i class="far fa-star"></i> \
                                    <i class="far fa-star"></i>';
            } else if (percent >= 50 && percent <= 60) {
                  htmlContent += '<i class="fa fa-star"></i> \
                                    <i class="fa fa-star"></i> \
                                    <i class="far fa-star"></i> \
                                    <i class="far fa-star"></i> \
                                    <i class="far fa-star"></i>';
            } else if (percent >= 61 && percent <= 70) {
                  htmlContent += '<i class="fa fa-star"></i> \
                                    <i class="fa fa-star"></i> \
                                    <i class="fa fa-star"></i> \
                                    <i class="far fa-star"></i> \
                                    <i class="far fa-star"></i>';
            } else if (percent >= 71 && percent <= 85) {
                  htmlContent += '<i class="fa fa-star"></i> \
                                    <i class="fa fa-star"></i> \
                                    <i class="fa fa-star"></i> \
                                    <i class="fa fa-star"></i> \
                                    <i class="far fa-star"></i>';
            } else if (percent >= 86 && percent <= 100) {
                  htmlContent += '<i class="fa fa-star"></i> \
                                    <i class="fa fa-star"></i> \
                                    <i class="fa fa-star"></i> \
                                    <i class="fa fa-star"></i> \
                                    <i class="fa fa-star"></i>';
            }

            return htmlContent;
      }

      function getCalibrateScoreContent(score) {
            htmlContent = '';

            if (score == 0) {
                  htmlContent += '<i class="far fa-star"></i> \
                                    <i class="far fa-star"></i> \
                                    <i class="far fa-star"></i> \
                                    <i class="far fa-star"></i> \
                                    <i class="far fa-star"></i>';
            } else if (score == 0.5) {
                  htmlContent += '<i class="fas fa-star-half"></i> \
                                    <i class="far fa-star"></i> \
                                    <i class="far fa-star"></i> \
                                    <i class="far fa-star"></i> \
                                    <i class="far fa-star"></i>';
            } else if (score == 1) {
                  htmlContent += '<i class="fa fa-star"></i> \
                                    <i class="far fa-star"></i> \
                                    <i class="far fa-star"></i> \
                                    <i class="far fa-star"></i> \
                                    <i class="far fa-star"></i>';
            } else if (score == 1.5) {
                  htmlContent += '<i class="fa fa-star"></i> \
                                    <i class="fas fa-star-half"></i> \
                                    <i class="far fa-star"></i> \
                                    <i class="far fa-star"></i> \
                                    <i class="far fa-star"></i>';
            } else if (score == 2) {
                  htmlContent += '<i class="fa fa-star"></i> \
                                    <i class="fa fa-star"></i> \
                                    <i class="far fa-star"></i> \
                                    <i class="far fa-star"></i> \
                                    <i class="far fa-star"></i>';
            } else if (score == 2.5) {
                  htmlContent += '<i class="fa fa-star"></i> \
                                    <i class="fa fa-star"></i> \
                                    <i class="fas fa-star-half"></i> \
                                    <i class="far fa-star"></i> \
                                    <i class="far fa-star"></i>';
            } else if (score == 3) {
                  htmlContent += '<i class="fa fa-star"></i> \
                                    <i class="fa fa-star"></i> \
                                    <i class="fa fa-star"></i> \
                                    <i class="far fa-star"></i> \
                                    <i class="far fa-star"></i>';
            } else if (score == 3.5) {
                  htmlContent += '<i class="fa fa-star"></i> \
                                    <i class="fa fa-star"></i> \
                                    <i class="fa fa-star"></i> \
                                    <i class="fas fa-star-half"></i> \
                                    <i class="far fa-star"></i>';
            } else if (score == 4) {
                  htmlContent += '<i class="fa fa-star"></i> \
                                    <i class="fa fa-star"></i> \
                                    <i class="fa fa-star"></i> \
                                    <i class="fa fa-star"></i> \
                                    <i class="far fa-star"></i>';
            } else if (score == 4.5) {
                  htmlContent += '<i class="fa fa-star"></i> \
                                    <i class="fa fa-star"></i> \
                                    <i class="fa fa-star"></i> \
                                    <i class="fa fa-star"></i> \
                                    <i class="fas fa-star-half"></i>';
            } else if (score == 5) {
                  htmlContent += '<i class="fa fa-star"></i> \
                                    <i class="fa fa-star"></i> \
                                    <i class="fa fa-star"></i> \
                                    <i class="fa fa-star"></i> \
                                    <i class="fa fa-star"></i>';
            }

            return htmlContent;
      }

      function getHoroCompoundNumberFormat(number, format) {
            var numberFormatContent = '';

            if ((/^\d{10}$/g).test(number)) {
                  switch (parseInt(format)) {
                        case 1:
                              numberFormatContent = '<span class="normal"> ' + number.substr(0, 3) + '<font color="#F2B500"> ' + number.substr(3, 2) + ' </font>' + number.substr(5) + '</span>';
                              break;
                        case 2:
                              numberFormatContent = '<span class="normal"> ' + number.substr(0, 4) + '<font color="#F2B500"> ' + number.substr(4, 2) + ' </font>' + number.substr(6) + '</span>';
                              break;
                        case 3:
                              numberFormatContent = '<span class="normal"> ' + number.substr(0, 5) + '<font color="#F2B500"> ' + number.substr(5, 2) + ' </font>' + number.substr(7) + '</span>';
                              break;
                        case 4:
                              numberFormatContent = '<span class="normal"> ' + number.substr(0, 6) + '<font color="#F2B500"> ' + number.substr(6, 2) + ' </font>' + number.substr(8) + '</span>';
                              break;
                        case 5:
                              numberFormatContent = '<span class="normal"> ' + number.substr(0, 7) + '<font color="#F2B500"> ' + number.substr(7, 2) + ' </font>' + number.substr(9) + '</span>';
                              break;
                        case 6:
                              numberFormatContent = '<span class="normal"> ' + number.substr(0, 8) + '<font color="#F2B500"> ' + number.substr(8, 2) + ' </font>' + number.substr(10) + '</span>';
                              break;
                  }
            }

            return numberFormatContent;
      }

      function getPhoneNumberSum(number) {
            var phoneNumberSum = 0;

            if ((/^\d{10}$/g).test(number)) {
                  for (var i = 0; i < number.toString().length; i++) {
                        phoneNumberSum += parseInt(number.toString()[i]);
                  }
            }

            return phoneNumberSum;
      }

      function getPhoneFormat(phoneNumber) {
            var newPnumberPhone = phoneNumber;

            if ((/^0[0-9]{9}$/g).test(phoneNumber)) {
                  switch (phoneFormat) {
                        case 'XX-XXXXXXXX':
                              newPnumberPhone = phoneNumber.substr(0, 2) + '-' + phoneNumber.substr(2);

                              break;
                        case 'XXX-XXXXXXX':
                              newPnumberPhone = phoneNumber.substr(0, 3) + '-' + phoneNumber.substr(3);

                              break;
                        case 'XX-XXX-XXXXX':
                              newPnumberPhone = phoneNumber.substr(0, 2) + '-' + phoneNumber.substr(2, 3) + '-' + phoneNumber.substr(5);

                              break;
                        case 'XX-XXXX-XXXX':
                              newPnumberPhone = phoneNumber.substr(0, 2) + '-' + phoneNumber.substr(2, 4) + '-' + phoneNumber.substr(6);

                              break;
                        case 'XXX-XXX-XXXX':
                              newPnumberPhone = phoneNumber.substr(0, 3) + '-' + phoneNumber.substr(3, 3) + '-' + phoneNumber.substr(6);

                              break;
                        case 'X-XXXX-XXXXX':
                              newPnumberPhone = phoneNumber.substr(0, 1) + '-' + phoneNumber.substr(1, 4) + '-' + phoneNumber.substr(5);

                              break;
                  }
            }

            return newPnumberPhone;
      }

      function getPagination(pageLink, pageCurr, pageSplit, pageTotal) {
            var pCurr = parseInt(pageCurr);
            var pSplit = parseInt(pageSplit);
            var pTotal = parseInt(pageTotal);
            var pStart = ((pCurr - pSplit) > 0) ? (pCurr - pSplit) : 1;
            var pEnd = ((pCurr + pSplit) < pTotal) ? (pCurr + pSplit) : pTotal;
            // console.log('pCurr', pCurr);
            // console.log('pSplit', pSplit);
            // console.log('pTotal', pTotal);
            // console.log('pStart', pStart);
            // console.log('pEnd', pEnd);

            htmlContent = '<li class="page-item ' + ((pCurr == 1 || pTotal == 1) ? 'disabled' : '') + '"> \
                              <a class="page-link" href="' + pageLink + 'page=1" tabindex="-1">&laquo;</a> \
                        </li>';

            if (pStart > 1) {
                  htmlContent += '<li class="page-item"> \
                                    <a class="page-link" href="' + pageLink + 'page=1" tabindex="-1">1</a> \
                              </li> \
                              <li class="page-item disabled"> \
                                    <a class="page-link" tabindex="-1">...</a> \
                              </li>';
            }

            for (var i = pStart; i <= pEnd; i++) {
                  htmlContent += '<li class="page-item ' + ((pCurr == i) ? 'active disabled' : '') + '"> \
                                    <a class="page-link" href="' + pageLink + 'page=' + i + '">' + i + '</a> \
                              </li>';
            }

            if (pEnd < pTotal) {
                  htmlContent += '<li class="page-item disabled"> \
                                    <a class="page-link" tabindex="-1">...</a> \
                              </li> \
                              <li class="page-item"> \
                                    <a class="page-link" href="' + pageLink + 'page=' + pTotal + '" tabindex="-1">' + pTotal + '</a> \
                              </li>';
            }

            htmlContent += '<li class="page-item ' + ((pCurr == pTotal) ? 'disabled' : '') + '"> \
                              <a class="page-link" href="' + pageLink + 'page=' + pTotal + '" tabindex="-1">&raquo;</a> \
                        </li>';

            return htmlContent;
      }
});