function Service() {
    console.log('This is Service');
}

Service.prototype.appPath = '/'+ window.location.pathname.split('/')[1];

Service.prototype.loadingContent = `<div class="loading" style="position: absolute;
                                        top: 0;
                                        width: 100%;
                                        height: 100%;
                                        text-align: center;
                                        background-color: #666;
                                        background-repeat: no-repeat;
                                        background-position: center;
                                        z-index: 10000000;
                                        opacity: 0.4;
                                        color: #FFFFFF;
                                        filter: alpha(opacity=40);">
                                    <i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw" style="top: 38%; position: absolute; color: #FFF; font-size: 5em;"></i>
                                    <h4 class="text-disabled" style="position: absolute;
                                        top: 50%;
                                        left: 53%;
                                        margin-top: 20px;
                                        transform: translate(-50%, -50%); color:#FFF; font-size:2em;">กรุณารอสักครู่...</h4>
                                    </div>`;

Service.prototype.callAPI = function(apiUrl, apiParams, apiType, requireLoading) {
    var self = this;
    var options = {};

    if (apiType == 'post') {
        options = {
            type: 'post',
            url: apiUrl,
            data: JSON.stringify(apiParams),
            cache: false,
            beforeSend: function() {
                if (requireLoading) {
                    $('body').append(self.loadingContent);
                }
            },
            success: function() {
                if (requireLoading) {
                    $('body .loading').remove();
                }
            }
        };
    } else {
        options = {
            type: 'get',
            url: apiUrl,
            async: true,
            crossDomain: true,
            beforeSend: function() {
                if (requireLoading) {
                    $('body').append(self.loadingContent);
                }
            },
            success: function() {
                if (requireLoading) {
                    $('body .loading').remove();
                }
            }
        };
    }

    return $.ajax(options);
}

Service.prototype.callAPIWithFormData = function(apiUrl, apiParams, requireLoading) {
    var self = this;
    var options = {};

    options = {
        type: 'post',
        url: apiUrl,
        data: apiParams,
        processData: false,
        contentType: false,
        beforeSend: function() {
            if (requireLoading) {
                $('body').append(self.loadingContent);
            }
        },
        success: function() {
            if (requireLoading) {
                $('body .loading').remove();
            }
        }
    };

    return $.ajax(options);
}